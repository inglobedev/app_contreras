<?php 
	// Generales
	$lang['pachi_lbl_pick_datetime']	= 'Seleccionar fecha y hora';
	$lang['pachi_lbl_pick_date']		= 'Seleccionar fecha';
	$lang['pachi_lbl_pick_year']		= 'Seleccionar año';
	$lang['pachi_lbl_pick_month']		= 'Seleccionar mes';
	$lang['pachi_lbl_search']			= 'Buscar';
	$lang['pachi_lbl_write_here']		= 'Escriba aquí';
	$lang['pachi_lbl_required']			= '*';
	
	$lang['pachi_btn_save']				= 'Guardar';
	$lang['pachi_btn_cancel']			= 'Cancelar';
	$lang['pachi_lbl_new']				= 'Nuevo';
	
	$lang['pachi_undefined']			= 'Sin Definir';
	$lang['pachi_no_records']			= 'No hay registros que mostrar ...';
	$lang['pachi_data_saved']			= 'Se guardó correctamente.';