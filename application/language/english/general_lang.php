<?php 
	// Generales
		$lang['general_msg_saved_ok'] 		= 'Los datos se actualizaron correctamente.';
		$lang['general_error_ocurred'] 		= 'Ocurrió un error guardando los datos. Por favor intente nuevamente.';
	

	//Entidades

		$lang['entity_user'] 				= 'usuario';
		$lang['entity_users'] 				= 'usuarios';

		$lang['entity_tenant'] 				= 'locatario';
		$lang['entity_tenants'] 			= 'locatarios';

		$lang['entity_consortium'] 			= 'consorcio';
		$lang['entity_consortiums'] 		= 'consorcios';

		$lang['entity_guarantor'] 			= 'garante';
		$lang['entity_guarantors'] 			= 'garantes';

		$lang['entity_professional'] 		= 'profesional';
		$lang['entity_professionals'] 		= 'profesionales';

		$lang['entity_property'] 			= 'inmueble';
		$lang['entity_properties'] 			= 'inmuebles';

		$lang['entity_owner'] 				= 'propietario';
		$lang['owner_user_name'] 			= 'Propietario';
		$lang['entity_owners'] 				= 'propietarios';

		$lang['entity_contract'] 			= 'contrato';
		$lang['entity_contracts'] 			= 'contratos';

		$lang['entity_bonification'] 		= 'bonificación';
		$lang['entity_bonifications'] 		= 'bonificaciónes';

		$lang['entity_maintenance'] 		= 'mantenimiento';
		$lang['entity_maintenances'] 		= 'mantenimientos';

		$lang['entity_expense'] 			= 'gasto';
		$lang['entity_expenses'] 			= 'gastos';

		$lang['entity_expense_type'] 		= 'tipo de gasto';
		$lang['entity_expense_types'] 		= 'tipos de gastos';

		$lang['entity_quote']				='presupuesto';
		$lang['entity_quotes']				='presupuestos';

		$lang['entity_administration']		='administración';
		$lang['entity_administrations']		='administraciones';

		$lang['entity_tax']					='impuesto';
		$lang['entity_taxes']				='impuestos';

	//Propiedades

		//Usuarios
		$lang['user_name'] 					= 'Nombre';
		$lang['user_lastname'] 				= 'Apellido';
		$lang['user_address'] 				= 'Dirección';
		$lang['user_email'] 				= 'E-mail';
		$lang['user_cuit'] 					= 'Cuit';
		$lang['user_phone'] 				= 'Teléfono';
		$lang['user_picture'] 				= 'Imagen de perfil';
		$lang['user_password'] 				= 'Contraseña';
		$lang['user_language'] 				= 'Idioma';
		$lang['user_observations'] 			= 'Observaciones';
		$lang['user_last_login'] 			= 'Último acceso';
		$lang['id_role'] 					= 'Rol';

		$lang['id_user_professional'] 		= 'Profesional';

		//Tipos de usuarios
		$lang['tenant_user_name'] 			= 'Locatario';
		$lang['professional_user_name'] 	= 'Profesional';

		//Propiedades
		$lang['id_user_consortium'] 		= 'Consorcio';
		$lang['property_name'] 				= 'Nombre';
		$lang['property_address'] 			= 'Dirección';
		$lang['property_year'] 				= 'Año';
		$lang['property_ambiences_qty'] 	= 'Cantidad de ambientes';
		$lang['property_bathrooms_qty'] 	= 'Cantidad de baños';
		$lang['property_rooms_qty'] 		= 'Cantidad de cuartos';
		$lang['property_square_meter'] 		= 'Metros cuadrados';
		$lang['property_garage'] 			= '¿Cuenta con garage?';
		$lang['property_balcony'] 			= '¿Cuenta con balcon?';
		$lang['property_building_name'] 	= 'Nombre del edificio';
		$lang['property_cadastre_number'] 	= 'Número catastral';
		$lang['property_observations'] 		= 'Observaciones';

		//Inmuebles
		$lang['id_property'] 					= 'Inmueble';
		$lang['id_user_owner'] 					= 'Propietario';
		$lang['date_start'] 					= 'Fecha de inicio';
		$lang['date_end'] 						= 'Fecha de fin';

		//Gastos

		$lang['id_expense']						= 'Gasto';
		$lang['id_expense_type']				= 'Tipo de gasto';
		$lang['expense_name']					= 'Nombre de gasto';
		$lang['expense_description']			= 'Descripción del gasto';
		$lang['expense_payable']				= 'Se paga';
		$lang['expense_chargeable']				= 'Se cobra';
		$lang['expense_checkable']				= 'Se controla';
		$lang['expense_identification']			= 'Referencia';

	 	//Tipo de gastos
		$lang['type_name']						='Nombre del tipo de gasto';
		$lang['type_description']				='Descripción del gasto de gasto';


		//Mantenimiento
		$lang['maintenance_name']				='Mantenimiento';
		$lang['maintenance_description']		='Descripción';
		$lang['maintenance_owner_cost']			='A propietario';
		$lang['maintenance_tenant_cost']		='A locatario';
		$lang['maintenance_administrative_fee']	='A Administracion';
		$lang['maintenance_observations']		='Ovservaciones';
		$lang['maintenance_status']				='Estado';
		$lang['maintenance_date']				='Fecha';
		$lang['maintenance_archived']			='Archviado';
		$lang['maintenance_completed']			='Mantenimiento Completo';

		//Presupuesto de mantenimiento
		$lang['quote_observations']				='Observaciones';
		$lang['quote_price']					='Presupuesto';
		$lang['quote_attachments']				='Archivos adjuntos';

		//Contrato
		$lang['contract_date_start']			='Fecha de inicio';
		$lang['contract_date_end']				='Fecha de fin';
		$lang['contract_rent_price']			='Precio de alquiler';
		$lang['contract_commission']			='Gastos Administrativos';
		$lang['contract_interest']				='Intereses';
		$lang['contract_tenant_attachments']	='Locatario adjuntos';
		$lang['contract_gas_check']				='Realización de servicio de gas';
		$lang['contract_electricity_check']		='Realización de servicio de luz';
		$lang['contract_expenses_check']		='Expensas';
		$lang['contract_expenses_attachments']	='Expensas adjuntos';
		$lang['contract_observations']			='Observaciones';
		$lang['id_user_tenant']				='Locatario';
		$lang['contract_payday_start']			='Pago desde';
		$lang['contract_payday_limit']			='Pago hasta';

		//Bonificaciones
		$lang['bonification_name']				='Descripción';
		$lang['bonification_rate']				='Importe bonificado';
		$lang['bonification_date_start']		='Fecha de inicio';
		$lang['bonification_date_end']			='Fecha de fin';
		$lang['bonification_observations']		='Observaciones';

		//Garantes
		$lang['guarantor_attachments']			='Adjuntos';
		$lang['guarantor_observations']			='Observaciones';

		//Detalles
		$lang['detail_attachments']				= 'Adjuntos';
		
		//Periodo
		$lang['period_code']					= 'Periodo';

	//Etiquetas
		$lang['lbl_new']						= 'nuevo';
		$lang['lbl_new_a']						= 'nueva';
		$lang['lbl_edit']						= 'editar';
		$lang['lbl_create']						= 'crear';
		$lang['lbl_delete']						= 'eliminar';
		$lang['lbl_search']						= 'buscar';
		$lang['lbl_filter']						= 'filtrar';
		$lang['lbl_filters']					= 'filtros';
		$lang['lbl_attributes']					= 'datos';
		$lang['lbl_add']						= 'agregar';

		$lang['lbl_results']					= 'resultados';
		$lang['lbl_temporary']					= 'temporarios';
		$lang['lbl_sales']						= 'ventas';

		$lang['_status']						='Estado';
		$lang['lbl_payment']					='Pago';
		$lang['lbl_payments']					='Cobros';
		$lang['lbl_liquidate']					='Liquidacion';
		$lang['lbl_advancement']				='Adelanto';
		$lang['lbl_liquidation_history']		='Historal de liquidaciones';


		$lang['lbl_debt'] 						='Deuda locatario';
		$lang['tenant_debts']					='Deuda locatario';
		$lang['_count_maintenances']			='Mantenimientos';

		$lang['lbl_not_phone']					='Sin definir';
		$lang['lbl_rental']						='Alquiler';
		$lang['lbl_period']						='Periodo';
		$lang['lbl_price']						='Monto';
		$lang['lbl_balance']					='Saldo';
		$lang['lbl_taxes']						='Impuestos';
		$lang['lbl_tax']						='Impuesto';
		$lang['lbl_service']					='Servicio';
		$lang['lbl_services']					='Servicios';
		$lang['lbl_provider']					='Proveedor';

		$lang['lbl_collect_rent']				='Cobrar un alquiler';
		$lang['lbl_see_collection_history']		='Ver historial de cobros';

		$lang['lbl_expense']					='Expensa';
		$lang['lbl_expenses']					='Gastos';
		$lang['lbl_expiration']					='Vencimiento';
		$lang['lbl_administration_expenses']	='Gastos adminitrativos';

		$lang['lbl_rent']						='Alquiler';
		$lang['lbl_rents']						='Alquileres';

		$lang['lbl_chargue']					='Cobro';
		$lang['lbl_chargues']					='Cobros';

		$lang['lbl_concept']					='Concepto';
		$lang['lbl_concepts']					='Conceptos';

		$lang['lbl_receiver']					='Receptor';


		$lang['lbl_administrative_expenses']	='Gastos Administrativos';
		$lang['lbl_administration']				='Adminitración';


		$lang['user_period'] 					= 'Periodo';
		$lang['user_last_liquidation'] 			= 'Última liquidacion';
		$lang['user_amount_to_liquidate'] 		= 'Monto a liquidar';

		$lang['ownership_date_start']			= 'Desde';
		$lang['ownership_date_end']				= 'Hasta';
		$lang['ownership_administrative_rate']	= 'Gastos Administrativos';


	// Permisos
		$lang['permission_app_manage_users'] = 'Gestionar usuarios';
		$lang['permission_app_manage_roles'] = 'Gestionar roles';
		$lang['permission_app_manage_system'] = 'Configurar Sistema';