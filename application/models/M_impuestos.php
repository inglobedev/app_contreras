<?php 
class M_Impuestos extends CI_Model 
{

	public function __construct() 
	{

	}

/**
	 * Tabla: gastos 
	 * Columnas Requeridas: 
	 * Columnas Opcionales: id_tipo, gasto_nombre
	 * Opcionales:
	 * @column 	id_tipo                       	int(11)        	
	 * @column 	gasto_nombre                  	int(11)        	
	 */

		/**
		 * Crea un registro de gasto
		 *
		 *
		 * @return      int 	En caso de exito retorna el ID del registro insertado.
		 * @return      bool 	En caso de error retorna FALSE.
		 */
		public function new_gasto($impuesto_nombre, $id_tipo)
		{
			$data['gasto_nombre'] = $impuesto_nombre;
			$data['id_tipo'] = $id_tipo;

			$result = $this->db->insert('gastos', $data);

			if ($result == TRUE) 
				return $this->db->insert_id();
			
			return FALSE; 
		}

		/**
		 * Elimina uno o mas registros de gasto
		 *
		 * @param 	$id_gasto           	ID del registro a eliminar. Array de IDs. IDs separados por coma.
		 * @param 	$limit              	Cantidad maxima de registros a eliminar.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function del_gasto($id_gasto = FALSE, $limit = 1)
		{
			if ($id_gasto === FALSE OR empty($id_gasto)) 
				return FALSE;

			if (is_array($id_gasto))
			{
				foreach ($id_gasto as $key => $value) {
					if (strpos($key, '.') === FALSE AND strpos($key, '(') === FALSE)
						$cond['gastos.'.$key] = $value;
					else
						$cond[$key] = $value;
				}
			}
			else
				$cond['id_gasto'] = $id_gasto;

			$this->db->where($cond);
			
			if ($limit > 0)
				$this->db->limit($limit);

			$result = $this->db->delete('gastos');

			return $result;
		}

		/**
		 * Retorna un gasto		 
		 *
		 * @param 	$id_gasto           	PK.
		 *
		 * @return	array	En caso de existir el registro retorna un array donde cada key es una columna.
		 * @return	bool	En caso de no existir el registro retorna FALSE.
		 */
		public function get_gasto($id_gasto = FALSE)
		{
			if ($id_gasto === FALSE OR empty($id_gasto)) 
				return FALSE;

			$cond['id_gasto'] = $id_gasto;

			$this->db->select('SQL_CALC_FOUND_ROWS gastos.*, gastos_tipo.*', FALSE);
			$this->db->from('gastos');
			$this->db->join('gastos_tipo' , 'gastos_tipo.id_tipo = gastos.id_tipo', 'LEFT');

			$this->db->where($cond);
			$result = $this->db->get();

			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				return $result[0];
			}

			return FALSE;
		}

		/**
		 * Actualiza una o mas columnas de un gasto		 
		 *
		 * @param 	$id_gasto           	PK.
		 * @param 	$filter_column      	Columna a actualizar. Ó Array con columnas=valores a setear.
		 * @param 	$filter_value       	Valor a setear, o FALSE si filter_column es un array.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function set_gasto($id_gasto, $filter_column = FALSE, $filter_value = FALSE)
		{
			$data = array();

			if (is_array($filter_column))
			{
				foreach ($filter_column as $key => $value) {
					if (strpos($key, '.') === FALSE)
						$data['gastos.'.$key] = $value;
					else
						$data[$key] = $value;
				}
			}
			else
				if ($filter_column !== FALSE AND $filter_value !== FALSE)
					if (strpos($filter_column, '.') === FALSE)
						$data['gastos.'.$filter_column] = $filter_value;
					else
						$data[$filter_column] = $filter_value;

			// En caso de que se proporcione un array de IDs
			if (is_array($id_gasto)) {
				$ids = implode(',', $id_gasto);
				$cond['id_gasto IN ('.$ids.')'] = NULL;
			}
			// En caso de que se proporcionen IDs separados por coma.
			elseif (strpos($id_gasto, ',') === FALSE)
				$cond['id_gasto IN ('.$id_gasto.')'] = NULL;
			else
				$cond['id_gasto'] = $id_gasto;

			$this->db->where($cond);

			$result = $this->db->update('gastos', $data);

			return $result;
		}

		/**
		 * Retorna una coleccion de gastos		 
		 *
		 * @param 	$id_gasto           	PK.
		 * @param 	$filter_column      	Columna a comparar. Ó Array con columnas=valores a buscar.
		 * @param 	$filter_value       	Valor buscado, o FALSE si filter_column es un array.
		 * @param 	$page               	Pagina desde la cual se busca.
		 * @param 	$page_items         	Cantidad de items de una pagina.
		 * @param 	$filter_column      	Array con columnas=orden.
		 * @param 	$term_filter        	Cadena de texto a buscar en columnas de texto o varchar.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function get_gastos($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $term_filter = FALSE)
		{
			$cond = array();

			$this->db->select('SQL_CALC_FOUND_ROWS gastos.*, gastos_tipo.*', FALSE);
			$this->db->from('gastos');
			$this->db->join('gastos_tipo' , 'gastos_tipo.id_tipo = gastos.id_tipo', 'LEFT');

			if (is_array($filter_column))
			{
				foreach ($filter_column as $key => $value) {
					if (strpos($key, '.') === FALSE)
						$cond['gastos.'.$key] = $value;
					else
						$cond[$key] = $value;
				}
			}
			else
				if ($filter_column !== FALSE AND $filter_value !== FALSE)
					if (strpos($filter_column, '.') === FALSE AND strpos($filter_column, '(') === FALSE)
						$cond['gastos.'.$filter_column] = $filter_value;
					else
						$cond[$filter_column] = $filter_value;

			if (count($cond) > 0)
				$this->db->where($cond);

			if (is_array($order_by))
			{
				foreach ($order_by as $order_column => $sort_order) {
					$this->db->order_by($order_column, $sort_order);
				}
			}

			if (!empty($term_filter)) {
				$this->db->group_start();
				$this->db->group_end();
			}

			if ($page !== FALSE)
			{
				$offset = $page*$page_items;
				$this->db->limit($page_items, $offset);
			}
			
			$result = $this->db->get();
			$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				foreach ($result as $key => $value) 
				{
					$result[$key]['total_results'] = $paginacion[0]['total_items'];
				}
				return $result;
			}

			return array();
		}


		/**
			 * Tabla: gastos_inmuebles 
			 * Columnas Requeridas: 
			 * Columnas Opcionales: id_gastos, gasto_paga, gasto_cobra, gasto_controla, gasto_monto, gasto_fecha
			 * Opcionales:
			 * @column 	id_gastos                     	int(11)        	
			 * @column 	gasto_paga                    	int(11)        	
			 * @column 	gasto_cobra                   	int(11)        	
			 * @column 	gasto_controla                	int(11)        	
			 * @column 	gasto_monto                   	varchar(45)    	
			 * @column 	gasto_fecha                   	datetime       	
			 */

				/**
				 * Crea un registro de gastos_inmueble
				 *
				 *
				 * @return      int 	En caso de exito retorna el ID del registro insertado.
				 * @return      bool 	En caso de error retorna FALSE.
				 */
				public function new_gastos_inmueble($id_inmueble, $id_gasto)
				{

					$data['id_inmueble'] = $id_inmueble;
					$data['id_gasto'] = $id_gasto;

					$result = $this->db->insert('gastos_inmuebles', $data);

					if ($result == TRUE) 
						return $this->db->insert_id();
					
					return FALSE; 
				}

				/**
				 * Elimina uno o mas registros de gastos_inmueble
				 *
				 * @param 	$id_gastoinmueble   	ID del registro a eliminar. Array de IDs. IDs separados por coma.
				 * @param 	$limit              	Cantidad maxima de registros a eliminar.
				 *
				 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
				 */
				public function del_gastos_inmueble($id_gastoinmueble = FALSE, $limit = 1)
				{
					if ($id_gastoinmueble === FALSE OR empty($id_gastoinmueble)) 
						return FALSE;

					if (is_array($id_gastoinmueble))
					{
						foreach ($id_gastoinmueble as $key => $value) {
							if (strpos($key, '.') === FALSE AND strpos($key, '(') === FALSE)
								$cond['gastos_inmuebles.'.$key] = $value;
							else
								$cond[$key] = $value;
						}
					}
					else
						$cond['id_gastoinmueble'] = $id_gastoinmueble;

					$this->db->where($cond);
					
					if ($limit > 0)
						$this->db->limit($limit);

					$result = $this->db->delete('gastos_inmuebles');

					return $result;
				}

				/**
				 * Retorna un gastos_inmueble		 
				 *
				 * @param 	$id_gastoinmueble   	PK.
				 *
				 * @return	array	En caso de existir el registro retorna un array donde cada key es una columna.
				 * @return	bool	En caso de no existir el registro retorna FALSE.
				 */
				public function get_gastos_inmueble($id_gastoinmueble = FALSE)
				{
					if ($id_gastoinmueble === FALSE OR empty($id_gastoinmueble)) 
						return FALSE;

					$cond['id_gastoinmueble'] = $id_gastoinmueble;

					$this->db->where($cond);
					$result = $this->db->get('gastos_inmuebles');

					if ($result->num_rows() > 0)
					{
						$result = $result->result_array();
						return $result[0];
					}

					return FALSE;
				}

				/**
				 * Actualiza una o mas columnas de un gastos_inmueble		 
				 *
				 * @param 	$id_gastoinmueble   	PK.
				 * @param 	$filter_column      	Columna a actualizar. Ó Array con columnas=valores a setear.
				 * @param 	$filter_value       	Valor a setear, o FALSE si filter_column es un array.
				 *
				 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
				 */
				public function set_gastos_inmueble($id_gastoinmueble, $filter_column = FALSE, $filter_value = FALSE)
				{
					$data = array();

					if (is_array($filter_column))
					{
						foreach ($filter_column as $key => $value) {
							if (strpos($key, '.') === FALSE)
								$data['gastos_inmuebles.'.$key] = $value;
							else
								$data[$key] = $value;
						}
					}
					else
						if ($filter_column !== FALSE AND $filter_value !== FALSE)
							if (strpos($filter_column, '.') === FALSE)
								$data['gastos_inmuebles.'.$filter_column] = $filter_value;
							else
								$data[$filter_column] = $filter_value;

					// En caso de que se proporcione un array de IDs
					if (is_array($id_gastoinmueble)) {
						$ids = implode(',', $id_gastoinmueble);
						$cond['id_gastoinmueble IN ('.$ids.')'] = NULL;
					}
					// En caso de que se proporcionen IDs separados por coma.
					elseif (strpos($id_gastoinmueble, ',') === FALSE)
						$cond['id_gastoinmueble IN ('.$id_gastoinmueble.')'] = NULL;
					else
						$cond['id_gastoinmueble'] = $id_gastoinmueble;

					$this->db->where($cond);

					$result = $this->db->update('gastos_inmuebles', $data);

					return $result;
				}

				/**
				 * Retorna una coleccion de gastos_inmuebles		 
				 *
				 * @param 	$id_gastoinmueble   	PK.
				 * @param 	$filter_column      	Columna a comparar. Ó Array con columnas=valores a buscar.
				 * @param 	$filter_value       	Valor buscado, o FALSE si filter_column es un array.
				 * @param 	$page               	Pagina desde la cual se busca.
				 * @param 	$page_items         	Cantidad de items de una pagina.
				 * @param 	$filter_column      	Array con columnas=orden.
				 * @param 	$term_filter        	Cadena de texto a buscar en columnas de texto o varchar.
				 *
				 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
				 */
				public function get_gastos_inmuebles($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $term_filter = FALSE)
				{
					$cond = array();

					$this->db->select('SQL_CALC_FOUND_ROWS gastos_inmuebles.*, inmuebles.*, gastos.*, gastos_tipo.*', FALSE);
					$this->db->from('gastos_inmuebles');
					$this->db->join('inmuebles' , 'inmuebles.id_inmueble = gastos_inmuebles.id_inmueble', 'LEFT');
					$this->db->join('gastos' , 'gastos.id_gasto = gastos_inmuebles.id_gasto', 'LEFT');
					$this->db->join('gastos_tipo' , 'gastos_tipo.id_tipo = gastos.id_tipo', 'LEFT');

					if (is_array($filter_column))
					{
						foreach ($filter_column as $key => $value) {
							if (strpos($key, '.') === FALSE)
								$cond['gastos_inmuebles.'.$key] = $value;
							else
								$cond[$key] = $value;
						}
					}
					else
						if ($filter_column !== FALSE AND $filter_value !== FALSE)
							if (strpos($filter_column, '.') === FALSE AND strpos($filter_column, '(') === FALSE)
								$cond['gastos_inmuebles.'.$filter_column] = $filter_value;
							else
								$cond[$filter_column] = $filter_value;

					if (count($cond) > 0)
						$this->db->where($cond);

					if (is_array($order_by))
					{
						foreach ($order_by as $order_column => $sort_order) {
							$this->db->order_by($order_column, $sort_order);
						}
					}

					if (!empty($term_filter)) {
						$this->db->group_start();
						$this->db->like('gasto_monto', $term_filter, 'both');
						$this->db->group_end();
					}

					if ($page !== FALSE)
					{
						$offset = $page*$page_items;
						$this->db->limit($page_items, $offset);
					}
					
					$result = $this->db->get();
					$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

					if ($result->num_rows() > 0)
					{
						$result = $result->result_array();
						foreach ($result as $key => $value) 
						{
							$result[$key]['total_results'] = $paginacion[0]['total_items'];
						}
						return $result;
					}

					return array();
				}


				/**
					 * Tabla: gastos_tipo 
					 * Columnas Requeridas: 
					 * Columnas Opcionales: tipo_nombre
					 * Opcionales:
					 * @column 	tipo_nombre                   	varchar(45)    	
					 */

						/**
						 * Crea un registro de gastos_tipo
						 *
						 *
						 * @return      int 	En caso de exito retorna el ID del registro insertado.
						 * @return      bool 	En caso de error retorna FALSE.
						 */
						public function new_gastos_tipo($tipo_nombre)
						{
							$data['tipo_nombre'] = $tipo_nombre;
							
							$result = $this->db->insert('gastos_tipo', $data);

							if ($result == TRUE) 
								return $this->db->insert_id();
							
							return FALSE; 
						}

						/**
						 * Elimina uno o mas registros de gastos_tipo
						 *
						 * @param 	$id_tipo            	ID del registro a eliminar. Array de IDs. IDs separados por coma.
						 * @param 	$limit              	Cantidad maxima de registros a eliminar.
						 *
						 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
						 */
						public function del_gastos_tipo($id_tipo = FALSE, $limit = 1)
						{
							if ($id_tipo === FALSE OR empty($id_tipo)) 
								return FALSE;

							if (is_array($id_tipo))
							{
								foreach ($id_tipo as $key => $value) {
									if (strpos($key, '.') === FALSE AND strpos($key, '(') === FALSE)
										$cond['gastos_tipo.'.$key] = $value;
									else
										$cond[$key] = $value;
								}
							}
							else
								$cond['id_tipo'] = $id_tipo;

							$this->db->where($cond);
							
							if ($limit > 0)
								$this->db->limit($limit);

							$result = $this->db->delete('gastos_tipo');

							return $result;
						}

						/**
						 * Retorna un gastos_tipo		 
						 *
						 * @param 	$id_tipo            	PK.
						 *
						 * @return	array	En caso de existir el registro retorna un array donde cada key es una columna.
						 * @return	bool	En caso de no existir el registro retorna FALSE.
						 */
						public function get_gastos_tipo($id_tipo = FALSE)
						{
							if ($id_tipo === FALSE OR empty($id_tipo)) 
								return FALSE;

							$cond['id_tipo'] = $id_tipo;

							$this->db->where($cond);
							$result = $this->db->get('gastos_tipo');

							if ($result->num_rows() > 0)
							{
								$result = $result->result_array();
								return $result[0];
							}

							return FALSE;
						}

						/**
						 * Actualiza una o mas columnas de un gastos_tipo		 
						 *
						 * @param 	$id_tipo            	PK.
						 * @param 	$filter_column      	Columna a actualizar. Ó Array con columnas=valores a setear.
						 * @param 	$filter_value       	Valor a setear, o FALSE si filter_column es un array.
						 *
						 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
						 */
						public function set_gastos_tipo($id_tipo, $filter_column = FALSE, $filter_value = FALSE)
						{
							$data = array();

							if (is_array($filter_column))
							{
								foreach ($filter_column as $key => $value) {
									if (strpos($key, '.') === FALSE)
										$data['gastos_tipo.'.$key] = $value;
									else
										$data[$key] = $value;
								}
							}
							else
								if ($filter_column !== FALSE AND $filter_value !== FALSE)
									if (strpos($filter_column, '.') === FALSE)
										$data['gastos_tipo.'.$filter_column] = $filter_value;
									else
										$data[$filter_column] = $filter_value;

							// En caso de que se proporcione un array de IDs
							if (is_array($id_tipo)) {
								$ids = implode(',', $id_tipo);
								$cond['id_tipo IN ('.$ids.')'] = NULL;
							}
							// En caso de que se proporcionen IDs separados por coma.
							elseif (strpos($id_tipo, ',') === FALSE)
								$cond['id_tipo IN ('.$id_tipo.')'] = NULL;
							else
								$cond['id_tipo'] = $id_tipo;

							$this->db->where($cond);

							$result = $this->db->update('gastos_tipo', $data);

							return $result;
						}

						/**
						 * Retorna una coleccion de gastos_tipo		 
						 *
						 * @param 	$id_tipo            	PK.
						 * @param 	$filter_column      	Columna a comparar. Ó Array con columnas=valores a buscar.
						 * @param 	$filter_value       	Valor buscado, o FALSE si filter_column es un array.
						 * @param 	$page               	Pagina desde la cual se busca.
						 * @param 	$page_items         	Cantidad de items de una pagina.
						 * @param 	$filter_column      	Array con columnas=orden.
						 * @param 	$term_filter        	Cadena de texto a buscar en columnas de texto o varchar.
						 *
						 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
						 */
						public function get_gastos_tipos($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $term_filter = FALSE)
						{
							$cond = array();

							$this->db->select('SQL_CALC_FOUND_ROWS gastos_tipo.*', FALSE);
							$this->db->from('gastos_tipo');

							if (is_array($filter_column))
							{
								foreach ($filter_column as $key => $value) {
									if (strpos($key, '.') === FALSE)
										$cond['gastos_tipo.'.$key] = $value;
									else
										$cond[$key] = $value;
								}
							}
							else
								if ($filter_column !== FALSE AND $filter_value !== FALSE)
									if (strpos($filter_column, '.') === FALSE AND strpos($filter_column, '(') === FALSE)
										$cond['gastos_tipo.'.$filter_column] = $filter_value;
									else
										$cond[$filter_column] = $filter_value;

							if (count($cond) > 0)
								$this->db->where($cond);

							if (is_array($order_by))
							{
								foreach ($order_by as $order_column => $sort_order) {
									$this->db->order_by($order_column, $sort_order);
								}
							}

							if (!empty($term_filter)) {
								$this->db->group_start();
								$this->db->like('tipo_nombre', $term_filter, 'both');
								$this->db->group_end();
							}

							if ($page !== FALSE)
							{
								$offset = $page*$page_items;
								$this->db->limit($page_items, $offset);
							}
							
							$result = $this->db->get();
							$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

							if ($result->num_rows() > 0)
							{
								$result = $result->result_array();
								foreach ($result as $key => $value) 
								{
									$result[$key]['total_results'] = $paginacion[0]['total_items'];
								}
								return $result;
							}

							return array();
						}


					/**
	 * Tabla: mentenimientos 
	 * Columnas Requeridas: id_inmueble, id_gasto_tipo, mantenimiento_concepto, id_profesional
	 * Columnas Opcionales: mantenimiento_cobro_propietario, mantenimiento_cobro_inquilino, mantenimiento_presupuesto, fecha_creacion, fecha_modificacion, fecha_eliminacion
	 * Opcionales:
	 * @column 	mantenimiento_cobro_propietario	text           	
	 * @column 	mantenimiento_cobro_inquilino 	text           	
	 * @column 	mantenimiento_presupuesto  	text           	
	 * @column 	fecha_creacion                	datetime       	
	 * @column 	fecha_modificacion            	datetime       	
	 * @column 	fecha_eliminacion             	datetime       	
	 */

		/**
		 * Crea un registro de mentenimiento
		 *
	 	* @param       int(10) unsigned	$id_inmueble                  	
	 	* @param       int(10) unsigned	$id_gasto_tipo                	
	 	* @param       text           	$mantenimiento_concepto       	
	 	* @param       text           	$id_profesional    	
		 *
		 * @return      int 	En caso de exito retorna el ID del registro insertado.
		 * @return      bool 	En caso de error retorna FALSE.
		 */
		public function new_mentenimiento($id_inmueble, $id_gasto_tipo, $mantenimiento_concepto, $id_profesional, $mantenimiento_cobro_propietario, $mantenimiento_cobro_inquilino, $mantenimiento_presupuesto)
		{
			$data['id_inmueble'] = $id_inmueble;
			$data['id_gasto_tipo'] = $id_gasto_tipo;
			$data['mantenimiento_concepto'] = $mantenimiento_concepto;
			$data['id_profesional'] = $id_profesional;
			$data['mantenimiento_cobro_propietario'] = $mantenimiento_cobro_propietario;
			$data['mantenimiento_cobro_inquilino'] = $mantenimiento_cobro_inquilino;
			$data['mantenimiento_presupuesto'] = $mantenimiento_presupuesto;

			$result = $this->db->insert('mentenimientos', $data);

			if ($result == TRUE) 
				return $this->db->insert_id();
			
			return FALSE; 
		}

		/**
		 * Elimina uno o mas registros de mentenimiento
		 *
		 * @param 	$id_mantenimiento   	ID del registro a eliminar. Array de IDs. IDs separados por coma.
		 * @param 	$limit              	Cantidad maxima de registros a eliminar.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function del_mentenimiento($id_mantenimiento = FALSE, $limit = 1)
		{
			if ($id_mantenimiento === FALSE OR empty($id_mantenimiento)) 
				return FALSE;

			if (is_array($id_mantenimiento))
			{
				foreach ($id_mantenimiento as $key => $value) {
					if (strpos($key, '.') === FALSE AND strpos($key, '(') === FALSE)
						$cond['mentenimientos.'.$key] = $value;
					else
						$cond[$key] = $value;
				}
			}
			else
				$cond['id_mantenimiento'] = $id_mantenimiento;

			$this->db->where($cond);
			
			if ($limit > 0)
				$this->db->limit($limit);

			$result = $this->db->delete('mentenimientos');

			return $result;
		}

		/**
		 * Retorna un mentenimiento		 
		 *
		 * @param 	$id_mantenimiento   	PK.
		 *
		 * @return	array	En caso de existir el registro retorna un array donde cada key es una columna.
		 * @return	bool	En caso de no existir el registro retorna FALSE.
		 */
		public function get_mentenimiento($id_mantenimiento = FALSE)
		{
			if ($id_mantenimiento === FALSE OR empty($id_mantenimiento)) 
				return FALSE;

			$cond['id_mantenimiento'] = $id_mantenimiento;

			$this->db->where($cond);
			$result = $this->db->get('mentenimientos');

			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				return $result[0];
			}

			return FALSE;
		}

		/**
		 * Actualiza una o mas columnas de un mentenimiento		 
		 *
		 * @param 	$id_mantenimiento   	PK.
		 * @param 	$filter_column      	Columna a actualizar. Ó Array con columnas=valores a setear.
		 * @param 	$filter_value       	Valor a setear, o FALSE si filter_column es un array.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function set_mentenimiento($id_mantenimiento, $filter_column = FALSE, $filter_value = FALSE)
		{
			$data = array();

			if (is_array($filter_column))
			{
				foreach ($filter_column as $key => $value) {
					if (strpos($key, '.') === FALSE)
						$data['mentenimientos.'.$key] = $value;
					else
						$data[$key] = $value;
				}
			}
			else
				if ($filter_column !== FALSE AND $filter_value !== FALSE)
					if (strpos($filter_column, '.') === FALSE)
						$data['mentenimientos.'.$filter_column] = $filter_value;
					else
						$data[$filter_column] = $filter_value;

			// En caso de que se proporcione un array de IDs
			if (is_array($id_mantenimiento)) {
				$ids = implode(',', $id_mantenimiento);
				$cond['id_mantenimiento IN ('.$ids.')'] = NULL;
			}
			// En caso de que se proporcionen IDs separados por coma.
			elseif (strpos($id_mantenimiento, ',') === FALSE)
				$cond['id_mantenimiento IN ('.$id_mantenimiento.')'] = NULL;
			else
				$cond['id_mantenimiento'] = $id_mantenimiento;

			$this->db->where($cond);

			$result = $this->db->update('mentenimientos', $data);

			return $result;
		}

		/**
		 * Retorna una coleccion de mentenimientos		 
		 *
		 * @param 	$id_mantenimiento   	PK.
		 * @param 	$filter_column      	Columna a comparar. Ó Array con columnas=valores a buscar.
		 * @param 	$filter_value       	Valor buscado, o FALSE si filter_column es un array.
		 * @param 	$page               	Pagina desde la cual se busca.
		 * @param 	$page_items         	Cantidad de items de una pagina.
		 * @param 	$filter_column      	Array con columnas=orden.
		 * @param 	$term_filter        	Cadena de texto a buscar en columnas de texto o varchar.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function get_mentenimientos($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $term_filter = FALSE)
		{
			$cond = array();

			$this->db->select('SQL_CALC_FOUND_ROWS mentenimientos.*, inmuebles.*, users.*', FALSE);
			$this->db->from('mentenimientos');
			$this->db->join('inmuebles' , 'inmuebles.id_inmueble = mentenimientos.id_inmueble', 'LEFT');
			$this->db->join('users' , 'users.id_user = mentenimientos.id_profesional', 'LEFT');


			if (is_array($filter_column))
			{
				foreach ($filter_column as $key => $value) {
					if (strpos($key, '.') === FALSE)
						$cond['mentenimientos.'.$key] = $value;
					else
						$cond[$key] = $value;
				}
			}
			else
				if ($filter_column !== FALSE AND $filter_value !== FALSE)
					if (strpos($filter_column, '.') === FALSE AND strpos($filter_column, '(') === FALSE)
						$cond['mentenimientos.'.$filter_column] = $filter_value;
					else
						$cond[$filter_column] = $filter_value;

			if (count($cond) > 0)
				$this->db->where($cond);

			if (is_array($order_by))
			{
				foreach ($order_by as $order_column => $sort_order) {
					$this->db->order_by($order_column, $sort_order);
				}
			}

			if (!empty($term_filter)) {
				$this->db->group_start();
				$this->db->like('mantenimiento_concepto', $term_filter, 'both');
				$this->db->or_like('id_profesional', $term_filter, 'both');
				$this->db->or_like('mantenimiento_cobro_propietario', $term_filter, 'both');
				$this->db->or_like('mantenimiento_cobro_inquilino', $term_filter, 'both');
				$this->db->or_like('mantenimiento_presupuesto', $term_filter, 'both');
				$this->db->group_end();
			}

			if ($page !== FALSE)
			{
				$offset = $page*$page_items;
				$this->db->limit($page_items, $offset);
			}
			
			$result = $this->db->get();
			$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				foreach ($result as $key => $value) 
				{
					$result[$key]['total_results'] = $paginacion[0]['total_items'];
				}
				return $result;
			}

			return array();
		}

}