<?php 
class Libreria extends CI_Model 
{

    public function __construct() 
    {

    }

    /**
     * Tabla: users_library 
     * Columnas Requeridas: id_user, id_file
     * Columnas Opcionales: file_reference, count_downloads, element_date
     * Opcionales:
     * @column  file_reference                  text            
     * @column  count_downloads                 smallint(6)     
     * @column  element_date                    datetime        
     */

        /**
         * Crea un registro de element
         *
         * @param       int(10) unsigned    $id_user                        
         * @param       int(10) unsigned    $id_file                        
         *
         * @return      int     En caso de exito retorna el ID del registro insertado.
         * @return      bool    En caso de error retorna FALSE.
         */
        public function new_element($id_user, $id_file)
        {
            $data['id_user'] = $id_user;
            $data['id_file'] = $id_file;

            $result = $this->db->insert('users_library', $data);

            if ($result == TRUE) 
                return $this->db->insert_id();
            
            return FALSE; 
        }

        /**
         * Elimina uno o mas registros de element
         *
         * @param   $id_element             ID del registro a eliminar. Array de IDs. IDs separados por coma.
         * @param   $limit                  Cantidad maxima de registros a eliminar.
         *
         * @return  bool    En caso de error retorna FALSE. En caso de exito TRUE.
         */
        public function del_element($id_element = FALSE, $limit = 1)
        {
            if ($id_element === FALSE OR empty($id_element)) 
                return FALSE;

            if (is_array($id_element))
            {
                foreach ($id_element as $key => $value) {
                    if (strpos($key, '.') === FALSE AND strpos($key, '(') === FALSE)
                        $cond['users_library.'.$key] = $value;
                    else
                        $cond[$key] = $value;
                }
            }
            else
                $cond['id_element'] = $id_element;

            $this->db->where($cond);
            
            if ($limit > 0)
                $this->db->limit($limit);

            $result = $this->db->delete('users_library');

            return $result;
        }

        /**
         * Retorna un element    
         *
         * @param   $id_element             PK.
         *
         * @return  array   En caso de existir el registro retorna un array donde cada key es una columna.
         * @return  bool    En caso de no existir el registro retorna FALSE.
         */
        public function get_element($id_element = FALSE)
        {
            if ($id_element === FALSE OR empty($id_element)) 
                return FALSE;

            $cond['id_element'] = $id_element;

            $this->db->where($cond);
            $result = $this->db->get('users_library');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0];
            }

            return FALSE;
        }

        /**
         * Actualiza una o mas columnas de un element    
         *
         * @param   $id_element             PK.
         * @param   $filter_column          Columna a actualizar. Ó Array con columnas=valores a setear.
         * @param   $filter_value           Valor a setear, o FALSE si filter_column es un array.
         *
         * @return  bool    En caso de error retorna FALSE. En caso de exito TRUE.
         */
        public function set_element($id_element, $filter_column = FALSE, $filter_value = FALSE)
        {
            $data = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    if (strpos($key, '.') === FALSE)
                        $data['users_library.'.$key] = $value;
                    else
                        $data[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    if (strpos($filter_column, '.') === FALSE)
                        $data['users_library.'.$filter_column] = $filter_value;
                    else
                        $data[$filter_column] = $filter_value;

            // En caso de que se proporcione un array de IDs
            if (is_array($id_element)) {
                $ids = implode(',', $id_element);
                $cond['id_element IN ('.$ids.')'] = NULL;
            }
            // En caso de que se proporcionen IDs separados por coma.
            elseif (strpos($id_element, ',') === FALSE)
                $cond['id_element'] = $id_element;
            else
                $cond['id_element IN ('.$id_element.')'] = NULL;

            $this->db->where($cond);

            $result = $this->db->update('users_library', $data);

            return $result;
        }

        /**
         * Retorna una coleccion de elements     
         *
         * @param   $id_element             PK.
         * @param   $filter_column          Columna a comparar. Ó Array con columnas=valores a buscar.
         * @param   $filter_value           Valor buscado, o FALSE si filter_column es un array.
         * @param   $page                   Pagina desde la cual se busca.
         * @param   $page_items             Cantidad de items de una pagina.
         * @param   $filter_column          Array con columnas=orden.
         * @param   $term_filter            Cadena de texto a buscar en columnas de texto o varchar.
         *
         * @return  bool    En caso de error retorna FALSE. En caso de exito TRUE.
         */
        public function get_elements($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $term_filter = FALSE, $selection = NULL)
        {
            $cond = array();

            $__sel = (!empty($selection)) ?  ", (CASE WHEN users_library.id_file IN ($selection) THEN 1 ELSE 0 END) AS selected" : ', 0 selected';

            $this->db->select('SQL_CALC_FOUND_ROWS users_library.*, files.*' . $__sel, FALSE);
            $this->db->from('users_library');
            $this->db->join('files', 'files.id_file = users_library.id_file', 'left');

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    if (strpos($key, '.') === FALSE)
                        $cond['users_library.'.$key] = $value;
                    else
                        $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    if (strpos($filter_column, '.') === FALSE AND strpos($filter_column, '(') === FALSE)
                        $cond['users_library.'.$filter_column] = $filter_value;
                    else
                        $cond[$filter_column] = $filter_value;

            if (count($cond) > 0)
                $this->db->where($cond);

            $this->db->order_by('selected', 'DESC');

            if (is_array($order_by))
            {
                foreach ($order_by as $order_column => $sort_order) {
                    $this->db->order_by($order_column, $sort_order);
                }
            }

            if (!empty($term_filter)) {
                $this->db->group_start();
                $this->db->like('file_reference', $term_filter, 'both');
                $this->db->or_like('file_name', $term_filter, 'both');
                $this->db->or_like('file_description', $term_filter, 'both');
                $this->db->group_end();
            }

            if (!empty($selection))
                $this->db->or_where("users_library.id_file IN ($selection)", NULL);

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $result = $this->db->get(); #echo $this->db->last_query();
            $paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                foreach ($result as $key => $value) 
                {
                    $result[$key]['total_results'] = $paginacion[0]['total_items'];
                }
                return $result;
            }

            return array();
        }


    /**
     * Tabla: files 
     * Columnas Requeridas: file_name, file_ext
     * Columnas Opcionales: id_user, file_description, file_type, file_size, is_image, file_local, file_remote, file_cleanup
     * Opcionales:
     * @column  id_user                         int(10) unsigned    Usuario que cargo el archivo.
     * @column  file_description                text            
     * @column  file_type                       varchar(35)     
     * @column  file_size                       int(11)         Tamaño del archivo en bytes
     * @column  is_image                        tinyint(1)      
     * @column  file_local                      varchar(40)     Nombre del archivo temporal local..
     * @column  file_remote                     text            Url al archivo en lugar remoto. ej CDN.
     * @column  file_cleanup                    datetime        Fecha programada para que un cron elimine este archivo. Al usar un archivo se debe setear en NULL.
     */

        /**
         * Crea un registro de file
         *
         * @param       varchar(255)    $file_name                      Nombre original del archivo
         * @param       varchar(5)      $file_ext                       
         *
         * @return      int     En caso de exito retorna el ID del registro insertado.
         * @return      bool    En caso de error retorna FALSE.
         */
        public function new_file($file_name, $file_ext)
        {
            $data['file_name'] = $file_name;
            $data['file_ext'] = $file_ext;

            $result = $this->db->insert('files', $data);

            if ($result == TRUE) 
                return $this->db->insert_id();
            
            return FALSE; 
        }

        /**
         * Elimina uno o mas registros de file
         *
         * @param   $id_file                ID del registro a eliminar. Array de IDs. IDs separados por coma.
         * @param   $limit                  Cantidad maxima de registros a eliminar.
         *
         * @return  bool    En caso de error retorna FALSE. En caso de exito TRUE.
         */
        public function del_file($id_file = FALSE)
        {
            if ($id_file === FALSE OR empty($id_file)) 
                return FALSE;

            $file = $this->get_file($id_file);
            @unlink(FCPATH . '/uploads/thumb/' . $file['file_key']);
            @unlink(FCPATH . '/uploads/' .  $file['file_key']);

            $cond['id_file'] = $id_file;

            $this->db->where($cond);
            
            $result = $this->db->delete('files');

            return $result;
        }

        /**
         * Retorna un file   
         *
         * @param   $id_file                PK.
         *
         * @return  array   En caso de existir el registro retorna un array donde cada key es una columna.
         * @return  bool    En caso de no existir el registro retorna FALSE.
         */
        public function get_file($id_file = FALSE)
        {
            if ($id_file === FALSE OR empty($id_file)) 
                return FALSE;

            $cond['id_file'] = $id_file;

            $this->db->where($cond);
            $result = $this->db->get('files');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                return $result[0];
            }

            return FALSE;
        }

        /**
         * Actualiza una o mas columnas de un file   
         *
         * @param   $id_file                PK.
         * @param   $filter_column          Columna a actualizar. Ó Array con columnas=valores a setear.
         * @param   $filter_value           Valor a setear, o FALSE si filter_column es un array.
         *
         * @return  bool    En caso de error retorna FALSE. En caso de exito TRUE.
         */
        public function set_file($id_file, $filter_column = FALSE, $filter_value = FALSE)
        {
            $data = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    if (strpos($key, '.') === FALSE)
                        $data['files.'.$key] = $value;
                    else
                        $data[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    if (strpos($filter_column, '.') === FALSE)
                        $data['files.'.$filter_column] = $filter_value;
                    else
                        $data[$filter_column] = $filter_value;

            // En caso de que se proporcione un array de IDs
            if (is_array($id_file)) {
                $ids = implode(',', $id_file);
                $cond['id_file IN ('.$ids.')'] = NULL;
            }
            // En caso de que se proporcionen IDs separados por coma.
            elseif (strpos($id_file, ',') === FALSE)
                $cond['id_file'] = $id_file;
            else
                $cond['id_file IN ('.$id_file.')'] = NULL;

            $this->db->where($cond);

            $result = $this->db->update('files', $data);

            return $result;
        }

        /**
         * Retorna una coleccion de files    
         *
         * @param   $id_file                PK.
         * @param   $filter_column          Columna a comparar. Ó Array con columnas=valores a buscar.
         * @param   $filter_value           Valor buscado, o FALSE si filter_column es un array.
         * @param   $page                   Pagina desde la cual se busca.
         * @param   $page_items             Cantidad de items de una pagina.
         * @param   $filter_column          Array con columnas=orden.
         * @param   $term_filter            Cadena de texto a buscar en columnas de texto o varchar.
         *
         * @return  bool    En caso de error retorna FALSE. En caso de exito TRUE.
         */
        public function get_files($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $term_filter = FALSE, $selection = NULL)
        {
            $cond = array();

            $__sel = (!empty($selection)) ?  ", (CASE WHEN id_file IN ($selection) THEN 1 ELSE 0 END) AS selected" : ', 0 selected';

            $this->db->select('SQL_CALC_FOUND_ROWS files.*, 0 id_element '.$__sel, FALSE);
            $this->db->from('files');

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    if (strpos($key, '.') === FALSE)
                        $cond['files.'.$key] = $value;
                    else
                        $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    if (strpos($filter_column, '.') === FALSE AND strpos($filter_column, '(') === FALSE)
                        $cond['files.'.$filter_column] = $filter_value;
                    else
                        $cond[$filter_column] = $filter_value;

            if (count($cond) > 0)
                $this->db->where($cond);

            if (!empty($selection))
                $this->db->or_where("id_file IN ($selection)", NULL);

            if (is_array($order_by))
            {
                foreach ($order_by as $order_column => $sort_order) {
                    $this->db->order_by($order_column, $sort_order);
                }
            }

            if (!empty($term_filter)) {
                $this->db->group_start();
                $this->db->like('file_name', $term_filter, 'both');
                $this->db->or_like('file_description', $term_filter, 'both');
                $this->db->or_like('file_type', $term_filter, 'both');
                $this->db->or_like('file_ext', $term_filter, 'both');
                $this->db->group_end();
            }

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $result = $this->db->get();
            $paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                foreach ($result as $key => $value) 
                {
                    $result[$key]['total_results'] = $paginacion[0]['total_items'];
                }
                return $result;
            }

            return array();
        }

}