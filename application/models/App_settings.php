<?php 
class App_settings extends CI_Model 
{
    
    public function __construct() {}

    /**
     * SETTINGS
     * Settings son relativos al user. 
     * Sirven para guardar key->valor asociados siempre al user. Pueden expirar. Se puede guardar arrays como valores.
     */

        public function save($setting_item, $setting_value, $date_expire = FALSE, $id_user = FALSE)
        {
            // Si no hay item, bye
            if (empty($setting_item)) 
                return FALSE;
            
            $date_expire    = ($date_expire === FALSE) ? NULL : $date_expire;
            $id_user        = ($id_user     === FALSE) ? $this->session->userdata('id_user') : $id_user;

            $already_exist = $this->get_setting($setting_item, $id_user, TRUE);

            $data['setting_item']   = $setting_item;
            $data['setting_value']  = (is_array($setting_value)) ? json_encode($setting_value) : $setting_value;
            $data['setting_complex']= is_array($setting_value);
            $data['date_expire']    = $date_expire;
            $data['date_creation']  = date('Y-m-d H:i:s');
            $data['id_user']        = $id_user;
            
            if ($already_exist === FALSE) 
                $result = $this->db->insert('users_settings', $data);
            else
            {
                $cond['id_setting'] = $already_exist['id_setting'];
                $this->db->where($cond);
                $result = $this->db->update('users_settings', $data);
            }

            if ($result == TRUE) 
                return $this->db->insert_id();
            else
                return FALSE; 
        }

        public function get_setting($setting_item, $id_user = FALSE, $full = FALSE)
        {
            // Si no hay item, bye
            if (empty($setting_item)) 
                return FALSE;

            // Controlamos user
            $id_user        = ($id_user     === FALSE) ? $this->session->userdata('id_user') : $id_user;
            if (empty($setting_item)) 
                return FALSE;

            // Limpieza gral de settings viejos.
            $this->clean_settings(date('Y-m-d H:i:s'));

            // Buscamos el setting.
            $cond['setting_item']   = $setting_item;
            $cond['id_user']        = $id_user;
            $this->db->where($cond);
            $result = $this->db->get('users_settings');

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                $result = $result[0];
                $result['setting_value'] = ($result['setting_complex']) ? json_decode($result['setting_value']) : $result['setting_value'];
                return ($full == FALSE) ? $result['setting_value'] : $result;;
            }

            return FALSE;
        }

        public function clean_settings($date_expire = FALSE)
        {
            if ($date_expire === FALSE) 
                return FALSE;
            $cond['date_expire<'] = $date_expire;

            $this->db->where($cond);
            $result = $this->db->delete('users_settings');

            return $this->db->affected_rows();
        }

    /**
     * CONFIGS
     * No estan segmentados por usuarios.
     * La estructura y valores por defecto se define en archivos de config.
     */

        // GESTION DE CONFIGURACIONES
            
            // Controla que una configuration ya este seteada.
            // En caso de que no este, la setea con los valores por defecto.
            public function get_config($scope, $item, $reference = NULL)
            {
                // Variantes para encontrar la configuracion. Omitimos el lenguaje en una primer instancia.
                $conditions['config_scope']       = $scope;
                $conditions['config_item']        = $item;
                $conditions['config_reference']   = $reference;

                // Buscamos el registro de config.
                          $this->db->where($conditions);
                $result = $this->db->get('app_configurations');

                // Si encontramos el registro..
                if ($result->num_rows() > 0)
                {
                    // Individualizamos el resultado
                    $result = $result->result_array();
                    $result = $result[0];

                    return $result['config_value'];
                }
                else
                {
                    // Obtenemos el valor por defecto.
                    $value  = $this->get_default_value($scope, $item);

                    // Si no existe en el scope, valse.
                    if ($value === FALSE)
                        return FALSE;

                    // Guardamos el valor por defecto para esta config.
                    $result = $this->set_config($scope, $item, $value, $reference, TRUE);
                    return $value;
                }
            }

            // Establece una confiiguracion, o si existe la actualiza.
            public function set_config($scope, $item, $value, $reference = NULL, $skip_permission = FALSE)
            {
                $value          = (string)trim($value);
                $defined_item   = $this->get_item($scope, $item);

                if ($defined_item === FALSE)
                    return FALSE; // dar error

                // El seteo es automatico?
                if ($skip_permission == FALSE)
                {
                    // Revisamos que tenga los permisos necesarios, en caso de ser necesario.
                    if (isset($defined_item['permissions'])) {
                        if ($defined_item['permissions'] !== FALSE)
                        {
                            // Vemos si se requieren varios permisos o uno solo (string o array).
                            if (is_array($defined_item['permissions']))
                                foreach ($defined_item['permissions'] as $key => $permission) 
                                    if(!check_permissions($scope, $permission, FALSE)) return FALSE;
                            else
                                if(!check_permissions($scope, $defined_item['permissions'], FALSE)) 
                                    return FALSE;
                        }
                    }
                }

                // Existe este registro de configuracion?
                $conditions['config_scope']       = $scope;
                $conditions['config_item']        = $item;
                $conditions['config_reference']   = empty($reference) ? NULL : $reference;
                $this->db->where($conditions);
                $result = $this->db->get('app_configurations');

                // Existe?!!
                if ($result->num_rows() > 0)
                {
                    // si... 
                    $result          = $result->result_array();
                    $result          = $result[0];
                    $id_config       = $result['id_configuration'];
                    
                    // entonces si es distinto, actualizamos el valor del registro.
                    if ($value != $result['config_value'])
                    {
                        $updated['config_value']   = $value;
                                    $this->db->where('id_configuration', $id_config);
                        return      $this->db->update('app_configurations', $updated);
                    }
                    else
                        return TRUE;
                }
                else
                {
                    // no... entonces lo insertamos.
                    $new_configuration                     = $conditions;
                    $new_configuration['config_value']     = $value;
                    
                    $result = $this->db->insert('app_configurations', $new_configuration);
                    return $result;
                }
            }

            // Elimina un registro de configuración. Podria ser una forma de resetear a default una cfg.
            public function del_config($scope, $item, $reference = NULL)
            {
                // Variantes para encontrar la configuracion. Omitimos el lenguaje en una primer instancia.
                $conditions['config_scope']       = $scope;
                $conditions['config_item']        = $item;
                $conditions['config_reference']   = empty($reference) ? NULL : $reference;

                // Buscamos el registro de config.
                $this->db->where($conditions);
                
                return $this->db->delete('app_configurations');
            }

        // OBTENCION DE CONFIGURACIONES

            // Retorna solo las configuraciones SETEADAS para un determinado scope y reference, en un idioma especifico.
            // En caso de faltar configuraciones de ese scope, devuelve los valores default definidos en el cfg.
            public function get_saved_config($scope, $reference = NULL)
            {
                $cond['config_scope']       = $scope;
                $cond['config_reference']   = $reference;

                $this->db->where($cond);
                $result     = $this->db->get('app_configurations');
                $defaults   = $this->get_default_scope($scope);

                // Sobrescribimos los defaults con los guardados
                if ($result->num_rows() > 0)
                {
                    $result = $result->result_array();
                    foreach ($result as $_key => $_item) 
                    {
                        $item   = $_item['config_item'];
                        $value  = $_item['config_value'];
                        $defaults[$item] = $value;
                    }
                }

                return $defaults;
            }

            // Retorna los items de configuracion completos, con su respectivo valor seteado.
            public function get_configurations($scope, $reference = NULL)
            {
                $saved = $this->get_saved_config($scope, $reference);
                $items = $this->get_items($scope);

                if ($items == FALSE)
                    return FALSE;

                foreach ($items as $_kitem => $_item) {

                    $items[$_kitem]['saved'] = $saved[$_kitem];
                }

                return $items;
            }

        // HELPERS

            // Devuelve un item de configuracion.
            private function get_item($scope, $item)
            {
                $result = $this->load_config_structure($scope);
                if ($result == FALSE)
                    return FALSE;

                $configurations = $this->config->item('app_configurations');
                
                if (!isset($configurations[$scope][$item]))
                    return FALSE;

                return $configurations[$scope][$item];
            }

            // Devuelve todos los item de un scope de configuracion.
            private function get_items($scope)
            {
                $result = $this->load_config_structure($scope);
                if ($result == FALSE)
                    return FALSE;

                $configurations = $this->config->item('app_configurations');
                
                if (!isset($configurations[$scope]))
                    return FALSE;

                return $configurations[$scope];
            }

            // Devuelve el valor por defecto de un item de configuracion.
            private function get_default_value($scope, $item)
            {
                $result = $this->load_config_structure($scope);
                if ($result == FALSE)
                    return FALSE;

                $configurations = $this->config->item('app_configurations');
                
                if (!isset($configurations[$scope][$item]['default']))
                    return FALSE;

                return $configurations[$scope][$item]['default'];
            }

            // Devuelve todos los valores por defectos de un scope de configuracion.
            public function get_default_scope($scope)
            {
                $result = $this->load_config_structure($scope);
                
                // Se cargo correctamente el cfg?
                if ($result == FALSE)
                    return FALSE;

                $keyvalues      = array();
                $configurations = $this->config->item('app_configurations');

                if (!isset($configurations[$scope]))
                    return FALSE;

                foreach ($configurations[$scope] as $_item => $_attrs) 
                    $keyvalues[$_item] = $_attrs['default'];

                return $keyvalues;
            }

            // Carga a la instancia de CI, la config indicada en el arhcivo,
            public function load_config_structure($scope)
            {
                // Obtenemos las opciones de configuracion de del tema
                $options = $this->load_config_file($scope);

                if ($options == FALSE)
                    return FALSE;

                // Regeneramos las opciones de configuracion.
                $configurations = $this->config->item('app_configurations');
                $configurations[$scope] = $options;
                $this->config->set_item('app_configurations', $configurations);
                
                return TRUE;
            }

            // Carga en memoria la estructura de configuacion requerida, 
            // Levantando las opciones desde archivos de configuracion ubicados en las carpetas del tema.
            private function load_config_file($scope)
            {
                $config_schema = APPPATH.'app'.DIRECTORY_SEPARATOR.'configs'.DIRECTORY_SEPARATOR.$scope.'.php';

                // Si el archivo de configuracion existe, lo cargamos.
                if (file_exists(@$config_schema))
                {
                    unset($_CFG);
                    include($config_schema);
                    return @$_CFG;
                }
                
                // De lo contrario retornamos 0 opciones de configuracion.
                return FALSE;
            }

}