<?php
class App_notifications extends CI_Model 
{

    public function __construct() {}

    /**
     * Herramientas
     */

        /**
         * Genera una notificacion para todos los usuarios de un rol.
         */
        public function notyfy_role($id_role, $notif_group, $notif_title, $notif_content, $notif_url, $notif_mail = FALSE, $apearence = NULL)
        {
            // Obtenemos todos los usuarios activos de este rol.
            $cond['user_deletion_date']     = NULL;
            $cond['id_role']                = $id_role;
            $users = $this->app_users->get_users($cond, FALSE, 0, 10000);

            // Notificamos a todos.
            foreach ($users as $_kuser => $_user) {
                $this->notyfy_user($_user['id_user'], $notif_group, $notif_title, $notif_content, $notif_url, $notif_mail, $apearence);
            }
        }

        /**
         * Genera una notificacion para un usuario.
         */
        public function notyfy_user($id_user, $notif_group, $notif_title, $notif_content, $notif_url, $notif_mail = FALSE, $apearence = NULL)
        {
            // Chequeamos si el user quiere ser notificado para este grupo de notificaicones.
            $notificar = ($notif_mail) ? $this->app_settings->get_setting('notif_'.$notif_group, $id_user) : FALSE;

            // Registramos la notificacion.
            return $this->new_notification($id_user, $notif_group, $notif_title, $notif_content, $notif_url, $notificar, $apearence);
        }

        /**
         * Marca como vista una notificacion.
         * Si no se especifica token, marca todas las del usuario.
         */
        public function view($id_user, $notif_token = FALSE)
        {
            if ($notif_token != FALSE OR !empty($notif_token))
                $cond['notif_token']    = $notif_token;
            else
                $cond['notif_readed']   = 0;

            $cond['id_user']        = $id_user;
            $notifications = $this->get_notifications($cond, FALSE, 0, 99999);

            foreach ($notifications as $_knf => $_nf)
                $this->set_notification($_nf['id_notification'], 'notif_readed', 1);

            return TRUE;
        }

        /**
         * Elimina todas las notificaciones ya leidas.
         * Si no se especifica token, marca todas las del usuario.
         */
        public function clean($id_user, $notif_token = FALSE)
        {
            if ($notif_token != FALSE OR !empty($notif_token))
                $cond['notif_token']    = $notif_token;
            else
                $cond['notif_readed']   = 1;

            $cond['id_user']        = $id_user;
            $notifications = $this->get_notifications($cond, FALSE, 0, 99999);

            foreach ($notifications as $_knf => $_nf)
                $this->del_notification($_nf['id_notification']);

            return TRUE;
        }
    
        /**
         * Restablece la configuracion de notificaciones de los usuarios.
         * Con HARD sobrescribe lo que haya. Sin HARD agrega claves que falten.
         */
        public function reset_settings($id_user)
        {
            $notifications = $this->config->item('notifications');
            foreach ($notifications['groups'] as $group_name => $group) 
            {
                reset($group);
                $default_value  = (int)$group[key($group)];
                $this->app_settings->save('notif_'.$group_name, $default_value);
            }
        }

        /**
         * Restablece la configuracion de notificaciones de los usuarios.
         * Con HARD sobrescribe lo que haya. Sin HARD agrega claves que falten.
         */
        public function get_settings()
        {
            $notifications  = $this->config->item('notifications');
            $settings       = array();
            foreach ($notifications['groups'] as $group_name => $group) 
            {
                reset($group);
                $setting['group'] = $group_name;
                $setting['descr'] = key($group);
                $setting['value'] = $this->app_settings->get_setting('notif_'.$group_name);
                $settings[] = $setting; unset($setting);
            }
            return $settings;
        }

    /**
     * VVV   Funciones ABM   VVV
     */

        public function new_notification($id_user, $notif_group, $notif_title, $notif_content, $notif_url, $notif_mail = FALSE, $apearence = NULL)
        {
            $data['id_user']            = $id_user;
            $data['notif_group']        = $notif_group;
            $data['notif_title']        = $notif_title;
            $data['notif_content']      = $notif_content;
            $data['notif_url']          = $notif_url;
            $data['notif_date']         = date('Y-m-d H:i:s');
            $data['notif_readed']       = 0;
            $data['notif_token']        = uniqid();
            $data['notif_mail']         = $notif_mail;
            $data['notif_apearence']    = $apearence;

            $result = $this->db->insert('users_notifications', $data);

            if ($result == TRUE) 
                return $this->db->insert_id();
            else
                return FALSE; 
        }

        public function get_notification($id_notification)
        {
            if ($id_notification === FALSE) return FALSE;
            $cond['users_notifications.id_notification'] = $id_notification;

            $this->db->select('users_notifications.*', FALSE);
            $this->db->from('users_notifications');
            $this->db->where($cond);
            $result = $this->db->get();

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                $result = $result[0];
                
                $extras = FALSE;
                if (!empty($result['notif_apearence'])) 
                    $extras = explode('|', $result['notif_apearence']);

                $result['target'] = (strpos($result['notif_url'], '?') !== FALSE) ? base_url($result['notif_url'].'&nf='.$result['notif_token']) : base_url($result['notif_url'].'?nf='.$result['notif_token']);
                $result['label']  = (isset($extras[0])) ? $extras[0] : 'default';
                $result['icon']   = (isset($extras[1])) ? $extras[1] : 'fa fa-bullhorn';
                return $result;
            }

            return FALSE;
        }

        public function del_notification($id_notification)
        {
            if ($id_notification === FALSE) return FALSE;
            $cond['id_notification'] = $id_notification;

            $this->db->where($cond);
            $result = $this->db->delete('users_notifications');

            return $this->db->affected_rows();
        }

        public function get_notifications($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $having = FALSE, $term_filter = FALSE)
        {
            $cond = array();

            $this->db->select('SQL_CALC_FOUND_ROWS users_notifications.*', FALSE);
            $this->db->from('users_notifications');

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $cond[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $cond[$filter_column] = $filter_value;
                else
                    if ($filter_column !== FALSE)
                        $cond['id_notification'] = $filter_column;

            if (count($cond) > 0)
                $this->db->where($cond);

            if (!is_array($order_by))
                $order_by['id_notification'] = 'DESC';
            
            if (is_array($order_by))
            {
                foreach ($order_by as $order_column => $sort_order) {
                    $this->db->order_by($order_column, $sort_order);
                }
            }
            if (!empty($term_filter)) {
                $this->db->group_start();
                $this->db->like('users_notifications.notification_name', $term_filter, 'both');
                $this->db->group_end();
            }
            if (is_array($having))
            {
                $havingp = array();
                foreach ($having as $key => $value) {
                    $havingp[$key] = $value;
                }
                $this->db->having($havingp);
            }

            if ($page !== FALSE)
            {
                $offset = $page*$page_items;
                $this->db->limit($page_items, $offset);
            }
            
            $result = $this->db->get();
            $paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

            if ($result->num_rows() > 0)
            {
                $result = $result->result_array();
                foreach ($result as $key => $_nf) 
                {
                    $extras = FALSE;
                    if (!empty($_nf['notif_apearence'])) 
                        $extras = explode('|', $_nf['notif_apearence']);

                    $result[$key]['target'] = (strpos($_nf['notif_url'], '?') !== FALSE) ? base_url($_nf['notif_url'].'&nf='.$_nf['notif_token']) : base_url($_nf['notif_url'].'?nf='.$_nf['notif_token']);
                    $result[$key]['label']  = (isset($extras[0])) ? $extras[0] : 'default';
                    $result[$key]['icon']   = (isset($extras[1])) ? $extras[1] : 'fa fa-bullhorn';
                    $result[$key]['total_results'] = $paginacion[0]['total_items'];
                }
                return $result;
            }

            return array();
        }

        public function set_notification($id_notification, $filter_column = FALSE, $filter_value = FALSE)
        {
            $data = array();

            if (is_array($filter_column))
            {
                foreach ($filter_column as $key => $value) {
                    $data[$key] = $value;
                }
            }
            else
                if ($filter_column !== FALSE AND $filter_value !== FALSE)
                    $data[$filter_column] = $filter_value;

            $cond['id_notification'] = $id_notification;

            $this->db->where($cond);
            $result = $this->db->update('users_notifications', $data);

            return (bool)$this->db->affected_rows();
        }

}
