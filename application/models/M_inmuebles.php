<?php 
class M_Inmuebles extends CI_Model 
{

	public function __construct() 
	{

	}

	/**
	 * Tabla: inmuebles 
	 * Columnas Requeridas: 
	 * Columnas Opcionales: id_propietario, id_impuesto, id_expensa, inmueble_nombre
	 * Opcionales:
	 * @column 	id_propietario                	int(11)        	
	 * @column 	id_impuesto                   	int(11)        	
	 * @column 	id_expensa                    	int(11)        	
	 * @column 	inmueble_nombre               	varchar(45)    	
	 */

		/**
		 * Crea un registro de inmueble
		 *
		 *
		 * @return      int 	En caso de exito retorna el ID del registro insertado.
		 * @return      bool 	En caso de error retorna FALSE.
		 */
		public function new_inmueble($inmueble_nombre)
		{
			$data['inmueble_nombre'] = $inmueble_nombre;
			$result = $this->db->insert('inmuebles', $data);

			if ($result == TRUE) 
				return $this->db->insert_id();
			
			return FALSE; 
		}

		/**
		 * Elimina uno o mas registros de inmueble
		 *
		 * @param 	$id_inmueble        	ID del registro a eliminar. Array de IDs. IDs separados por coma.
		 * @param 	$limit              	Cantidad maxima de registros a eliminar.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function del_inmueble($id_inmueble = FALSE, $limit = 1)
		{
			if ($id_inmueble === FALSE OR empty($id_inmueble)) 
				return FALSE;

			if (is_array($id_inmueble))
			{
				foreach ($id_inmueble as $key => $value) {
					if (strpos($key, '.') === FALSE AND strpos($key, '(') === FALSE)
						$cond['inmuebles.'.$key] = $value;
					else
						$cond[$key] = $value;
				}
			}
			else
				$cond['id_inmueble'] = $id_inmueble;

			$this->db->where($cond);
			
			if ($limit > 0)
				$this->db->limit($limit);

			$result = $this->db->delete('inmuebles');

			return $result;
		}

		/**
		 * Retorna un inmueble		 
		 *
		 * @param 	$id_inmueble        	PK.
		 *
		 * @return	array	En caso de existir el registro retorna un array donde cada key es una columna.
		 * @return	bool	En caso de no existir el registro retorna FALSE.
		 */
		public function get_inmueble($id_inmueble = FALSE)
		{
			if ($id_inmueble === FALSE OR empty($id_inmueble)) 
				return FALSE;

			$cond['id_property'] = $id_inmueble;

			$this->db->where($cond);
			$result = $this->db->get('properties');

			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				return $result[0];
			}

			return FALSE;
		}

		/**
		 * Actualiza una o mas columnas de un inmueble		 
		 *
		 * @param 	$id_inmueble        	PK.
		 * @param 	$filter_column      	Columna a actualizar. Ó Array con columnas=valores a setear.
		 * @param 	$filter_value       	Valor a setear, o FALSE si filter_column es un array.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function set_inmueble($id_inmueble, $filter_column = FALSE, $filter_value = FALSE)
		{
			$data = array();

			if (is_array($filter_column))
			{
				foreach ($filter_column as $key => $value) {
					if (strpos($key, '.') === FALSE)
						$data['inmuebles.'.$key] = $value;
					else
						$data[$key] = $value;
				}
			}
			else
				if ($filter_column !== FALSE AND $filter_value !== FALSE)
					if (strpos($filter_column, '.') === FALSE)
						$data['inmuebles.'.$filter_column] = $filter_value;
					else
						$data[$filter_column] = $filter_value;

			// En caso de que se proporcione un array de IDs
			if (is_array($id_inmueble)) {
				$ids = implode(',', $id_inmueble);
				$cond['id_inmueble IN ('.$ids.')'] = NULL;
			}
			// En caso de que se proporcionen IDs separados por coma.
			elseif (strpos($id_inmueble, ',') === FALSE)
				$cond['id_inmueble IN ('.$id_inmueble.')'] = NULL;
			else
				$cond['id_inmueble'] = $id_inmueble;

			$this->db->where($cond);

			$result = $this->db->update('inmuebles', $data);

			return $result;
		}

		/**
		 * Retorna una coleccion de inmuebles		 
		 *
		 * @param 	$id_inmueble        	PK.
		 * @param 	$filter_column      	Columna a comparar. Ó Array con columnas=valores a buscar.
		 * @param 	$filter_value       	Valor buscado, o FALSE si filter_column es un array.
		 * @param 	$page               	Pagina desde la cual se busca.
		 * @param 	$page_items         	Cantidad de items de una pagina.
		 * @param 	$filter_column      	Array con columnas=orden.
		 * @param 	$term_filter        	Cadena de texto a buscar en columnas de texto o varchar.
		 *
		 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
		 */
		public function get_inmuebles($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $term_filter = FALSE)
		{
			$cond = array();

			$this->db->select('SQL_CALC_FOUND_ROWS inmuebles.*', FALSE);
			$this->db->from('inmuebles');

			if (is_array($filter_column))
			{
				foreach ($filter_column as $key => $value) {
					if (strpos($key, '.') === FALSE)
						$cond['inmuebles.'.$key] = $value;
					else
						$cond[$key] = $value;
				}
			}
			else
				if ($filter_column !== FALSE AND $filter_value !== FALSE)
					if (strpos($filter_column, '.') === FALSE AND strpos($filter_column, '(') === FALSE)
						$cond['inmuebles.'.$filter_column] = $filter_value;
					else
						$cond[$filter_column] = $filter_value;

			if (count($cond) > 0)
				$this->db->where($cond);

			if (is_array($order_by))
			{
				foreach ($order_by as $order_column => $sort_order) {
					$this->db->order_by($order_column, $sort_order);
				}
			}

			if (!empty($term_filter)) {
				$this->db->group_start();
				$this->db->like('inmueble_nombre', $term_filter, 'both');
				$this->db->group_end();
			}

			if ($page !== FALSE)
			{
				$offset = $page*$page_items;
				$this->db->limit($page_items, $offset);
			}
			
			$result = $this->db->get();
			$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

			if ($result->num_rows() > 0)
			{
				$result = $result->result_array();
				foreach ($result as $key => $value) 
				{
					$result[$key]['total_results'] = $paginacion[0]['total_items'];
				}
				return $result;
			}

			return array();
		}

}