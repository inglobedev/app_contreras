<?php
class App_permissions extends CI_Model 
{

    public function __construct() {}

    // Controla que exista el permiso para esta operacion. De lo contrario, crea el registro y lo setea en false.
    public function get($id_role, $permission_group, $permission_item)
    {
        $conditions['id_role']           = $id_role;
        $conditions['permission_group']  = $permission_group;
        $conditions['permission_item']   = $permission_item;

        $this->db->where($conditions);
        $result = $this->db->get('roles_permissions');

        if ($result->num_rows() > 0)
        {
            // Existe el registro para este permiso.
            $result = $result->result_array();
            return $result[0]['permission_value'];
        }
        else
        {
            // Obtenemos el valor por defecto (app_permissions)
            $default_permissions = $this->config->item('permissions');
            if (isset($default_permissions[$permission_group][$permission_item]))
                $permission_value = $default_permissions[$permission_group][$permission_item];
            else
                $permission_value = ($id_role == '1') ? TRUE : DEFAULT_PERMISSION_VALUE; // El rol 1 es administrador. 
            
            // Lo guardamos.
            $this->set($id_role, $permission_group, $permission_item, $permission_value);
            return $permission_value;
        }
    }

    // Establece un permiso para el usuario. Si el permiso ya existe, lo actualiza.
    public function set($id_role, $permission_group, $permission_item, $permission_value)
    {
        $conditions['id_role']            = $id_role;
        $conditions['permission_group']  = $permission_group;
        $conditions['permission_item']   = $permission_item;

        $this->db->where($conditions);
        $result = $this->db->get('roles_permissions');

        if ($result->num_rows() > 0)
        {
            // Existe el registro para este permiso.
            // Entonces lo actualizamos.
            $result         = $result->result_array();
            $data['permission_value'] = $permission_value;

                        $this->db->where($conditions);
            $result =   $this->db->update('roles_permissions', $data);

            if ($result == TRUE)
                return (bool)$this->db->affected_rows();

            return FALSE;
        }
        else
        {
            $new_permission                     = $conditions;
            $new_permission['permission_value'] = $permission_value;
            $new_permission['permission_date']  = date('Y-m-d H:i:s');
            $result =   $this->db->insert('roles_permissions', $new_permission);

            if ($result == TRUE)
                return (bool)$this->db->affected_rows();

            return FALSE;
        }
    }

    // Retorna todos los permisos de un rol.
    public function get_role_permissions($id_role)
    {
        // Cargamos la estructura de permisos de GestorP (Los necesarios para utilizar el panel.)
        $permissions       = $this->config->item('permissions');
        
        // Obtenemos todos los permisos del usuario
                             $this->db->where('id_role', $id_role);
        $saved_permissions = $this->db->get('roles_permissions')->result_array();

        // Anexamos cada permiso guardado, a la tabla de permisos del usuario.
        foreach ($saved_permissions as $key => $permission) 
        {
            $p_group    = $permission['permission_group'];
            $p_item     = $permission['permission_item'];
            $p_value    = $permission['permission_value'];

            $permissions[$p_group][$p_item] =  $p_value;
        }

        return $permissions;
    }   

    // Elimmina todos los registros de permisos, de una determinado tipo de item.
    public function clean_permissions($p_group, $p_item)
    {
        $cond['permission_group']   = $p_group;
        $cond['permission_item']    = $p_item;

        $this->db->where($cond);
        handle_custom_db_errors($this->db->error());
        return $this->db->delete('roles_permissions');
    }
}
