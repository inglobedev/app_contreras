<?php
/**
 * Pachi
 *
 * @package	CodeIgniter
 * @author	Chirino Pablo (pablo.chirino@gmail.com)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://peybol.com
 * @since	Version 1.0.0
 */
class Pachi extends CI_Model 
{
	public $DATABASE 		= DB_NAME;
	public $errors   		= array();
	public $allow_create 	= FALSE;

    public function insert($table, $columns)
    {
    	$table_data = $this->_analize_table($table);

    	if ($table_data == FALSE) {
    		$this->errors[] = "No se encuentra {$table}.";
    		return FALSE;
    	}

    	// Validamos que se esten insertando como minimo todas las columnas requeridas
    	$pending_columns = array_flip($table_data['required_columns_strict']);
    	foreach ($columns as $_column => $_value) {
    		unset($pending_columns[$_column]);
    	}

    	// Controlamos las columnas requeridas para insertar un nuevo registro
    	if (count($pending_columns)) {
    		foreach ($pending_columns as $_column => $_value)
    			$_column_names[] = $this->lang->line($_column);

    		$_column_names = implode(', ', $_column_names);
    		$this->errors[] = "Las siguientes datos son requeridos: {$_column_names}.";
    		return FALSE;
    	}

    	// Controlamos si habia columnas unique, que estas no esten repetidas.
    	$unique_columns = array_flip($table_data['unique_columns']);
    	$date_columns = array_flip($table_data['date_columns']);
    	foreach ($columns as $_column => $_value) 
    	{
    		if (in_array($_column, array_keys($unique_columns))) 
    		{
    			unset($search);
    			$search[$_column] = $_value;
    			$r = $this->fetch($table, $search, 0, 1);
    			if (count($r)) {
    				$this->errors[] = "'{$_value}' ya esta utilizado.";
    				return FALSE;
    			}
    		}

			if (in_array($_column, array_keys($date_columns))) 
    		{
    			$columns[$_column] = store_date($_value);
    		}
    	}

		$result = $this->db->insert($table, $columns);

		if ($result == TRUE) 
			return $this->db->insert_id();
		
		$this->errors[] = "Ocurrió un error guardando los datos.";
		return FALSE; 
    }

    public function get($table, $id_or_conditions, $extend_select = NULL)
    {
    	$table_data = $this->_analize_table($table);

    	if ($table_data == FALSE) {
    		$this->errors[] = "No se encuentra {$table}.";
    		return FALSE;
    	}

    	$extend_select = (!empty($extend_select)) ? ', ' . $extend_select : NULL;

		$this->db->select("{$table}.* {$extend_select}", FALSE);
		$this->db->from($table);
		
		if (is_array($id_or_conditions))
			$cond = $id_or_conditions;
		else
			$cond[$table_data['primary_key']] = (int)$id_or_conditions;

		$this->db->where($cond);
		$result = $this->db->get();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result[0];
		}

		return FALSE;
    }

    public function fetch($table, $filters = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE, $search_filter = FALSE, $group_by = FALSE, $extend_select = NULL)
    {
    	$table_data = $this->_analize_table($table);

    	if ($table_data == FALSE) {
    		$this->errors[] = "No se encuentra {$table}.";
    		return FALSE;
    	}

    	$extend_select = (!empty($extend_select)) ? ', ' . $extend_select : NULL;

		$cond = array();
		$this->db->select("SQL_CALC_FOUND_ROWS {$table}.* {$extend_select}", FALSE);
		$this->db->from($table);

		if (is_array($filters))
			$this->db->where($filters);

		if (is_string($filters))
			$this->db->where($table_data['primary_key'] . ' IN (' . $filters . ')', NULL);

		if (is_array($order_by))
		{
			foreach ($order_by as $order_column => $sort_order) {
				$this->db->order_by($order_column, $sort_order);
			}
		}

		if (!empty($search_filter)) 
		{
			if (count($table_data['text_columns']))
			{
				$this->db->group_start();
				foreach ($table_data['text_columns'] as $_nro => $_column) {
					if ($_nro == 0)
						$this->db->like($_column, $search_filter, 'both');
					else
		            	$this->db->or_like($_column, $search_filter, 'both');
				}
				$this->db->group_end();
			}
		}

		if ($page !== FALSE AND $page >= 0)
		{
			$offset = $page*$page_items;
			$this->db->limit($page_items, $offset);
		}
		
		$result 	= $this->db->get();
		$paginacion = $this->db->query('SELECT FOUND_ROWS() total_items')->result_array();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			foreach ($result as $key => $value) {
				$result[$key]['_total_results'] = $paginacion[0]['total_items'];
				$result[$key]['_page'] 			= $page;
				$result[$key]['_items_per_page']= $page_items;
			}

			if ($page == -1)
				return $result[0];

			return $result;
		}

		return array();
    }

    public function fetch_table($identifier, $table, $fixed_filters = FALSE, $group_by = FALSE, $extend_select = NULL, $fixed_order = FALSE)
    {
    	$_tbl = pachi_table_get($identifier, $default_page = 0, $default_items_per_page = 10);
    	$_filter = (is_array($fixed_filters)) ? array_merge($fixed_filters, $_tbl['f']) : $_tbl['f'];
    	$_tbl['o'] = (is_array($fixed_order)) ? array_merge($fixed_order, $_tbl['o']) : $_tbl['o'];
		return $this->fetch($table, $_filter, $_tbl['p'], $_tbl['ipp'], $_tbl['o'], $_tbl['s'], $group_by, $extend_select);
    }

    public function set($table, $id_or_conditions, $data)
    {
    	$table_data = $this->_analize_table($table);

    	if ($table_data == FALSE) {
    		$this->errors[] = "No se encuentra {$table}.";
    		return FALSE;
    	}

		if (is_array($id_or_conditions))
			$cond = $id_or_conditions;
		else
			$cond[$table_data['primary_key']] = (int)$id_or_conditions;

		$this->db->where($cond);
		$result = $this->db->update($table, $data);

		if ($result)
			return TRUE;

		return FALSE;
    }

    public function del($table, $id_or_conditions)
    {
    	$table_data = $this->_analize_table($table);

    	if ($table_data == FALSE) {
    		$this->errors[] = "No se encuentra {$table}.";
    		return FALSE;
    	}

		if (is_array($id_or_conditions))
			$cond = $id_or_conditions;
		else
			$cond[$table_data['primary_key']] = (int)$id_or_conditions;

		$this->db->where($cond);
		$result = $this->db->delete($table);

		if ($result)
			return TRUE;

		return FALSE;
    }

    /*
     * Generacion de Formularios
     */

	    public function get_form_data($table, $entity_id, $columns = FALSE)
	    {
	    	$table_data = $this->_analize_table($table);

	    	if ($table_data == FALSE) {
	    		$this->errors[] = "No se encuentra {$table}.";
	    		return FALSE;
	    	}

	   		$table_data = $this->_determine_columns($table_data, $columns);


	    	// Si es un form de edicion, cargamos los datos.
	   		$data['saved_data'] = ($entity_id) ? $this->get($table, $entity_id) : FALSE;
	   		$data['table_data'] = $table_data;

	   		// Determinamos el tipo de accion que se hace con el form. create, update, readonly
	   		$data['form_key']  = pachi_mask(json_encode($this->_determine_form($table_data, $entity_id, $data['saved_data'])));

	   		return $data;
	    }

	    public function _analize_table($table)
	    {
	    	$tables = $this->db->query('SELECT TABLE_NAME FROM information_schema.tables WHERE table_schema = ?', array($this->DATABASE))->result_array();
			$tables = array_column($tables, 'TABLE_NAME');

			// Si la tabla especificada no existe, cortamos.
			if (!in_array($table, $tables))
				return FALSE;

			// Columnas generales de las tablas.
			$columns 	= $this->db->query('SHOW FULL COLUMNS FROM ' . $this->DATABASE . '.' . $table)->result_array();

			// Individualizamos columnas claves.
			$table_data['table_name'] 		= $table;
			$table_data['required_columns_strict'] = array_column(array_filter($columns, function ($var) {return ($var['Null'] == 'NO' AND $var['Key'] != 'PRI' AND !not_empty($var['Default']));}), 'Field');
			$table_data['required_columns'] = array_column(array_filter($columns, function ($var) {return ($var['Null'] == 'NO' AND $var['Key'] != 'PRI');}), 'Field');
			$table_data['optional_columns'] = array_column(array_filter($columns, function ($var) {return ($var['Null'] == 'YES');}), 'Field');
			$table_data['unique_columns'] 	= array_column(array_filter($columns, function ($var) {return ($var['Key'] == 'UNI');}), 'Field');
			$table_data['text_columns'] 	= array_column(array_filter($columns, function ($var) {return (strpos($var['Type'], 'varchar') !== FALSE OR strpos($var['Type'], 'text') !== FALSE);}), 'Field');
			$table_data['all_columns'] 		= array_merge($table_data['required_columns'], $table_data['optional_columns']);
			$table_data['primary_key'] 		= array_column(array_filter($columns, function ($var) {return ($var['Key'] == 'PRI');}), 'Field');
			$table_data['primary_key'] 		= (isset($table_data['primary_key'][0])) ? $table_data['primary_key'][0] : FALSE;

			$table_data['date_columns'] 		= array_column(array_filter($columns, function ($var) {return ($var['Type'] == 'date');}), 'Field');

			// Todas las columnas con toda la data.
			$table_data['columns'] = $columns;
			
			return $table_data;
	    }

	    public function _determine_columns($table_data, $columns = FALSE)
	    {
	    	$columns = $this->_normalize_column_selection($columns);

			foreach ($table_data['columns'] as $_idx => $_column) 
			{
				// Si se paso la columna seleccionada

				if ($_column['Key'] == 'PRI') {
					$table_data['hidden_columns'] = $_column;
					unset($table_data['columns'][$_idx]);
					continue;
				}

				if ($defined = isset($columns[$_column['Field']]) OR empty($columns))
				{
					$_lenght 		= str_extract($_column['Type'], '\(', '\)');
					$_lenght_string = (isset($_lenght[0][0])) ? $_lenght[0][0] : FALSE;
					$meta['lenght'] = (isset($_lenght[1][0])) ? $_lenght[1][0] : FALSE;
					
					// Removemos el lenght si existiese.
					$meta['type']   	= str_replace($_lenght_string, NULL, $_column['Type']);

					// Removemos el unsigned si existiese.
					$meta['unsigned'] 	= (strpos($meta['type'], 'unsigned') !== FALSE) ? TRUE : FALSE;
					if ($meta['unsigned'])
						$meta['type'] 	= trim(str_replace('unsigned', NULL, $meta['type']));

					// Determinamos si esta columna esta relacionada a otra tabla.
					$meta['reference'] 	= ($_column['Key'] == 'MUL') ? $this->_determine_reference($table_data['table_name'], $_column['Field']) : FALSE;
					
					// Determinamos si es una columna obligatoria
					$meta['required'] 	= ($_column['Null'] == 'NO') ? TRUE : FALSE;
					
					// Valor default
					$meta['default'] 	= $_column['Default'];
					
					// Data name
					$meta['data_name'] 	= $_column['Field'];

					// Vemos si hay data custom.
					if ($defined AND is_array($columns[$_column['Field']]))
						$meta = array_merge_recursive ($meta, $columns[$_column['Field']]);

					// Determinamos el tipo de field adecuado para esta columna.
					$meta = $this->_determine_field($meta);
					
					$table_data['columns'][$_idx]['meta'] = $meta;
					unset($meta);
				}
				else
					unset($table_data['columns'][$_idx]);
			}

	    	return $table_data;
	    }

	    private function _normalize_column_selection($columns)
	    {
			// Si hay filtro de columnas, extraemos los nombres.
			$_columns = array();
			if (is_array($columns)) {
				foreach ($columns as $_column_name => $_column_data) {
					if (is_array($_column_data)) {
						$_columns[$_column_name] = $_column_data;
						unset($columns[$_column_name]);
					}
					else
						$_columns[$_column_data] = TRUE;
				}
			}
			return $_columns;
	    }

	    private function _determine_reference($table, $column)
	    {
	    	$query = "
	    		SELECT REFERENCED_TABLE_NAME 'table', REFERENCED_COLUMN_NAME 'column'
				FROM information_schema.KEY_COLUMN_USAGE
				WHERE CONSTRAINT_SCHEMA = '".DB_NAME."'
				AND TABLE_NAME 	= '{$table}'
				AND COLUMN_NAME = '{$column}';
			";

			$result = $this->db->query($query);

			if ($result->num_rows() > 0) {
				$result = $result->result_array();
				$result = $result[0];

				// Debemos obtener la columna por defecto a representar.

				$result['display'] = $this->_determine_display_column($result['table']);
			}
			else
				$result = FALSE;

			return $result;
	    }

	    private function _determine_display_column($table)
	    {
	    	$columns = $this->db->query('SHOW FULL COLUMNS FROM ' . $this->DATABASE . '.' . $table . ' WHERE 
	    		Field LIKE "%name%" OR 
	    		Field LIKE "%title%" OR
	    		Field LIKE "%description%" OR
	    		Field LIKE "%nombre%"
	    	')->result_array();

	    	$columns = array_column($columns, 'Field');

	    	return reset($columns);
	    }

	    private function _determine_field($field)
	    {
	    	// Se debe chequear que los meta a setear no esten previamente definidos, ya que podrian haberse pasados custmizados.
	    	switch ($field['type']) 
	    	{
	    		case 'varchar':
	    				// Se aproxima un attr_type a partir del nombre del campo.
	    			if (!isset($field['attr']['type'])) {
	    				if (strpos($field['data_name'], 'password'))
	    					$field['attr']['type'] = 'password';

	    				if (strpos($field['data_name'], 'mail'))
	    					$field['attr']['type'] = 'email';

	    				if (strpos($field['data_name'], 'phone')) {
	    					$field['attr']['type'] 	= 'text';
	    					$field['class'] 		= 'pachi-mask-phone';
	    				}

	    			}

	    			if (!isset($field['input_type'])) {
	    				// Si parece que es un tipo files.
						if (strpos($field['data_name'], 'attachment') OR strpos($field['data_name'], 'file')) {
							$field['input_type'] 	= 'files';

							if (strpos($field['data_name'], 'attachments') OR strpos($field['data_name'], 'files'))
								$field['maxlength'] = round($field['lenght'] / 10);
							else
								$field['maxlength'] = 1;
						}
						elseif (strpos($field['data_name'], 'picture') OR strpos($field['data_name'], 'image')) {
							$field['input_type'] 	= 'images';
							if (strpos($field['data_name'], 'pictures') OR strpos($field['data_name'], 'images'))
								$field['maxlength'] = round($field['lenght'] / 10);
							else
								$field['maxlength'] = 1;
						}
						else
						{
		    				$field['input_type'] 	= 'text';
		    				$field['maxlength'] 	= $field['lenght'];
						}
	    			}
	    			break;
	    		case 'tinytext':
	    		case 'text':
	    		case 'mediumtext':
	    		case 'longtext':
	    			if (!isset($field['input_type'])) {

						$_types['tinytext']			= '255';
						$_types['text']				= '65535';
						$_types['mediumtext']		= '16777215';
						$_types['longtext']			= '4294967295';

	    				$field['input_type'] 	= 'textarea';
	    				$field['maxlength'] 	= $_types[$field['type']];
	    			}
	    			break;
	    		case 'datetime':
	    			if (!isset($field['input_type'])) {
	    				$field['placeholder'] 	= $this->lang->line('pachi_lbl_pick_datetime');
		    			$field['input_type'] 	= 'text';
		    			$field['class'] 		= 'pachi-datetime-picker';
	    			}
	    			break;
	    		case 'date':
	    			if (!isset($field['input_type'])) {
	    				$field['placeholder'] 	= $this->lang->line('pachi_lbl_pick_date');
		    			$field['input_type'] 	= 'text';
		    			$field['class'] 		= 'pachi-date-picker';
	    			}
	    			break;
	    		case 'year':
	    			if (!isset($field['input_type'])) {
	    				$field['placeholder'] 	= $this->lang->line('pachi_lbl_pick_year');
		    			$field['input_type'] 	= 'text';
		    			$field['maxlength']		= 4;
		    			$field['class'] 		= 'pachi-mask-number';
	    			}
	    			break;
	    		case 'month':
	    			if (!isset($field['input_type'])) {
	    				$field['placeholder'] 	= $this->lang->line('pachi_lbl_pick_month');
		    			$field['input_type'] 	= 'text';
		    			$field['class'] 		= 'pachi-month-picker';
	    			}
	    			break;
	    		case 'bigint':
	    		case 'int':
	    		case 'mediumint':
	    		case 'mediumint':
	    		case 'smallint':
	    		case 'tinyint':
	    			if (!isset($field['input_type'])) {

						$_types['tinyint']['min']			= '-128';
						$_types['tinyint']['max']			= '128';
						$_types['tinyint']['unsigned']		= '255';
						$_types['smallint']['min']			= '-32768';
						$_types['smallint']['max']			= '32768';
						$_types['smallint']['unsigned']		= '65535';
						$_types['mediumint']['min']			= '-8388608';
						$_types['mediumint']['max']			= '8388608';
						$_types['mediumint']['unsigned']	= '16777215';
						$_types['int']['min']				= '-2147483648';
						$_types['int']['max']				= '2147483648';
						$_types['int']['unsigned']			= '4294967295';
						$_types['bigint']['min']			= '-9223372036854775808';
						$_types['bigint']['max']			= '9223372036854775808';
						$_types['bigint']['unsigned']		= '18446744073709551615';

						if ($field['unsigned']) 
						{
							$field['data']['min']		= 0;
							$field['data']['max']		= $_types[$field['type']]['unsigned'];
							$field['maxlength'] 		= strlen($_types[$field['type']]['unsigned']);
						}
						else
						{
							$field['data']['min']		= $_types[$field['type']]['min'];
							$field['data']['max']		= $_types[$field['type']]['max'];
							$field['maxlength'] 		= strlen($_types[$field['type']]['max']) + 1;
						}


		    			// Es numerico o es una referencia?
		    			if ($field['reference'] != FALSE)
		    			{
		    				$field['placeholder'] 			= $this->lang->line('pachi_lbl_search') . ' ' . $this->lang->line('entity_' . $field['reference']['table']);
			    			$field['input_type'] 			= 'select';
			    			$field['data']['pachi-search'] 	= pachi_search($field['reference']['table'], $field['reference']['column'], $field['reference']['display'], @$field['reference']['filter'], @$field['reference']['custom_display']);

							if ($this->allow_create OR isset($field['modal'])) {
								if (!isset($field['modal']['modal_title']))
									$field['modal']['modal_title'] 	= $this->lang->line('pachi_lbl_new') . ' ' . $this->lang->line('entity_' . $field['reference']['table']);
								if (!isset($field['modal']['button_title']))
									$field['modal']['button_title'] = $this->lang->line('pachi_lbl_new') . ' ' . $this->lang->line('entity_' . $field['reference']['table']);
								if (!isset($field['modal']['table']))
									$field['modal']['table'] 		= $field['reference']['table'];
								if (!isset($field['modal']['columns']))
									$field['modal']['columns'] 		= FALSE;
								if (!isset($field['modal']['entity_id']))
									$field['modal']['entity_id'] 	= FALSE;
								if (!isset($field['modal']['_data']))
									$field['modal']['_data'] 		= array();

								$field['modal']['key'] = pachi_modal($field['modal']['table'], $field['modal']['columns'], $field['modal']['entity_id'],$field['modal']['_data'], $field['modal']['modal_title']);
							}
		    			}
		    			else
		    			{
		    				if ($field['lenght'] == 1 AND $field['type'] == 'tinyint')
		    				{
				    			$field['input_type'] 	= 'checkbox';
		    				}
		    				else
		    				{
				    			$field['input_type'] 	= 'text';
				    			$field['class'] 		= 'pachi-mask-number';
		    				}
		    			}
	    			}
	    			break;
	    		default:
	    			if (!isset($field['input_type'])) {
	    				$field['input_type'] 	= 'text';
	    			}
	    			break;
	    	}

	    	return $field;
	    }

	    private function _determine_form($table_data, $entity_id, $saved_data)
	    {
	    	$form_data['table'] = $table_data['table_name'];

	    	// Si no se especifico id, es un formulario de creacion.
	    	if (empty($entity_id))
	    		$form_data['method'] = 'create';
	    	// Si se especifico un id y hay cosas guardadas.
	    	elseif (is_array($saved_data))
	    		$form_data['method'] = 'update';
	    	// Sino es un error.
	    	else 
	    		$form_data['method'] = 'error';

	    	$form_data['id'] = $entity_id;

			return $form_data;    	
	    }

    /*
     * Generacion de Tablas
     */

	    public function _analize_dataset($dataset)
	    {
	    	$table['body'] 			= array();
	    	$table['header'] 		= array();
	    	$table['pagination'] 	= array();

	    	// Determinar las columnas a partir de todas las columnas
	    	if (!isset($dataset[0]))
	    		return $table;

	    	$_sample = $dataset[0];

			
	    	foreach ($_sample as $_column => $_value) 
	    	{
	    		if (in_array($_column, array('_total_results', '_page', '_items_per_page')))
	    			$table['pagination'][$_column] = $_value;
	    		else
	    			$table['header'][] = $_column;
	    	}

	    	foreach ($dataset as $_key => $_item) 
	    	{
	    		unset($_item['_total_results']);
	    		unset($_item['_page']);
	    		unset($_item['_items_per_page']);
	    		$table['body'][] = $_item;
	    	}

	    	// Acomodamos los header.
	    	$table['header'] = array_flip($table['header']);

			return $table;
	    }

}


