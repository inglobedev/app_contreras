<?php 
class M_accounts extends CI_Model 
{

	public function __construct() 
	{

	}

	/*
	 * Retorna el numero de cuenta de un usuario.
	 * Si el usuario no tiene cuenta, crea una y retorna ese numero.
	 */
	public function get_user_account($id_user)
	{
		$account_filter['id_user_owner'] = $id_user;

		$account = $this->pachi->fetch('accounts', $account_filter, -1, 1);

		if (empty($account))
		{
			// Creamos una cuenta.
			unset($account);
			$account['id_user_owner'] 	= $id_user;
			$account['account_name'] 	= get_user_name($id_user);
			$account['account_balance'] = 0;

			return $this->pachi->insert('accounts', $account);
		}

		return $account['id_account'];
	}

}