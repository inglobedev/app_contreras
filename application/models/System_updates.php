<?php
class System_updates extends CI_Model 
{
	public $AU_FOLDER = 'autoupdate';

	/**
	 * Tabla: au 
	 * Columnas Requeridas: database_version, comment
	 * Columnas Opcionales: updated
	 * Opcionales:
	 * @column 	updated                       	datetime       	
	 */

	public function __construct() 
	{
		if (!defined('DATABASE_VERSION'))
			return FALSE;
		
		// La tabla de versionado existe?
		$au_installed = $this->db->table_exists('system_updates');
		if (!$au_installed) {
			return $this->_setup();
		}

		// Obtenemos la ultima version aplicada.
		$current_version = $this->get_updates(FALSE, FALSE, 0, 1, array('id_update' => 'DESC'));
		if (empty($current_version)) 
		{
			// Si no se encuentra version, se procede con el setup.
			return $this->_setup();
		}
		else
		{
			// Si hay una version registrada, la vaidamos contra la version de codigo.
			$last_version = $current_version[0]['database_version'];

			if ($last_version < DATABASE_VERSION)
				return $this->_run_update($last_version);
		}
		
	}

	private function _setup()
	{
		// Crear modelo inicial.
			$this->_excecute_file(FCPATH . DIRECTORY_SEPARATOR . $this->AU_FOLDER . DIRECTORY_SEPARATOR . 'setup.sql');
			$this->new_update('00.00.00', 'setup');


		// Aplicamos las actualizaciones.
			return $this->_run_update(0);
	}

	/**
	 * Conjunto de instrucciones para actualizacion.
	 *
	 * @param      string  $last_version 	Ultima version impactada.
	 *
	 * @return     void
	 */
	private function _run_update($last_version = FALSE)
	{
		// Impactar todas las actualizaciones desde last version.
        $updates = glob(FCPATH .  $this->AU_FOLDER . DIRECTORY_SEPARATOR . 'updates' . DIRECTORY_SEPARATOR . '*.sql');

        // Limpiamos las rutas, porque glob nos da la ruta completa del directorio.
        foreach ($updates as $key => $update) 
        {
        	// Tomamos el nombre del archivo.
            $fname = explode(DIRECTORY_SEPARATOR, $update);
            if (isset($fname[1]))
                $file_name  = $fname[count($fname) - 1];
            else
            	continue;

            // Apartamos los datos de version: <version> _...
            $version = strstr($file_name, '_', TRUE);

            // Apartamos el comentario: ..._<comentario>.sql

            if ($version <= DATABASE_VERSION AND $version > $last_version) {
	            
	            $comment 		= explode('.', strstr($file_name, '_'));
	            $comment 		= substr($comment[0], 1);

				// Lo ejecutamos
				$r = $this->_excecute_file($update);
				if ($r) {
					$this->new_update($version, $comment);
				}
            }
        }
	}

	/**
	 * Ejecuta linea a linea un archivo SQL
	 *
	 * @param      string  $file 	Ruta del archivo.
	 *
	 * @return     void
	 */
	private function _excecute_file($file)
	{
		if (!file_exists($file))
			return FALSE;

		// Cargamos el archivo a importar
		$au_setup = file_get_contents($file);
		
		// Limpiamos los comentarios, y lo separamos por instrucciones.
		$au_setup = explode(';', $this->_clean_comments($au_setup));
		#$au_setup = preg_split("/(;|$$)/", $this->_clean_comments($au_setup));

		// Ejecutamos cada instruccion individualmente
		foreach ($au_setup as $instruction) {
			// Por si se pasa algun espacio o algo. Dudo que haya instrucciones de menos de 3 caracteres.
			if (strlen(trim($instruction)) > 5 AND !empty($instruction))
				$this->db->query($instruction . ';');
		}

		return TRUE;
	}

	/**
	 * Remueve comentarios de TSQL.
	 *
	 * @param       string    	$stream
	 *
	 * @return      string 	Contenido de dump de mysql.
	 * @return      string 	Contenido de dump de mysql sin comentarios.
	 */
	private function _clean_comments($stream)
	{
	    $RXSQLComments = '@(--[^\r\n]*)|(/\*[\w\W]*?(?=\*/)\*/)@ms';
	    return (($stream == '') ?  '' : preg_replace( $RXSQLComments, '', $stream ));
	}

	/**
	 * Crea un registro de version
	 *
	 * @param       varchar(10)    	$database_version                  	
	 * @param       smallint(6)    	$hotfix                       	
	 * @param       varchar(50)    	$comment                      	
	 *
	 * @return      int 	En caso de exito retorna el ID del registro insertado.
	 * @return      bool 	En caso de error retorna FALSE.
	 */
	public function new_update($database_version, $comment)
	{
		$data['database_version'] 	= $database_version;
		$data['comment'] 			= $comment;
		$data['code_version'] 		= APP_VERSION;

		$result = $this->db->insert('system_updates', $data);

		if ($result == TRUE) 
			return $this->db->insert_id();
		
		return FALSE; 
	}

	/**
	 * Elimina uno o mas registros de version
	 *
	 * @param 	$id_update          	ID del registro a eliminar. Array de IDs. IDs separados por coma.
	 * @param 	$limit              	Cantidad maxima de registros a eliminar.
	 *
	 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
	 */
	public function del_update($id_update = FALSE, $limit = 1)
	{
		if ($id_update === FALSE OR empty($id_update)) 
			return FALSE;

		if (is_array($id_update))
		{
			foreach ($id_update as $key => $value) {
				if (strpos($key, '.') === FALSE AND strpos($key, '(') === FALSE)
					$cond['system_updates.'.$key] = $value;
				else
					$cond[$key] = $value;
			}
		}
		else
			$cond['id_update'] = $id_update;

		$this->db->where($cond);
		
		if ($limit > 0)
			$this->db->limit($limit);

		$result = $this->db->delete('system_updates');

		return $result;
	}

	/**
	 * Retorna un version	 
	 *
	 * @param 	$id_update          	PK.
	 *
	 * @return	array	En caso de existir el registro retorna un array donde cada key es una columna.
	 * @return	bool	En caso de no existir el registro retorna FALSE.
	 */
	public function get_update($id_update = FALSE)
	{
		if ($id_update === FALSE OR empty($id_update)) 
			return FALSE;

		$cond['id_update'] = $id_update;

		$this->db->where($cond);
		$result = $this->db->get('system_updates');

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result[0];
		}

		return FALSE;
	}

	/**
	 * Actualiza una o mas columnas de un version	 
	 *
	 * @param 	$id_update          	PK.
	 * @param 	$filter_column      	Columna a actualizar. Ó Array con columnas=valores a setear.
	 * @param 	$filter_value       	Valor a setear, o FALSE si filter_column es un array.
	 *
	 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
	 */
	public function set_update($id_update, $filter_column = FALSE, $filter_value = FALSE)
	{
		$data = array();

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				if (strpos($key, '.') === FALSE)
					$data['system_updates.'.$key] = $value;
				else
					$data[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				if (strpos($filter_column, '.') === FALSE)
					$data['system_updates.'.$filter_column] = $filter_value;
				else
					$data[$filter_column] = $filter_value;

		// En caso de que se proporcione un array de IDs
		if (is_array($id_update)) {
			$ids = implode(',', $id_update);
			$cond['id_update IN ('.$ids.')'] = FALSE;
		}
		// En caso de que se proporcionen IDs separados por coma.
		elseif (strpos($id_update, ',') === FALSE)
			$cond['id_update IN ('.$id_update.')'] = FALSE;
		else
			$cond['id_update'] = $id_update;

		$this->db->where($cond);

		$result = $this->db->update('system_updates', $data);

		return $result;
	}

	/**
	 * Retorna una coleccion de versions	 
	 *
	 * @param 	$id_update          	PK.
	 * @param 	$filter_column      	Columna a comparar. Ó Array con columnas=valores a buscar.
	 * @param 	$filter_value       	Valor buscado, o FALSE si filter_column es un array.
	 * @param 	$page               	Pagina desde la cual se busca.
	 * @param 	$page_items         	Cantidad de items de una pagina.
	 * @param 	$filter_column      	Array con columnas=orden.
	 * @param 	$term_filter        	Cadena de texto a buscar en columnas de texto o varchar.
	 *
	 * @return	bool	En caso de error retorna FALSE. En caso de exito TRUE.
	 */
	public function get_updates($filter_column = FALSE, $filter_value = FALSE, $page = FALSE, $page_items = 20, $order_by = FALSE)
	{
		$cond = array();

		$this->db->select('system_updates.*', FALSE);
		$this->db->from('system_updates');

		if (is_array($filter_column))
		{
			foreach ($filter_column as $key => $value) {
				if (strpos($key, '.') === FALSE)
					$cond['system_updates.'.$key] = $value;
				else
					$cond[$key] = $value;
			}
		}
		else
			if ($filter_column !== FALSE AND $filter_value !== FALSE)
				if (strpos($filter_column, '.') === FALSE AND strpos($filter_column, '(') === FALSE)
					$cond['system_updates.'.$filter_column] = $filter_value;
				else
					$cond[$filter_column] = $filter_value;

		if (count($cond) > 0)
			$this->db->where($cond);

		if (is_array($order_by))
		{
			foreach ($order_by as $order_column => $sort_order) {
				$this->db->order_by($order_column, $sort_order);
			}
		}

		if ($page !== FALSE)
		{
			$offset = $page*$page_items;
			$this->db->limit($page_items, $offset);
		}
		
		$result = $this->db->get();

		if ($result->num_rows() > 0)
		{
			$result = $result->result_array();
			return $result;
		}

		return array();
	}

}
