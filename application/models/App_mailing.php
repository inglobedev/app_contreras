<?php
class App_mailing extends CI_Model 
{
    public $template = 'default';
    public $debugger = FALSE;

    public function __construct() 
    {

    }

    // Envio del email de bienvenida.
    public function welcome($id_user = FALSE, $tmp_pwd = FALSE)
    {
        $id_user = ($id_user) ? (int)$id_user : $this->session->userdata('id_user');
        if (empty($id_user)) return FALSE;

        // Obtenemos los datos basicos del usuario, para armarle el email.
        $data['user'] = $this->app_users->get_user($id_user);

        $email       = $data['user']['user_email'];
        $subject        = 'Bienvenido a '.APP_TITLE;
        
        $message['title']       = 'Bienvenido a '.APP_TITLE;
        $message['message']     = 'Se ha dado de alta tu cuenta en '.APP_TITLE.'.';

        if ($tmp_pwd != FALSE) {
          $message['message'] .= "<br><br>Tu clave de acceso temporal es '$tmp_pwd' (sin las comillas). La misma es valida hasta las 23:59 de hoy, pasada esa hora utiliza la opción de recueprar clave.";
        }
        
        $message['target_url']  = base_url();
        $message['client_name'] = $data['user']['user_name'];
        $content = $this->load->view('emails/'.$this->template.'/general', $message, TRUE);

        $this->send($email, $subject, $content, 'WELCOME');
    }

    // Enviamos el email con la clave temporal para resetear la cuenta
    public function new_temporal_password($id_user, $tmp_pwd)
    {
        if (empty($id_user)) return FALSE;

        // Obtenemos los datos basicos del usuario, para armarle el email.
        $data['user']       = $this->app_users->get_user($id_user);
        $data['password']   = $tmp_pwd;

        $email          = $data['user']['user_email'];
        $subject        = 'Recuperación de clave - '.APP_TITLE;
        
        $message['title']       = 'Recuperación de clave - '.APP_TITLE;
        $message['message']     = 'Tu clave de acceso temporal es: "'.$tmp_pwd.'" (sin las comillas). La misma es valida hasta las 23:59 de hoy.';
        $message['target_url']  = base_url().APP_LOGIN;
        $message['client_name'] = $data['user']['user_name'];
        $content = $this->load->view('emails/'.$this->template.'/general', $message, TRUE);

        $this->send($email, $subject, $content, 'NPSW');
    }

    // Notificación generica.
    public function notification($nf)
    {
        $nf     = (is_array($nf)) ? $nf : $this->app_notifications->get_notification($nf);
        $user   = $this->app_users->get_user($nf['id_user']);

        if (!empty($nf['notif_url']))
            $nf['notif_url'] = (strpos($nf['notif_url'], '?') !== FALSE) ? base_url($nf['notif_url'].'&nf='.$nf['notif_token']) : base_url($nf['notif_url'].'?nf='.$nf['notif_token']);

        // Variables para el cuerpo del mail.
        $message['notification']    = $nf;
        $message['target_url']      = (empty($nf['notif_url'])) ? base_url() : $nf['notif_url'];
        $message['client_name']     = $user['user_name'];
        $message['title']           = $nf['notif_title'];
        $message['message']         = $nf['notif_content'];

        // Datos para el envio del mail.
        $content    = $this->load->view('emails/'.$this->template.'/notification', $message, TRUE);
        $email      = $user['user_email'];
        $subject    = APP_TITLE.' - '.$nf['notif_title'];

        return $this->send($email, $subject, $content, 'NOTIFICATION');
    }

    // Funcion general que canaliza el despacho de email.
    public function send($email, $subject, $content, $on_debug = 'DFLT')
    {
        $config = $this->app_settings->get_saved_config('mailing');

        if ($config['enable'] == 1) 
        {
            if ($config['enable_smtp']) 
            {
                $mailing['protocol']  = 'smtp';
                $mailing['smtp_host'] = $config['smtp_host'];
                $mailing['smtp_port'] = $config['smtp_port'];
                $mailing['smtp_user'] = $config['smtp_user'];
                $mailing['smtp_pass'] = $config['smtp_pass'];
                $mailing['mailtype']  = 'html';
                $mailing['charset']   = 'iso-8859-1';
                $this->load->library('email', $mailing);
                $this->email->set_newline("\r\n");
            }
            else
                $this->load->library('email');

            $this->email->from($config['address'], $config['name']);
            $this->email->to($email);
            $this->email->subject(mb_convert_encoding($subject, "UTF-8"));        
            $this->email->message(mb_convert_encoding($content, "UTF-8"));
            $this->email->set_mailtype("html");
            

            if ($this->debugger == TRUE) {
                $result = $this->email->send(); 
                error_reporting(-1);
                ini_set('display_errors', 1);
                echo 'Se intentó enviar a: '.$email.' <br>';
                echo 'Nota: Si intentaste enviar a traves de SMTP con google, posiblemente recibas un alerta de Google para que habilites aplicaciones inseguras en tu cuenta: "Revisión de un intento de acceso bloqueado"<br>';
                echo 'LOG: (si esta vacio todo salio "OK")<br>';
                echo ' ------------- <br>';
                $this->email->print_debugger(array('headers'));
                echo ' ------------- <br>';
            }
            else
                $result = @$this->email->send(); 
        }

        // En entorno de desarrollo guaradmos los mails
        if (ENVIRONMENT == 'development') 
        {
            $log_path = FCPATH.'devs/debug/emails/';
            $filename = date('YmdHis').'-'.$on_debug.'-'.$email.'.htm';
            $result = @file_put_contents($log_path.$filename, $content);
        }

        return $result;
    }

}