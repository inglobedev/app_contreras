<?php $_table_dat 			= pachi_table_get($table_key); ?>
<?php $_custom_identifiers 	= (isset($options['custom_identifiers'])) ? $options['custom_identifiers'] : NULL ?>

<div class="row">
	<div class="col-xs-12">
		<?php 
			$options['enable_search'] 	= (isset($options['enable_search'])) ? $options['enable_search'] : TRUE;
			$options['enable_ipp'] 		= (isset($options['enable_ipp'])) ? $options['enable_ipp'] : TRUE;
			$options['filters'] 		= (isset($options['filters'])) ? $options['filters'] : array();
		?>
		<?php if ($options['enable_search'] OR $options['enable_ipp'] OR count($options['filters'])): ?>
		<div class="marginB" style="min-height: 30px;">
			<?php // Boton de nuevo. ?>
			<?php if (isset($options['create_entity']) AND (is_array($options['create_entity']) OR $options['create_entity'] === TRUE)): ?>
				<?php $_button_title = (empty($options['create_entity']['button_title'])) ? $this->lang->line('pachi_lbl_new') . ' ' . $this->lang->line('entity_' . $table) : $options['create_entity']['button_title'] ?>
				<?php if (!empty($options['create_entity']['target'])): ?>
					<a class="btn btn-sm btn-primary" href="<?php echo $options['create_entity']['target'] ?>"> <?php echo $_button_title ?></a>	
				<?php else: ?>
					<?php 
						$_data = $options['create_entity'];

						if (!isset($options['create_entity']['save_action'])) {
							$_data['save_action']['action'] 	= 'reload';
							$_data['save_action']['delay'] 		= 500;
						}
						else
						 	$_data['save_action'] = $options['create_entity']['save_action'];

						$_columns = (!empty($options['create_entity']['columns'])) ? $options['create_entity']['columns'] : FALSE;
						$_table   = (isset($options['create_entity']['table'])) ? $options['create_entity']['table'] : $table;

					?>
					<a class="btn btn-sm btn-primary" href="javascript:;" data-pachi-modal="<?php echo pachi_modal($_table, $_columns, FALSE, $_data, $_button_title) ?>"> <?php echo $_button_title ?></a>	
				<?php endif ?>
			<?php endif ?>
			
			<?php // Buscador. ?>
			<div class="pull-right" style="display: -webkit-box;">
				<?php if (!isset($options['enable_search']) OR $options['enable_search'] == TRUE): ?>
				<form class="pachi_table_search">
					<div class="input-group input-group-sm marginR">
						<?php if (!empty($_table_dat['s'])): ?>
				        <span class="input-group-btn">
				            <button class="btn btn-danger pachi_table_clean_search" type="button"><i class="fa fa-times"></i></button>
				        </span>
						<?php endif ?>
				        <input type="text" class="form-control" name="search" value="<?php echo $_table_dat['s'] ?>" placeholder="<?php echo $this->lang->line('pachi_lbl_search') ?> ...">
				        <span class="input-group-btn">
				            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
				        </span>
				    </div>
					<input type="hidden" name="url" value="<?php echo current_url() . $_custom_identifiers ?>">
					<input type="hidden" name="args" value="<?php echo $_SERVER['QUERY_STRING'] ?>">
					<input type="hidden" name="key" value="<?php echo $table_key ?>">
				</form>
				<?php endif ?>
                <?php if (is_array($options['filters']) AND count($options['filters'])): ?>
					<div class="input-group input-group-sm marginR">
						<?php // TODO: implementar mt-multiselect ?>
	                    <select class="form-control redirect-to-option-value">
	                    	<option value="<?php echo current_url() . generate_get_string_2(pachi_table_set($table_key, 0 , $_table_dat['ipp'], array(), $_table_dat['o'], $_table_dat['s'])) . $_custom_identifiers ?>">Todo</option>
	                    	<?php foreach ($options['filters'] as $_filter => $_label): ?>
	                    		<option <?php if (in_array($_filter, array_keys($_table_dat['f']))) echo 'selected' ?> value="<?php echo current_url() . generate_get_string_2(pachi_table_set($table_key, 0 , $_table_dat['ipp'], array($_filter => NULL), $_table_dat['o'], $_table_dat['s'])) . $_custom_identifiers ?>"><?php echo $_label ?></option>
	                    	<?php endforeach ?>
	                    </select>
	                </div>
                <?php endif ?>
                <?php if (!isset($options['enable_ipp']) OR $options['enable_ipp'] == TRUE): ?>
				<div class="input-group input-group-sm">
                    <select class="form-control redirect-to-option-value">
                        <option <?php if ($_table_dat['ipp'] == 15) echo 'selected' ?> value="<?php echo current_url() . generate_get_string_2(pachi_table_set($table_key, 0 , 15, $_table_dat['f'], $_table_dat['o'], $_table_dat['s'])) . $_custom_identifiers ?>">15 elm. por página</option>
                        <option <?php if ($_table_dat['ipp'] == 25) echo 'selected' ?> value="<?php echo current_url() . generate_get_string_2(pachi_table_set($table_key, 0 , 25, $_table_dat['f'], $_table_dat['o'], $_table_dat['s'])) . $_custom_identifiers ?>">25 elm. por página</option>
                        <option <?php if ($_table_dat['ipp'] == 50) echo 'selected' ?> value="<?php echo current_url() . generate_get_string_2(pachi_table_set($table_key, 0 , 50, $_table_dat['f'], $_table_dat['o'], $_table_dat['s'])) . $_custom_identifiers ?>">50 elm. por página</option>
                        <option <?php if ($_table_dat['ipp'] == 100) echo 'selected' ?> value="<?php echo current_url() . generate_get_string_2(pachi_table_set($table_key, 0 , 100, $_table_dat['f'], $_table_dat['o'], $_table_dat['s'])) . $_custom_identifiers ?>">100 elm. por página</option>
                        <option <?php if ($_table_dat['ipp'] == 250) echo 'selected' ?> value="<?php echo current_url() . generate_get_string_2(pachi_table_set($table_key, 0 , 250, $_table_dat['f'], $_table_dat['o'], $_table_dat['s'])) . $_custom_identifiers ?>">250 elm. por página</option>
                    </select>
                </div>
                <?php endif ?>
			</div>
		</div>
		<?php endif ?>
	</div>
</div>

<?php #debugger($_table_dat) ?>
<?php #debugger($options) ?>