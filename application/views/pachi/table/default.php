<?php #debugger($table); ?>
<?php 
/*
	// Custom column options:
	title: remplaza el title.
	custom_field: indicar que valor se va a mostrar en esta columna
	custom_render: pasar un string/html para insertar en esta columna, se remplazaran los {variables} por valores de la row.
*/
?>
<?php if (!empty($table['pagination'])): ?>
	<?php $_pages 		= ceil($table['pagination']['_total_results'] / $table['pagination']['_items_per_page']) ?>
	<?php $_table_key 	= $table_key; ?>
	<?php $_table_dat 	= pachi_table_get($_table_key, $table['pagination']['_page'], $table['pagination']['_items_per_page']); ?>
	<?php $_next_page 	= generate_get_string_2(pachi_table_set($_table_key, $_table_dat['p'] +1 , $_table_dat['ipp'], $_table_dat['f'], $_table_dat['o'], $_table_dat['s'])) ?>
	<?php $_prev_page 	= generate_get_string_2(pachi_table_set($_table_key, $_table_dat['p'] -1 , $_table_dat['ipp'], $_table_dat['f'], $_table_dat['o'], $_table_dat['s'])) ?>
<?php endif ?>

<div class="table-responsive">
	<table class="table <?php echo $class ?>" id="<?php echo $table_key ?>">
		<thead>
			<tr>
				<?php foreach ($table['header'] as $_column => $_options): ?>
					<?php if (isset($_options['hide']) AND $_options['hide'] == TRUE) continue; ?>
					<?php 
						$_sortable 		= (isset($_options['sortable']) AND !empty($table['pagination']));
					?>
					<th <?php if (isset($_options['width'])) echo "width='{$_options['width']}'" ?> <?php if (isset($_options['class'])) echo "class='{$_options['class']}'" ?>>
						<?php if ($_sortable): ?>
							<?php 
								$_sort_column 	= ($_options['sortable'] === TRUE) ? $_column : $_options['sortable'];
								$_order = $_table_dat['o'];
								// Si no esta sorteado, sorteamos asc.
								if (!isset($_table_dat['o'][$_sort_column]))
									$_order[$_sort_column] = 'ASC';
								// Si esta asc, sorteamos desc.
								elseif ($_table_dat['o'][$_sort_column] == 'ASC')
									$_order[$_sort_column] = 'DESC';
								// Si esta desc, quitamos.
								else
									unset($_order[$_sort_column]);
							?>
							<a href="<?php echo current_url().generate_get_string_2(pachi_table_set($_table_key, $_table_dat['p'] , $_table_dat['ipp'], $_table_dat['f'], $_order, $_table_dat['s'])) ?>">
								<?php echo (isset($_options['title'])) ? $_options['title'] : $this->lang->line($_column) ?>

								<?php if (isset($_table_dat['o'][$_sort_column])): ?>
									<?php if ($_table_dat['o'][$_sort_column] == 'DESC'): ?>
										<i class="fa fa-sort-desc pull-right"></i>
									<?php endif ?>
									<?php if ($_table_dat['o'][$_sort_column] == 'ASC'): ?>
										<i class="fa fa-sort-asc pull-right"></i>
									<?php endif ?>
								<?php endif ?>
							</a>
							<?php unset($_order); ?>
						<?php else: ?>
							<?php echo (isset($_options['title'])) ? $_options['title'] : $this->lang->line($_column) ?>
						<?php endif ?>
					</th>					
				<?php endforeach ?>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($table['body'] as $_row => $_values): ?>
				<tr>
					<?php foreach ($table['header'] as $_column => $_options): ?>
						<?php if (isset($_options['hide']) AND $_options['hide'] == TRUE) continue; ?>
						<td <?php if (isset($_options['class'])) echo "class='{$_options['class']}'" ?>>
							<?php if (isset($_options['custom_render'])): ?>
								<?php echo $this->parser->parse_string($_options['custom_render'], $_values, TRUE); ?>
							<?php else: ?>
								<?php if (array_key_exists($_column, $_values)): ?>
									<?php if (isset($_options['type'])): ?>
										<?php 
											switch ($_options['type']) {
												case 'money':
													echo "$ " . number_format($_values[$_column], 2);
													break;
												default:
													echo $_values[$_column];
													break;
											}
										?>
									<?php else: ?>
										<?php echo $_values[$_column] ?>
									<?php endif ?>
								<?php else: ?>
									<?php echo $this->lang->line('pachi_undefined'); ?>
								<?php endif ?>
							<?php endif ?>
						</td>					
					<?php endforeach ?>
				</tr>
			<?php endforeach ?>
			<?php if (empty($table['body'])): ?>
				<tr>
					<td colspan="100%" class="text-center"><?php echo $this->lang->line('pachi_no_records') ?></td>
				</tr>
			<?php endif ?>
		</tbody>
	</table>
</div>

<?php if (!empty($table['pagination'])): ?>
	<?php if ($_pages > 1): ?>
		<div class="text-center">
			<ul class="pagination pagination-sm">
				<li>
					<a class="<?php if ($_table_dat['p'] == 0) echo 'bg-grey' ?>" href="<?php if ($_table_dat['p'] == 0) echo 'javascript:;'; else echo current_url().$_prev_page ?>">
						<i class="fa fa-angle-left"></i>
					</a>
				</li>
				<?php $pagination_start = ($_table_dat['p'] - 5 > 0) ? ($_table_dat['p'] - 5) : 0; ?>
				<?php $pagination_end 	= ($pagination_start + 10 < $_pages) ? $pagination_start + 10 : $_pages; ?>
				<?php for ($i = $pagination_start; $i < $pagination_end; $i++) { ?>
					<li class="<?php if ($_table_dat['p'] == $i) echo 'active' ?>">
						<a href="<?php echo current_url().generate_get_string_2(pachi_table_set($_table_key, $i , $_table_dat['ipp'], $_table_dat['f'], $_table_dat['o'], $_table_dat['s'])) ?>">
							<?php echo $i + 1 ?> 
						</a>
					</li>
				<?php } ?>
				<li>
					<a class="<?php if ($_table_dat['p'] >= $_pages - 1) echo 'bg-grey' ?>" href="<?php if ($_table_dat['p'] >= $_pages - 1) echo 'javascript:;'; else echo current_url().$_next_page ?>">
						<i class="fa fa-angle-right"></i>
					</a>
				</li>
			</ul>
		</div>
	<?php endif ?>
<?php endif ?>