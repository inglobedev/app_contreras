<?php 
	$__id = (!empty($ext['id'])) ? $ext['id'] : uniqid(pachi_mask($table_data['table_name']));
	$__class = (!empty($ext['class'])) ? $ext['class'] : 'pachi_frm';
	$__custom_target = (!empty($ext['custom_target'])) ? $ext['custom_target'] : NULL;
?>
<form role="form" id="<?php echo $__id ?>" class="<?php echo $__class ?>" autocomplete="off" data-target="<?php echo $__custom_target ?>">
    <div class="form-body">
        <?php $_rand_form = rand(1,5000) * 100; ?>
        <?php foreach ($table_data['columns'] as $_kcolumn => $_column): ?>
        	
        	<?php $_column['_index'] = $_rand_form + $_kcolumn; ?>

        	<?php $this->load->view("pachi/form/default/{$_column["meta"]["input_type"]}", $_column) ?>

        <?php endforeach ?>
    </div>
    <div class="form-actions">
    	<input type="hidden" name="form" value="<?php echo $form_key ?>">
    	<input type="hidden" name="meta" value="<?php echo pachi_mask(json_encode($ext)) ?>">
        <button type="submit" class="btn blue"><?php echo $this->lang->line('pachi_btn_save') ?></button>
        <?php if (isset($ext['cancel'])): ?>
            <a href="<?php echo $ext['cancel'] ?>" class="btn default"><?php echo $this->lang->line('pachi_btn_cancel') ?></a>
        <?php endif ?>
    </div>
</form>