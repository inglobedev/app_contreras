<?php 
	$__required 	= ($meta['required']) ? 'required' : NULL;
	$__id 			= (isset($meta['id'])) ? $meta['id'] : uniqid(pachi_mask($Field));
	$__classes 		= (isset($meta['class'])) ? $meta['class'] : NULL;
	$__name 		= (ENVIRONMENT == 'production' OR PACHI_MASK == TRUE) ? pachi_mask($Field) : $Field;
	$__placeholder 	= (isset($meta['placeholder'])) ? $meta['placeholder'] : $this->lang->line('pachi_lbl_write_here');
	$__saved 		= (isset($saved_data[$Field])) ? $saved_data[$Field] : $Default;
	$__maxlength 	= (isset($meta['maxlength'])) ? ' maxlength="'.$meta['maxlength'].'" ' : NULL;

	$__title 		= $this->lang->line($Field);
	$__title 		.= ($meta['required']) ? $this->lang->line('pachi_lbl_required') : NULL;
?>

<div class="form-group">
    <?php 
		echo input_files("data[{$__name}]", $__saved, $__title, $meta['maxlength'], ALL_EXTENSIONS, $isolated_selection = TRUE);
     ?>
</div>