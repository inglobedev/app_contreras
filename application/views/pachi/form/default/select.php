<?php 
	$__required 	= ($meta['required']) ? 'required' : NULL;
	$__id 			= (isset($meta['id'])) ? $meta['id'] : uniqid(pachi_mask($Field));
	$__classes 		= (isset($meta['class'])) ? $meta['class'] : NULL;
	$__name 		= (ENVIRONMENT == 'production' OR PACHI_MASK == TRUE) ? pachi_mask($Field) : $Field;
	$__placeholder 	= (isset($meta['placeholder'])) ? $meta['placeholder'] : $this->lang->line('pachi_search');
	$__saved 		= (isset($saved_data[$Field])) ? $saved_data[$Field] : $Default;
	$__maxlength 	= (isset($meta['maxlength'])) ? ' maxlength="'.$meta['maxlength'].'" ' : NULL;
	$__type 		= (isset($meta['attr']['type'])) ? $meta['attr']['type'] : 'text';
	$__multiple 	= (isset($meta['multiple'])) ? 'multiple=""' : '';
	$__options 		= (isset($meta['options'])) ? $meta['options'] : array();
?>

<div class="form-group">
    <label><?php echo $this->lang->line($Field) ?> <?php if ($meta['required']) echo $this->lang->line('pachi_lbl_required'); ?></label>
    
    <?php if (isset($meta['modal'])): ?>
    <div class="input-group">
    <?php endif ?>

	    <select 
	    		name="data[<?php echo $__name ?>]" 
	    		id="<?php echo $__id ?>" 
	    		class="form-control <?php echo $__classes ?>" 
				<?php if (isset($meta['data']) AND is_array($meta['data'])): ?>
				<?php foreach ($meta['data'] as $___item => $___value): ?>
				data-<?php echo $___item ?>="<?php echo $___value ?>"
				<?php endforeach ?>
				<?php endif ?>
				tabindex="<?php echo $_index; ?>"
				placeholder="<?php echo $__placeholder ?>"
				<?php echo $__multiple ?>
	    	>

	    	<?php if (!empty($__saved) AND isset($meta['reference'])): ?>
	    		<?php $_items = $this->pachi->fetch($meta['reference']['table'], $__saved) ?>
	    		<?php foreach ($_items as $_kitem => $_item): ?>
	    			<?php if (isset($meta['reference']['custom_display'])): ?>
	    				<option selected value="<?php echo $_item[$meta['reference']['column']] ?>"><?php echo $this->parser->parse_string($meta['reference']['custom_display'], $_item, TRUE) ?></option>
	    			<?php else: ?>
	    				<option selected value="<?php echo $_item[$meta['reference']['column']] ?>"><?php echo (is_array($meta['reference']['display'])) ? $_item[reset($meta['reference']['display'])] : $_item[$meta['reference']['display']] ?></option>
	    			<?php endif ?>
	    		<?php endforeach ?>
	    	<?php else: ?>
	    	<?php endif ?>

	    	<?php if (!empty($__options)): ?>
	    		<?php foreach ($__options as $key => $value): ?>
	    			<option value="<?php echo $key ?>" <?php if ($key == $__saved) echo 'selected' ?>><?php echo $value ?></option>
	    		<?php endforeach ?>
	    	<?php endif ?>
		</select>

    <?php if (isset($meta['modal'])): ?>
        
        <span class="input-group-btn">
            <button class="btn btn-primary" type="button" data-pachi-modal="<?php echo $meta['modal']['key'] ?>"><?php echo $meta['modal']['button_title'] ?></button>
        </span>

    </div>
    <?php endif ?>

</div>

<?php #debugger($meta) ?>
