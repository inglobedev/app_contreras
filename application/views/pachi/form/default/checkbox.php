<?php 
	$__required 	= ($meta['required']) ? 'required' : NULL;
	$__id 			= (isset($meta['id'])) ? $meta['id'] : uniqid(pachi_mask($Field));
	$__classes 		= (isset($meta['class'])) ? $meta['class'] : NULL;
	$__name 		= (ENVIRONMENT == 'production' OR PACHI_MASK == TRUE) ? pachi_mask($Field) : $Field;
	$__saved 		= (isset($saved_data[$Field])) ? $saved_data[$Field] : $Default;
	$__maxlength 	= (isset($meta['maxlength'])) ? ' maxlength="'.$meta['maxlength'].'" ' : NULL;
?>

<div class="form-group">
    <div class="mt-checkbox-list">
        <label for="<?php echo $__id ?>" class="mt-checkbox margin-none <?php echo $__classes ?>"> <?php echo $this->lang->line($Field) ?>
            <input type="hidden" name="data[<?php echo $__name ?>]"  value="0">
            <input name="data[<?php echo $__name ?>]"  type="checkbox" value="1"  />
		    <input
		    		type="checkbox" 
		    		name="data[<?php echo $__name ?>]" 
		    		id="<?php echo $__id ?>" 
		    		value="1"
					<?php if ($__saved) echo ' checked ' ?>
					<?php if (isset($meta['data']) AND is_array($meta['data'])): ?>
					<?php foreach ($meta['data'] as $___item => $___value): ?>
					data-<?php echo $___item ?>="<?php echo $___value ?>"
					<?php endforeach ?>
					<?php endif ?>
		    > 
            <span></span>
        </label>
    </div>
</div>