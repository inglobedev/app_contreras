<?php 
	$__required 	= ($meta['required']) ? 'required' : NULL;
	$__id 			= (isset($meta['id'])) ? $meta['id'] : uniqid(pachi_mask($Field));
	$__classes 		= (isset($meta['class'])) ? $meta['class'] : NULL;
	$__name 		= (ENVIRONMENT == 'production' OR PACHI_MASK == TRUE) ? pachi_mask($Field) : $Field;
	$__placeholder 	= (isset($meta['placeholder'])) ? $meta['placeholder'] : $this->lang->line('pachi_lbl_write_here');
	$__saved 		= (isset($saved_data[$Field])) ? $saved_data[$Field] : $Default;
	$__maxlength 	= (isset($meta['maxlength'])) ? ' maxlength="'.$meta['maxlength'].'" ' : NULL;
	$__value 		= (isset($meta['value'])) ? $meta['value'] : FALSE;
?>

<div class="form-group">
    <label for="<?php echo $__id ?>"><?php echo $this->lang->line($Field) ?> <?php if ($meta['required']) echo $this->lang->line('pachi_lbl_required'); ?></label>
    <textarea 	<?php echo $__required . $__maxlength ?>
    		name="data[<?php echo $__name ?>]" 
    		id="<?php echo $__id ?>" 
    		class="form-control <?php echo $__classes ?>" 
    		placeholder="<?php echo $__placeholder ?>" 

			<?php if (isset($meta['data']) AND is_array($meta['data'])): ?>
			<?php foreach ($meta['data'] as $___item => $___value): ?>
			data-<?php echo $___item ?>="<?php echo $___value ?>"
			<?php endforeach ?>
			<?php endif ?>
			tabindex="<?php echo $_index; ?>"
			autocomplete="off"
    ><?php echo (!empty($__value)) ? $__value : $__saved ?></textarea>
</div>