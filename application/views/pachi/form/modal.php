<?php 
	$__id = (!empty($ext['id'])) ? $ext['id'] : uniqid(pachi_mask($table_data['table_name']));
	$__class = (!empty($ext['class'])) ? $ext['class'] : 'pachi_frm';
	$__custom_target = (isset($ext['custom_target'])) ? $ext['custom_target'] : NULL;
?>

<div class="modal fade" id="<?php echo $ext['__id_modal'] ?>" tabindex="-1" role="modal" >
	<form role="form" id="<?php echo $__id ?>" class="<?php echo $__class ?>" autocomplete="off" data-target="<?php echo $__custom_target ?>">
	    <div class="modal-dialog"> 
	        <div class="modal-content">
	            <div class="modal-header">
	                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
	                <h4 class="modal-title"><?php echo $ext['modal_title'] ?></h4>
	            </div>
	            <div class="modal-body">

			        <?php foreach ($table_data['columns'] as $_kcolumn => $_column): ?>
			        	
			        	<?php $_column['_index'] = $_kcolumn; ?>

			        	<?php $this->load->view("pachi/form/default/{$_column["meta"]["input_type"]}", $_column) ?>

			        <?php endforeach ?>

	            </div>
	            <div class="modal-footer">
			    	<input type="hidden" name="form" value="<?php echo $form_key ?>">
			    	<input type="hidden" name="meta" value="<?php echo pachi_mask(json_encode($ext)) ?>">
			        <button type="submit" class="btn blue"><?php echo $this->lang->line('pachi_btn_save') ?></button>
			        <button type="button" class="btn default" data-dismiss="modal" data-pachi-remove="#<?php echo $ext['__id_modal'] ?>"><?php echo $this->lang->line('pachi_btn_cancel') ?></button>
	            </div>
	        </div>
	    </div>
    </form>
</div>
