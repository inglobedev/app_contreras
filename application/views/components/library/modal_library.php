<div class="modal fade" id="mod_library" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog <?php if (!$isolated) echo 'modal-big' ?>">
        <form id="frm_library" id="user_library">
            <div class="modal-content ">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">
                        <?php if ($isolated): ?>
                            Carga de archivos
                        <?php else: ?>
                            Biblioteca
                        <?php endif ?>
                    </h4>
                </div>
                <div class="modal-body" style="height: <?php echo ($isolated) ? '64vh' : '75vh' ?>; overflow: hidden;"> 
                	<div class="row">
                        <div class="col-md-12">
                            <div class="portlet light margin-none padding-top-none padding-left-none padding-right-none">
                                <div class="portlet-title margin-none <?php if ($isolated) echo 'hidden' ?>">
                                    <div class="actions btn-set <?php if ($isolated) echo 'hidden' ?>">
                                        <input name="search_terms" class="form-control input-inline input-medium input-sm" placeholder="Buscar en la biblioteca..." type="text"></input>
                                    </div>
                                    <div class="actions btn-set pull-left">
                                        <a class="btn btn-primary" data-toggle="collapse" data-target=".uploader-container">
                                            <i class="fa fa-cloud-upload"></i> Subir Archivos
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="uploader-container collapse" aria-expanded="false" style="height: 0px;">
                                <input type="file" name="upload" class="library_uploader" data-fileuploader-extensions="<?php echo $extension ?>" data-isolated="<?php echo $isolated ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row" style="height: 64vh; overflow: auto;">
                        <div class="col-md-12 tiles" id="library_elements">
                			
                		</div>
                	</div>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="selection" value="<?php echo $selection ?>">
                    <input type="hidden" name="id_target" value="<?php echo $id_target ?>">
                    <input type="hidden" name="max_items" value="<?php echo $max_items ?>">
                    <input type="hidden" name="extension" value="<?php echo $extension ?>">
                    <input type="hidden" name="isolated"  value="<?php echo $isolated ?>">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Cancelar </button>
                    <button type="submit" class="btn btn-primary"> Confirmar </button>
                </div>
            </div>
        </form>
    </div>
</div>