<?php $extension = NULL; ?>
<form id="frm_library" id="user_library">
    <div class=""> 
    	<div class="row">
            <div class="col-md-12">
                <div class="library_toolbar clearfix">
                    <div class="pull-right">
                        <input name="search_terms" class="form-control input-inline input-medium input-sm" placeholder="Buscar en la biblioteca..." type="text"></input>
                    </div>
                    <div class="pull-left">
                        <a class="btn btn-sm btn-primary" data-toggle="collapse" data-target=".uploader-container">
                            <i class="fa fa-cloud-upload"></i> Subir Archivos
                        </a>
                        <div class="btn-group hidden" id="library_selection_options">
                            <a class="btn btn-sm green-meadow dropdown-toggle" data-toggle="dropdown" href="javascript:;" aria-expanded="false"> Selección
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="javascript:;"> Compartir</a>
                                </li>
                                <li>
                                    <a href="javascript:;"> Eliminar </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="uploader-container collapse" aria-expanded="false" style="height: 0px;">
                    <input type="file" name="upload" class="library_uploader" data-fileuploader-extensions="<?php echo $extension ?>">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 tiles" id="library_elements">
    		</div>
    	</div>
    </div>
    <input type="hidden" name="selection" value="">
    <input type="hidden" name="extension" value="<?php echo $extension ?>">
</form>
