<?php if (count($elements)): ?>
    <?php foreach ($elements as $_kelement => $_element): ?>
    <?php $__bg = (!$_element['is_image']) ? NULL : 'style="background-image:url('.file_uri($_element['id_file'], TRUE).');background-position: center; background-size: cover;"' ?>
    <div class="tile selectable <?php echo string_to_color($_element['file_ext']) ?> <?php echo string_to_color($_element['file_ext'], 'font') ?> <?php if ($_element['selected']) echo 'selected' ?>" data-id_file="<?php echo $_element['id_file'] ?>" data-id_element="<?php echo $_element['id_element'] ?>">
        <div class="corner"> </div>
        <div class="check"> </div>
        <div class="tile-body" <?php echo $__bg; ?>>
            <?php if (!$_element['is_image']): ?>
                <?php echo $_element['file_ext'] ?>
            <?php endif ?>
        </div>
        <div class="tile-object">
            <div class="name"> <?php echo text_preview($_element['file_name'], 40) ?> </div>
            <div class="number"><span class="ext"><?php echo $_element['file_ext'] ?></span> <?php echo print_size($_element['file_size']) ?> </div>
        </div>
    </div>
    <?php endforeach ?>
    <?php if ($dataset < $_element['total_results']): ?>
    <div class="tile bg-green-meadow" id="library_btn_load_more">
        <div class="tile-body" >
            <i class="fa fa-plus"></i>
        </div>
        <div class="tile-object">
            <div class="name"> Cargar más </div>
        </div>
    </div>
    <?php endif ?>
<?php endif ?>