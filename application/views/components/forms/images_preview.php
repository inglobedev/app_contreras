<?php if (count($selection)): ?>
    <?php foreach ($selection as $_kpos => $_element): ?>
        <span class="sl_image"><img src="<?php echo file_uri($_element['id_file'], TRUE) ?>" width="125" height="125"></span>
    <?php endforeach ?>
<?php else: ?>
    <p class="text-center text-muted margin-none" style="padding-top: 50px;"><i class="fa fa-picture-o fa-4x"></i></p>
<?php endif ?>