<?php if (count($selection)): ?>
    <?php foreach ($selection as $_kpos => $_element): ?>
        <?php if ($_element['is_image']): ?>
                <span class="sl_file"><img src="<?php echo file_uri($_element['id_file'], TRUE) ?>" width="29" height="29"><?php echo $_element['file_name'] ?></span>
            <?php else: ?>
            <span class="sl_file">
                <span class="ext <?php echo string_to_color($_element['file_ext']) ?> <?php echo string_to_color($_element['file_ext'], 'font') ?>"><?php echo $_element['file_ext'] ?></span>
                <?php echo $_element['file_name'] ?>
            </span>
        <?php endif ?>
    <?php endforeach ?>
<?php else: ?>
    <p class="text-center text-muted margin-none" style="padding-top: 5px;">Ningún elemento seleccionado</p>
<?php endif ?>