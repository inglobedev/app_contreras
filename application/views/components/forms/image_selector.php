<?php $sug_id = uniqid('file_'); ?>

<div class="form-group">
    <label class="control-label"><?php echo $input_label ?></label><br>
	<div class="sl_images btn_open_library hnd_selected_images_previews <?php echo $sug_id ?>" data-id_target="<?php echo $sug_id ?>" data-max_items="<?php echo $input_max ?>" data-extension="<?php echo $input_extensions ?>" data-isolated="<?php echo $input_isolated_selection ?>">
		<?php echo input_images_preview($input_values) ?>
	</div>
	<input type="hidden" class="hnd_selected_images" name="<?php echo $input_name ?>" id="<?php echo $sug_id ?>" value="<?php echo $input_values ?>">
</div>