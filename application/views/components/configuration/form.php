<?php if (is_array($configs)): ?>
<form role="form" class="frm-save-configurations">
    <div class="form-body">
    	<?php foreach ($configs as $_kitem => $_item): ?>
		    
		    <?php if ($_item['type'] == 'checkbox'): ?>
		    	<?php $_val = (empty($_item['saved'])) ? $_item['default'] : $_item['saved']; ?>
                <div class="form-group">
                    <div class="mt-checkbox-list">
                        <label class="mt-checkbox margin-none"> <?php echo $_item['label'] ?>
                            <input type="hidden" name="items[<?php echo $_kitem ?>]" value="0">
                            <input name="items[<?php echo $_kitem ?>]" type="checkbox" value="1" <?php if ($_val == 1) echo 'checked' ?> />
                            <span></span>
                        </label>
                    </div>
                </div>

		    <?php endif ?>

		    <?php if ($_item['type'] == 'text'): ?>
		        <div class="form-group">
		            <label><?php echo $_item['label'] ?></label>
		            <input type="text" name="items[<?php echo $_kitem ?>]" id="<?php if (isset($_item['id'])) echo $_item['id'] ?>" class="form-control <?php if (isset($_item['class'])) echo $_item['class'] ?>" placeholder="<?php echo (isset($_item['placeholder'])) ? $_item['placeholder'] : 'Escriba aquí' ?>" value="<?php echo (empty($_item['saved'])) ? $_item['default'] : $_item['saved'] ?>"> 
		        </div>
		    <?php endif ?>

		    <?php if ($_item['type'] == 'textarea'): ?>
		        <div class="form-group">
		            <label><?php echo $_item['label'] ?></label>
		            <textarea name="items[<?php echo $_kitem ?>]" id="<?php if (isset($_item['id'])) echo $_item['id'] ?>" class="form-control <?php if (isset($_item['class'])) echo $_item['class'] ?>" placeholder="<?php echo (isset($_item['placeholder'])) ? $_item['placeholder'] : 'Escriba aquí' ?>"><?php echo (empty($_item['saved'])) ? $_item['default'] : $_item['saved'] ?></textarea>
		        </div>
		    <?php endif ?>

		    <?php if ($_item['type'] == 'select'): ?>
		        <div class="form-group">
		            <label><?php echo $_item['label'] ?></label>
		            <select name="items[<?php echo $_kitem ?>]" id="<?php if (isset($_item['id'])) echo $_item['id'] ?>" class="form-control <?php if (isset($_item['class'])) echo $_item['class'] ?>">
		            	<?php if (count($_item['options'])): ?>
		            		<?php foreach ($_item['options'] as $_value => $_option): ?>
		            			<option <?php if ($_item['saved'] == $_value) echo 'selected' ?> value="<?php echo $_value ?>"><?php echo $_option ?></option>
		            		<?php endforeach ?>
		            	<?php else: ?>
		            		<option value="-">Ninguna opción definida.</option>
		            	<?php endif ?>
	            	</select>
		        </div>
		    <?php endif ?>

		    <?php if ($_item['type'] == 'file'): ?>
		    	<?php $__max = (isset($_item['max'])) ? $_item['max'] : 1; ?>
		    	<?php $__sav = (empty($_item['saved'])) ? $_item['default'] : $_item['saved']; ?>
		    	<?php $__nam = 'items['.$_kitem.']'; ?>
		    	<?php $__ext = (empty($_item['extensions'])) ? ALL_EXTENSIONS : $_item['extensions']; ?>
		    	<?php $__iso = (empty($_item['isolated'])) ? 0 : 1; ?>
		    	<?php echo input_files($__nam, $__sav, $_item['label'], $__max, $__ext, $__iso) ?>
		    <?php endif ?>

		    <?php if ($_item['type'] == 'image'): ?>
		    	<?php $__max = (isset($_item['max'])) ? $_item['max'] : 1; ?>
		    	<?php $__sav = (empty($_item['saved'])) ? $_item['default'] : $_item['saved']; ?>
		    	<?php $__nam = 'items['.$_kitem.']'; ?>
		    	<?php $__ext = (empty($_item['extensions'])) ? ALL_IMAGE_EXTENSIONS : $_item['extensions']; ?>
		    	<?php $__iso = (empty($_item['isolated'])) ? 0 : 1; ?>
		    	<?php echo input_images($__nam, $__sav, $_item['label'], $__max, $__ext, $__iso) ?>
		    <?php endif ?>

    	<?php endforeach ?>
    </div>
    <div class="form-actions">
		<input type="hidden" name="config_scope" value="<?php echo $scope; ?>">
		<input type="hidden" name="config_reference" value="<?php echo $reference; ?>">
        <button type="submit" class="btn blue">Guardar</button>
        <button type="button" class="btn default">Cancelar</button>
    </div>
</form>
<?php else: ?>
	<div class="alert alert-danger">
        <strong>¡Error!</strong> No se encuentra el archivo de configuración solicitado: <?php echo $scope ?>. 
    </div>
<?php endif ?>


