<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-editable/bootstrap-editable/js/bootstrap-editable.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js" type="text/javascript"></script>

<!-- PACHI -->
	<script src="<?php echo base_url() ?>assets/global/plugins/jquery-mask/jquery.mask.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js" type="text/javascript"></script>
	<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
	
	<script src="<?php echo base_url() ?>assets/pachi/scripts/pachi.js" type="text/javascript"></script>
<!-- // PACHI -->

<script src="<?php echo base_url() ?>assets/app/scripts/payments.js" type="text/javascript"></script>
