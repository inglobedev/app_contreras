<?php 
	$columns['transaction_date']				= array('custom_render' => '#=print_datetime|{transaction_date}|0=# <br><small>#=print_time|{transaction_date}|1=#</small>',
														'title' => 'Fecha',
														'width' => '100px'
														);
	
	$columns['detail_description']				= array('title' => 'Concepto',
														'custom_render' => '{if [transaction_observations]}
																				{transaction_observations} 
																			{else}
																				{detail_description} 
																			{/if}
																			<br>
																			<small>
																			{if [transaction_ammount] > 0}
																				{account_name_issuer}
																			{else}
																				{account_name_receiver}
																			{/if}
																			{if [id_user] > 0}
																				#=get_user_name|{id_user}=#
																			{/if}
																			</small>'
														);
	
	$columns['transaction_ammount']				= array('title' => 'Importe',
														'custom_render' => '$ #=number_format|{transaction_ammount}|2=#',
														'class' => 'text-right',
														'width' => '110px'
														);
	
	$columns['account_name_issuer']				= array('hide' => TRUE);
	$columns['account_name_receiver']			= array('hide' => TRUE);
	$columns['id_transaction']					= array('hide' => TRUE);
	$columns['id_account']						= array('hide' => TRUE);
	$columns['transaction_id']					= array('hide' => TRUE);
	$columns['id_receipt_detail']				= array('hide' => TRUE);
	$columns['id_receipt']						= array('hide' => TRUE);
	$columns['id_expense_type']					= array('hide' => TRUE);
	$columns['id_period']						= array('hide' => TRUE);
	$columns['id_property']						= array('hide' => TRUE);
	$columns['detail_info']						= array('hide' => TRUE);
	$columns['detail_ammount']					= array('hide' => TRUE);
	$columns['id_issuing_account']				= array('hide' => TRUE);
	$columns['id_receiving_account']			= array('hide' => TRUE);
	$columns['detail_concept']					= array('hide' => TRUE);
	$columns['detail_observations']				= array('hide' => TRUE);
	$columns['detail_expire_date']				= array('hide' => TRUE);
	$columns['detail_interest_since']			= array('hide' => TRUE);
	$columns['detail_interest_rate']			= array('hide' => TRUE);
	$columns['detail_original_ammount']			= array('hide' => TRUE);
	$columns['detail_attachments']				= array('hide' => TRUE);
	$columns['detail_custom']					= array('hide' => TRUE);
	$columns['detail_id_user_modifier']			= array('hide' => TRUE);
	$columns['detail_id_detail_sibling']		= array('hide' => TRUE);
	$columns['creation_date']					= array('hide' => TRUE);
	$columns['modification_date']				= array('hide' => TRUE);
	$columns['_checked']						= array('hide' => TRUE);
	$columns['sibling_receipt_attachments']		= array('hide' => TRUE);
	$columns['period_year']						= array('hide' => TRUE);
	$columns['period_month']					= array('hide' => TRUE);
	$columns['period_code']						= array('hide' => TRUE);
	$columns['receipt_date']					= array('hide' => TRUE);
	$columns['id_user_issuer']					= array('hide' => TRUE);
	$columns['id_user_receiver']				= array('hide' => TRUE);
	$columns['receipt_payed']					= array('hide' => TRUE);
	$columns['receipt_pay_date']				= array('hide' => TRUE);
	$columns['receipt_voided']					= array('hide' => TRUE);
	$columns['receipt_description']				= array('hide' => TRUE);
	$columns['receipt_observations']			= array('hide' => TRUE);
	$columns['receipt_attachments']				= array('hide' => TRUE);
	$columns['type_name']						= array('hide' => TRUE);
	$columns['type_description']				= array('hide' => TRUE);
	$columns['type_order']						= array('hide' => TRUE);
	$columns['account_id_user_issuer']			= array('hide' => TRUE);
	$columns['account_id_user_receiver']		= array('hide' => TRUE);
	$columns['id_user']							= array('hide' => TRUE);
	$columns['transaction_observations']		= array('hide' => TRUE);
	$columns['transaction_type']				= array('hide' => TRUE);
	$columns['_payed']							= array('hide' => TRUE);

	$options['fixed_filters'] 	 = array(
									'id_account'  => $id_account
									);

	$options['fixed_orders'] 	 = array(
									'id_transaction'  => 'DESC'
									);

	if (isset($periods)) {
		$options['filters']			 = array();
		foreach ($periods as $_kperiod => $_period) 
			$options['filters']['id_period='.$_period['id_period']] = $_period['period_code'];
	}

	echo pachi_fullmanaged_table('vw_transactions', $columns, $options); 
?>