<div class="row">
	<div class="col-md-4">
		<div class="portlet light ">
		    <div class="portlet-title">
		        <div class="caption">
		            <h3 class="custom-h1"> Estado de cajas </h3>
		        </div>
		    </div>
			<div class="portlet-body">
				<div class="row sale-summary">
					<div class="col-xs-12">
						<ul class="list-unstyled">
				        	<?php $_c = 0; ?>
				        	<?php foreach ($accounts as $_kaccount => $_account): ?>
				        	<?php $_c += $_account['account_balance'] ?>
				            <li>
				                <span class="sale-info"> 
				                	<a class="padding-none" data-id_account="" href="<?php echo base_url(APP_ADMINISTRATION . 'account/' . $_account['id_account']) ?>"><?php echo $_account['account_name'] ?></a>
				                </span>
				                <span class="sale-num"> $ <?php echo number_format($_account['account_balance'], 2) ?> </span>
				            </li>
				        	<?php endforeach ?>
				        	<li class="padding-none"></li>
				            <li>
				                <span class="sale-info"> 
				                	Total
				                </span>
				                <span class="sale-num"> $ <?php echo number_format($_c, 2) ?> </span>
				            </li>
				        </ul>
					</div>
				</div>
			</div>
		</div>

		<div class="portlet light ">
		    <div class="portlet-title">
		        <div class="caption">
		            <h3 class="custom-h1"> Otros </h3>
		        </div>
		    </div>
			<div class="portlet-body">
				<div class="row sale-summary">
					<div class="col-xs-12">
						<ul class="list-unstyled">
							<li>
						        <span class="sale-info btn-view-transactions" data-filter="adm-ext"> 
						        	<a href="javascript:;">Extracciones Adm</a>
						        </span>
						        <span class="sale-num"> <i class=" fa fa-search"></i> </span>
						    </li>
						    <li>
						        <span class="sale-info btn-view-transactions" data-filter="adm-payments"> 
						        	<a href="javascript:;">Pagos de Imp & Sev Adm</a>
						        </span>
						        <span class="sale-num"> <i class=" fa fa-search"></i> </span>
						    </li>
						</ul>
					</div>
				</div>
			</div>
		</div>

	</div>
	<div class="col-md-8">
		<div class="portlet light ">
		    <div class="portlet-title">
		        <div class="caption">
		            <h3 class="custom-h1" id="account-title">Movimientos
		            	<?php if (!empty($account)): ?>
		            		<?php echo $account['account_name'] ?>
		            	<?php endif ?>
		            </h3>
		        </div>
		    </div>
			<div class="portlet-body">
				<div class="row sale-summary">
					<div class="col-xs-12" id="account-transactions">
						<?php if (!empty($id_account)): ?>
							<?php $this->load->view('pages/administration/components/transactions', FALSE); ?>
						<?php else: ?>
							<h4 class="text-center">Seleccione una caja para ver el detalle ...</h4>
						<?php endif ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>