<div class="portlet light">
	<div class="portlet-body">
		<div class="row">
			<div class="col-md-12">
				<?php 
		            $_columns['id_property']					= array('hide' => TRUE);
		            $_columns['id_user_consortium']				= array('hide' => TRUE);
		            $_columns['property_name']					= array();
		            $_columns['property_address']				= array('hide' => TRUE);
		            $_columns['property_year']					= array('hide' => TRUE);
		            $_columns['property_ambiences_qty']			= array('hide' => TRUE);
		            $_columns['property_bathrooms_qty']			= array('hide' => TRUE);
		            $_columns['property_rooms_qty']				= array('hide' => TRUE);
		            $_columns['property_square_meter']			= array('hide' => TRUE);
		            $_columns['property_garage']				= array('hide' => TRUE);
		            $_columns['property_balcony']				= array('hide' => TRUE);
		            $_columns['property_building_name']			= array('hide' => TRUE);
		            $_columns['property_cadastre_number']		= array('hide' => TRUE);
		            $_columns['property_observations']			= array('hide' => TRUE);
		            $_columns['creation_date']					= array('hide' => TRUE);
		            $_columns['modificaction_date']				= array('hide' => TRUE);
		            $_columns['deletion_date']					= array('hide' => TRUE);
		            $_columns['id_user_owner']					= array('hide' => TRUE);
		            $_columns['ownership_date_start']			= array('hide' => TRUE);
		            $_columns['owner_user_name']				= array();
		            $_columns['owner_user_lastname']			= array('hide' => TRUE);
		            $_columns['owner_user_address']				= array('hide' => TRUE);
		            $_columns['owner_user_email']				= array('hide' => TRUE);
		            $_columns['owner_user_phone']				= array('hide' => TRUE);
		            $_columns['owner_user_picture']				= array('hide' => TRUE);
		            $_columns['consortium_user_name']			= array('hide' => TRUE);
		            $_columns['consortium_user_lastname']		= array('hide' => TRUE);
		            $_columns['consortium_user_address']		= array('hide' => TRUE);
		            $_columns['consortium_user_email']			= array('hide' => TRUE);
		            $_columns['consortium_user_phone']			= array('hide' => TRUE);
		            $_columns['consortium_user_picture']		= array('hide' => TRUE);
		            $_columns['tenant_user_name']				= array();
		            $_columns['tenant_user_lastname']			= array('hide' => TRUE);
		            $_columns['tenant_user_address']			= array('hide' => TRUE);
		            $_columns['tenant_user_email']				= array('hide' => TRUE);
		            $_columns['tenant_user_phone']				= array('hide' => TRUE);
		            $_columns['tenant_user_picture']			= array('hide' => TRUE);
		            $_columns['id_contract']					= array('hide' => TRUE);
		            $_columns['id_user_tenant']					= array('hide' => TRUE);
		            $_columns['contract_date_start']			= array('hide' => TRUE);
		            $_columns['contract_date_end']				= array('hide' => TRUE);
		            $_columns['contract_rent_price']			= array();
		            $_columns['contract_interest']				= array('hide' => TRUE);
		            $_columns['contract_tenant_attachments']	= array('hide' => TRUE);
		            $_columns['contract_gas_check']				= array('hide' => TRUE);
		            $_columns['contract_electricity_check']		= array('hide' => TRUE);
		            $_columns['contract_expenses_check']		= array('hide' => TRUE);
		            $_columns['contract_expenses_attachments'] 	= array('hide' => TRUE);
		            $_columns['contract_observations']			= array('hide' => TRUE);
		            $_columns['tenant_debts']					= array();
		            $_columns['_count_maintenances']			= array();
				?>

				<?php echo pachi_fullmanaged_table('vw_properties', $_columns); ?>
			</div>
		</div>
	</div>
</div>