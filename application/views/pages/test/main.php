<?php 

	$_opt['create_entity'] 	= TRUE;
	$_opt['search'] 		= TRUE;
	$_opt['filters'] 		= array();

	echo pachi_fullmanaged_table('contracts', array(), $_opt);

?>

<?php if (FALSE): ?>
	<div class="portlet light">
		<div class="portlet-body">
			<?php 
				// A los formularios, dentro de las opciones.. 
				// se les puede pasar un custom target que indica la url a la que va postear.
				$_opt['custom_target'] = base_url('perro/perro');
				
				echo pachi_form('roles', FALSE, FALSE, $_opt);
			?>
		</div>
	</div>


	<?php echo pachi_fullmanaged_table('users'); ?>

	<?php 
		// -------------------------------------------------------------------------------------------------------------------------------------------
		// Ejemplo, de personalizacion de columnas para la generacion de formulario de la tabla users.
		
		// El formulario por defecto se hace con todas las columnas de la tabla, a menos que se especifique un grupo particular, como en el siguiente ejemplo.
		
		$cols = array('col1', 'col2', 'etc');

		// O de una forma mas compleja:

		$cols = array(  'user_name', 
						'user_last_name', 
						'user_picture', 
						'user_email', 
						'user_password', 
						'user_language', 
						'user_registration_date', 
						'id_role' => array(	// Buscador
											'reference' => array( //'table' => 'users', // aca podria determinar otra tabla distinta para buscar.
																	'display' => 'role_name', 	// Si bien por defecto muestra el _name, podria mostrar otra columna.
																	'filter' => array('id_role>' => 2), // Pasamos filtros al buscador, ej roles > 2
																	), // aca determino que columna es la que se muestra en el buscador.
											// Modal de creacion.
											'modal' => array('columns' 	=> array('role_name'), 
															 '_data' 	=> array('save_action' => array('action' => 'redirect',
																										'target' => 'http://www.google.com',
																										'delay'  => '500')
																				),
															 'modal_title' => 'asdasd'
															)
										  ),
						);

		// Extra:
		// Para autogenerar los botones de nuevo, con la config por defecto, se puede habilitar y deshabilitar esta caracteristica con las siguientes lineas:
		// $this->pachi->allow_create = TRUE;
		// $this->pachi->allow_create = FALSE;

		// -------------------------------------------------------------------------------------------------------------------------------------------
		// Ejemplo, personalizacion de la accion del formulario.

		// El formulario por defecto va a guardar y mostrar el mensaje de de confirmacion.
		$form['save_action']['action'] = 'redirect';
		$form['save_action']['target'] = '?id={id_role}';
		$form['save_action']['delay'] = '?id={id_role}';

		// Las acciones posibles son: 
			// redirect, 
			// reload, 
			// dismiss_modal,
		// Donde target sera una url, o un id o clase de modal.
		// En el target se reemplazan los marcadores {nombre_columna} por el valor guardado para esa coumna en el registro insertado/actualizado.
		// Por ejemplo, un target de un redirect podria ser: ?id={id_user}&action=edit o.. //{slug}/bla_bla

		// la save_action se pueden pasar tanto a un formulario, como en _data de un modal, para que aplique al formulario del modal que se abrira.

	?>


	<div class="portlet light">
		<div class="portlet-body">
			<a class="btn btn-default" href="javascript:;" data-pachi-modal="<?php echo pachi_modal('users', $cols, FALSE, $form, 'Titulo del modal') ?>">Hacer Magia</a>
		</div>
	</div>

	<?php 
	/*
		// Custom column options:
		title: remplaza el title.
		custom_field: indicar que valor se va a mostrar en esta columna
		custom_render: pasar un string/html para insertar en esta columna, se remplazaran los {variables} por valores de la row.
	*/
	?>
	<div class="portlet light">
		<div class="portlet-body">
			<h4>Generación de tabla a partir de un pachi fecth</h4>
			<p><strong>pachi_table</strong>(<strong>string</strong> <i>table_identifier</i>, <strong>array</strong> <i>data</i>, <strong>array</strong> <i>column_settings</i>, <strong>string</strong> <i>table_classes</i>);</p>
			<div class="row">
				<div class="col-md-8">
					<h5>Camino Facil.</h5>
					<p>Permite interceptar los datos antes del dibujado, para algun eventual modificacion.</p>
					<?php 
			            $_columns['id_property']					= array('hide' => TRUE);
			            $_columns['id_user_consortium']				= array('hide' => TRUE);
			            $_columns['property_name']					= array();
			            $_columns['property_address']				= array('hide' => TRUE);
			            $_columns['property_year']					= array('hide' => TRUE);
			            $_columns['property_ambiences_qty']			= array('hide' => TRUE);
			            $_columns['property_bathrooms_qty']			= array('hide' => TRUE);
			            $_columns['property_rooms_qty']				= array('hide' => TRUE);
			            $_columns['property_square_meter']			= array('hide' => TRUE);
			            $_columns['property_garage']				= array('hide' => TRUE);
			            $_columns['property_balcony']				= array('hide' => TRUE);
			            $_columns['property_building_name']			= array('hide' => TRUE);
			            $_columns['property_cadastre_number']		= array('hide' => TRUE);
			            $_columns['property_observations']			= array('hide' => TRUE);
			            $_columns['creation_date']					= array('hide' => TRUE);
			            $_columns['modificaction_date']				= array('hide' => TRUE);
			            $_columns['deletion_date']					= array('hide' => TRUE);
			            $_columns['id_user_owner']					= array('hide' => TRUE);
			            $_columns['ownership_date_start']			= array('hide' => TRUE);
			            $_columns['owner_user_name']				= array();
			            $_columns['owner_user_lastname']			= array('hide' => TRUE);
			            $_columns['owner_user_address']				= array('hide' => TRUE);
			            $_columns['owner_user_email']				= array('hide' => TRUE);
			            $_columns['owner_user_phone']				= array('hide' => TRUE);
			            $_columns['owner_user_picture']				= array('hide' => TRUE);
			            $_columns['consortium_user_name']			= array('hide' => TRUE);
			            $_columns['consortium_user_lastname']		= array('hide' => TRUE);
			            $_columns['consortium_user_address']		= array('hide' => TRUE);
			            $_columns['consortium_user_email']			= array('hide' => TRUE);
			            $_columns['consortium_user_phone']			= array('hide' => TRUE);
			            $_columns['consortium_user_picture']		= array('hide' => TRUE);
			            $_columns['tenant_user_name']				= array();
			            $_columns['tenant_user_lastname']			= array('hide' => TRUE);
			            $_columns['tenant_user_address']			= array('hide' => TRUE);
			            $_columns['tenant_user_email']				= array('hide' => TRUE);
			            $_columns['tenant_user_phone']				= array('hide' => TRUE);
			            $_columns['tenant_user_picture']			= array('hide' => TRUE);
			            $_columns['id_contract']					= array('hide' => TRUE);
			            $_columns['id_user_tenant']					= array('hide' => TRUE);
			            $_columns['contract_date_start']			= array('hide' => TRUE);
			            $_columns['contract_date_end']				= array('hide' => TRUE);
			            $_columns['contract_rent_price']			= array();
			            $_columns['contract_interest']				= array('hide' => TRUE);
			            $_columns['contract_tenant_attachments']	= array('hide' => TRUE);
			            $_columns['contract_gas_check']				= array('hide' => TRUE);
			            $_columns['contract_electricity_check']		= array('hide' => TRUE);
			            $_columns['contract_expenses_check']		= array('hide' => TRUE);
			            $_columns['contract_expenses_attachments'] 	= array('hide' => TRUE);
			            $_columns['contract_observations']			= array('hide' => TRUE);
			            $_columns['tenant_debts']					= array('custom_render' => '<i>{if tenant_user_name == 0}{tenant_user_name}{/if}</i>');
			            $_columns['_count_maintenances']			= array();
											
						// Columna inventada:
						$columns['extracol']			= array('title' => 'Columna Inventada', 'custom_render' => 'cel: {user_phone} <br> lang: {user_language}');
						
						#echo pachi_table('tbl-users', $dataset, $columns);
					?>
				</div>
				<div class="col-md-4">
					<h5>Camino complejo.</h5>
					<p>Permite modificar la paginación, o establecer filtros rigidos de antemano..</p>
					<?php 
						// Obtener información de paginacion para una tabla definida. Y establecer los valores default.
						$_tbl 		= pachi_table_get('tbl-roles', $default_page = 0, $default_items_per_page = 3);

						// Consultar la informacion con estos datos de paginacion usando el metodo fetch clasico.
						$dataset2 	= $this->pachi->fetch('roles', $_tbl['f'], $_tbl['p'], $_tbl['ipp'], $_tbl['o']);
						
						// Dibujar la tabla usando el id planteado anteriormente, con los datos obtenidos. Y opcionalmente una configutacion de columna.
						echo pachi_table('tbl-roles', $dataset2);
					?>
				</div>
			</div>
		</div>
	</div>

	<div class="portlet light">
		<div class="portlet-body">
			<?php $_options['create_entity'] = TRUE; ?>
			<?php $_options['search'] 		 = TRUE; ?>
			<?php $_options['filters'] 		 = array(); ?>
			<?php echo pachi_fullmanaged_table('roles_permissions', array(), $_options); ?>
		</div>
	</div>

	<div class="portlet light">
		<div class="portlet-body">
			<h4>Generación de tabla a partir de un array con datos cualquiera.</h4>
			<p><strong>pachi_table</strong>(<strong>string</strong> <i>table_identifier</i>, <strong>array</strong> <i>data</i>, <strong>array</strong> <i>column_settings</i>, <strong>string</strong> <i>table_classes</i>);</p>
			<?php 
				$datas[0]['Columna 1'] = 'VC1R1';
				$datas[0]['Columna 2'] = 'VC2R1';
				$datas[0]['Columna 3'] = 'VC3R1';
				$datas[1]['Columna 1'] = 'VC1R2';
				$datas[1]['Columna 2'] = 'VC2R2';
				$datas[1]['Columna 3'] = 'VC3R2';
				echo pachi_table('tbl-xxx', $datas);
			?>
		</div>
	</div>

	<div class="portlet light">
		<div class="portlet-body">
			<h4>Tablas full managed</h4>
			<p>pachi_fullmanage(string $table, array $column_settings, array $options)</p>
			<?php 
				// Opciones de columnas
				$column_settings['id_user'] 				= array('hide' => TRUE);
				$column_settings['user_name'] 				= array('custom_render' => '<i>{user_name}/{user_language}</i>');
				$column_settings['user_password'] 			= array('hide' => TRUE);
				$column_settings['user_picture'] 			= array('hide' => TRUE);
				$column_settings['user_language'] 			= array('hide' => TRUE);
				$column_settings['user_password_restore'] 	= array('hide' => TRUE);
				$column_settings['user_deletion_date'] 		= array('hide' => TRUE);
				$column_settings['user_registration_date'] 	= array('hide' => TRUE);
				$column_settings['user_last_login'] 		= array('hide' => TRUE);
				$column_settings['user_pin'] 				= array('hide' => TRUE);
				$column_settings['user_login_count']		= array('hide' => TRUE);

				// Opciones de tablas
				$options['class'] 		= 'table-bordered table-hover bg-white table-vertical-align';
				$options['searcher'] 	= TRUE;
				$options['filter'] 		= TRUE;

				echo pachi_fullmanaged_table('users', $column_settings, $options);
			?>
		</div>
	</div>
<?php endif ?>
