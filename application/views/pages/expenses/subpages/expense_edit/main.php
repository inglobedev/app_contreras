<div class="col-md-3"></div>
<div class="col-md-6">
	<div class="portlet light">
	    <div class="portlet-body">

			<?php 
			$column['id_expense_type'] = array();
			$column['expense_name'] = array();
			$column['expense_description'] = array();

			$options['cancel'] = base_url(APP_IMPUESTOS);
			$options['save_action']['action'] = 'redirect';
            $options['save_action']['target'] = base_url(APP_IMPUESTOS);
			echo pachi_form('expenses',$column, $expense['id_expense'], $options) ?>
		</div>
	</div>
</div>