<div class="col-md-3"></div>
<div class="col-md-6">
    <div class="portlet light">
        <div class="portlet-body">

            <?php 
            $column['type_name'] = array();
            $column['type_description'] = array();

            $options['cancel'] = base_url(APP_IMPUESTOS.'/type_expense');
            $options['save_action']['action'] = 'redirect';
            $options['save_action']['target'] = base_url(APP_IMPUESTOS.'/type_expense');
            echo pachi_form('expense_types',$column, $type['id_expense_type'], $options) ?>
        </div>
    </div>
</div>