
<div class="col-md-3"></div>
<div class="col-md-6">
	<div class="portlet light">
	    <div class="portlet-body">
	    	<?php 
		    	$columns['id_expense_type'] 	= array('hide' => TRUE); 
		    	$columns['type_description'] 	= array('hide' => TRUE); 
		    	$columns['creation_date'] 		= array('hide' => TRUE); 
		    	$columns['modification_date']   = array('hide' => TRUE); 
		    	$columns['deletion_date']   	= array('hide' => TRUE);

		    	$columns['type_name']   		= array('title' => 'Tipo de gasto');

		    	$columns['edit']   				= array('title' => '',
		    											'width' => '50px',
		    											'custom_render' => '<a title="Editar gasto de gasto" class="btn default btn-sm" href="/'.APP_IMPUESTOS.'/view_type_expense/{id_expense_type}"><i class="fa fa-pencil"></i></a>');

		    	$columns_expense['type_name'] = array();

		    	$options['create_entity'] = array('button_title' 	=> ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_expense_type'),
											#	'table' 		=> 'expenses',
												'columns' 		=> $columns_expense,
											#	'target' 		=> '?action=new', // Si se especifica target, no se tiene en cuenta table y columns. Se redirige al target.
												);
	    		echo pachi_fullmanaged_table('expense_types', $columns, $options); ?>
	    </div>
	</div>
</div>
<div class="col-md-3"></div>