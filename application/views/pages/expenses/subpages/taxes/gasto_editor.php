<?php $cancel_url = base_url(APP_IMPUESTOS.'gastos');  ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Actualizar gasto</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_general" data-toggle="tab">Información general</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_general">
                                    <form class="frm-actualizar-gasto">
                                        <div class="form-group">
                                            <div class="form-group">
                                                <label class="control-label">Nombre del gasto</label>
                                                <input autocomplete="off" required name="gasto_nombre" type="text" placeholder="Nombre" class="form-control" value="<?php echo $gasto['gasto_nombre'] ?>" /> 
                                            </div>
                                            <div class="form-group">
                                                <label for="ids_tipo" class="control-label">Tipo de gasto</label>
                                                <select required="" name="ids_tipo" id="ids_tipo" class="form-control ajax-search" data-placeholder="Buscar tipos de gastos" data-url="<?php echo base_url('ajax/ajax_impuestos/vincular_tipo_gasto') ?>" data-filter="">                               
                                                    <option selected value="<?php echo $gasto['id_tipo'] ?>"><?php echo $gasto['tipo_nombre']; ?></option>
                                                </select>
                                            </div>                       
                                        </div>
                                        <div class="margiv-top-10">
                                            <input type="hidden" name="id_gasto" value=" <?php echo $gasto['id_gasto'] ?> ">
                                            <button type="submit" class="btn green"> Actualizar </button>
                                            <a href="<?php echo $cancel_url ?>" class="btn default"> Cancelar </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3"></div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>