<div class="row">
	<div class="col-xs-12 col-sm-4">
		<div class="portlet light">
			<h3 class="custom-h1">Impuestos y servicios a pagar</h3>
			<div class="portlet-body">
				<?php
					unset($_columns);
					$_columns['detail_description']			= array('custom_render' => '<strong><a class="btn_new_coupon" data-id_user_receiver="{account_id_user_receiver}" data-filter_details="{id_receipt_detail}">{detail_description}</a></strong><br>
																						<small>{detail_info}</small>',
																	'title' => 'Detalle'

																					);
					$_columns['detail_ammount']				= array('custom_render' => '$ #=number_format|{detail_ammount}|2=#',
																	'width' => '80px',
																	'title' => 'Importe',
																	'class' => 'text-right');
	
					$_columns['detail_info']				= array('hide' => TRUE);
					$_columns['id_receipt_detail']			= array('hide' => TRUE);
					$_columns['id_receipt']					= array('hide' => TRUE);
					$_columns['id_expense_type']			= array('hide' => TRUE);
					$_columns['id_period']					= array('hide' => TRUE);
					$_columns['id_property']				= array('hide' => TRUE);
					$_columns['id_issuing_account']			= array('hide' => TRUE);
					$_columns['id_receiving_account']		= array('hide' => TRUE);
					$_columns['detail_concept']				= array('hide' => TRUE);
					$_columns['detail_observations']		= array('hide' => TRUE);
					$_columns['detail_expire_date']			= array('hide' => TRUE);
					$_columns['detail_interest_since']		= array('hide' => TRUE);
					$_columns['detail_interest_rate']		= array('hide' => TRUE);
					$_columns['detail_original_ammount']	= array('hide' => TRUE);
					$_columns['detail_custom']				= array('hide' => TRUE);
					$_columns['detail_id_user_modifier']	= array('hide' => TRUE);
					$_columns['detail_id_detail_sibling']	= array('hide' => TRUE);
					$_columns['creation_date']				= array('hide' => TRUE);
					$_columns['modification_date']			= array('hide' => TRUE);
					$_columns['_checked']					= array('hide' => TRUE);
					$_columns['period_year']				= array('hide' => TRUE);
					$_columns['period_month']				= array('hide' => TRUE);
					$_columns['period_code']				= array('hide' => TRUE);
					$_columns['receipt_date']				= array('hide' => TRUE);
					$_columns['id_user_issuer']				= array('hide' => TRUE);
					$_columns['id_user_receiver']			= array('hide' => TRUE);
					$_columns['receipt_payed']				= array('hide' => TRUE);
					$_columns['receipt_pay_date']			= array('hide' => TRUE);
					$_columns['receipt_voided']				= array('hide' => TRUE);
					$_columns['receipt_description']		= array('hide' => TRUE);
					$_columns['receipt_observations']		= array('hide' => TRUE);
					$_columns['type_name']					= array('hide' => TRUE);
					$_columns['type_description']			= array('hide' => TRUE);
					$_columns['type_order']					= array('hide' => TRUE);
					$_columns['account_id_user_issuer']		= array('hide' => TRUE);
					$_columns['account_name_issuer']		= array('hide' => TRUE);
					$_columns['account_id_user_receiver']	= array('hide' => TRUE);
					$_columns['account_name_receiver']		= array('hide' => TRUE);
					$_columns['receipt_attachments']		= array('hide' => TRUE);
					$_columns['detail_attachments']			= array('hide' => TRUE);
					$_columns['sibling_receipt_attachments']= array('hide' => TRUE);
					$_columns['_payed']						= array('hide' => TRUE);

					$_options['create_entity'] 	 = FALSE;
					$_options['search'] 		 = FALSE;
					$_options['enable_search'] 	 = FALSE;
					$_options['enable_ipp'] 	 = FALSE;
					$_options['fixed_filters'] 	 = array(
											'id_expense_type' 			=> ET_IMPUESTO, // Son los imp o servicios
											'_payed' 		  			=> 0, // No pagados
											'detail_ammount>' 		  	=> 0, // Que tengan el importe cargado.
											'account_id_user_receiver IS NOT NULL'  => NULL, // Pero solo los detalles destinados a pagar, no a cobrar.
									);

					echo pachi_fullmanaged_table('vw_receipt_details', $_columns, $_options);
					unset($_columns);
					unset($_options);
				?>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4">
		<div class="portlet light">
			<h3 class="custom-h1">Expensas a pagar</h3>
			<div class="portlet-body">
				<?php
					unset($_columns);
					$_columns['detail_description']			= array('custom_render' => '<strong><a class="btn_new_coupon" data-id_user_receiver="{account_id_user_receiver}" data-filter_details="{id_receipt_detail}">{detail_description}</a></strong><br>
																						<small>{detail_info}</small>',
																	'title' => 'Detalle'

																					);
					$_columns['detail_ammount']				= array('custom_render' => '$ #=number_format|{detail_ammount}|2=#',
																	'width' => '80px',
																	'title' => 'Importe',
																	'class' => 'text-right');
	
					$_columns['detail_info']				= array('hide' => TRUE);
					$_columns['id_receipt_detail']			= array('hide' => TRUE);
					$_columns['id_receipt']					= array('hide' => TRUE);
					$_columns['id_expense_type']			= array('hide' => TRUE);
					$_columns['id_period']					= array('hide' => TRUE);
					$_columns['id_property']				= array('hide' => TRUE);
					$_columns['id_issuing_account']			= array('hide' => TRUE);
					$_columns['id_receiving_account']		= array('hide' => TRUE);
					$_columns['detail_concept']				= array('hide' => TRUE);
					$_columns['detail_observations']		= array('hide' => TRUE);
					$_columns['detail_expire_date']			= array('hide' => TRUE);
					$_columns['detail_interest_since']		= array('hide' => TRUE);
					$_columns['detail_interest_rate']		= array('hide' => TRUE);
					$_columns['detail_original_ammount']	= array('hide' => TRUE);
					$_columns['detail_custom']				= array('hide' => TRUE);
					$_columns['detail_id_user_modifier']	= array('hide' => TRUE);
					$_columns['detail_id_detail_sibling']	= array('hide' => TRUE);
					$_columns['creation_date']				= array('hide' => TRUE);
					$_columns['modification_date']			= array('hide' => TRUE);
					$_columns['_checked']					= array('hide' => TRUE);
					$_columns['period_year']				= array('hide' => TRUE);
					$_columns['period_month']				= array('hide' => TRUE);
					$_columns['period_code']				= array('hide' => TRUE);
					$_columns['receipt_date']				= array('hide' => TRUE);
					$_columns['id_user_issuer']				= array('hide' => TRUE);
					$_columns['id_user_receiver']			= array('hide' => TRUE);
					$_columns['receipt_payed']				= array('hide' => TRUE);
					$_columns['receipt_pay_date']			= array('hide' => TRUE);
					$_columns['receipt_voided']				= array('hide' => TRUE);
					$_columns['receipt_description']		= array('hide' => TRUE);
					$_columns['receipt_observations']		= array('hide' => TRUE);
					$_columns['type_name']					= array('hide' => TRUE);
					$_columns['type_description']			= array('hide' => TRUE);
					$_columns['type_order']					= array('hide' => TRUE);
					$_columns['account_id_user_issuer']		= array('hide' => TRUE);
					$_columns['account_name_issuer']		= array('hide' => TRUE);
					$_columns['account_id_user_receiver']	= array('hide' => TRUE);
					$_columns['account_name_receiver']		= array('hide' => TRUE);
					$_columns['receipt_attachments']		= array('hide' => TRUE);
					$_columns['detail_attachments']			= array('hide' => TRUE);
					$_columns['sibling_receipt_attachments']= array('hide' => TRUE);
					$_columns['_payed']						= array('hide' => TRUE);

					$_options['create_entity'] 	 = FALSE;
					$_options['search'] 		 = FALSE;
					$_options['enable_search'] 	 = FALSE;
					$_options['enable_ipp'] 	 = FALSE;
					$_options['fixed_filters'] 	 = array(
											'id_expense_type' 			=> ET_EXPENSA, // Son los imp o servicios
											'_payed' 		  			=> 0, // No pagados
											'detail_ammount>' 		  	=> 0, // Que tengan el importe cargado.
											'account_id_user_receiver'  => USER_PAYED, // Pero solo los detalles destinados a pagar, no a cobrar.
									);

					echo pachi_fullmanaged_table('vw_receipt_details', $_columns, $_options);
					unset($_columns);
					unset($_options);
				?>
			</div>
		</div>
	</div>
	<div class="col-xs-12 col-sm-4">
		<div class="portlet light">
			<h3 class="custom-h1">Mantenimientos</h3>
			<div class="portlet-body">
				<?php
					unset($_columns);
					$_columns['detail_description']			= array('custom_render' => '<strong><a class="btn_new_coupon" data-id_user_receiver="{account_id_user_receiver}" data-filter_details="{id_receipt_detail}">{detail_description}</a></strong><br>
																						<small>{detail_info}</small>',
																	'title' => 'Detalle'

																					);
					$_columns['detail_ammount']				= array('custom_render' => '$ #=number_format|{detail_ammount}|2=#',
																	'width' => '80px',
																	'title' => 'Importe',
																	'class' => 'text-right');
	
					$_columns['detail_info']				= array('hide' => TRUE);
					$_columns['id_receipt_detail']			= array('hide' => TRUE);
					$_columns['id_receipt']					= array('hide' => TRUE);
					$_columns['id_expense_type']			= array('hide' => TRUE);
					$_columns['id_period']					= array('hide' => TRUE);
					$_columns['id_property']				= array('hide' => TRUE);
					$_columns['id_issuing_account']			= array('hide' => TRUE);
					$_columns['id_receiving_account']		= array('hide' => TRUE);
					$_columns['detail_concept']				= array('hide' => TRUE);
					$_columns['detail_observations']		= array('hide' => TRUE);
					$_columns['detail_expire_date']			= array('hide' => TRUE);
					$_columns['detail_interest_since']		= array('hide' => TRUE);
					$_columns['detail_interest_rate']		= array('hide' => TRUE);
					$_columns['detail_original_ammount']	= array('hide' => TRUE);
					$_columns['detail_custom']				= array('hide' => TRUE);
					$_columns['detail_id_user_modifier']	= array('hide' => TRUE);
					$_columns['detail_id_detail_sibling']	= array('hide' => TRUE);
					$_columns['creation_date']				= array('hide' => TRUE);
					$_columns['modification_date']			= array('hide' => TRUE);
					$_columns['_checked']					= array('hide' => TRUE);
					$_columns['period_year']				= array('hide' => TRUE);
					$_columns['period_month']				= array('hide' => TRUE);
					$_columns['period_code']				= array('hide' => TRUE);
					$_columns['receipt_date']				= array('hide' => TRUE);
					$_columns['id_user_issuer']				= array('hide' => TRUE);
					$_columns['id_user_receiver']			= array('hide' => TRUE);
					$_columns['receipt_payed']				= array('hide' => TRUE);
					$_columns['receipt_pay_date']			= array('hide' => TRUE);
					$_columns['receipt_voided']				= array('hide' => TRUE);
					$_columns['receipt_description']		= array('hide' => TRUE);
					$_columns['receipt_observations']		= array('hide' => TRUE);
					$_columns['type_name']					= array('hide' => TRUE);
					$_columns['type_description']			= array('hide' => TRUE);
					$_columns['type_order']					= array('hide' => TRUE);
					$_columns['account_id_user_issuer']		= array('hide' => TRUE);
					$_columns['account_name_issuer']		= array('hide' => TRUE);
					$_columns['account_id_user_receiver']	= array('hide' => TRUE);
					$_columns['account_name_receiver']		= array('hide' => TRUE);
					$_columns['receipt_attachments']		= array('hide' => TRUE);
					$_columns['detail_attachments']			= array('hide' => TRUE);
					$_columns['sibling_receipt_attachments']= array('hide' => TRUE);
					$_columns['_payed']						= array('hide' => TRUE);

					$_options['create_entity'] 	 = FALSE;
					$_options['search'] 		 = FALSE;
					$_options['enable_search'] 	 = FALSE;
					$_options['enable_ipp'] 	 = FALSE;
					$_options['fixed_filters'] 	 = array(
											'id_expense_type' 			=> ET_MANTENIMIENTO, // Son los imp o servicios
											'_payed' 		  			=> 0, // No pagados
											'account_id_user_receiver IS NOT NULL'  => NULL, // Pero solo los detalles destinados a pagar, no a cobrar.
									);

					echo pachi_fullmanaged_table('vw_receipt_details', $_columns, $_options);
					unset($_columns);
					unset($_options);
				?>
			</div>
		</div>
	</div>
</div>