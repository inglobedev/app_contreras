<div class="portlet light">
	<div class="portlet-body">
		<?php 
	        $_columns['property_name']					= array();
	        $_columns['property_address']				= array();
	        $_columns['id_user_owner']					= array();
	        $_columns['owner_user_name']				= array();
	        $_columns['owner_user_lastname']			= array();
	        $_columns['id_maintenance']					= array();
	        $_columns['id_property']					= array();
	        $_columns['maintenance_name']				= array();
	        $_columns['maintenance_description']		= array();
	        $_columns['id_maintenance_quote']			= array();
	        $_columns['maintenance_completed']			= array();
	        $_columns['maintenance_owner_cost']			= array();
	        $_columns['maintenance_tenant_cost']		= array();
	        $_columns['maintenance_administrative_fee']	= array();
	        $_columns['maintenance_status']				= array();
	        $_columns['maintenance_observations']		= array();
	        $_columns['id_user_professional']			= array();
	        $_columns['quote_observations']				= array();
	        $_columns['quote_price']					= array();
	        $_columns['quote_attachments']				= array();
	        $_columns['quote_ready']					= array();
	        $_columns['professional_user_name']			= array();
	        $_columns['professional_user_lastname']		= array();
	        $_columns['professional_user_address']		= array();
	        $_columns['professional_user_email']		= array();
	        $_columns['professional_user_phone']		= array();
	        $_columns['professional_user_picture']		= array();
	        $_columns['_calc_quotes_ready']				= array();
	        $_columns['_calc_quotes']					= array();
	        $_columns['_avg_quote_price']				= array();
			
			echo pachi_fullmanaged_table('vw_maintenances', $_columns); ?>
	</div>
</div>