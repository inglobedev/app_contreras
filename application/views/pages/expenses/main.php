
<div class="col-md-3"></div>
<div class="col-md-6">
	<div class="portlet light">
	    <div class="portlet-body">
	    	<?php

	    		$consortium_form = array(
									'type_name',
									'type_description',
						        	);


		    	$columns['id_expense'] 			= array('hide' => TRUE); 
		    	$columns['id_expense_type'] 	= array('hide' => TRUE); 
		    	$columns['expense_description'] = array('hide' => TRUE); 
		    	$columns['expense_website'] 	= array('hide' => TRUE); 
		    	$columns['creation_date'] 		= array('hide' => TRUE); 
		    	$columns['modification_date']   = array('hide' => TRUE); 
		    	$columns['deletion_date']   	= array('hide' => TRUE); 
		    	$columns['expense_name']   		= array(); 
		    	$columns['type_name']   		= array('title' => 'Tipo de gasto');
		    	$columns['type_description'] 	= array('hide' => TRUE); 
		    	$columns['edit']   				= array('title' => '',
		    											'width' => '50px',
		    											'custom_render' => '<a title="Editar gasto de gasto" class="btn default btn-sm" href="/'.APP_IMPUESTOS.'/expense_view/{id_expense}"><i class="fa fa-pencil"></i></a>');

		    	$columns_expense['id_expense_type'] 	= array(  // Buscador
                                        'reference' => array( 	//'table' => 'expenses', // aca podria determinar otra tabla distinta para buscar.
                                                                'display' => 'type_name',   // Si bien por defecto muestra el _name, podria mostrar otra columna.
                                                                //'filter' => array('id_role' => ROLE_CONSORTIUM)
                                                                ),
                                                                // Modal de creacion.
                                        'modal' => array('columns'  => $consortium_form, 
                                                         'modal_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_expense_type'),
                                                         'button_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_expense_type')
                                                        ) // aca determino que columna es la que se muestra en el buscador.
                                    );
		    	$columns_expense['expense_name'] 		= array();

		    	$options['create_entity'] = array('button_title' 	=> ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_expense'),
												'table' 		=> 'expenses',
												'columns' 		=> $columns_expense,
											#	'target' 		=> '?action=new', // Si se especifica target, no se tiene en cuenta table y columns. Se redirige al target.
												);
	    		echo pachi_fullmanaged_table('vw_expenses', $columns, $options); ?>
	    </div>
	</div>
</div>
<div class="col-md-3"></div>