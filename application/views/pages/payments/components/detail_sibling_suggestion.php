<div class="col-md-6">
    <div class="form-group">
        <div class="mt-checkbox-list">
            <label class="mt-checkbox margin-none"> <?php echo $operation_type_label ?>
                <input type="hidden" name="has_sibling" value="0">
                <input name="has_sibling" type="checkbox" value="1" data-reveal="#sel-sibling-account-<?php echo $linker = uniqid('id') ?>" />
                <span></span>
            </label>
            <br>
            <div id="sel-sibling-account-<?php echo $linker ?>">
                <select name="id_sibling_account" id="id_sibling_account" data-placeholder="Buscar cuenta ..." class="form-control" data-pachi-search="<?php echo $_search_key_accounts ?>">
                </select>
            </div>
        </div>
    </div>
</div>

<input type="hidden" name="operation" value="<?php echo $operation_type ?>">

<script type="text/javascript">
    handle_checkbox_rules();
</script>