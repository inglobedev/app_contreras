<?php #debugger($contracts) ?>
<?php foreach ($contracts as $_kcontract => $_contract): ?>
<div class="note note-info">
	<h4 class="block padding-none"><?php echo $_contract['property_address'] ?></h4>
</div>

<?php


$owner = isset($_contract['id_user_owner']) ? $_contract['id_user_owner']: '';


 endforeach;

$owner = isset($owner) ? $owner: false;

 ?>

 <script type="text/javascript">
 console.log('leo-1');
 console.log('<?php echo $calltype ?>');
 console.log('<?php echo $owner ?>');
 var owner_js = "<?php echo $owner ?>";

 var calltype_js = "<?php echo $calltype ?>";
 //	console.log('--' + owner_js);

console.log(calltype_js+ '  //');

 	 if(!owner_js && calltype_js === 'Locatario'){
 	 	$('#btn_submit_coupon').hide();
 	 	$('#no_owner').show();
 	 	$('.has_owner').hide();
 	 	console.log('aca');
 	 }
 	 else{
 		$('#btn_submit_coupon').show();
 	 	$('#no_owner').hide();
 	 	$('.has_owner').show();
 	 	console.log('aqui');
 	 }
 </script>

<div id="no_owner" style="text-align: center;
    font-size: 16px;
    background: #ff000040;
    padding: 20px;
    color: #9b0202;
    font-weight: bold;
    border-left: 5px solid;">
	No se puede emitir cupones de pago ya que este inmueble no tiene un propietario asignado.
</div>
<div class="has_owner">
<div  class="table-scrollable" >
	<table class="table table-striped table-hover table-condensed">
	    <thead>
	        <tr>
	            <th style="width:40px" class="text-center"></th>
	            <th class="" style="width:auto;"> Concepto</th>
	            <th class="text-center" style="width:100px"> Periodo</th>
	            <th class="text-right" style="width:100px"> Monto</th>
	        </tr>
	    </thead>
	    <?php $details_grouped = array_group($details, 'id_expense_type') ?>
	    <tbody>
	    	<?php $_coupon_total = 0; ?>

	    	<?php foreach ($details_grouped as $_id_expense_type => $_details): ?>
	    		<tr>
	    			<td colspan="100%"><i class="strong"><?php echo $_details[0]['type_name'] ?></i></td>
	    		</tr>
		    	<?php foreach ($_details as $_kdetail => $_detail): ?>
			    	<?php 
			    		// Si la cuenta que recibe el dinero, es la misma que recibe el cupon, 
			    		$_detail_modifier = ($_detail['id_receiving_account'] == $id_account) ? '-' : NULL;
			    	?>
		        	<tr class="tr-detail">
			            <td class="text-center">
			            	<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline">
			            		<?php if ($_detail['_checked']): ?>
			            			<input type="checkbox" class="checkboxes detail-check" data-id_receipt_detail="<?php echo $_detail['id_receipt_detail'] ?>" id="receipt_detail_<?php echo $_detail['id_receipt_detail'] ?>" checked name="receipt_detail[<?php echo $_detail['id_receipt_detail'] ?>]" value="1">
		        					<?php $_coupon_total += ($_detail['id_receiving_account'] == $id_account) ? $_detail['detail_ammount']*-1 : $_detail['detail_ammount']; ?>
			            		<?php else: ?>
			            			<input type="checkbox" class="checkboxes detail-check" data-id_receipt_detail="<?php echo $_detail['id_receipt_detail'] ?>" id="receipt_detail_<?php echo $_detail['id_receipt_detail'] ?>" name="receipt_detail[<?php echo $_detail['id_receipt_detail'] ?>]" value="1">
			            		<?php endif ?>
			            		<span></span>
			            	</label>
			            </td>
						<td><label for="receipt_detail_<?php echo $_detail['id_receipt_detail'] ?>" title="<?php echo $_detail['detail_info'] ?>">
									
								<?php // Modal de edicion de adjuntos
									unset($columns);
									$columns['detail_attachments'] = array();
									$_attachment_modal = pachi_modal('receipt_details', $columns, $_detail['id_receipt_detail'], $_data = array(), 'Adjuntos de: ' . $_detail['detail_description'], 'default');
									$_attachment_count = count(clean_explode(',', $_detail['detail_attachments']));
								 ?>

								<?php echo $_detail['detail_description'] ?> <span class="detail_attachments"><span class="ammount"></span><a href="javascript:;" data-pachi-modal="<?php echo $_attachment_modal ?>" class="btn btn-xs btn-default btn-link" title="<?php echo $_attachment_count ?> adjuntos."><?php echo ($_attachment_count) ? $_attachment_count : NULL; ?>  <i class="fa fa-paperclip"></i></a></span>
								<?php if ($_detail['detail_info']): ?>
									<br>
									<small><small><?php echo $_detail['detail_info'] ?></small></small>
								<?php endif ?>
								<br>
								<?php if ($_detail['detail_observations']): ?>
									<small class="text-muted"><?php echo $_detail['detail_observations'] ?></small>
								<?php endif ?>
							</label>
							<?php if ($_detail['detail_custom']): ?>
								<a class="btn btn-xs btn-link text-danger btn_delete_receipt_detail" data-confirmation="¿Estás seguro?" data-id_receipt_detail="<?php echo $_detail['id_receipt_detail'] ?>" href="javascript:;"><i class="fa fa-trash"></i></a>
							<?php endif ?>
						</td>        		
						<td class="text-center"><?php echo $_detail['period_month'] ?>/<?php echo $_detail['period_year'] ?></td>
			            <td class="text-right"> $ <span class="detail-ammount-full"><?php echo $_detail_modifier ?> <span class="detail-ammount" data-id_receipt_detail="<?php echo $_detail['id_receipt_detail'] ?>"><?php echo number_format($_detail['detail_ammount'], 2) ?></span></span></td>
		        	</tr>
		    	<?php endforeach ?>

	    	<?php endforeach ?>

	    	<?php if (!$filtered): ?>
	        <tr class="info strong" id="row_new_detail">
	            <td class="text-center"></td>
	            <td><a href="javascript:;" class="btn_new_detail"  data-counterpart="<?php echo $calltype ?>" data-id_user="<?php echo $id_user ?>">Agregar nuevo concepto</a></td>
	            <td></td>
	            <td></td>
	        </tr>
	    	<?php endif ?>
	    </tbody>
	</table>
</div>

<div class="col-md-12 padding-none has_owner ">
    <h4 class="text-right"><b>Total: $ <span class="coupon-total"><?php echo number_format($_coupon_total, 2) ?></b></h4>
	<div class="row">
		<div class="col-md-6">
			<div class="form-group">
				<label class="control-label">Observaciones</label>
				<textarea name="receipt_observations" class="receipt_observations form-control input-sm"></textarea>
			</div>
		</div>
		<div class="col-md-12 hidden">
			<?php echo input_files('receipt_attachments', NULL, 'Adjuntos', 10, ALL_EXTENSIONS, 1) ?>
		</div>
	</div>
</div>

</div>