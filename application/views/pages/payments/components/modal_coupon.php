<div class="modal fade" id="modal_coupon" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog modal-lg">
        <form class="frm_coupon" id="frm_coupon">
            <div class="modal-content ">
                <div class="modal-header">
                    <h3 class="custom-h1"><?php echo $coupon_title ?></h3>
                </div>
                <div class="modal-body"> 
                	<div class="row">
                        <div class="col-md-12">
                            <div class="portlet-body">
                                <div class="row">
                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label">Emisor</label>
                                            <input type="text" disabled="" class="form-control" value="<?php echo get_user_name($this->session->userdata('id_user')) ?>" />
                                            <input type="hidden" name="id_user_issuer" value="<?php echo $this->session->userdata('id_user') ?>">
                                        </div> 
                                    </div>

                                    <?php 
                                    	// Buscador de la contraparte.
                                        $_table 					= 'users'; // Tabla o vista en la que trabaja
                                        $_column_to_save 			= 'id_user'; // nombre de la columna a guardar el valor
                                        $_column_to_display 		= 'user_name'; // nombre de la columna a mostrar
                                        
                                        if (!empty($roles))
                                        	$_fixed_filters 			= array('id_role IN ('.$roles.')' => NULL); // filtros.
                                        else
                                        	$_fixed_filters 			= FALSE;
                                        
                                        $_custom_render_to_display 	= '{user_name}, {user_lastname}'; // Opcional, sobreescribe a _column_to_display, usa {variables}.
                                        
                                        $_search_key = pachi_search($_table, $_column_to_save, $_column_to_display, $_fixed_filters, $_custom_render_to_display);
                                        // coloar a un select, data-pachi-search="$_search_key"
                                    ?>

                                    <div class="col-md-6 col-sm-6">
                                        <div class="form-group">
                                            <label class="control-label"><?php echo $counterpart_name ?></label>
                                            <select name="id_user_receiver" id="id_user_receiver" data-type="<?php echo $counterpart_name ?>" data-placeholder="Buscar contraparte" class="form-control" data-pachi-search="<?php echo $_search_key ?>" <?php if ($filtered) echo 'disabled=""' ?>>
                                            <?php if (!empty($id_user_receiver)): ?>
                                            	<option value="<?php echo $id_user_receiver ?>" selected><?php echo get_user_name($id_user_receiver) ?></option>
                                            <?php endif ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-12" id="table-coupon-details">
                                        <div class="table-scrollable" >
                                        	<h3 class="text-center innerB">Seleccione un <?php echo $counterpart_name ?> para comenzar ... </h3>
                                        </div>
                                    </div>
                                    <input type="hidden" id="filter_details" name="filter_details" value="<?php echo @$filter_details ?>">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Cancelar </button>
                    <button type="submit" class="btn btn-primary hidden" id="btn_submit_coupon"> Confirmar </button>
                </div>
            </div>
        </form>
    </div>
</div>