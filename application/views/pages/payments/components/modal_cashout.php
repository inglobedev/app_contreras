<div class="modal fade" id="modal_cashout" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog">
        <form class="frm_cashout" id="frm_cashout">
            <div class="modal-content ">
                <div class="modal-header">
                    <h3 class="custom-h1">Extracción</h3>
                </div>
                <div class="modal-body"> 
                    <div class="form-group">
                        <label class="control-label">Extrae</label>
                        <input type="text" disabled="" class="form-control" value="<?php echo get_user_name($this->session->userdata('id_user')) ?>" />
                        <input type="hidden" name="id_user" value="<?php echo $this->session->userdata('id_user') ?>">
                    </div> 
                    <div class="form-group">
                        <label class="control-label">Caja</label>
                        <input type="text" disabled="" class="form-control" value="Administracion" />
                        <input type="hidden" name="id_account" value="1">
                    </div> 
	                <div class="form-group">
	                    <label class="control-label">Importe</label>
	                    <input type="text" name="ammount" class="form-control pachi-mask-money" value="0"/>
	                </div> 
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Cancelar </button>
                    <button type="submit" class="btn btn-primary" id="btn_submit_cashout"> Confirmar </button>
                </div>
            </div>
        </form>
</div>