<div class="modal fade" id="modal_detail" tabindex="-1" role="basic" aria-hidden="true" data-backdrop="static" data-keyboard="false">
<div class="modal-dialog modal-lg">
        <form class="frm_new_detail" id="frm_new_detail" >
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="custom-h1">Nuevo concepto</h3>
                </div>
                <div class="modal-body"> 


                    
                	<div class="row">
                        <div class="col-md-12">
                            <div class="portlet-body">
                                <div class="row">

                                    <?php 
                                    	// Buscador de la contraparte.
                                        $_table 					= 'expense_types'; // Tabla o vista en la que trabaja
                                        $_column_to_save 			= 'id_expense_type'; // nombre de la columna a guardar el valor
                                        $_column_to_display 		= 'type_name'; // nombre de la columna a mostrar
                                        if($calltype == "Propietario"){
                                             $_fixed_filters             = array("id_expense_type != 2" => NULL); // filtros
                                            $_search_key_tipos = pachi_search($_table, $_column_to_save, $_column_to_display, $_fixed_filters);
                                        }  
                                        else{
                                             $_search_key_tipos = pachi_search($_table, $_column_to_save, $_column_to_display);
                                        }                                      
                                       
                                        // coloar a un select, data-pachi-search="$_search_key"
                                    ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Tipo de Concepto</label>
                                            <select required name="id_expense_type" id="id_expense_type" data-placeholder="Buscar tipos ..." class="form-control" data-pachi-search="<?php echo $_search_key_tipos ?>">
                                            </select>
                                        </div>
                                    </div>

                                    <?php 
                                    	// Buscador de cuentas.
                                        $_table 					= 'vw_accounts'; // Tabla o vista en la que trabaja
                                        $_column_to_save 			= 'id_account'; // nombre de la columna a guardar el valor
                                        $_column_to_display 		= '_account_description'; // nombre de la columna a mostrar
                                        $_fixed_filters 			= array("id_user_owner IS NULL OR id_user_owner = $id_user" => NULL); // filtros.
                                        
                                        $_search_key_accounts = pachi_search($_table, $_column_to_save, $_column_to_display, $_fixed_filters);
                                        // coloar a un select, data-pachi-search="$_search_key"
                                    ?>
                                    <div class="col-md-9">
                                    	<div class="row">
		                                    <div class="col-md-6">
		                                        <div class="form-group">
		                                            <label class="control-label">Cuenta Origen <?php echo $this->lang->line('pachi_lbl_required') ?></label>
		                                            <select required name="id_issuing_account" id="id_issuing_account" data-placeholder="Buscar cuenta ..." class="form-control" data-pachi-search="<?php echo $_search_key_accounts ?>">
		                                            </select>
		                                        </div>
		                                    </div>

		                                    <div class="col-md-6">
		                                        <div class="form-group">
		                                            <label class="control-label">Cuenta Destino <?php echo $this->lang->line('pachi_lbl_required') ?></label>
		                                            <select required name="id_receiving_account" id="id_receiving_account" data-placeholder="Buscar cuenta ..." class="form-control" data-pachi-search="<?php echo $_search_key_accounts ?>">
		                                            </select>
		                                        </div>
		                                    </div>
                                    	</div>
                                    </div>
                                  
                                    <?php 
                                    	// Buscador de la contraparte.
                                        $_table 					= '_periods'; // Tabla o vista en la que trabaja
                                        $_column_to_save 			= 'id_period'; // nombre de la columna a guardar el valor
                                        $_column_to_display 		= 'period_code'; // nombre de la columna a mostrar
                                        $_custom_render 			= '{period_month}/{period_year}'; // nombre de la columna a mostrar
                                        
                                        $_search_key_periodo = pachi_search($_table, $_column_to_save, $_column_to_display, FALSE, $_custom_render);
                                        // coloar a un select, data-pachi-search="$_search_key"
                                    ?>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Periodo</label>
                                            <select required name="id_period" id="id_period" data-placeholder="Buscar periodo ..." class="form-control" data-pachi-search="<?php echo $_search_key_periodo ?>">
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Importe <?php echo $this->lang->line('pachi_lbl_required') ?></label>
                                            <input required type="text" class="form-control pachi-mask-money" name="detail_ammount" placeholder="Escriba aquí ..." data-min="0" data-max="99999999999" value="0.00">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Fecha de Venc.</label>
                                            <input type="text" class="form-control pachi-date-picker" name="detail_expire_date" placeholder="Seleccione una fecha ...">
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="control-label">Interés diario <?php echo $this->lang->line('pachi_lbl_required') ?> <small>(1% = 1)</small></label>
                                            <input required type="text" class="form-control pachi-mask-number decimal" name="detail_interest_rate" placeholder="Escriba aquí ..." data-min="0" data-max="100" value="0">
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Concepto <?php echo $this->lang->line('pachi_lbl_required') ?></label>
                                            <input required type="text" class="form-control" name="detail_description" placeholder="Escriba aquí ...">
                                        </div>
                                    </div>

                                    <div class="col-md-12 col-sm-12">
                                        <div class="form-group">
                                            <label class="control-label">Observaciones</label>
                                            <textarea class="form-control" name="detail_observations" placeholder="Escriba aquí ..."></textarea>
                                        </div>
                                    </div>

                                    <div id="sibling_detail">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"> Cancelar </button>
                    <button type="submit" id="btn_submit_detail" class="btn btn-primary hidden"> Confirmar </button>
                </div>

            </div>
        </form>
    </div>
</div>