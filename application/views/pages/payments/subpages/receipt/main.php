<?php $_receiver = $this->pachi->get('users', $receipt['id_user_receiver']);
      $contracts = array();
?>
<?php if ($_receiver['id_role'] == ROLE_TENANT): ?>
    <?php 
        $_contracts_filter['id_user_tenant']    = $_receiver['id_user'];
        $_contracts_filter['_is_active']        = 1;
        $contracts = $this->pachi->fetch('vw_contracts', $_contracts_filter, 0, 100);
    ?>
<?php endif ?>

<style>


@media print {

    .print {font-size: 14px !important};
   .scroll-to-top>i.icon-arrow-up{display: none !important;}
  
}


</style>

<?php $_receipt_total = 0; ?>
<?php foreach ($details as $_kdetail => $_detail): ?>
    <?php $_receipt_total += ($_detail['id_receiving_account'] == $this->m_accounts->get_user_account($receipt['id_user_receiver'])) ? $_detail['detail_ammount']  * -1 : $_detail['detail_ammount']; ?>
<?php endforeach ?>

<div class="portlet light print">
    <div class="portlet-body">
        <div class="invoice">
            <div class="row invoice-logo">
                <div class="col-xs-6 invoice-logo-space">
                    <img src="<?php echo base_url('assets/app/img/logo.png') ?>" class="img-responsive" alt="" width="150" /> 
                </div>
                <div class="col-xs-6">
                    <p class="margin-none" style="font-size: 16px;"> 
                    	<span class="print" style="font-size:24px">Recibo #<?php echo str_pad($receipt['id_receipt'], 8, '0', STR_PAD_LEFT) ?> 
                        <span class="muted print" style="
    font-size: 12px;
    color: #999;
"> Generado a las <?php echo print_time($receipt['receipt_pay_date']) ?></span>
                    </p>
                </div>
            </div>
            <hr/>

            <div class="row">
                <div class="col-xs-12">
                    <div class="print" style="text-align: right; font-size: 18px; margin-bottom: 15px;">Córdoba, <?php echo print_datetime($receipt['receipt_pay_date'], FALSE, FALSE, 'd F Y') ?></div>
                    <div class="print" style="line-height: 24px; font-size: 18px;"><?php if ($_receipt_total > 0 AND !in_array($_receiver['id_role'], array(ROLE_TENANT))): ?>
                        Recibí de Martín Contreras
                    <?php else: ?>
                        Recibí de <strong><?php echo $_receiver['user_lastname'] ?> <?php echo $_receiver['user_name'] ?></strong>,<?php endif ?> la suma de $ <span class="recepit-total"><?php echo number_format($_receipt_total, 2) ?></span> (<span class="recepit-total-letters"><?php echo mb_strtoupper($this->cifrasenletras->convertirNumeroEnLetras($_receipt_total, 2)) ?></span> PESOS) por los conceptos que se detallan a continuación correspondientes al inmueble ubicado en:
                          <strong><?php foreach ($contracts as $_kcontract => $_contract): ?>
                            <?php echo $_contract['property_address'] ?>
                    <?php endforeach ?>
                </strong>
                </div>
                </div>
                
            </div>
            <?php if (count($contracts)): ?>
           
            <?php endif ?>
            <div class="row">
                <div class="col-xs-12">
                    <table class="table table-striped table-hover print">
                        <thead>
                            <tr>
                               
                                <th style="width: 250px;"> Concepto </th>
                                <th class=""> Observaciones </th>
                                <th style="width: 90px;" class="text-center"> Periodo </th>
                                <th style="width: 100px;" class="text-right"> Importe </th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php $details_grouped = array_group($details, 'id_expense_type') ?>

                             <?php foreach ($details_grouped as $_id_expense_type => $_details): ?>
                <tr>
                    <td colspan="100%"><i class="strong"><?php echo $_details[0]['type_name'] ?></i></td>
                </tr>
                <?php $_administrative_ammount = 0; ?>
             <?php $_administrative_detail = NULL; ?>
                <?php foreach ($_details as $_kdetail => $_detail): ?>
                   
                   <?php $_detail['detail_ammount'] = (in_array($_receiver['id_role'], array(ROLE_TENANT))) ? $_detail['detail_ammount'] : $_detail['detail_ammount'] * -1 ?>
                                <?php // Todos los conceptos de comision administativa los sumamos y los pasamos para el final. ?>
                                
                                <?php if ($_detail['id_expense_type'] == '1'): ?>
                                    <?php 
                                        $_administrative_ammount += ($_detail['id_receiving_account'] == $this->m_accounts->get_user_account($receipt['id_user_receiver'])) ? $_detail['detail_ammount']  * -1 : $_detail['detail_ammount'];
                                        $_administrative_detail .= ($_detail['id_receiving_account'] == $this->m_accounts->get_user_account($receipt['id_user_receiver'])) ? '- ' . $_detail['detail_info'] . ' '  : '+ '.$_detail['detail_info'] . ' ';
                                        continue;
                                    ?>
                                <?php endif ?>
                    <tr class="tr-detail">
                       
                        <td><label for="receipt_detail_<?php echo $_detail['id_receipt_detail'] ?>" title="<?php echo $_detail['detail_info'] ?>">
                               <?php echo $_detail['detail_description'] ?>
                               </label>
                        </td>               
                        <td class="text-center print">
                                 <?php if ($_detail['detail_observations']): ?>
                                    <small class="text-muted"><?php echo $_detail['detail_observations'] ?></small>
                                <?php endif ?></td>
                        <td class="text-center print"><?php echo $_detail['period_month'] ?>/<?php echo $_detail['period_year'] ?></td>
                        <td class="text-right"> <span class="print detail-dummy-money"><?php echo ($_detail['id_receiving_account'] == $this->m_accounts->get_user_account($receipt['id_user_receiver'])) ? number_format($_detail['detail_ammount']  * -1, 2) : number_format($_detail['detail_ammount'], 2) ?> </span> </td>
                    </tr>
                <?php endforeach ?>

            <?php endforeach ?>
              <?php if ($_administrative_ammount != 0): ?>
                                <tr class="tr-detail">
                                    <td class="hidden-print"> <a href="javascript:;" class="btn_remove_row"><i class="fa fa-times"></i></a> </td>
                                    <td> <span class="print detail-dummy-text">Gastos Administrativos</span> </td>
                                    <td> <span class="print detail-dummy-text"><small><small><?php echo $_administrative_detail ?></small></small></span> </td>
                                    <td class="text-center"></td>
                                    <td class="text-right"> <span class=" print detail-dummy-money"><?php echo number_format($_administrative_ammount, 2) ?> </span> </td>
                                </tr>
                            <?php endif ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row print">
                <div class="col-xs-8">
                     <h5>Observaciones:</h5>
                    <p><span class="detail-dummy-text"><?php
                     echo $receipt['receipt_observations'] ?></span></p>
                </div>
                <div class="col-xs-4 invoice-block">
                    <ul class="list-unstyled amounts">
                        <li class="print">
                            <h3 class="print" >Total</h3> <h4>$ <span class=" print recepit-total" id="total"><?php echo number_format($_receipt_total, 2) ?></span> </h4>
                        </li>
                    </ul>
                    <br/>
                    <a class="btn btn-lg blue hidden-print margin-bottom-5" onclick="javascript:window.print();"> Imprimir
                        <i class="fa fa-print"></i>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>                   