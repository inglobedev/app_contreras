<?php $cancel_url = base_url(APP_PROPIEDADES);  ?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="profile-sidebar col-sm-4" style="width: 33.33%;margin-right: 0;">
                <div class="portlet light profile-sidebar-portlet ">
                    <div class="profile-userpic">
                    </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> 
                            <?php echo $property['property_name'] ?> 
                        <!--<?php if ($contract['id_contract']): ?>
                                <small>[archivado]</small>
                            <?php endif ?>
                        -->
                        </div>
                       
                        <div class="profile-usertitle-job">  <?php echo $property['property_address'] ?> <br>
                          <small>Contrato Número: <?php echo $contract['id_contract'] ?></small> </div>
                    </div>
                    <?php if (check_permissions('app', 'manage_contracts')): ?>
                    <!--<div class="profile-userbuttons">
                        <div class="btn-group">
                            <button type="button" class="btn btn-circle blue-madison dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="1000" data-close-others="true" aria-expanded="true"> Acciones
                                <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                            	<?php if (empty($property['id_contract'])): ?>
                                <li>
                                    <a href="javascript:;"> Nuevo Contrato </a>
                                </li>
                                <li>
                                    <a href="javascript:;"> Archivar </a>
                                </li>
                            	<?php else: ?>
                                <li>
                                    <a href="javascript:;"> Renovar Contrato </a>
                                </li>
                                <li>
                                    <a href="javascript:;"> Finalizar Contrato </a>
                                </li>
                            	<?php endif ?>
                                <?php if ($property['property_archived']): ?>
                                <li>
                                    <a href="javascript:;" class="btn_restore_property" data-id_property="<?php echo $property['id_property'] ?>"> Restaurar Inmueble </a>
                                </li>
                                <?php else: ?> 
                                <li>
                                    <a href="javascript:;" class="btn_archive_property" data-id_property="<?php echo $property['id_property'] ?>"> Archivar Inmueble </a>
                                </li>
                                <?php endif ?>
                            </ul>
                        </div>
                    </div>
                     -->
                    <?php endif ?>
                   <!-- <div class="profile-usermenu padding-none">
                        <ul class="nav">
                            <li class="active">
                                <a href="#tab_datos" data-toggle="tab"><i class="icon-home"></i>Información general</a>
                            </li>
                            <li>
                                <a href="#tab_gastos" data-toggle="tab"><i class="icon-pie-chart"></i>Impuestos y servicios</a>
                            </li>
                            <?php if ($this->session->userdata('id_role') != ROLE_OWNER): ?>
                            <li>
                                <a href="#tab_mantenimientos" data-toggle="tab"><i class="icon-briefcase"></i>Mantenimiento</a>
                            </li>
                            <?php endif ?>
                            <li class="hidden">
                                <a href="#tab_cuenta" data-toggle="tab"><i class="icon-credit-card"></i>Cuenta corriente</a>
                            </li>
                            <li>
                                <a href="#tab_comprobantes" data-toggle="tab"><i class="icon-docs"></i>Comprobantes</a>
                            </li>
                             <li>
                                <a href="#tab_contratos" data-toggle="tab"><i class="icon-docs"></i>Contratos</a>
                            </li>
                            <?php if ($this->session->userdata('id_role') != ROLE_OWNER): ?>
                            <li>
                                <a href="#tab_config" data-toggle="tab"><i class="icon-wrench"></i>Configuracion</a>
                            </li>                               
                            <?php endif ?>
                        </ul>
                    </div>
                     -->
                </div>
            </div>
            <div class="profile-content col-sm-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_datos">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Visión general</span>
                                        </div>
                                      <!-- <div class="actions hidden">
                                            <a href="#mod_recibo_agregar" data-toggle="modal" class="btn btn-primary ">
                                                <i class="fa fa-plus"></i> Registrar cobro
                                            </a>
                                        </div>
                                    -->
                                    </div>
                                    <div class="portlet-body">
                                    	<?php 
                                    		// debugger($property)
                                    		# Falta completar datos
                                    	?>
                                         <div class="row">
                                            <div class="col-md-3 custom-box-property">
                                                <h5>Inicio de Contrato</h5>
                                                <h5 class="font-blue-sharp">
                                                    <span data-counter="counterup"><?php echo print_date($contract['contract_date_start']) ?></span>
                                                </h5>
                                            </div>

                                            <div class="col-md-3 custom-box-property">
                                                <h5>Fin de Contrato</h5>
                                                <h5 class="font-blue-sharp">
                                                  <span data-counter="counterup"><?php echo print_date($contract['contract_date_end']) ?></span>
                                                </h5>
                                            </div>

                                            <div class="col-md-6 custom-box-property">
                                                <h5>Precio Final del Contrato</h5>
                                                <h5 class="font-blue-sharp">
                                                  <span data-counter="counterup">$ <?php echo $contract['contract_rent_price']; echo $contract['id_user_owner']?></span>
                                                </h5>
                                            </div>
                                        </div>

                                       <div class="row">
                                          
                                          
                                        
                                            <?php if (!empty($property['contract_rent_price'])): ?>
                                                <div class="col-md-3 custom-box-property">
                                                    <h5 class="font-green-sharp">
                                                        <span data-counter="counterup">Alquiler</span>
                                                    </h5>
                                                    <small>$<?php echo number_format($property['contract_rent_price'], 2) ?></small>
                                                </div>
                                            <?php endif ?>
                                              <div class="col-md-3 custom-box-property">
                                                <h5>
                                                    <span data-counter="counterup">Monto Alquiler Actual</span>
                                                </h5>

                                                 <h5 class="font-blue-sharp">
                                                  <span data-counter="counterup">
                                                    <?php 
                                                        $currDate = new DateTime();
                                                        $activeBon = [];
                                                        $ctctPrice = $contract['contract_rent_price'];
                                                        foreach($bonifications as $k_bonification => $bonifcation_): 
                                                            $bonDateBegin = new DateTime($bonifcation_['bonification_date_start']);
                                                            $bonDateEnd  = new DateTime($bonifcation_['bonification_date_end']);
                                                            
                                                            if (
                                                              $currDate->getTimestamp() > $bonDateBegin->getTimestamp() && 
                                                              $currDate->getTimestamp() < $bonDateEnd->getTimestamp()){
                                                                $currRent = $ctctPrice -  $bonifcation_['bonification_rate'];
                                                                echo '$ '.number_format($currRent, 2);
                                                                $activeBon[] = $bonifcation_['id_bonification'];
                                                                break;
                                                            }

                                                       endforeach;

                                                       if(empty($activeBon) ){
                                                         echo "No hay Periodos Activos";
                                                       }
                                                    ?>
                                                </span>
                                                </h5>
                                            </div>
                                            <div class="col-md-3 custom-box-property">
                                                <h5 class="font-red-haze">
                                                    <span data-counter="counterup">Gastos Administrativos</span>
                                                </h5>
                                                <small>$<?php echo number_format($contract['contract_commission'], 2) ?></small>
                                            </div>
                                        </div>
                                    </div>
                                   <!-- removed portlet-body -->
                                   
                                </div>

                                 <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Bonificaciones</span>
                                        </div>
                                      <!-- <div class="actions hidden">
                                            <a href="#mod_recibo_agregar" data-toggle="modal" class="btn btn-primary ">
                                                <i class="fa fa-plus"></i> Registrar cobro
                                            </a>
                                        </div>
                                    -->
                                    </div>
                                    <div class="portlet-body compact">

                                        <div class="row">
                                         <?php foreach($bonifications as $k_bonification => $bonifcation_): 
                                            $bonId = $bonifcation_['id_bonification'];
                                            if(in_array($bonId, $activeBon)){ ?>
                                                <div class="col-md-12 custom-box-property active"> 
                                            <?php 
                                                
                                            }
                                            else{ ?>
                                                 <div class="col-md-12 custom-box-property"> 
                                                     <?php 
                                            }

                                            ?>
                                                    <div class="col-md-8">
                                                        <h5>Periodo</h5>
                                                        <h5 class="font-blue-sharp">
                                                            <span data-counter="counterup">
                                                            <?php 

                                                            echo print_date($bonifcation_['bonification_date_start']) ?> - 
                                                             <?php echo print_date($bonifcation_['bonification_date_end']) ?>
                                                            </span>
                                                        </h5>
                                                    </div>
                                                     <div class="col-md-4">
                                                        <h5>Monto</h5>
                                                        <h5 class="font-blue-sharp">
                                                            <span data-counter="counterup" style="font-size: 10px; color: #ccc;">
                                                                $<?php echo number_format($ctctPrice, 2) ?> -
                                                                $<?php echo number_format($bonifcation_['bonification_rate'], 2) ?></span><br/>
                                                                <span data-counter="counterup" >
                                                                $<?php 
                                                                $_alquilerFinal = $ctctPrice - $bonifcation_['bonification_rate'];
                                                                echo number_format($_alquilerFinal, 2) ?> -
                                                                </span>

                                                        </h5>
                                                    </div>
                                                </div>
                                            <?php endforeach ?>

                                        </div>

                                     
                                    </div>
                                   <!-- removed portlet-body -->
                                   
                                </div>

                            </div>

                            <div class="tab-pane" id="tab_gastos">
                                <?php $this->load->view('pages/properties/subpages/properties_editor/tab_taxes') ?>
                            </div>

                            <div class="tab-pane" id="tab_mantenimientos">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Mantenimientos</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php $this->load->view('pages/properties/subpages/properties_editor/tab_maintenances') ?>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_comprobantes">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Comprobantes</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php $this->load->view('pages/properties/subpages/properties_editor/tab_comprobantes') ?>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_cuenta">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Cuenta corriente</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="dropdown">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> Cuentas
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="#admin" tabindex="-1" data-toggle="tab"> <?php echo ucfirst($this->lang->line('lbl_administration')) ?></a>
                                                    </li>
                                                    <li>
                                                        <a href="#inqui" tabindex="-1" data-toggle="tab"> <?php echo ucfirst($this->lang->line('entity_tenant')) ?> </a>
                                                    </li>
                                                    <li>
                                                        <a href="#prop" tabindex="-1" data-toggle="tab"> <?php echo ucfirst($this->lang->line('entity_owner')) ?> </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                   
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_config">
                                <?php $this->load->view('pages/properties/subpages/properties_editor/tab_configuration') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>