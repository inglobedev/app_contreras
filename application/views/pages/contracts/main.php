<div class="row">

    <div class="col-md-12">
        <?php 

        
         $_columns['property_name']            = array(     'title' => 'Propiedad',
                                                            'custom_render' => '<a href="'
                                                                                .base_url(APP_PROPIEDADES).
                                                                                'view/{id_property}">
                                                                                 #=get_property_name|{id_property}=# </a>',

                                                        );
         
         $_columns['id_contract']               = array(    'title' => 'Contrato #',
                                                            'custom_render' => 
                                                                                '<a href="'
                                                                                .base_url(APP_CONTRACTS).
                                                                                'view/{id_contract}">{id_contract}</a>',

                                                                                //'<a href="#"> {id_contract} </a>'
                                                        );

         $_columns['_status']                   = array(    'custom_render' => '{if [contract_voided] == 0} Activo
                                                                                    {/if}
                                                                                    {if [contract_voided] == 1}
                                                                                        Finalizado
                                                                                    {/if}
                                                                            ',
                                                            'width' => '100',
                                                            'title' => 'Estado'
                                                        );
         $_columns['_contact_period']            = array(   'custom_render' => '<small class="text-muted">
                                                                                            #=print_date|{contract_date_start}=# al #=print_date|{contract_date_end}=#
                                                                                        </small>
                                                                            ',
                                                            'width' => '160',
                                                            'title' => 'Periodo'
                                                        );
         $_columns['id_user_tenant']            = array(     'custom_render' => '<a href="'
                                                                                .base_url(APP_BACKEND).
                                                                                'view_user/{id_user_tenant}">
                                                                                 #=get_user_name|{id_user_tenant}=# </a>',

                                                        );
       
         //$_columns['contract_commission']           = array('title' => 'Gastos Adm. ',);
         $_columns['contract_rent_price']           = array('title' => 'Precio ',);
         //$_columns['contract_interest']             = array();


        
         $_columns['_options']                      = array(    'title' => 'Acciones ',
                                                            'width' => '130px',
                                                            'class' => 'text-center',
                                                            'custom_render' => '<a href="'
                                                                                .base_url(APP_CONTRACTS).
                                                                                'view/{id_contract}">
                                                                                 ver contrato </a>'
                                                        ); 
         $_columns['contract_interest']             = array('hide' => TRUE);
         $_columns['contract_commission']           = array('hide' => TRUE);
         $_columns['contract_date_start']           = array('hide' => TRUE);
         $_columns['contract_date_end']             = array('hide' => TRUE);
         $_columns['id_property']                   = array('hide' => TRUE);
         $_columns['deletion_date']                 = array('hide' => TRUE);
         $_columns['modification_date']             = array('hide' => TRUE);
         $_columns['contract_observations']         = array('hide' => TRUE);
         $_columns['contract_expenses_attachments'] = array('hide' => TRUE);
         $_columns['contract_tenant_attachments']   = array('hide' => TRUE);
         $_columns['contract_expenses_check']       = array('hide' => TRUE);
         $_columns['contract_electricity_check']    = array('hide' => TRUE);
         $_columns['contract_gas_check']            = array('hide' => TRUE);
         $_columns['contract_voided']               = array('hide' => TRUE);
         $_columns['contract_archived']             = array('hide' => TRUE);
         $_columns['contract_observations']         = array('hide' => TRUE);
         $_columns['creation_date']                 = array('hide' => TRUE);
         $_columns['contract_payday_start']         = array('hide' => TRUE);
         $_columns['contract_payday_limit']         = array('hide' => TRUE);

        ?>

        <?php 
        	// Opciones de toolbar.
			// create_entity: 
        	// - Puede ser falso y no se muestra el boton de nuevo.
        	// - Puede ser true, y se muestra para la tabla en cuestion, no aplica para views.
        	// - Puede ser un array, con las keys: table, columns, donde table especifica la tabla con la que trabaja y columns una config de columnas.
			$_options['create_entity'] 	 = array(	'button_title' 	=> ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_contract'),
													'table' 		=> 'contracts',
												#	'columns' 		=> FALSE,
													'target' 		=> '?action=new', // Si se especifica target, no se tiene en cuenta table y columns. Se redirige al target.
													);
			// La opcion de buscador, busca sobre todas las columnas de tipo text o varchar.
			// Puede ser TRUE o FALSE.
			$_options['search'] 		 = TRUE;
			// La opcion filters puede ser FALSE o ARRAY. 
			// Si es array, la etrutura es condicion => nombre del filtro.
			// donde condicion puede ser:
			// - simple: columna <>= valor
			// - cominado: (columna1 <>= valor AND/OR columna2 <>= valor)
			
                //$_options['filters'] 		 = array(
    			//								'id_contract IS NOT NULL' => 'Alquilados',
    			//								'id_contract IS NULL' => 'Desocupados',
                  //                              '(property_archived = 1) OR (property_archived = 1)' => 'Archivados'
    				//							);
			// Se puede pasar tambien una serie de condiciones que seran fijas, para modificar el set
			// de datos incial con el que se genera la tabla. Luego arriba se aplican los filters variables.
			//$_options['fixed_filters'] 	 = array(
											#'id_contract IS NOT NULL' => NULL
              //                              'property_archived' => 0,
				//							);

			// CASO PARTICULAR DE LOS OWNERS
            // if ($this->session->userdata('id_role') == ROLE_OWNER) 
            // {
            // 	$_columns['tenant_debts'] 			= array('hide' => TRUE);
            // 	$_columns['owner_user_name'] 		= array('hide' => TRUE);
            // 	$_columns['_count_maintenances'] 	= array('hide' => TRUE);
            // 	$_columns['tenant_user_name']['width'] 		= '200';
            // 	$_columns['contract_rent_price']['width'] 	= '200';

            // 	$_options['fixed_filters']['id_user_owner'] = $this->session->userdata('id_user');

            // 	unset($_options['filters']['(property_archived = 1) OR (property_archived = 1)']);

            // 	$_options['create_entity'] = FALSE;
            // }

        ?>

        <?php echo pachi_fullmanaged_table('contracts', $_columns, $_options); ?>
    </div>
</div>

