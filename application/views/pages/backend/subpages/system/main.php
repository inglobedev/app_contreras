<div class="portlet light ">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase">Configuraciones de sistema</span>
        </div>
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_mailing" data-toggle="tab">Mailing</a>
            </li>
        </ul>
    </div>
    <div class="portlet-body">
        <div class="tab-content">
            <!-- MAILING -->
            <div class="tab-pane active" id="tab_mailing">
                <?php config_form('mailing'); ?>
                <p class="innerT">
                    <a href="javascript:;" class="btn btn-default btn-test-email">Enviar un email de prueba</a>
                </p>
                <div id="mailing-results"></div>
            </div>
            <!-- END MAILING -->
        </div>
    </div>
</div>
