<?php
// new form
	$create_form = array(
							'user_name',
							'user_lastname',
							'user_address',
							'user_email',
							'user_cuit',
							'user_phone',
							'user_observations',
							'user_password' => array('input_type' => 'hidden',
													 'value' => md5('contreras')),
							'id_role' 		=> array('input_type' => 'hidden',
													 'value' => ROLE_OWNER),
			        	);

// Opciones de columnas
	$column_settings['owner_user_picture'] 			= array(
													'width' => '55px',
													'title' => ' ',
													'custom_render' => '<img src="#=file_uri|{owner_user_picture}|0=#" width="40">'
													);
	$column_settings['user_name'] 				= array(
													'width' => '275px',
													'custom_render' => '<a href="/'.APP_BACKEND.'view_user/{id_user}">{owner_user_name} {owner_user_lastname}</a><br>
																		<small class="text-muted">{owner_user_email}</smal>',
													'sortable' => TRUE
													);
	$column_settings['user_address'] 			= array('width' => '375px',
														'custom_render' => '{if [owner_user_address]}
																				{owner_user_address}<br>
																			{/if}
																			<small class="text-muted">
																				{if [owner_user_phone]}
																					tel: {owner_user_phone}
																				{/if}
																			</smal>');
	
	$column_settings['_last_receipt'] 				= array('title' => 'Ult. Liquidación',
															'custom_render' => '{if [_last_receipt]}
																					#=print_date|{_last_receipt}|=#
																				{else}
																					-
																				{/if}
																				'
															);
	$column_settings['_balance']					= array('title' => 'Saldo a liquidar',
															'custom_render' => '{if [_balance] > 0}
																					<a href="javascript:;" class="btn_new_coupon" data-id_user_receiver="{id_user}" data-counterpart_name="'.ucfirst($this->lang->line('entity_owner')).'">
																						$ #=number_format|{_balance}|2=#
																					</a>
																				{else}
																					$ 0.00
																				{/if}
																				'
														);
	$column_settings['_options']					= array(
														'title' => ' ',
														'width' => '87px',
														'custom_render' => '<a title="Editar" class="btn default btn-sm margin-none" href="/'.APP_BACKEND.'view_user/{id_user}">
																				<i class="fa fa-pencil"></i>
																			</a>
																			{if [owner_user_deletion_date]}
																				<a title="Restaurar" class="btn green btn-sm btn-enable-user margin-none" href="javascript:;" data-id_user="{id_user}" data-confirmation="¿Estás seguro? El usuario podrá acceder a esta aplicación.">
																					<i class="fa fa-undo"></i>
																				</a>
																			{else}
																				<a title="Archivar" class="btn btn-info margin-none btn-sm btn-disable-user" href="javascript:;" data-id_user="{id_user}" data-confirmation="¿Estás seguro? El usuario no podrá acceder a esta aplicación.">
																					<i class="fa fa-archive"></i>
																				</a>
																			{/if}');
	

    $column_settings['id_user'] 				= array('hide' => TRUE);
    $column_settings['id_role'] 				= array('hide' => TRUE);
    $column_settings['owner_user_name'] 		= array('hide' => TRUE);
    $column_settings['owner_user_lastname'] 	= array('hide' => TRUE);
    $column_settings['owner_user_address'] 		= array('hide' => TRUE);
    $column_settings['owner_user_email'] 		= array('hide' => TRUE);
    $column_settings['owner_user_phone'] 		= array('hide' => TRUE);
    $column_settings['owner_user_cuit'] 		= array('hide' => TRUE);
    $column_settings['owner_user_deletion_date'] 	= array('hide' => TRUE);
    $column_settings['owner_user_archived'] 		= array('hide' => TRUE);


// Opciones de Tabla.
	$options['create_entity'] 	 = array(	
											'button_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_owner'),
											'columns' => $create_form,
										);
	$options['filters'] 	 	= array(
											'(owner_user_deletion_date IS NOT NULL) OR (owner_user_deletion_date IS NOT NULL)' => 'Archivados'
										);
	$options['fixed_filters'] 	 = array(
											'id_role' => ROLE_OWNER,
											'owner_user_deletion_date IS NULL' => NULL
										);

	echo pachi_fullmanaged_table('vw_owners', $column_settings, $options);

	
?>
