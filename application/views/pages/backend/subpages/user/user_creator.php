<?php $cancel_url = base_url(APP_BACKEND.'users');  ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Alta de usuario</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_datos" data-toggle="tab">Información personal</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_datos">
                                    <?php 
	                                    $columns['user_name'] 		= array();
	                                    $columns['user_lastname'] 	= array();
	                                    $columns['user_address'] 	= array();
	                                    $columns['user_email'] 		= array();
	                                    $columns['user_cuit'] 		= array();
	                                    $columns['user_phone'] 		= array();
	                                    $columns['user_picture'] 	= array();
										$columns['user_password'] 	= array('input_type' => 'hidden', 'value' => md5('123456'));
                                        //$columns['id_role']         = array('input_type' => 'hidden','value' => ROLE_ADMINISTRATOR);
										$columns['id_role'] 		= array();
	                                    
										$_options['save_action']['action'] 	= 'redirect';
	    								$_options['save_action']['delay'] 	= '500';
	    								$_options['save_action']['target'] 	= base_url(APP_BACKEND . 'users');
	                                    
	                                    echo pachi_form('users', $columns, FALSE, $_options);
	                                ?>
	                                <br>
	                                <div class="alert alert-info">
                                        <strong>Info!</strong> La contraseña por defecto es 123456. 
                                    </div>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>