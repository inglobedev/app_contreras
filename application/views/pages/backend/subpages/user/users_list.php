<?php
// Opciones de columnas
	$column_settings['user_picture'] 			= array(
													'width' => '55px',
													'title' => ' ',
													'custom_render' => '<img src="#=file_uri|{user_password}|0=#" width="40">'
													);
	$column_settings['user_name'] 				= array(
													'width' => '275px',
													'custom_render' => '<a href="/'.APP_BACKEND.'view_user/{id_user}">{user_name} {user_lastname}</a><br>
																		<small class="text-muted">{user_email}</smal>',
													'sortable' => TRUE
													);
	$column_settings['user_address'] 			= array('width' => '375px',
														'custom_render' => '{if [user_address]}
																				{user_address}<br>
																			{/if}
																			<small class="text-muted">
																				{if [user_phone]}
																					tel: {user_phone}
																				{/if}
																			</smal>');
	
	$column_settings['id_role'] 				= array('width' => '155px', 'custom_render' => '#=get_role_name|{id_role}=#');
	$column_settings['user_observations'] 		= array();
	$column_settings['_options']				= array(
														'title' => ' ',
														'width' => '87px',
														'custom_render' => '<a title="Editar" class="btn default btn-sm margin-none" href="/'.APP_BACKEND.'view_user/{id_user}">
																				<i class="fa fa-pencil"></i>
																			</a>
																			{if [user_deletion_date]}
																				<a title="Restaurar" class="btn green btn-sm btn-enable-user margin-none" href="javascript:;" data-id_user="{id_user}" data-confirmation="¿Estás seguro? El usuario podrá acceder a esta aplicación.">
																					<i class="fa fa-undo"></i>
																				</a>
																			{else}
																				<a title="Archivar" class="btn btn-info margin-none btn-sm btn-disable-user" href="javascript:;" data-id_user="{id_user}" data-confirmation="¿Estás seguro? El usuario no podrá acceder a esta aplicación.">
																					<i class="fa fa-archive"></i>
																				</a>
																			{/if}');
	

	$column_settings['user_lastname'] 			= array('hide' => TRUE);
	$column_settings['user_email'] 				= array('hide' => TRUE);
	$column_settings['user_phone'] 				= array('hide' => TRUE);
	$column_settings['user_cuit'] 				= array('hide' => TRUE);
	$column_settings['user_password'] 			= array('hide' => TRUE);
	$column_settings['user_language'] 			= array('hide' => TRUE);
	$column_settings['user_password_restore'] 	= array('hide' => TRUE);
	$column_settings['user_deletion_date'] 		= array('hide' => TRUE);
	$column_settings['user_registration_date'] 	= array('hide' => TRUE);
	$column_settings['user_last_login'] 		= array('hide' => TRUE);
	$column_settings['user_pin'] 				= array('hide' => TRUE);
	$column_settings['user_login_count']		= array('hide' => TRUE);
	$column_settings['id_user'] 				= array('hide' => TRUE);
    $column_settings['user_archived']			= array('hide' => TRUE);


// Opciones de Tabla.
	$options['create_entity'] 	 = array(	'button_title' 	=> ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_user'),
											'target' => '?action=new'	);

	$options['filters'] 	 	= array(
											'user_deletion_date IS NOT NULL' => 'Archivados'
									);

	$options['fixed_filters'] 	 = array(
											'id_role NOT IN (' . implode(',', array(ROLE_OWNER,ROLE_TENANT,ROLE_GUARANTOR,ROLE_PROFESSIONAL,ROLE_CONSORTIUM)) . ')' => NULL
										);

	echo pachi_fullmanaged_table('users', $column_settings, $options);
?>
