<?php $cancel_url = (empty($this->input->get('action'))) ? base_url(APP_HOME) : generate_get_string('action', 'list', array('id'));  ?>
<div class="row">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet ">
                <div class="profile-userpic">
                    <img src="<?php echo avatar(FALSE, $user['user_picture']) ?>" class="img-responsive" alt=""> </div>
                <div class="profile-usertitle">
                    <div class="profile-usertitle-name"> <?php echo $user['user_name'] ?> </div>
                    <!--<div class="profile-usertitle-job">  <?php echo $user['role_name'] ?>  </div>-->
                </div>
                <div class="profile-usermenu">
                    <ul class="nav">
                        <li class="active">
                            <a href="javascript:;">
                                <i class="icon-info"></i> General 
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Administrar cuenta</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_datos" data-toggle="tab">Información personal</a>
                                </li>
                                
                                <li class="<?php if ($user['id_role'] == ROLE_SISTEM_ADMINTRATOR){ ?> visible <?php } else { ?> hidden <?php } ?>">
                                    <a href="#tab_calve" data-toggle="tab">Clave</a>
                                </li>
                                <?php if ($this->session->userdata('id_user') == $user['id_user']): ?>
                                <li class="<?php if ($user['id_role'] == ROLE_SISTEM_ADMINTRATOR){ ?> visible <?php } else { ?> hidden <?php } ?>">
                                    <a href="#tab_notif" data-toggle="tab">Notificaciones</a>
                                </li>
                                <?php endif ?>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <div class="tab-pane active" id="tab_datos">

                                    <?php 
                                    $columns['user_name'] 		= array();
                                    $columns['user_lastname'] 	= array();
                                    $columns['user_address'] 	= array();
                                    $columns['user_email'] 		= array();
                                    $columns['user_cuit'] 		= array();
                                    $columns['user_phone'] 		= array();
                                    $columns['user_picture']    = array();
                                     if ($this->session->userdata('id_role') == ROLE_SISTEM_ADMINTRATOR){
                                        $columns['id_role'] 	= array();
                                    }
                                        
                                    echo pachi_form('users', $columns, $user['id_user'])  ?>
                                    
                                </div>
                                <div class="tab-pane" id="tab_calve">
                                    <form class="frm-update-password">
                                        <?php if ($this->session->userdata('id_user') == $user['id_user']): ?>
                                        <div class="form-group">
                                            <label class="control-label">Clave actual</label>
                                            <input name="old_password" type="password" class="form-control" /> 
                                        </div>
                                        <?php endif ?>
                                        <div class="form-group">
                                            <label class="control-label">Nueva clave</label>
                                            <input name="new_password" type="password" class="form-control" /> 
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Repita la nueva clave</label>
                                            <input name="new_r_password" type="password" class="form-control" /> 
                                        </div>
                                        <div class="margin-top-10">
                                            <input type="hidden" name="id_user" value="<?php echo $user['id_user'] ?>">
                                            <button type="submit" class="btn green"> Cambiar clave </button>
                                            <a href="<?php echo $cancel_url ?>" class="btn default"> Cancelar </a>
                                        </div>
                                    </form>
                                </div>
                                <?php if ($this->session->userdata('id_user') == $user['id_user']): ?>
                                <div class="tab-pane" id="tab_notif">
                                    <form class="frm-update-notifications-settings">
                                        <p>Seleccione el tipo de notificaciones que quiere recibir en su email.</p>
                                        <div class="row">
                                            <?php foreach ($notifications as $_item => $_notif): ?>
                                            <div class="col-sm-6 form-group">
                                                <div class="mt-checkbox-list">
                                                    <label class="mt-checkbox margin-none"> <?php echo $_notif['descr'] ?>
                                                        <input type="hidden" name="notif_<?php echo $_notif['group'] ?>" value="0">
                                                        <input name="notif_<?php echo $_notif['group'] ?>" type="checkbox" value="1" <?php if ($_notif['value'] == '1') echo 'checked' ?> />
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <?php endforeach ?>
                                        </div>
                                        <div class="margin-top-10">
                                            <button type="submit" class="btn green"> Guardar </button>
                                            <a href="<?php echo $cancel_url ?>" class="btn default"> Cancelar </a>
                                        </div>
                                        <?php #$this->app_notifications->reset_settings($this->session->userdata('id_user'), $hard = FALSE) ?>
                                    </form>
                                </div>
                                <?php endif ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>