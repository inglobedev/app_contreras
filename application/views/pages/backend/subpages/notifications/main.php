<div class="portlet light portlet-fit full-height-content full-height-content-scrollable ">
    <div class="portlet-title tabbable-line">
        <div class="caption">
            <i class="icon-globe font-dark hide"></i>
            <span class="caption-subject font-dark bold uppercase">Notificaciones</span>
        </div>
        <div class="actions">
            <div class="btn-group">
                <a class="btn btn-circle btn-default " href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-cog"></i> Opciones
                    <i class="fa fa-angle-down"></i>
                </a>
                <ul class="dropdown-menu pull-right">
                    <li>
                        <a href="javascript:app_notifications_read_all();">
                            <i class="fa fa-check"></i> Marcar todas como leidas 
                        </a>
                    </li>
                    <li>
                        <a href="javascript:app_notifications_clean();">
                            <i class="fa fa-trash-o"></i> Eliminar todas las leidas
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="portlet-body">
        <div class="full-height-content-body">
            <ul class="feeds">
                <?php foreach ($notifications as $_knf => $_nf): ?>
                <li>
                    <div class="col1">
                        <div class="cont">
                            <div class="cont-col1">
                                <div class="label label-sm label-<?php echo $_nf['label'] ?>">
                                    <i class="<?php echo $_nf['icon'] ?>"></i>
                                </div>
                            </div>
                            <div class="cont-col2">
                                <div class="desc <?php if ($_nf['notif_readed'] == 0) echo 'strong' ?>"> 
                                    <a class="text-black" href="<?php echo $_nf['target'] ?>">
                                        <?php echo $_nf['notif_title'] ?> -  <?php echo $_nf['notif_content'] ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col2">
                        <div class="date" title="<?php echo $_nf['notif_date'] ?>"> <?php echo get_time_ago($_nf['notif_date'], TRUE) ?> </div>
                    </div>
                </li>
                <?php endforeach ?>
                <?php if (count($notifications) == 0): ?>
                    <li class="text-center bg-white">
                        No hay notificaciones.
                    </li>
                <?php endif ?>
            </ul>
        </div>
    </div>
</div>
