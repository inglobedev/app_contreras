<?php $cancel_url = base_url(APP_HOME.'roles');  ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Ajustes de Rol</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_general" data-toggle="tab">General</a>
                                </li>
                                <?php foreach ($permissions as $_group => $_permission_group): ?>
                                <li class="">
                                    <a href="#tab_permissions_<?php echo $_group; ?>" data-toggle="tab">Permisos <?php echo ucfirst($_group) ?></a>
                                </li>
                                <?php endforeach ?>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- ROLE INFO TAB -->
                                <div class="tab-pane active" id="tab_general">
                                    <form class="frm-update-role">
                                        <div class="form-group">
                                            <label class="control-label">Nombre</label>
                                            <input autocomplete="off" required name="role_name" type="text" placeholder="Nombre del rol" class="form-control" value="<?php echo $role['role_name'] ?>" /> 
                                        </div>
                                        <div class="margiv-top-10">
                                            <input type="hidden" name="id_role" value="<?php echo $role['id_role'] ?>">
                                            <button type="submit" class="btn green"> Continuar </button>
                                            <a href="<?php echo $cancel_url ?>" class="btn default"> Cancelar </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END ROLE INFO TAB -->

                                <!-- ROLE PERMISSIONS TABS -->
                                <?php foreach ($permissions as $_group => $_items): ?>
                                <div class="tab-pane" id="tab_permissions_<?php echo "$_group"; ?>">
                                    <form class="frm-update-role-permissions">
                                        <div class="row">
                                            <?php foreach ($_items as $_item => $_value): ?>
                                            <div class="col-sm-6 col-md-4 col-lg-3 form-group">
                                                <div class="mt-checkbox-list">
                                                    <label class="mt-checkbox margin-none"> <?php echo $this->lang->line('permission_'.$_group.'_'.$_item) ?>
                                                        <input type="hidden" name="<?php echo $_item ?>" value="0">
                                                        <input name="<?php echo $_item ?>" type="checkbox" value="1" <?php if ($_value == '1') echo 'checked' ?> />
                                                        <span></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <?php endforeach ?>
                                        </div>

                                        <!--end profile-settings-->
                                        <div class="margin-top-10">
                                            <input type="hidden" name="id_role" value="<?php echo $role['id_role'] ?>">
                                            <input type="hidden" name="permission_group" value="<?php echo $_group ?>">
                                            <button type="submit" class="btn green">Guardar permisos</button>
                                            <a class="btn default" href='<?php echo $cancel_url ?>'>Cancelar</a>
                                        </div>
                                    </form>
                                </div>
                                <?php endforeach ?>
                                <!-- END ROLE PERMISSIONS TAB -->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>