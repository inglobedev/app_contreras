<?php $cancel_url = base_url(APP_HOME.'roles');  ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Alta de rol</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_general" data-toggle="tab">Información general</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_general">
                                    <form class="frm-create-role">
                                        <div class="form-group">
                                            <label class="control-label">Nombre</label>
                                            <input autocomplete="off" required name="role_name" type="text" placeholder="Nombre del rol" class="form-control" /> 
                                        </div>
                                        <div class="margiv-top-10">
                                            <button type="submit" class="btn green"> Continuar </button>
                                            <a href="<?php echo $cancel_url ?>" class="btn default"> Cancelar </a>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>