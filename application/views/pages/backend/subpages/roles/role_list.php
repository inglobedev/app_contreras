<div class="row">
	<div class="col-md-2 col-sm-2 col-xs-12">
		<a href="<?php echo generate_get_string('action', 'new') ?>" class="btn green btn-md">Nuevo rol</a>
	</div>
	<div class="col-md-10 col-sm-10 col-xs-12 text-right">
		<div class="hidden-sm hidden-md hidden-lg"><br></div>
    	<div class="inline-block">
	        <form action="<?php echo generate_get_string(); ?>">
	            <?php if (!empty($this->input->get('busqueda')) OR !empty($this->input->get('filtro')) OR !empty($this->input->get('categoria'))): ?>
	                <a class="btn btn-xs grey" href="<?php echo generate_get_string('action', 'list', array('busqueda', 'filtro', 'categoria')) ?>"><i class="fa fa-times"></i> Limpiar búsqueda y filtros</a>
	            <?php endif ?>
	            <label>
	                <span class="hidden-xs">Buscar: &nbsp;</span>
	                <input type="search" class="form-control input-sm input-small input-inline" name="busqueda" placeholder="Buscar ..." value="<?php echo $this->input->get('busqueda') ?>">
	            </label>
	            <button type="submit" class="hidden"></button>
	        </form>
    	</div>
    </div>
</div>
<br>
<div class="table-responsive">
	<table class="table table-striped table-bordered table-advance table-hover table-vertical-align">
		<thead>
			<tr>
				<th>
					Rol
				</th>
				<th style="width: 160px;" class="hidden-xs text-center">
					Usuarios activos
				</th>
				<th style="width: 140px;">
				</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($roles as $key => $role): ?>
			<tr>
				<td>
					<?php echo $role['role_name'] ?>
				</td>
				<td class="hidden-xs text-center">
					<?php echo $role['count_users'] ?>
				</td>
				<td class="text-center">
					<div class="btn-group btn-group-solid">
						<a title="Editar rol" class="btn default btn-sm" href="?action=edit&id=<?php echo $role['id_role']; ?>">
							<i class="fa fa-pencil"></i>
						</a>
						<?php if ($role['role_deletable'] == '1'): ?>
						<a title="Eliminar rol" class="btn red btn-sm btn-delete-role" href="javascript:;" data-id_role="<?php echo $role['id_role'] ?>" data-confirmation="¿Estás seguro? El usuario no podrá acceder a esta aplicación.">
							<i class="fa fa-trash-o"></i>
						</a>
						<?php endif ?>
					</div>
				</td>
			</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>
<?php echo pag_get_html($pagination_current, $pagination_total_items, $pagination_per_page); ?>