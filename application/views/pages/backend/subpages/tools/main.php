<div class="portlet light ">
    <div class="portlet-title tabbable-line">
        <div class="caption caption-md">
            <i class="icon-globe theme-font hide"></i>
            <span class="caption-subject font-blue-madison bold uppercase">Herramientas</span>
        </div>
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_mailing" data-toggle="tab">Tester de Correos</a>
            </li>
        </ul>
    </div>
    <div class="portlet-body">
        <div class="tab-content">
            <!-- MAILING -->
            <div class="tab-pane active" id="tab_mailing">
                <form class="frm_tool_test_email_design">

				    <div class="form-group">
				        <label class="control-label">Destinatario:</label>
				        <div class="controls">
				            <input type="email" class="form-control" name="target" value="<?php echo $this->session->userdata('user_email') ?>"> 
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">Asunto:</label>
				        <div class="controls">
				            <input type="text" class="form-control" name="subject" value="Prueba de correo: <?php echo date('YmdHis') ?>"> 
				        </div>
				    </div>
				    <div class="form-group">
				        <label class="control-label">HTML</label>
				        <textarea class="inbox-wysihtml5 form-control" name="content" rows="12"></textarea>
				    </div>
				    <div class="inbox-compose-btn">
				        <button class="btn green" type="submit">
				            <i class="fa fa-check"></i> Enviar
				        </button>
				    </div>
                </form>
                <hr>
                <div id="mailing-results"></div>
            </div>
            <!-- END MAILING -->
        </div>
    </div>
</div>
