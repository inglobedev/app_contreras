<?php $cancel_url = base_url(APP_MAINTENANCES);  ?>
<div class="row">
    <div class="col-md-12">
        <!-- BEGIN PROFILE CONTENT -->
        <div class="profile-content">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">Manteninimiento <span class="text-muted"><?php echo $maintenance['maintenance_name'] ?></span></span>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_general"> 

                                	<?php 

	    								$_columns['id_property']					= array();
	    								$_columns['maintenance_name']				= array();
	    								$_columns['maintenance_description']		= array();
	    								$_columns['maintenance_completed']			= array();
	    								$_columns['maintenance_owner_cost']			= array();
	    								$_columns['maintenance_tenant_cost']		= array();
	    								$_columns['maintenance_administrative_fee']	= array();
	    								$_columns['maintenance_status']				= array('input_type' => 'select', 
	    																					'options' => array('0' => 'Pendiente',
	    																									   '1' => 'Aprobado',
	    																									   '2' => 'Rechazado')

	    																					);
	    								$_columns['maintenance_observations']		= array();

	    								$_options['cancel'] 				= base_url(APP_MAINTENANCES);
	    								$_options['save_action']['action'] 	= 'redirect';
	    								$_options['save_action']['delay'] 	= '500';
	    								$_options['save_action']['target'] 	= base_url(APP_MAINTENANCES);
	    								
	    								// Hay un handler para guardado custom, ya que se requiere que:
	    								// Cuando un mantenimiento se marca como completo, el estado se establece en Aprobado y se archiva.

	    								$_options['custom_target'] 	= base_url('ajax/Ajax_maintenance/update_maintenante'); 

	    								echo pachi_form('property_maintenances', $_columns, $maintenance['id_maintenance'], $_options);
                                	?>

                                </div>
                                <!-- END PERSONAL INFO TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>

<?php if (FALSE): ?>
Array
(
    [property_name] => Oficina 11 Cerro
    [property_address] =>  Rafael Nuñez Nº 3578- OF: 11 - Cochera
    [id_user_owner] => 5
    [owner_user_name] => María del Carmen 
    [owner_user_lastname] => Montironi
    [tenant_user_name] => 
    [tenant_user_lastname] => 
    [id_maintenance] => 1
    [id_property] => 2
    [maintenance_name] => Plomeria
    [maintenance_description] => caño roto
    [id_maintenance_quote] => 1
    [maintenance_completed] => 0
    [maintenance_owner_cost] => 1000.00
    [maintenance_tenant_cost] => 1200.00
    [maintenance_administrative_fee] => 200.00
    [maintenance_status] => 0
    [maintenance_observations] => 
    [id_user_professional] => 56
    [quote_observations] => 
    [quote_price] => 1000.00
    [quote_attachments] => 
    [quote_ready] => 0
    [professional_user_name] => Juan
    [professional_user_lastname] => Perez
    [professional_user_address] => 
    [professional_user_email] => 1professional@contreras.com
    [professional_user_phone] => 
    [professional_user_picture] => 
    [_count_quotes_ready] => 0
    [_count_quotes] => 1
    [_avg_quote_price] => 1000.000000
)
<?php endif ?>