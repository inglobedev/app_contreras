<?php 

	// La variable $RESOURCES esta controlada, para agregarse en el header y footer.
	$RESOURCES = array(
			'header_css' => array(
				'app/css/backend.css',
				'global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
				),
			'footer_js' => array(
				'app/scripts/maintenances.js',
				'global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
				'global/plugins/jquery.blockui.min.js',
				)
		);

 ?>