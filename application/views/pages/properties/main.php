<div class="row">
    <div class="col-md-12">
        <?php 
            $_columns['id_property']                    = array('hide' => TRUE);
            $_columns['id_user_consortium']             = array('hide' => TRUE);
            $_columns['property_name']                  = array('title' => 'Inmueble',
                                                                'custom_render' => '<a href="/'.APP_PROPIEDADES.'view/{id_property}">{property_name}<br><small class="text-muted">{property_address}</small></i></a>',
        														'sortable' => TRUE);
            $_columns['property_address']               = array('hide' => TRUE);
            $_columns['property_year']                  = array('hide' => TRUE);
            $_columns['property_ambiences_qty']         = array('hide' => TRUE);
            $_columns['property_bathrooms_qty']         = array('hide' => TRUE);
            $_columns['property_rooms_qty']             = array('hide' => TRUE);
            $_columns['property_square_meter']          = array('hide' => TRUE);
            $_columns['property_garage']                = array('hide' => TRUE);
            $_columns['property_balcony']               = array('hide' => TRUE);
            $_columns['property_building_name']         = array('hide' => TRUE);
            $_columns['property_cadastre_number']       = array('hide' => TRUE);
            $_columns['property_observations']          = array('hide' => TRUE);
            $_columns['creation_date']                  = array('hide' => TRUE);
            $_columns['modificaction_date']             = array('hide' => TRUE);
            $_columns['deletion_date']                  = array('hide' => TRUE);
            $_columns['id_user_owner']                  = array('hide' => TRUE);
            $_columns['ownership_date_start']           = array('hide' => TRUE);
            $_columns['owner_user_name']                = array('custom_render' => '{if [id_user_owner] > 0}
            																			{owner_user_name} <br>
            																			<small class="text-muted">
            																				Desde #=print_datetime|{ownership_date_start}|0=#
            																			</small>
            																		{/if}
            																		',
            													'width' => '160',
            													'sortable' => TRUE
            													);
            $_columns['owner_user_lastname']            = array('hide' => TRUE);
            $_columns['owner_user_address']             = array('hide' => TRUE);
            $_columns['owner_user_email']               = array('hide' => TRUE);
            $_columns['owner_user_phone']               = array('hide' => TRUE);
            $_columns['owner_user_picture']             = array('hide' => TRUE);
            $_columns['consortium_user_name']           = array('hide' => TRUE);
            $_columns['consortium_user_lastname']       = array('hide' => TRUE);
            $_columns['consortium_user_address']        = array('hide' => TRUE);
            $_columns['consortium_user_email']          = array('hide' => TRUE);
            $_columns['consortium_user_phone']          = array('hide' => TRUE);
            $_columns['consortium_user_picture']        = array('hide' => TRUE);
            $_columns['tenant_user_name']               = array('custom_render' => '{tenant_user_name}
            																		{if [tenant_user_phone]}
            																			<br>
            																			<small class="text-muted">te: {tenant_user_phone}</small>
            																		{/if}
            																		',
            													'width' => '160',
            													);
            $_columns['tenant_user_lastname']           = array('hide' => TRUE);
            $_columns['tenant_user_address']            = array('hide' => TRUE);
            $_columns['tenant_user_email']              = array('hide' => TRUE);
            $_columns['tenant_user_phone']              = array('hide' => TRUE);
            $_columns['tenant_user_picture']            = array('hide' => TRUE);
            $_columns['id_contract']                    = array('hide' => TRUE);
            $_columns['id_user_tenant']                 = array('hide' => TRUE);
            $_columns['contract_date_start']            = array('hide' => TRUE);
            $_columns['contract_date_end']              = array('hide' => TRUE);
            $_columns['contract_rent_price']            = array('custom_render' => '{if [id_contract] > 0}
            																			$ #=number_format|{contract_rent_price}|2=# 
            																		{/if}
            																		{if [id_bonification] > 0}
            																			<span class="text-success tooltips" data-container="body" data-placement="top" data-original-title="#=print_date|{bonification_date_start}=# a #=print_date|{bonification_date_end}=#"> -$ #=number_format|{bonification_rate}|2=#</span>
            																		{/if}
            																		{if [id_contract] > 0}
            																			<br>
            																			<small class="text-muted">
            																				#=print_date|{contract_date_start}=# al #=print_date|{contract_date_end}=#
            																			</small>
            																		{/if}
            																		',
            													'width' => '160',
            													'sortable' => TRUE
            													);
            $_columns['contract_interest']              = array('hide' => TRUE);
            $_columns['contract_tenant_attachments']    = array('hide' => TRUE);
            $_columns['contract_gas_check']             = array('hide' => TRUE);
            $_columns['contract_electricity_check']     = array('hide' => TRUE);
            $_columns['contract_expenses_check']        = array('hide' => TRUE);
            $_columns['contract_expenses_attachments']  = array('hide' => TRUE);
            $_columns['contract_observations']          = array('hide' => TRUE);
            $_columns['tenant_debts']					= array('custom_render' => '{if [id_contract] > 0}
            																			$ #=number_format|{tenant_debts}|2=#
            																		{/if}
            																		',
            													'width' => '145',
            													'sortable'=> TRUE
            													);
            $_columns['_count_maintenances']            = array('custom_render' => '{if [_count_maintenances] > 0}
            																			{_count_maintenances}
            																		{/if}
            																		{if [_count_maintenances] == 0}
            																			No
            																		{/if}
            																		',
            													'width' => '100',
            													'class' => 'text-center'
            													);

            $_columns['id_bonification']    			= array('hide' => TRUE);
            $_columns['bonification_name']             	= array('hide' => TRUE);
            $_columns['bonification_rate']     			= array('hide' => TRUE);
            $_columns['bonification_date_start']        = array('hide' => TRUE);
            $_columns['bonification_date_end']  		= array('hide' => TRUE);
            $_columns['bonification_observations']      = array('hide' => TRUE);
            $_columns['contract_payday_limit']			= array('hide' => TRUE);
            $_columns['contract_payday_start']			= array('hide' => TRUE);
            $_columns['ownership_administrative_rate']	= array('hide' => TRUE);
            $_columns['property_archived']				= array('hide' => TRUE);
            $_columns['contract_commission']           = array('hide' => TRUE);

        ?>

        <?php 
        	// Opciones de toolbar.
			// create_entity: 
        	// - Puede ser falso y no se muestra el boton de nuevo.
        	// - Puede ser true, y se muestra para la tabla en cuestion, no aplica para views.
        	// - Puede ser un array, con las keys: table, columns, donde table especifica la tabla con la que trabaja y columns una config de columnas.
			$_options['create_entity'] 	 = array(	'button_title' 	=> ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_property'),
												#	'table' 		=> 'properties',
												#	'columns' 		=> FALSE,
													'target' 		=> '?action=new', // Si se especifica target, no se tiene en cuenta table y columns. Se redirige al target.
													);
			// La opcion de buscador, busca sobre todas las columnas de tipo text o varchar.
			// Puede ser TRUE o FALSE.
			$_options['search'] 		 = TRUE;
			// La opcion filters puede ser FALSE o ARRAY. 
			// Si es array, la etrutura es condicion => nombre del filtro.
			// donde condicion puede ser:
			// - simple: columna <>= valor
			// - cominado: (columna1 <>= valor AND/OR columna2 <>= valor)
			$_options['filters'] 		 = array(
											'id_contract IS NOT NULL' => 'Alquilados',
											'id_contract IS NULL' => 'Desocupados',
                                            '(property_archived = 1) OR (property_archived = 1)' => 'Archivados'
											);
			// Se puede pasar tambien una serie de condiciones que seran fijas, para modificar el set
			// de datos incial con el que se genera la tabla. Luego arriba se aplican los filters variables.
			$_options['fixed_filters'] 	 = array(
											#'id_contract IS NOT NULL' => NULL
                                            'property_archived' => 0,
											);

			// CASO PARTICULAR DE LOS OWNERS
            if ($this->session->userdata('id_role') == ROLE_OWNER) 
            {
            	$_columns['tenant_debts'] 			= array('hide' => TRUE);
            	$_columns['owner_user_name'] 		= array('hide' => TRUE);
            	$_columns['_count_maintenances'] 	= array('hide' => TRUE);
            	$_columns['tenant_user_name']['width'] 		= '200';
            	$_columns['contract_rent_price']['width'] 	= '200';

            	$_options['fixed_filters']['id_user_owner'] = $this->session->userdata('id_user');

            	unset($_options['filters']['(property_archived = 1) OR (property_archived = 1)']);

            	$_options['create_entity'] = FALSE;
            }

        ?>

        <?php echo pachi_fullmanaged_table('vw_properties', $_columns, $_options); ?>
    </div>
</div>

