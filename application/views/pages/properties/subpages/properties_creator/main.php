<?php $cancel_url = base_url(APP_PROPIEDADES);  ?>

<div class="portlet light">
    <div class="portlet-body">
        <?php 
            #$this->pachi->allow_create = TRUE;

        	$consortium_form = array(
									'user_name',
									'user_lastname',
									'user_address',
									'user_email',
									'user_cuit',
									'user_phone',
									'user_observation',
									'user_password' => array('input_type' => 'hidden',
															 'value' => '123456'),
									'id_role' 		=> array('input_type' => 'hidden',
															 'value' => ROLE_CONSORTIUM),
						        	);


            $cols = array(  
							'property_name',
							'property_address',
							'property_year' => array('input_type' => 'hidden'),
							'property_garage'  => array('input_type' => 'hidden'),
							'property_balcony'  => array('input_type' => 'hidden'),
							'property_building_name'  => array('input_type' => 'hidden'),
							'property_cadastre_number',
							'property_observations',
                            'id_user_consortium' => array(  // Buscador
                                        'reference' => array( //'table' => 'users', // aca podria determinar otra tabla distinta para buscar.
                                                                'display' => 'user_name',   // Si bien por defecto muestra el _name, podria mostrar otra columna.
                                                                'filter' => array('id_role' => ROLE_CONSORTIUM)
                                                                ),
                                                                // Modal de creacion.
                                        'modal' => array('columns'  => $consortium_form, 
                                                         'modal_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_consortium'),
                                                         'button_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_consortium')
                                                        ) // aca determino que columna es la que se muestra en el buscador.
                                    ),
                        );

            $options['save_action']['action'] = 'redirect';
            $options['save_action']['target'] = '/properties/view/{id_property}';
            $options['save_action']['delay'] = '500';

            echo pachi_form('properties', $cols, FALSE ,$options);
        ?>
    </div>
</div>