<?php if ($this->session->userdata('id_role') != ROLE_OWNER): ?>

	<div class="portlet light ">
	    <div class="portlet-title tabbable-line">
	        <div class="caption caption-md">
	            <i class="icon-globe theme-font hide"></i>
	            <span class="caption-subject font-blue-madison bold uppercase">Impuestos y servicios</span>
	            <span class="caption-helper">A pagar</span>
	        </div>
	    </div>
	    <div class="portlet-body">
			<?php
				$_columns['detail_description']			= array('custom_render' => '
																					{if [detail_attachments] OR [sibling_receipt_attachments]}
																						<span class="pull-right"> #=download_attachments|{detail_attachments}=# #=download_attachments|{sibling_receipt_attachments}=# </span>
																					{/if}
																					{if [_payed]}
																						<strong>{detail_description}</strong><br>
																					{else}
																						<strong><a class="btn_new_coupon" data-id_user_receiver="{account_id_user_receiver}" data-filter_details="{id_receipt_detail}">{detail_description}</a></strong><br>
																					{/if}
																					<small>
																						{detail_info}
																					</small>',
																'title' => 'Detalle'
																				);

				$_columns['period_code']				= array('class' => 'text-center', 'width' => '90px');
				$_columns['detail_ammount']				= array('custom_render' => '$ #=number_format|{detail_ammount}|2=#',
																'width' => '80px',
																'title' => 'Importe',
																'class' => 'text-right');
				$_columns['_payed']						= array('title' => 'Estado',
																'width' => '80px',
																'custom_render' => "
																{if [_payed]}
																	Pagado
																{else}
																	Pendiente
																{/if}
																");

				$_columns['detail_info']				= array('hide' => TRUE);
				$_columns['id_receipt_detail']			= array('hide' => TRUE);
				$_columns['id_receipt']					= array('hide' => TRUE);
				$_columns['id_expense_type']			= array('hide' => TRUE);
				$_columns['id_period']					= array('hide' => TRUE);
				$_columns['id_property']				= array('hide' => TRUE);
				$_columns['id_issuing_account']			= array('hide' => TRUE);
				$_columns['id_receiving_account']		= array('hide' => TRUE);
				$_columns['detail_concept']				= array('hide' => TRUE);
				$_columns['detail_observations']		= array('hide' => TRUE);
				$_columns['detail_expire_date']			= array('hide' => TRUE);
				$_columns['detail_interest_since']		= array('hide' => TRUE);
				$_columns['detail_interest_rate']		= array('hide' => TRUE);
				$_columns['detail_original_ammount']	= array('hide' => TRUE);
				$_columns['detail_custom']				= array('hide' => TRUE);
				$_columns['detail_id_user_modifier']	= array('hide' => TRUE);
				$_columns['detail_id_detail_sibling']	= array('hide' => TRUE);
				$_columns['creation_date']				= array('hide' => TRUE);
				$_columns['modification_date']			= array('hide' => TRUE);
				$_columns['_checked']					= array('hide' => TRUE);
				$_columns['period_year']				= array('hide' => TRUE);
				$_columns['period_month']				= array('hide' => TRUE);
				$_columns['receipt_date']				= array('hide' => TRUE);
				$_columns['id_user_issuer']				= array('hide' => TRUE);
				$_columns['id_user_receiver']			= array('hide' => TRUE);
				$_columns['receipt_payed']				= array('hide' => TRUE);
				$_columns['receipt_pay_date']			= array('hide' => TRUE);
				$_columns['receipt_voided']				= array('hide' => TRUE);
				$_columns['receipt_description']		= array('hide' => TRUE);
				$_columns['receipt_observations']		= array('hide' => TRUE);
				$_columns['type_name']					= array('hide' => TRUE);
				$_columns['type_description']			= array('hide' => TRUE);
				$_columns['type_order']					= array('hide' => TRUE);
				$_columns['account_id_user_issuer']		= array('hide' => TRUE);
				$_columns['account_name_issuer']		= array('hide' => TRUE);
				$_columns['account_id_user_receiver']	= array('hide' => TRUE);
				$_columns['account_name_receiver']		= array('hide' => TRUE);
				$_columns['receipt_attachments']		= array('hide' => TRUE);
				$_columns['detail_attachments']			= array('hide' => TRUE);
				$_columns['sibling_receipt_attachments']= array('hide' => TRUE);

				$_options['create_entity'] 	 = FALSE;
				$_options['search'] 		 = FALSE;
				$_options['enable_search'] 	 = FALSE;
				$_options['enable_ipp'] 	 = FALSE;
				$_options['fixed_filters'] 	 = array(
										'id_expense_type' 			=> ET_IMPUESTO, // Son los imp o servicios
										'id_property'				=> $property['id_property'],
										#'_payed' 		  			=> 0, // No pagados
										#'detail_ammount>' 		  	=> 0, // Que tengan el importe cargado.
										'account_id_user_receiver IS NOT NULL'  => NULL, // Pero solo los detalles destinados a pagar, no a cobrar.
								);
				$_options['fixed_orders']    = array('period_year' => 'DESC', 'period_month' => 'DESC', '_payed' => 'ASC', 'detail_description' => 'ASC');

				echo pachi_fullmanaged_table('vw_receipt_details', $_columns, $_options);
				unset($_columns);
				unset($_options);
			?>
	    </div>
	</div>

	<div class="portlet light ">
	    <div class="portlet-title tabbable-line">
	        <div class="caption caption-md">
	            <i class="icon-globe theme-font hide"></i>
	            <span class="caption-subject font-blue-madison bold uppercase">Impuestos y servicios</span>
	            <span class="caption-helper">A cobrar</span>
	        </div>
	    </div>
	    <div class="portlet-body">
			<?php
				$_columns['detail_description']			= array('custom_render' => '
																				{if [detail_attachments] OR [sibling_receipt_attachments]}
																					<span class="pull-right"> #=download_attachments|{detail_attachments}=# #=download_attachments|{sibling_receipt_attachments}=# </span>
																				{/if}
																				{if [_payed]}
																					<strong>{detail_description}</strong><br>
																				{else}
																					<strong><a class="btn_new_coupon" data-id_user_receiver="{account_id_user_receiver}" data-filter_details="{id_receipt_detail}">{detail_description}</a></strong><br>
																				{/if}
																				<small>
																					{detail_info}
																				</small>',
															'title' => 'Detalle'
																			);
				
				$_columns['period_code']				= array('class' => 'text-center', 'width' => '90px');
				$_columns['detail_ammount']				= array('custom_render' => '$ #=number_format|{detail_ammount}|2=#',
																'width' => '80px',
																'title' => 'Importe',
																'class' => 'text-right');
				$_columns['_payed']						= array('title' => 'Estado',
																'width' => '80px',
																'custom_render' => "
																{if [_payed]}
																	Cobrado
																{else}
																	Pendiente
																{/if}
																");

				$_columns['detail_info']				= array('hide' => TRUE);
				$_columns['id_receipt_detail']			= array('hide' => TRUE);
				$_columns['id_receipt']					= array('hide' => TRUE);
				$_columns['id_expense_type']			= array('hide' => TRUE);
				$_columns['id_period']					= array('hide' => TRUE);
				$_columns['id_property']				= array('hide' => TRUE);
				$_columns['id_issuing_account']			= array('hide' => TRUE);
				$_columns['id_receiving_account']		= array('hide' => TRUE);
				$_columns['detail_concept']				= array('hide' => TRUE);
				$_columns['detail_observations']		= array('hide' => TRUE);
				$_columns['detail_expire_date']			= array('hide' => TRUE);
				$_columns['detail_interest_since']		= array('hide' => TRUE);
				$_columns['detail_interest_rate']		= array('hide' => TRUE);
				$_columns['detail_original_ammount']	= array('hide' => TRUE);
				$_columns['detail_custom']				= array('hide' => TRUE);
				$_columns['detail_id_user_modifier']	= array('hide' => TRUE);
				$_columns['detail_id_detail_sibling']	= array('hide' => TRUE);
				$_columns['creation_date']				= array('hide' => TRUE);
				$_columns['modification_date']			= array('hide' => TRUE);
				$_columns['_checked']					= array('hide' => TRUE);
				$_columns['period_year']				= array('hide' => TRUE);
				$_columns['period_month']				= array('hide' => TRUE);
				$_columns['receipt_date']				= array('hide' => TRUE);
				$_columns['id_user_issuer']				= array('hide' => TRUE);
				$_columns['id_user_receiver']			= array('hide' => TRUE);
				$_columns['receipt_payed']				= array('hide' => TRUE);
				$_columns['receipt_pay_date']			= array('hide' => TRUE);
				$_columns['receipt_voided']				= array('hide' => TRUE);
				$_columns['receipt_description']		= array('hide' => TRUE);
				$_columns['receipt_observations']		= array('hide' => TRUE);
				$_columns['type_name']					= array('hide' => TRUE);
				$_columns['type_description']			= array('hide' => TRUE);
				$_columns['type_order']					= array('hide' => TRUE);
				$_columns['account_id_user_issuer']		= array('hide' => TRUE);
				$_columns['account_name_issuer']		= array('hide' => TRUE);
				$_columns['account_id_user_receiver']	= array('hide' => TRUE);
				$_columns['account_name_receiver']		= array('hide' => TRUE);
				$_columns['receipt_attachments']		= array('hide' => TRUE);
				$_columns['detail_attachments']			= array('hide' => TRUE);
				$_columns['sibling_receipt_attachments']= array('hide' => TRUE);

				$_options['create_entity'] 	 = FALSE;
				$_options['search'] 		 = FALSE;
				$_options['enable_search'] 	 = FALSE;
				$_options['enable_ipp'] 	 = FALSE;
				$_options['fixed_filters'] 	 = array(
										'id_expense_type' 			=> ET_IMPUESTO, // Son los imp o servicios
										'id_property'				=> $property['id_property'],
										#'_payed' 		  			=> 0, // No pagados
										#'detail_ammount>' 		  	=> 0, // Que tengan el importe cargado.
										'account_id_user_receiver IS NULL'  => NULL, // Pero solo los detalles destinados a pagar, no a cobrar.
								);
				$_options['fixed_orders']    = array('period_year' => 'DESC', 'period_month' => 'DESC', '_payed' => 'ASC', 'detail_description' => 'ASC');

				echo pachi_fullmanaged_table('vw_receipt_details', $_columns, $_options);
				unset($_columns);
				unset($_options);
			?>
	    </div>
	</div>
	
	<?php else: ?>

		<div class="portlet light ">
		    <div class="portlet-title tabbable-line">
		        <div class="caption caption-md">
		            <i class="icon-globe theme-font hide"></i>
		            <span class="caption-subject font-blue-madison bold uppercase">Impuestos y servicios</span>
		        </div>
		    </div>
		    <div class="portlet-body">
				<?php 
				                unset($cols);
				                $cols['expense_name']            =  array('custom_render' => '{expense_name}<br><small class="text-muted">{type_name}</small>');
				                $cols['expense_identification']  =  array('hide' => FALSE);
				                $cols['expense_payable']         =  array('hide' => TRUE);
				                $cols['expense_chargeable']      =  array('hide' => TRUE);
				                $cols['expense_checkable']       =  array('hide' => TRUE);
		

				                $cols['id_property_expense']     =  array('hide' => TRUE);
				                $cols['type_name']     		     =  array('hide' => TRUE);
				                $cols['id_property']             = array('hide' => TRUE);
				                $cols['id_expense']              = array('hide' => TRUE);
				                $cols['id_expense_type']         = array('hide' => TRUE);
				                $cols['expense_periodicity']     = array('hide' => TRUE);
				                $cols['expense_date_start']      = array('hide' => TRUE);
				                $cols['expense_observations']    = array('hide' => TRUE);
				                $cols['creation_date']           = array('hide' => TRUE);
				                $cols['modification_date']       = array('hide' => TRUE);
				                $cols['deletion_date']           = array('hide' => TRUE);
				                $cols['expense_website']         = array('hide' => TRUE);
				                $cols['expense_description']     = array('hide' => TRUE);
				                $cols['type_description']        = array('hide' => TRUE);
				                $cols['property_name']           = array('hide' => TRUE);
				                $cols['id_user_owner']				= array('hide' => TRUE);
				                $cols['ownership_date_start']		= array('hide' => TRUE);
				                $cols['owner_user_name']				= array('hide' => TRUE);
				                $cols['owner_user_lastname']			= array('hide' => TRUE);
				                $cols['owner_user_address']			= array('hide' => TRUE);
				                $cols['owner_user_email']			= array('hide' => TRUE);
				                $cols['owner_user_phone']			= array('hide' => TRUE);
				                $cols['owner_user_picture']			= array('hide' => TRUE);
				                $cols['consortium_user_name']		= array('hide' => TRUE);
				                $cols['consortium_user_lastname']	= array('hide' => TRUE);
				                $cols['consortium_user_address']		= array('hide' => TRUE);
				                $cols['consortium_user_email']		= array('hide' => TRUE);
				                $cols['consortium_user_phone']		= array('hide' => TRUE);
				                $cols['consortium_user_picture']		= array('hide' => TRUE);
				                $cols['tenant_user_name']			= array('hide' => TRUE);
				                $cols['tenant_user_lastname']		= array('hide' => TRUE);
				                $cols['tenant_user_address']			= array('hide' => TRUE);
				                $cols['tenant_user_email']			= array('hide' => TRUE);
				                $cols['tenant_user_phone']			= array('hide' => TRUE);
				                $cols['tenant_user_picture']			= array('hide' => TRUE);
				                $cols['id_contract']					= array('hide' => TRUE);
				                $cols['id_user_tenant']				= array('hide' => TRUE);
				                $cols['contract_date_start']			= array('hide' => TRUE);
				                $cols['contract_date_end']			= array('hide' => TRUE);
				                $cols['contract_rent_price']			= array('hide' => TRUE);
				                $cols['contract_interest']			= array('hide' => TRUE);
				                $cols['contract_tenant_attachments']	= array('hide' => TRUE);
				                $cols['contract_gas_check']			= array('hide' => TRUE);
				                $cols['contract_electricity_check']	= array('hide' => TRUE);
				                $cols['contract_expenses_check']		= array('hide' => TRUE);
				                $cols['contract_expenses_attachments']		= array('hide' => TRUE);
				                $cols['contract_payday_limit']				= array('hide' => TRUE);
				                $cols['contract_payday_start']				= array('hide' => TRUE);
				                $cols['contract_observations']				= array('hide' => TRUE);

				                unset($table_options);
				                $table_options['create_entity']       = FALSE;
				                $table_options['fixed_filters']       = array('id_property' => $property['id_property']);
				                $table_options['custom_identifiers']  = '#tab_expenses';
				                echo pachi_fullmanaged_table('vw_property_expenses', $cols, $table_options);
				 ?>
		    </div>
	    </div>
<?php endif ?>