<?php 
	unset($_columns);
	unset($_options);

	$_columns['period_code']	= array('width' => '90px');
	$_columns['id_period']		= array('hide' => TRUE);
	$_columns['id_property']	= array('hide' => TRUE);
	$_columns['description']	= array('custom_render' => '{description} <span class="pull-right">#=download_attachments|{attachments}|1=#</span>');
	$_columns['attachments']	= array('hide' => TRUE);

	$_options = FALSE;
	if (isset($periods)) {
		$_options['filters']			 = array();
		foreach ($periods as $_kperiod => $_period) 
			$_options['filters']['id_period='.$_period['id_period']] = $_period['period_code'];
	}

	$_options['custom_identifiers'] = '#tab_comprobantes';
?>


<?php echo pachi_fullmanaged_table('vw_receipt_attachemts', $_columns, $_options) ?>