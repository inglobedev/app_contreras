<?php 
	unset($_columns);
	unset($_options);

///'.APP_PROPIEDADES.'view/{id_property}
	 $_columns['id_contract']	= array('title' => 'Contrato',
	                                                                'custom_render' => '<a href="#">
	                                                                						{id_contract}
	                                                                					</a>'
	        														);

	 $_columns['contract_date_start']	= array('hide' => TRUE);
	 $_columns['contract_date_end']	= array('hide' => TRUE);
	
	
	 $_columns['id_property']	= array('hide' => TRUE);
	 $_columns['deletion_date']	= array('hide' => TRUE);
	 $_columns['modification_date']	= array('hide' => TRUE);
	 $_columns['contract_observations']	= array('hide' => TRUE);
	 $_columns['contract_expenses_attachments']	= array('hide' => TRUE);
	 $_columns['contract_tenant_attachments']	= array('hide' => TRUE);
	 $_columns['contract_expenses_check']	= array('hide' => TRUE);
	 $_columns['contract_electricity_check']	= array('hide' => TRUE);
	 $_columns['contract_gas_check']	= array('hide' => TRUE);
	 $_columns['contract_voided']	= array('hide' => TRUE);
	 $_columns['contract_archived']	= array('hide' => TRUE);
	 $_columns['contract_observations']	= array('hide' => TRUE);
	 $_columns['creation_date']	= array('hide' => TRUE);

	 $_columns['_status']                		= array('custom_render' => '{if [contract_voided] == 0}
            																			Activo
            																		{/if}
            																		{if [contract_voided] == 1}
            																			Finalizado
            																		{/if}
            																		
            																		',
            													'width' => '100',
            													'title' => 'Estado'
             													);

	  $_columns['_contact_period']            = array('custom_render' => '<small class="text-muted">
            																				#=print_date|{contract_date_start}=# al #=print_date|{contract_date_end}=#
            																			</small>
            																		',
            													'width' => '160',
            													'title' => 'Periodo'
            													);

	 	 $_columns['contract_commission']	= array();
		$_columns['_options']					= array(
														'title' => 'Acciones ',
														'width' => '130px',
														'class' => 'text-center',
														'custom_render' => 'actions'
														);

		

		  $_columns['tenant_user_name']               = array('custom_render' => '{tenant_user_name}
            																		{if [tenant_user_phone]}
            																			<br>
            																			<small class="text-muted">te: {tenant_user_phone}</small>
            																		{/if}
            																		',
            													'width' => '160',
            													);

	// $_options = FALSE;
	// if (isset($periods)) {
	// 	$_options['filters']			 = array();
	// 	foreach ($periods as $_kperiod => $_period) 
	// 		$_options['filters']['id_period='.$_period['id_period']] = $_period['period_code'];
	// }

	$_options['custom_identifiers'] = '#tab_contratos';
	$_options['fixed_filters']['id_property'] = $property['id_property'];
	//$_options['fixed_filters']['id_user_owner'] = $this->session->userdata('id_user');

?>


<?php echo pachi_fullmanaged_table('vw_contracts', $_columns, $_options) ?>

