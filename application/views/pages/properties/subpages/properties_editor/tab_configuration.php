<div class="portlet light ">
    <div class="portlet-title tabbable-line">
        <div class="caption">
            <span class="caption-subject font-blue-madison bold uppercase"><?php echo $this->lang->line('entity_property') ?></span>
        </div>
        <ul class="nav nav-tabs">
            <li class="active">
                <a href="#tab_attributes" data-toggle="tab" aria-expanded="false"> <?php echo ucfirst($this->lang->line('lbl_attributes')) ?> </a>
            </li>
            <li class="">
                <a href="#tab_expenses" data-toggle="tab" aria-expanded="true">  <?php echo ucfirst($this->lang->line('entity_expenses')) ?>  </a>
            </li>
            <li class="">
                <a href="#tab_owner" data-toggle="tab" aria-expanded="false">  <?php echo ucfirst($this->lang->line('entity_owner')) ?>  </a>
            </li>
        </ul>
    </div>
    <div class="portlet-body">
        <div class="tab-content">
            <div class="tab-pane active" id="tab_attributes">
                <?php 
                	unset($consortium_form);
                    $consortium_form = array(
                                            'user_name',
                                            'user_lastname',
                                            'user_address',
                                            'user_email',
                                            'user_cuit',
                                            'user_phone',
                                            'user_observation',
                                            'user_password' => array('input_type' => 'hidden',
                                                                     'value' => '123456'),
                                            'id_role'       => array('input_type' => 'hidden',
                                                                     'value' => ROLE_CONSORTIUM),
                                            );

                    unset($cols);
                    $cols = array(  
                                    'property_name',
                                    'property_address',
                                    'property_year',
                                    'property_garage',
                                    'property_balcony',
                                    'property_building_name',
                                    'property_cadastre_number',
                                    'property_observations',
                                    'id_user_consortium' => array(  // Buscador
                                                'reference' => array( //'table' => 'users',         // aca podria determinar otra tabla distinta para buscar.
                                                                      //'display' => 'user_name',   // Si bien por defecto muestra el _name, podria mostrar otra columna.
                                                                        'custom_display' => '{user_name} {user_lastname}',   // O una comb de columnas.
                                                                        'filter' => array('id_role' => ROLE_CONSORTIUM)
                                                                        ),
                                                                        // Modal de creacion.
                                                'modal' => array('columns'  => $consortium_form, 
                                                                 'modal_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_consortium'),
                                                                 'button_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_consortium')
                                                                ), // aca determino que columna es la que se muestra en el buscador.
                                                // 'multiple' => TRUE // Podria ser un selector multiple,
                                            ),
                                );

                    echo pachi_form('properties', $cols, $property['id_property']);
                ?>
            </div>

            <div class="tab-pane " id="tab_expenses">
                <?php 
                unset($cols);
                $cols['expense_name']            =  array('custom_render' => '{expense_name}<br><small class="text-muted">{type_name}</small>');
                $cols['expense_identification']  =  array('hide' => FALSE);
                $cols['expense_payable']         =  array('class' => 'text-center',
	                                                                    'custom_render' => '{if [expense_payable] > 0}
	                                                                    						<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="checkboxes chk_expense_update" data-id_property_expense="{id_property_expense}" data-attribute="expense_payable" checked value="1"><span></span></label>
																							{else}
	                                                                    						<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="checkboxes chk_expense_update" data-id_property_expense="{id_property_expense}" data-attribute="expense_payable" value="1"><span></span></label>
																							{/if}
																							');
                $cols['expense_chargeable']      =  array('class' => 'text-center',
	                                                                    'custom_render' => '{if [expense_chargeable] > 0}
	                                                                    						<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="checkboxes chk_expense_update" data-id_property_expense="{id_property_expense}" data-attribute="expense_chargeable" checked value="1"><span></span></label>
																							{else}
	                                                                    						<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="checkboxes chk_expense_update" data-id_property_expense="{id_property_expense}" data-attribute="expense_chargeable" value="1"><span></span></label>
																							{/if}
																							');
                $cols['expense_checkable']       =  array('class' => 'text-center',
	                                                                    'custom_render' => '{if [expense_checkable] > 0}
	                                                                    						<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="checkboxes chk_expense_update" data-id_property_expense="{id_property_expense}" data-attribute="expense_checkable" checked value="1"><span></span></label>
																							{else}
	                                                                    						<label class="mt-checkbox mt-checkbox-single mt-checkbox-outline"><input type="checkbox" class="checkboxes chk_expense_update" data-id_property_expense="{id_property_expense}" data-attribute="expense_checkable" value="1"><span></span></label>
																							{/if}
																							');
                $cols['delete']                  = array('title' => '',
                                                                    'class' => 'text-center',
                                                                    'custom_render' => '<a href="javascript:;" class="btn btn-outline btn-circle dark btn-sm black btn_delete_expense" data-return_url="'. base_url(uri_string()) . '?x='.rand(1,5000) .'#tab_expenses" data-id_property_expense="{id_property_expense}" data-confirmation="¿Estás seguro?"><i class="fa fa-trash-o"></i> </a>');
                

                $cols['id_property_expense']     =  array('hide' => TRUE);
                $cols['type_name']     			=  array('hide' => TRUE);
                $cols['id_property']             = array('hide' => TRUE);
                $cols['id_expense']              = array('hide' => TRUE);
                $cols['id_expense_type']         = array('hide' => TRUE);
                $cols['expense_periodicity']     = array('hide' => TRUE);
                $cols['expense_date_start']      = array('hide' => TRUE);
                $cols['expense_observations']    = array('hide' => TRUE);
                $cols['creation_date']           = array('hide' => TRUE);
                $cols['modification_date']       = array('hide' => TRUE);
                $cols['deletion_date']           = array('hide' => TRUE);
                $cols['expense_website']         = array('hide' => TRUE);
                $cols['expense_description']     = array('hide' => TRUE);
                $cols['type_description']        = array('hide' => TRUE);
                $cols['property_name']           = array('hide' => TRUE);
                $cols['id_user_owner']				= array('hide' => TRUE);
                $cols['ownership_date_start']		= array('hide' => TRUE);
                $cols['owner_user_name']				= array('hide' => TRUE);
                $cols['owner_user_lastname']			= array('hide' => TRUE);
                $cols['owner_user_address']			= array('hide' => TRUE);
                $cols['owner_user_email']			= array('hide' => TRUE);
                $cols['owner_user_phone']			= array('hide' => TRUE);
                $cols['owner_user_picture']			= array('hide' => TRUE);
                $cols['consortium_user_name']		= array('hide' => TRUE);
                $cols['consortium_user_lastname']	= array('hide' => TRUE);
                $cols['consortium_user_address']		= array('hide' => TRUE);
                $cols['consortium_user_email']		= array('hide' => TRUE);
                $cols['consortium_user_phone']		= array('hide' => TRUE);
                $cols['consortium_user_picture']		= array('hide' => TRUE);
                $cols['tenant_user_name']			= array('hide' => TRUE);
                $cols['tenant_user_lastname']		= array('hide' => TRUE);
                $cols['tenant_user_address']			= array('hide' => TRUE);
                $cols['tenant_user_email']			= array('hide' => TRUE);
                $cols['tenant_user_phone']			= array('hide' => TRUE);
                $cols['tenant_user_picture']			= array('hide' => TRUE);
                $cols['id_contract']					= array('hide' => TRUE);
                $cols['id_user_tenant']				= array('hide' => TRUE);
                $cols['contract_date_start']			= array('hide' => TRUE);
                $cols['contract_date_end']			= array('hide' => TRUE);
                $cols['contract_rent_price']			= array('hide' => TRUE);
                $cols['contract_interest']			= array('hide' => TRUE);
                $cols['contract_tenant_attachments']	= array('hide' => TRUE);
                $cols['contract_gas_check']			= array('hide' => TRUE);
                $cols['contract_electricity_check']	= array('hide' => TRUE);
                $cols['contract_expenses_check']		= array('hide' => TRUE);
                $cols['contract_expenses_attachments']		= array('hide' => TRUE);
                $cols['contract_payday_limit']				= array('hide' => TRUE);
                $cols['contract_payday_start']				= array('hide' => TRUE);
                $cols['contract_observations']				= array('hide' => TRUE);

                unset($consortium_form);
                $consortium_form = array(
                                    'expense_name',
                                    'expense_description',
                                    'id_expense_type'
                                    );

                $form_options['id_property'] = array(
                                                    'input_type' => 'hidden',
                                                     'value' => $property['id_property']);
                $form_options['id_expense']     = array(  // Buscador
                                        'title' => 'Gastos',
                                        'reference' => array(   //'table' => 'expenses', // aca podria determinar otra tabla distinta para buscar.
                                                                'display' => 'expense_name',   // Si bien por defecto muestra el _name, podria mostrar otra columna.
                                                                'filter' => array('id_expense_type' => ET_IMPUESTO)
                                                                ),
                                                                // Modal de creacion.
                                        'modal' => array('columns'  => $consortium_form, 
                                                         'modal_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_expense'),
                                                         'button_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_expense'),
                                                        ) // aca determino que columna es la que se muestra en el buscador.
                                    );

                #unset($form_options);
                $form_options['expense_identification'] = array(); //array('input_type' => 'hidden', 'value' => 0);
                $form_options['expense_payable'] 		= array(); //array('input_type' => 'hidden', 'value' => 0);
                $form_options['expense_chargeable'] 	= array(); //array('input_type' => 'hidden', 'value' => 0);
                $form_options['expense_checkable'] 		= array(); //array('input_type' => 'hidden', 'value' => 0);

                unset($table_options);
                $table_options['create_entity']   = array(   'button_title'  => ucfirst($this->lang->line('lbl_add')) . ' ' . $this->lang->line('entity_expense'),
                                                       'table'         => 'property_expenses',
                                                       'columns'       => $form_options,
                                                       'save_action'   => array('action' => 'redirect', 'target' => base_url(uri_string()) . '?x='.rand(1,5000).'#tab_expenses', 'delay' => '500')
                                                       //'filter' => array('id_property' => $property['id_property'])
                                                    #   'target'        => '?action=new', // Si se especifica target, no se tiene en cuenta table y columns. Se redirige al target.
                                                        );

                $table_options['fixed_filters']       = array('id_property' => $property['id_property']);
                $table_options['custom_identifiers']  = '#tab_expenses';
                echo pachi_fullmanaged_table('vw_property_expenses', $cols, $table_options); ?>
            </div>

            <div class="tab-pane" id="tab_owner">
		        <?php 
		        	unset($cols);
			        $cols['owner_user_name']						= array('width' => '250px',
			        														'custom_render' => "{owner_user_name} {owner_user_lastname}");
			        $cols['ownership_date_start']				= array('width' => '20%',
			    															'custom_render' => "#=print_date|{ownership_date_start}=#");
			        $cols['ownership_date_end']					= array('width' => '20%',
			    															'custom_render' => "{if [ownership_date_end]}
			    																					#=print_date|{ownership_date_end}=#
			    																				{else}
			    																					Actualmente
			    																				{/if}
			    																				");
			        $cols['ownership_administrative_rate']		= array('width' => '100px',
			        														'custom_render' => "#=number_format|{ownership_administrative_rate}|2=# %",
			        														'class' => 'text-center'
			    															);
			        $cols['creation_date']						= array('hide' => TRUE);
			        $cols['modification_date']					= array('hide' => TRUE);
			        $cols['deletion_date']						= array('hide' => TRUE);
			        $cols['id_user']								= array('hide' => TRUE);
			        $cols['id_role']								= array('hide' => TRUE);
			        $cols['id_property']							= array('hide' => TRUE);
			        $cols['id_user_owner']						= array('hide' => TRUE);
			        $cols['id_ownership']						= array('hide' => TRUE);
			        $cols['owner_user_lastname']					= array('hide' => TRUE);
			        $cols['owner_user_address']					= array('hide' => TRUE);
			        $cols['owner_user_email']					= array('hide' => TRUE);
			        $cols['owner_user_phone']					= array('hide' => TRUE);
			        $cols['owner_user_cuit']						= array('hide' => TRUE);
			        $cols['owner_user_picture']					= array('hide' => TRUE);
			        $cols['owner_user_archived']					= array('hide' => TRUE);
			        $cols['owner_user_deletion_date']			= array('hide' => TRUE);

			        unset($cols_ownerships);
		            $cols_ownerships['id_property']                  = array('input_type' => "hidden", 'value' => $property['id_property']);
		            $cols_ownerships['id_user_owner']          		 = array(    // Buscador
			                                                                 'reference' => 
			                                                                    array(
			                                                                          'filter' => array('id_role' => ROLE_OWNER),
			                                                                          'custom_display' => '{user_name}, {user_lastname}'
			                                                                          ), // Pasamos filtros al buscador, ej roles > 2
			                                                                ); // aca determino que columna es la que se muestra en el buscador.
		            $cols_ownerships['ownership_administrative_rate'] = array();
		        ?>

		        <?php 
		        	// Opciones de toolbar.
					// create_entity: 
		        	// - Puede ser falso y no se muestra el boton de nuevo.
		        	// - Puede ser true, y se muestra para la tabla en cuestion, no aplica para views.
		        	// - Puede ser un array, con las keys: table, columns, donde table especifica la tabla con la que trabaja y columns una config de columnas.
					unset($_options);
					$_options['create_entity'] 	 = array(	'button_title' 	=> ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_owner'),
															'table' 		=> 'ownerships',
															'columns' 		=> $cols_ownerships,
		                                                    'custom_target'   => '/ajax/Ajax_properties/create_ownership'
														#	'target' 		=> '?action=new', // Si se especifica target, no se tiene en cuenta table y columns. Se redirige al target.
															);
					// La opcion de buscador, busca sobre todas las columnas de tipo text o varchar.
					// Puede ser TRUE o FALSE.
					$_options['search'] 		 = TRUE;
					// La opcion filters puede ser FALSE o ARRAY. 
					// Si es array, la etrutura es condicion => nombre del filtro.
					// donde condicion puede ser:
					// - simple: columna <>= valor
					// - cominado: (columna1 <>= valor AND/OR columna2 <>= valor)
					$_options['filters'] 		 = FALSE; /*array(
													'(_count_quotes_ready >= 0 AND id_maintenance_quote IS NULL AND maintenance_archived = 0)' => 'Presupuestados',
													'(maintenance_completed = 0 AND id_maintenance_quote IS NOT NULL AND maintenance_archived = 0)' => 'A Realizar',
		                                            '(maintenance_completed = 1  AND maintenance_archived = 0)' => 'Realizados',
													'maintenance_archived  = 1 OR maintenance_archived  = 1' => 'Archivados',
													);*/
					// Se puede pasar tambien una serie de condiciones que seran fijas, para modificar el set
					// de datos incial con el que se genera la tabla. Luego arriba se aplican los filters variables.
					$_options['fixed_filters'] 	 = array(
													'id_property' => $property['id_property']
													);
					$_options['fixed_orders'] 	        = array('id_ownership' => 'DESC');
                    $_options['custom_identifiers']     = '#tab_owner';
		        ?>

		        <?php echo pachi_fullmanaged_table('vw_ownerships', $cols, $_options); ?>
            </div>
        </div>
    </div>
</div>