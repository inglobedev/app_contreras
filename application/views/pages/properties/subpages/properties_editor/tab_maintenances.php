        <?php 
        	// <a href="/'.APP_MAINTENANCES.'view/{id_maintenance}">{maintenance_name}</a><br>
        	$_columns['maintenance_date']               = array('custom_render' => '#=print_date|{maintenance_date}=#');
            $_columns['maintenance_name'] 				= array('custom_render' => '<a href="/'.APP_MAINTENANCES.'view/{id_maintenance}">{maintenance_name}</a><br>
            																		<small class="text-muted">
            																			{maintenance_description}
            																		</small>
            																	   ',
            													);
            $_columns['property_name']                  = array('hide' => TRUE);
            $_columns['professional_user_name']         = array('custom_render' => '{if [quote_price]}
        																				$ #=number_format|{quote_price}|2=#
        																			{/if}
            																		{if [id_user_professional] > 0}
            																			{professional_user_name} {professional_user_lastname} <br>
            																			<small class="text-muted">
            																				{professional_user_phone}
            																			</small>
            																		{/if}
            																		',
            													'width' => '120px',
            													'sortable' => TRUE
            													);
            $_columns['quote_price'] 					= array('title' => ucfirst($this->lang->line('entity_quote')),
        														'custom_render' => '{if [quote_price]}
        																				$ #=number_format|{quote_price}|2=#
        																			{/if}
        																			',
        														'width' => '120px',
        														'hide' => TRUE
        														);
            $_columns['maintenance_owner_cost'] 		= array('title' => ucfirst($this->lang->line('entity_owner')),
        														'custom_render' => '$ #=number_format|{maintenance_owner_cost}|2=#
        																			{if [id_user_owner] > 0}
            																			<br><small class="text-muted">{owner_user_name} {owner_user_lastname}</small>
            																		{/if}
					        													   ',
        														'width' => '120px'
        														);
            $_columns['maintenance_tenant_cost'] 		= array('title' => ucfirst($this->lang->line('entity_tenant')),
        														'custom_render' => '$ #=number_format|{maintenance_tenant_cost}|2=#
        																			{if [id_user_owner] > 0}
            																			<br><small class="text-muted">{tenant_user_name} {tenant_user_lastname}</small>
            																		{/if}
            																	   ',
        														'width' => '120px'
        														);
            $_columns['maintenance_administrative_fee'] = array('title' => ucfirst($this->lang->line('entity_administration')),
        														'custom_render' => '$ #=number_format|{maintenance_administrative_fee}|2=#',
            													'sortable' => TRUE,
            													'width' => '100',
            													'class' => 'text-center',
            													'hide' => TRUE
        														);
			$_columns['_status']                		= array('custom_render' => '{if [maintenance_status] == 0}
            																			Pendiente
            																		{/if}
            																		{if [maintenance_status] == 1}
            																			Aprobado
            																		{/if}
            																		{if [maintenance_status] == 2}
            																			Rechazado
            																		{/if}
            																		{if [maintenance_completed]}
            																			Realizado
            																		{/if}
            																		',
            													'width' => '100',
            													'sortable' => TRUE,
            													);
			$_columns['_options']					= array(
														'title' => ' ',
														'width' => '130px',
														'class' => 'text-center',
														'custom_render' => '<div class="btn btn-group padding-none">
																			<a title="Editar" class="btn btn-default btn-sm margin-none" href="/'.APP_MAINTENANCES.'view/{id_maintenance}">
																				<i class="fa fa-pencil"></i>
																			</a>
																			{if [maintenance_archived]}
																				<a title="Restaurar" class="btn btn-default btn-sm btn-restore-maintenance margin-none" href="javascript:;" data-id_maintenance="{id_maintenance}">
																					<i class="fa fa-undo"></i>
																				</a>
																			{else}
																				<a title="Archivar" class="btn btn-default margin-none btn-sm btn-archive-maintenance" href="javascript:;" data-id_maintenance="{id_maintenance}">
																					<i class="fa fa-archive"></i>
																				</a>
																			{/if}
																			<a title="Borrar" class="btn btn-danger btn-sm margin-none btn-delete-maintenance" data-id_maintenance="{id_maintenance}" href="javascript:;" data-confirmation="¿Seguro?">
																				<i class="fa fa-trash-o"></i>
																			</a>
																			</div>
																			',
														'hide' => TRUE);

            $_columns['_count_quotes_ready'] 			= array('hide' => TRUE);
            $_columns['_count_quotes'] 					= array('hide' => TRUE);
            $_columns['owner_user_lastname'] 			= array('hide' => TRUE);
            $_columns['tenant_user_name']               = array('hide' => TRUE);
			$_columns['owner_user_name']                = array('hide' => TRUE);
            $_columns['maintenance_completed'] 			= array('hide' => TRUE);
            $_columns['maintenance_status']             = array('hide' => TRUE);
            $_columns['maintenance_date']               = array('hide' => TRUE);
            $_columns['maintenance_archived'] 			= array('hide' => TRUE);
            $_columns['professional_user_lastname'] 	= array('hide' => TRUE);
            $_columns['maintenance_description'] 		= array('hide' => TRUE);
            $_columns['property_address'] 				= array('hide' => TRUE);
            $_columns['id_user_owner'] 					= array('hide' => TRUE);
            $_columns['id_maintenance'] 				= array('hide' => TRUE);
            $_columns['id_property'] 					= array('hide' => TRUE);
            $_columns['id_maintenance_quote'] 			= array('hide' => TRUE);
            $_columns['maintenance_observations'] 		= array('hide' => TRUE);
            $_columns['id_user_professional'] 			= array('hide' => TRUE);
            $_columns['quote_observations'] 			= array('hide' => TRUE);
            $_columns['quote_attachments'] 				= array('hide' => TRUE);
            $_columns['quote_ready'] 					= array('hide' => TRUE);
            $_columns['professional_user_address'] 		= array('hide' => TRUE);
            $_columns['professional_user_email'] 		= array('hide' => TRUE);
            $_columns['professional_user_phone'] 		= array('hide' => TRUE);
            $_columns['professional_user_picture'] 		= array('hide' => TRUE);
            $_columns['_calc_quotes'] 					= array('hide' => TRUE);
            $_columns['_avg_quote_price'] 				= array('hide' => TRUE);
            $_columns['tenant_user_lastname']           = array('hide' => TRUE);
            $_columns['tenant_user_name']		        = array('hide' => TRUE);
            $_columns['id_user_tenant']		        	= array('hide' => TRUE);

            $columns_mantenice['id_property']                   = array('input_type' => "hidden", 'value' => $property['id_property']); // aca determino que columna es la que se muestra en el buscador.);
            $columns_mantenice['id_maintenance']                = array('value' => 0, 'input_type' => 'hidden');
            $columns_mantenice['maintenance_name']              = array();
            $columns_mantenice['maintenance_description']       = array();
            $columns_mantenice['maintenance_date']              = array();
            $columns_mantenice['id_user_professional']          = array(    // Buscador
                                                                'reference' => 
                                                                    array('table' => 'users', // aca podria determinar otra tabla distinta para buscar.
                                                                          'column' => 'id_user', // 
                                                                          'display' => 'user_name',   // Si bien por defecto muestra el _name, podria mostrar otra columna.
                                                                          'custom_display' => '{user_name}, {user_lastname}',   // Si bien por defecto muestra el _name, podria mostrar otra columna, se pueden agregar funciones "#= funcion =#"
                                                                          'filter' => array('id_role' => ROLE_PROFESSIONAL),
                                                                          ), // Pasamos filtros al buscador, ej roles > 2
                                                                ); // aca determino que columna es la que se muestra en el buscador.
            $columns_mantenice['quote_price']                   = array();
            $columns_mantenice['quote_attachments']             = array();
            $columns_mantenice['maintenance_owner_cost']        = array('class' => 'pachi-mask-number');
            $columns_mantenice['maintenance_tenant_cost']       = array('class' => 'pachi-mask-number');
            $columns_mantenice['maintenance_completed']         = array('input_type' => "hidden");
            $columns_mantenice['maintenance_status']          	= array('input_type' => "hidden");          

        ?>

        <?php 
        	// Opciones de toolbar.
			// create_entity: 
        	// - Puede ser falso y no se muestra el boton de nuevo.
        	// - Puede ser true, y se muestra para la tabla en cuestion, no aplica para views.
        	// - Puede ser un array, con las keys: table, columns, donde table especifica la tabla con la que trabaja y columns una config de columnas.
			$_options['create_entity'] 	 = array(	'button_title' 	=> ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_maintenance'),
													'table' 		=> 'vw_maintenances',
													'columns' 		=> $columns_mantenice,
                                                    'custom_target' => '/ajax/Ajax_maintenance/create_maintenance',
                                                    'save_action' 	=> array('action' => 'redirect', 'target' => base_url(uri_string()) . '?x='.rand(1,5000).'#tab_mantenimientos', 'delay' => '500')
												#	'target' 		=> '?action=new', // Si se especifica target, no se tiene en cuenta table y columns. Se redirige al target.
													);
			// La opcion de buscador, busca sobre todas las columnas de tipo text o varchar.
			// Puede ser TRUE o FALSE.
			$_options['search'] 		 = TRUE;
			// La opcion filters puede ser FALSE o ARRAY. 
			// Si es array, la etrutura es condicion => nombre del filtro.
			// donde condicion puede ser:
			// - simple: columna <>= valor
			// - cominado: (columna1 <>= valor AND/OR columna2 <>= valor)
			$_options['filters'] 		 = array(
											'(_count_quotes_ready >= 0 AND id_maintenance_quote IS NULL AND maintenance_archived = 0)' => 'Presupuestados',
											'(maintenance_completed = 0 AND id_maintenance_quote IS NOT NULL AND maintenance_archived = 0)' => 'A Realizar',
                                            '(maintenance_completed = 1  AND maintenance_archived = 0)' => 'Realizados',
											"maintenance_archived  = 1 OR (maintenance_archived  = 1 AND id_property = {$property['id_property']})" => 'Archivados',
											);
			// Se puede pasar tambien una serie de condiciones que seran fijas, para modificar el set
			// de datos incial con el que se genera la tabla. Luego arriba se aplican los filters variables.
			$_options['fixed_filters'] 	 = array(
											'maintenance_archived' => 0,
											'id_property' => $property['id_property']
											);
            $_options['custom_identifiers']     = '#tab_mantenimientos';
        ?>

        <?php echo pachi_fullmanaged_table('vw_maintenances', $_columns, $_options); ?>