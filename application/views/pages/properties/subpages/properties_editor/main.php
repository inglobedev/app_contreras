<?php $cancel_url = base_url(APP_PROPIEDADES);  ?>
<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="profile-sidebar col-sm-4" style="width: 33.33%;margin-right: 0;">
                <div class="portlet light profile-sidebar-portlet ">
                    <div class="profile-userpic">
                    </div>
                    <div class="profile-usertitle">
                        <div class="profile-usertitle-name"> 
                            <?php echo $property['property_name'] ?> 
                            <?php if ($property['property_archived']): ?>
                                <small>[archivado]</small>
                            <?php endif ?>
                        </div>
                        <div class="profile-usertitle-job">  <?php echo $property['property_address'] ?>  </div>
                    </div>
                    
                    <div class="profile-usermenu padding-none">
                        <ul class="nav">
                            <li class="active">
                                <a href="#tab_datos" data-toggle="tab"><i class="icon-home"></i>Información general</a>
                            </li>
                            <li>
                                <a href="#tab_gastos" data-toggle="tab"><i class="icon-pie-chart"></i>Impuestos y servicios</a>
                            </li>
                            <?php if ($this->session->userdata('id_role') != ROLE_OWNER): ?>
                            <li>
                                <a href="#tab_mantenimientos" data-toggle="tab"><i class="icon-briefcase"></i>Mantenimiento</a>
                            </li>
                            <?php endif ?>
                            <li class="hidden">
                                <a href="#tab_cuenta" data-toggle="tab"><i class="icon-credit-card"></i>Cuenta corriente</a>
                            </li>
                            <li>
                                <a href="#tab_comprobantes" data-toggle="tab"><i class="icon-docs"></i>Comprobantes</a>
                            </li>
                             <li>
                                <a href="#tab_contratos" data-toggle="tab"><i class="icon-docs"></i>Contratos</a>
                            </li>
                            <?php if ($this->session->userdata('id_role') != ROLE_OWNER): ?>
                            <li>
                                <a href="#tab_config" data-toggle="tab"><i class="icon-wrench"></i>Configuracion</a>
                            </li>                               
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="profile-content col-sm-8">
                <div class="row">
                    <div class="col-md-12">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_datos">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Visión general</span>
                                        </div>
                                        <div class="actions hidden">
                                            <a href="#mod_recibo_agregar" data-toggle="modal" class="btn btn-primary ">
                                                <i class="fa fa-plus"></i> Registrar cobro
                                            </a>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                    	<?php 
                                    		# debugger($property)
                                    		# Falta completar datos
                                    	?>
                                        <div class="row">
                                            <div class="col-md-3 custom-box-property">
                                                <h5>Propietario</h5>
                                                <h5 class="font-blue-sharp">
                                                    <span data-counter="counterup"><?php echo $property['owner_user_name'] ?></span>
                                                </h5>
                                                <small>Desde: <?php echo print_date($property['ownership_date_start']) ?></small>
                                            </div>
                                            <?php if (!empty($property['tenant_user_name'])): ?>
                                            <div class="col-md-3 custom-box-property">
                                                <h5>Inquilino</h5>
                                                <h5 class="font-blue-sharp">
                                                    <span data-counter="counterup"><?php echo $property['tenant_user_name'] ?></span>
                                                </h5>
                                                <?php if (!empty($property['tenant_user_phone'])): ?>
                                                    <small><?php echo $property['tenant_user_phone'] ?></small>
                                                <?php else: ?>
                                                    <small>Tel: <?php echo ucfirst($this->lang->line('lbl_not_phone')) ?></small>
                                                <?php endif ?>
                                            </div>
                                            <?php endif ?>
                                            <?php foreach($guarantor as $k_guarantor => $guarantor_): ?>
                                                <div class="col-md-3 custom-box-property">
                                                    <h5>Garante</h5>
                                                    <h5 class="font-blue-sharp">
                                                        <span data-counter="counterup"><?php echo ucfirst(strtolower($guarantor_['guarantor_user_name'])) ?></span>
                                                    </h5>
                                                    <?php if (!empty($guarantor_['guarantor_user_phone'])): ?>
                                                        <small><?php echo $guarantor_['guarantor_user_phone']?></small>
                                                    <?php else: ?>
                                                        <small>Tel: <?php echo ucfirst($this->lang->line('lbl_not_phone')) ?></small>
                                                    <?php endif ?>
                                                </div>
                                            <?php endforeach ?>
                                            <div class="col-md-3 custom-box-property">
                                                <h5>Consorcio</h5>
                                                <h5 class="font-blue-sharp">
                                                    <span data-counter="counterup"><?php echo ucfirst(strtolower($property['consortium_user_name'])) ?></span>
                                                </h5>
                                                <?php if (!empty($property['consortium_user_phone'])): ?>
                                                    <small><?php echo $guarantor_['consortium_user_phone']?></small>
                                                <?php else: ?>
                                                    <small>Tel: <?php echo ucfirst($this->lang->line('lbl_not_phone')) ?></small>
                                                <?php endif ?>
                                            </div>
                                            <?php if (!empty($property['contract_rent_price'])): ?>
                                                <div class="col-md-3 custom-box-property">
                                                    <h5 class="font-green-sharp">
                                                        <span data-counter="counterup">Alquiler</span>
                                                    </h5>
                                                    <small>$<?php echo number_format($property['contract_rent_price'], 2) ?></small>
                                                </div>
                                            <?php endif ?>
                                            <div class="col-md-3 custom-box-property">
                                                <h5 class="font-red-haze">
                                                    <span data-counter="counterup">Deuda</span>
                                                </h5>
                                                <small>$<?php echo number_format(3000, 2) ?></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php if (FALSE): ?>
                                        <div class="table-scrollable">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th class="" style="width:400px"> <?php echo ucfirst($this->lang->line('lbl_period')) ?></th>
                                                        <th class="text-center" style="width:100px"> <?php echo ucfirst($this->lang->line('lbl_taxes')) ?> </th>
                                                        <th class="text-center" style="width:100px"> <?php echo ucfirst($this->lang->line('entity_expenses')) ?> </th>
                                                        <th class="text-center" style="width:100px"> <?php echo ucfirst($this->lang->line('entity_maintenances')) ?> </th>
                                                        <th class="text-center" style="width:100px"> <?php echo ucfirst($this->lang->line('lbl_rental')) ?> </th>
                                                        <th class="text-center" style="width:100px"> <?php echo ucfirst($this->lang->line('lbl_administrative_expenses')) ?> </th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td class=""> 01/018 </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class=""> 02/018  </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"><i class="fa fa-exclamation-triangle" style="color: #f0a62f"></i></td>
                                                    </tr>
                                                    <tr>
                                                        <td class=""> 03/018  </td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-exclamation-triangle" style="color: #f0a62f"></i></td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-exclamation-triangle" style="color: #f0a62f"></i></td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                    </tr>
                                                    <tr>
                                                        <td class=""> 04/018  </td>
                                                        <td class="text-center"> <i class="fa fa-exclamation-triangle" style="color: #f0a62f"></i></td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"> <i class="fa fa-exclamation-triangle" style="color: #f0a62f"></i></td>
                                                        <td class="text-center"> <i class="fa fa-check" style="color:#3ec03e"></i> </td>
                                                        <td class="text-center"><i class="fa fa-exclamation-triangle" style="color: #f0a62f"></i></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                        <?php endif ?>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_gastos">
                                <?php $this->load->view('pages/properties/subpages/properties_editor/tab_taxes') ?>
                            </div>

                            <div class="tab-pane" id="tab_mantenimientos">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Mantenimientos</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php $this->load->view('pages/properties/subpages/properties_editor/tab_maintenances') ?>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_comprobantes">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Comprobantes</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php $this->load->view('pages/properties/subpages/properties_editor/tab_comprobantes') ?>
                                    </div>
                                </div>
                            </div>

                             <div class="tab-pane" id="tab_contratos">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Contratos</span>
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <?php $this->load->view('pages/properties/subpages/properties_editor/tab_contratos') ?>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_cuenta">
                                <div class="portlet light ">
                                    <div class="portlet-title tabbable-line">
                                        <div class="caption caption-md">
                                            <i class="icon-globe theme-font hide"></i>
                                            <span class="caption-subject font-blue-madison bold uppercase">Cuenta corriente</span>
                                        </div>
                                        <ul class="nav nav-tabs">
                                            <li class="dropdown">
                                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown"> Cuentas
                                                    <i class="fa fa-angle-down"></i>
                                                </a>
                                                <ul class="dropdown-menu" role="menu">
                                                    <li>
                                                        <a href="#admin" tabindex="-1" data-toggle="tab"> <?php echo ucfirst($this->lang->line('lbl_administration')) ?></a>
                                                    </li>
                                                    <li>
                                                        <a href="#inqui" tabindex="-1" data-toggle="tab"> <?php echo ucfirst($this->lang->line('entity_tenant')) ?> </a>
                                                    </li>
                                                    <li>
                                                        <a href="#prop" tabindex="-1" data-toggle="tab"> <?php echo ucfirst($this->lang->line('entity_owner')) ?> </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="portlet-body">
                                            <div class="tab-content">
                                                <div class="tab-pane fade active in" id="admin">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Administración</span>
                                                    </div>
                                                    <div class="table-scrollable">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="" style="width:175px"><?php echo ucfirst($this->lang->line('lbl_period')) ?></th>
                                                                    <th class=""> <?php echo ucfirst($this->lang->line('lbl_concept')) ?></th>
                                                                    <th class="" style="width:100px"> Importe</th>
                                                                    <th class="" style="width:100px"> Saldo</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class=""> 01/01/018 14:00:00hs</td>
                                                                    <td class=""> Alquiler</td>
                                                                    <td class="" style="color:green"> $<?php echo number_format(1000, 2) ?></td>
                                                                    <td class=""> $<?php echo number_format(6000, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class=""> 01/02/018 11:40:30hs</td>
                                                                    <td class=""> Administracion</td>
                                                                    <td class="" style="color:red"> $<?php echo number_format(2000, 2) ?></td>
                                                                    <td class=""> $<?php echo number_format(5000, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class=""> 05/03/018 17:20:00hs</td>
                                                                    <td class=""> Gasto</td>
                                                                    <td class="" style="color:green"> $<?php echo number_format(2000, 2) ?></td>
                                                                    <td class=""> $<?php echo number_format(7000, 2) ?></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="inqui">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Inquilino</span>
                                                    </div>
                                                    <div class="table-scrollable">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="" style="width:175px"> Fecha</th>
                                                                    <th class=""> Concepto</th>
                                                                    <th class="" style="width:100px"> Importe</th>
                                                                    <th class="" style="width:100px"> Saldo</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class=""> 01/01/018 14:00:00hs</td>
                                                                    <td class=""> Alquiler</td>
                                                                    <td class="" style="color:green"> $<?php echo number_format(1000, 2) ?></td>
                                                                    <td class=""> $<?php echo number_format(6000, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class=""> 01/02/018 11:40:30hs</td>
                                                                    <td class=""> Administracion</td>
                                                                    <td class="" style="color:red"> $<?php echo number_format(2000, 2) ?></td>
                                                                    <td class=""> $<?php echo number_format(5000, 2) ?></td>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <div class="tab-pane fade" id="prop">
                                                    <div class="caption caption-md">
                                                        <i class="icon-globe theme-font hide"></i>
                                                        <span class="caption-subject font-blue-madison bold uppercase">Propietario</span>
                                                    </div>
                                                    <div class="table-scrollable">
                                                        <table class="table table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th class="" style="width:175px"> Fecha</th>
                                                                    <th class=""> Concepto</th>
                                                                    <th class="" style="width:100px"> Importe</th>
                                                                    <th class="" style="width:100px"> Saldo</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                    <td class=""> 01/01/018 14:00:00hs</td>
                                                                    <td class=""> Alquiler</td>
                                                                    <td class="" style="color:green"> $<?php echo number_format(999000, 2) ?></td>
                                                                    <td class=""> $<?php echo number_format(6000, 2) ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <td class=""> 01/02/018 11:40:30hs</td>
                                                                    <td class=""> Administracion</td>
                                                                    <td class="" style="color:red"> $<?php echo number_format(2000, 2) ?></td>
                                                                    <td class=""> $<?php echo number_format(5000, 2) ?></td>
                                                                </tr>
                                                                
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_config">
                                <?php $this->load->view('pages/properties/subpages/properties_editor/tab_configuration') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>