<?php 

	// La variable $RESOURCES esta controlada, para agregarse en el header y footer.
	$RESOURCES = array(
			'header_css' => array(
				'app/css/backend.css',
				'app/css/properties.css',
				'global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
				),
			'footer_js' => array(
				'app/scripts/properties.js',
				'global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
				'global/plugins/jquery.blockui.min.js',
				)
		);

 ?>