<style type="text/css">
	.btn-sm {font-size:11px;}
</style>

<div class="row">
<div class="col-xs-12 col-sm-4">
    <div class="portlet light page-title" style="margin:0">
	    <h1 class="custom-h1">Alquileres a cobrar</h1>
	    <div class="portlet-body" style="display: block;">
	        <?php
				$_columns_rents['detail_info'] 			= array('title' => ucfirst($this->lang->line('entity_property')),
																'custom_render' => '<a href="javascript:;" class="btn_new_coupon" data-id_user_receiver="{account_id_user_issuer}" data-counterpart_name="'.ucfirst($this->lang->line('entity_tenant')).'">
																						{detail_info}
																					</a>'
																);
				$_columns_rents['period_month'] 		= array('title'	=>'Periodo',
																'custom_render' => '{period_month}-{period_year}',
																'width' => '50px',
																'class' => 'text-center'
																);
				$_columns_rents['detail_ammount'] 		= array('title' => 'Importe', 'class'=>'text-right', 'custom_render' => '$ #=number_format|{detail_ammount}|2=#');
				
				$_columns_rents['id_receipt_detail'] 	= array('hide' => TRUE);
				$_columns_rents['id_receipt'] 			= array('hide' => TRUE);
				$_columns_rents['id_expense_type'] 		= array('hide' => TRUE);
				$_columns_rents['id_period'] 			= array('hide' => TRUE);
				$_columns_rents['detail_description'] 	= array('hide' => TRUE);
				$_columns_rents['id_issuing_account'] 	= array('hide' => TRUE);
				$_columns_rents['id_receiving_account'] = array('hide' => TRUE);
				$_columns_rents['detail_concept'] 		= array('hide' => TRUE);
				$_columns_rents['detail_observations'] 	= array('hide' => TRUE);
				$_columns_rents['detail_expire_date'] 	= array('hide' => TRUE);
				$_columns_rents['detail_interest_since'] 	= array('hide' => TRUE);
				$_columns_rents['detail_interest_rate'] 	= array('hide' => TRUE);
				$_columns_rents['detail_original_ammount'] 	= array('hide' => TRUE);
				$_columns_rents['detail_custom'] 			= array('hide' => TRUE);
				$_columns_rents['detail_id_user_modifier'] 	= array('hide' => TRUE);
				$_columns_rents['creation_date'] 			= array('hide' => TRUE);
				$_columns_rents['modification_date'] 		= array('hide' => TRUE);
				$_columns_rents['_checked'] 				= array('hide' => TRUE);
				$_columns_rents['period_year'] 				= array('hide' => TRUE);
				$_columns_rents['period_code'] 				= array('hide' => TRUE);
				$_columns_rents['receipt_date'] 			= array('hide' => TRUE);
				$_columns_rents['id_user_issuer'] 			= array('hide' => TRUE);
				$_columns_rents['id_user_receiver'] 		= array('hide' => TRUE);
				$_columns_rents['receipt_payed'] 			= array('hide' => TRUE);
				$_columns_rents['receipt_pay_date'] 		= array('hide' => TRUE);
				$_columns_rents['receipt_voided'] 			= array('hide' => TRUE);
				$_columns_rents['receipt_observations'] 	= array('hide' => TRUE);
				$_columns_rents['type_name'] 				= array('hide' => TRUE);
				$_columns_rents['type_description'] 		= array('hide' => TRUE);
				$_columns_rents['type_order'] 				= array('hide' => TRUE);
				$_columns_rents['account_id_user_issuer'] 	= array('hide' => TRUE);
				$_columns_rents['account_name_issuer'] 		= array('hide' => TRUE);
				$_columns_rents['account_id_user_receiver'] = array('hide' => TRUE);
				$_columns_rents['account_name_receiver'] 	= array('hide' => TRUE);
				$_columns_rents['id_property']				= array('hide' => TRUE);
				$_columns_rents['detail_id_detail_sibling']	= array('hide' => TRUE);
				$_columns_rents['receipt_description']		= array('hide' => TRUE);
				$_columns_rents['detail_attachments']		= array('hide' => TRUE);
				$_columns_rents['receipt_attachments']		= array('hide' => TRUE);
				$_columns_rents['sibling_receipt_attachments'] = array('hide' => TRUE);
				$_columns_rents['_payed'] 					= array('hide' => TRUE);
	        ?>

	        <?php 
				$_options_rents['create_entity'] 	 	= FALSE;
				$_options_rents['search'] 		 		= FALSE;
				$_options_rents['enable_search'] 	 	= FALSE;
				$_options_rents['enable_ipp'] 	 		= FALSE;
				$_options_rents['fixed_filters'] 	 	= array(
															'id_receiving_account' => ACCOUNT_RENTS,
															'id_expense_type' => ET_ALQUILER,
															'_payed' => 0,
															);
	         ?>
	        <?php echo pachi_fullmanaged_table('vw_receipt_details', $_columns_rents, $_options_rents); ?>
	    </div>
	</div>
</div>

<div class="col-xs-12 col-sm-4">
    <div class="portlet light page-title" style="margin:0">
        <h1 class="custom-h1">Proximos vencimientos</h1>
	    <div class="portlet-body" style="display: block;">
		    <?php 
	            $_columns['id_property']                    = array('hide' => TRUE);
	            $_columns['id_user_consortium']             = array('hide' => TRUE);
	            $_columns['property_name']                  = array('title' => 'Inmueble',
	                                                                'custom_render' => '<a href="/'.APP_PROPIEDADES.'view/{id_property}">
	                                                                						{property_name}
	                                                                						<br>
	                                                                						<small class="text-muted">{property_address}</small>
	                                                                						<br>
	                                                                						<small>finaliza en #=print_time_diference|{contract_date_end} 23:59:59|2|0=#</small>
	                                                                					</a>',
	        														'sortable' => TRUE);
	            $_columns['property_address']               = array('hide' => TRUE);
	            $_columns['property_year']                  = array('hide' => TRUE);
	            $_columns['property_ambiences_qty']         = array('hide' => TRUE);
	            $_columns['property_bathrooms_qty']         = array('hide' => TRUE);
	            $_columns['property_rooms_qty']             = array('hide' => TRUE);
	            $_columns['property_square_meter']          = array('hide' => TRUE);
	            $_columns['property_garage']                = array('hide' => TRUE);
	            $_columns['property_balcony']               = array('hide' => TRUE);
	            $_columns['property_building_name']         = array('hide' => TRUE);
	            $_columns['property_cadastre_number']       = array('hide' => TRUE);
	            $_columns['property_observations']          = array('hide' => TRUE);
	            $_columns['creation_date']                  = array('hide' => TRUE);
	            $_columns['modificaction_date']             = array('hide' => TRUE);
	            $_columns['deletion_date']                  = array('hide' => TRUE);
	            $_columns['id_user_owner']                  = array('hide' => TRUE);
	            $_columns['ownership_date_start']           = array('hide' => TRUE);
	            $_columns['owner_user_name']                = array('hide' => TRUE);
	            $_columns['owner_user_lastname']            = array('hide' => TRUE);
	            $_columns['owner_user_address']             = array('hide' => TRUE);
	            $_columns['owner_user_email']               = array('hide' => TRUE);
	            $_columns['owner_user_phone']               = array('hide' => TRUE);
	            $_columns['owner_user_picture']             = array('hide' => TRUE);
	            $_columns['consortium_user_name']           = array('hide' => TRUE);
	            $_columns['consortium_user_lastname']       = array('hide' => TRUE);
	            $_columns['consortium_user_address']        = array('hide' => TRUE);
	            $_columns['consortium_user_email']          = array('hide' => TRUE);
	            $_columns['consortium_user_phone']          = array('hide' => TRUE);
	            $_columns['consortium_user_picture']        = array('hide' => TRUE);
	            $_columns['tenant_user_name']               = array('hide' => TRUE);
	            $_columns['tenant_user_lastname']           = array('hide' => TRUE);
	            $_columns['tenant_user_address']            = array('hide' => TRUE);
	            $_columns['tenant_user_email']              = array('hide' => TRUE);
	            $_columns['tenant_user_phone']              = array('hide' => TRUE);
	            $_columns['tenant_user_picture']            = array('hide' => TRUE);
	            $_columns['id_contract']                    = array('hide' => TRUE);
	            $_columns['id_user_tenant']                 = array('hide' => TRUE);
	            $_columns['contract_date_start']            = array('hide' => TRUE);
	            $_columns['contract_date_end']              = array('hide' => TRUE);
	            $_columns['contract_rent_price']            = array('hide' => TRUE);
	            $_columns['contract_interest']              = array('hide' => TRUE);
	            $_columns['contract_tenant_attachments']    = array('hide' => TRUE);
	            $_columns['contract_gas_check']             = array('hide' => TRUE);
	            $_columns['contract_electricity_check']     = array('hide' => TRUE);
	            $_columns['contract_expenses_check']        = array('hide' => TRUE);
	            $_columns['contract_expenses_attachments']  = array('hide' => TRUE);
	            $_columns['contract_observations']          = array('hide' => TRUE);
	            $_columns['tenant_debts']					= array('hide' => TRUE);
	            $_columns['_count_maintenances']            = array('hide' => TRUE);
	            $_columns['id_bonification']    			= array('hide' => TRUE);
	            $_columns['bonification_name']             	= array('hide' => TRUE);
	            $_columns['bonification_rate']     			= array('hide' => TRUE);
	            $_columns['bonification_date_start']        = array('hide' => TRUE);
	            $_columns['bonification_date_end']  		= array('hide' => TRUE);
	            $_columns['bonification_observations']      = array('hide' => TRUE);
	            $_columns['contract_payday_limit']      	= array('hide' => TRUE);
	            $_columns['contract_payday_start']      	= array('hide' => TRUE);
	            $_columns['ownership_administrative_rate']  = array('hide' => TRUE);
	            $_columns['property_archived']  			= array('hide' => TRUE);
	            $_columns['contract_commission']	        = array('hide' => TRUE);
	        ?>

	        <?php 
	        	// Opciones de toolbar.
				// create_entity: 
	        	// - Puede ser falso y no se muestra el boton de nuevo.
	        	// - Puede ser true, y se muestra para la tabla en cuestion, no aplica para views.
	        	// - Puede ser un array, con las keys: table, columns, donde table especifica la tabla con la que trabaja y columns una config de columnas.
				$_options['create_entity'] 	 = FALSE;
				// La opcion de buscador, busca sobre todas las columnas de tipo text o varchar.
				// Puede ser TRUE o FALSE.
				$_options['search'] 		 = FALSE;
				$_options['enable_search'] 	 = FALSE;
				$_options['enable_ipp'] 	 = FALSE;

				$_options['fixed_filters'] 	 = array(
												'contract_date_end BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 180 DAY)' => NULL,
												);
				$_options['fixed_orders']	 = array('contract_date_end' => 'ASC');
	        ?>

	        <?php echo pachi_fullmanaged_table('vw_properties', $_columns, $_options); ?>
	    </div>
	</div>
</div>

<div class="col-xs-12 col-sm-4">
    <div class="portlet light page-title" style="margin:0">
    	<h1 class="custom-h1">Inmuebles desocupados</h1>
	    <div class="portlet-body" style="display: block;">
		    <?php // Usamos la misma config de columns que para la tabla anterior. ?>

	        <?php 
	        	unset($_options);
	            $_columns['property_name']                  = array('title' => 'Inmueble',
	                                                                'custom_render' => '<a href="/'.APP_PROPIEDADES.'view/{id_property}">{property_name}<br><small class="text-muted">{property_address}</small></a>',
	        														'sortable' => TRUE);
	        	// Opciones de toolbar.
				// create_entity: 
	        	// - Puede ser falso y no se muestra el boton de nuevo.
	        	// - Puede ser true, y se muestra para la tabla en cuestion, no aplica para views.
	        	// - Puede ser un array, con las keys: table, columns, donde table especifica la tabla con la que trabaja y columns una config de columnas.
				$_options['create_entity'] 	 = FALSE;
				// La opcion de buscador, busca sobre todas las columnas de tipo text o varchar.
				// Puede ser TRUE o FALSE.
				$_options['search'] 		 = FALSE;
				$_options['enable_search'] 	 = FALSE;
				$_options['enable_ipp'] 	 = FALSE;

				$_options['fixed_filters'] 	 = array(
												'id_contract IS NULL' => NULL,
												);
	        ?>

	        <?php echo pachi_fullmanaged_table('vw_properties', $_columns, $_options); ?>
	    </div>
	</div>
</div>

</div>