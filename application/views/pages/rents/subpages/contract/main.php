<div class="portlet light ">
    <div class="portlet-body">
        <h3 class="block">Nuevo Contrato</h3>
        <div class="row">
            <div class="portlet light">
                <div class="portlet-body contract_form">
                    <?php


                 // new form
                        $create_form = array(
                                                'user_name',
                                                'user_lastname',
                                                'user_address',
                                                'user_email',
                                                'user_cuit',
                                                'user_phone',
                                                'user_observations',
                                                'user_password' => array('input_type' => 'hidden',
                                                                         'value' => '123456'),
                                                'id_role'       => array('input_type' => 'hidden',
                                                                         'value' => ROLE_TENANT),
                                            );


                        //$columns['id_user_tenant'] = array();
                        //$columns['contract_date_start'] = array();
                        //$columns['id_property'] = array();
                        //$columns['contract_date_end'] = array();
                        //$columns['contract_rent_price'] = array();
                        //$columns['contract_interest'] = array();
                        //$columns['contract_commission'] = array();
                        //$columns['contract_payday_start'] = array();
                        //$columns['contract_payday_limit'] = array();
                        //$columns['contract_gas_check'] = array();
                        //$columns['contract_electricity_check'] = array();
                        //$columns['contract_expenses_check'] = array();
                        //$columns['contract_observations'] = array();
                        //$columns['contract_tenant_attachments'] = array();


                        $columns = array(
                                            'contract_date_start',
                                            'id_property',
                                            'contract_date_end',
                                            'contract_rent_price',
                                            'contract_interest',
                                            'contract_commission',
                                            'contract_payday_start',
                                            'contract_payday_limit',
                                            'contract_gas_check',
                                            'contract_electricity_check',
                                            'contract_expenses_check',
                                            'contract_observations',
                                            'contract_tenant_attachments',
                                            'id_user_tenant' => array(  // Buscador
                                                    'reference' => array( //'table' => 'users', // aca podria determinar otra tabla distinta para buscar.
                                                                            'display' => 'user_name',   // Si bien por defecto muestra el _name, podria mostrar otra columna.
                                                                            'filter' => array('id_role' => ROLE_TENANT)
                                                                            ),
                                                                            // Modal de creacion.
                                                    'modal' => array('columns'  => $create_form, 
                                                                     'modal_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_tenant'),
                                                                     'button_title' => ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_tenant')
                                                                    ) // aca determino que columna es la que se muestra en el buscador.
                                                )
                        );


                        

                        //$_options['save_action']['action']  = 'redirect';
                        //$_options['save_action']['delay']   = '500';
                        //$_options['save_action']['target']  = base_url(APP_MAINTENANCES);

                       // $_options['custom_target']  = base_url('ajax/Ajax_contracts/create_contract'); 

                        echo pachi_form('contracts', $columns, FALSE ,FALSE);
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>