<?php $TK = $this->layout->get_tasks(); ?>
<?php if ($TK !== FALSE): ?>

    <li class="dropdown dropdown-extended dropdown-tasks dropdown-dark" id="header_task_bar">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <i class="icon-calendar"></i>
            <span class="badge badge-default"> <?php echo $TK['unreaded_tasks'] ?> </span>
        </a>
        <ul class="dropdown-menu extended tasks">
            <li class="external">
                <h3>
                    Tienes
                    <span class="bold"><?php echo $TK['unreaded_tasks'] ?> tareas </span> pendientes
                </h3>
                <a href="<?php echo $TK['url_tasks'] ?>">ver todas</a>
            </li>
            <li>
                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                    <?php foreach ($TK['tasks'] as $_ktask => $_task): ?>
                    <li>
                        <a href="<?php echo $_task['task_url'] ?>">
                            <span class="task">
                                <span class="desc"><?php echo $_task['task_name'] ?> </span>
                                <span class="percent"><?php echo $_task['task_percent'] ?> %</span>
                            </span>
                            <span class="progress">
                                <span style="width: <?php echo $_task['task_percent'] ?>%;" class="progress-bar progress-bar-success" aria-valuenow="<?php echo $_task['task_percent'] ?>" aria-valuemin="0" aria-valuemax="100">
                                    <span class="sr-only"><?php echo $_task['task_percent'] ?>% Completado</span>
                                </span>
                            </span>
                        </a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </li>
        </ul>
    </li>
    
<?php endif ?>