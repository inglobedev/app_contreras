<?php $PT = $this->layout->get_page_title(); ?>
<?php if ($PT !== FALSE): ?>
    
    <?php if (is_array($PT)): ?>
        <div class="page-title margin-top-none"> 
        	<h1><?php echo $PT[0] ?>
            	<small><?php echo $PT[1] ?></small>
        	</h1>
        </div>
    <?php else: ?>
        <div class="page-title margin-top-none"> 
        	<h1><?php echo $PT ?></h1>
        </div>
    <?php endif ?>

<?php endif ?>
