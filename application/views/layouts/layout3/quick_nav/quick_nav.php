<?php $QN = $this->layout->get_quick_nav(); ?>
<?php if ($QN !== FALSE): ?>

    <nav class="quick-nav">
        <a class="quick-nav-trigger" href="#0">
            <span aria-hidden="true"></span>
        </a>
        <ul>
            <?php foreach ($QN as $_kqn => $_qn): ?>
            <li>
                <a href="<?php echo $_qn['url'] ?>" target="_blank" class="active">
                    <span><?php echo $_qn['title'] ?></span>
                    <i class="<?php echo $_qn['icon'] ?>"></i>
                </a>
            </li>
            <?php endforeach ?>
        </ul>
        <span aria-hidden="true" class="quick-nav-bg"></span>
    </nav>
    <div class="quick-nav-overlay"></div>

<?php endif ?>