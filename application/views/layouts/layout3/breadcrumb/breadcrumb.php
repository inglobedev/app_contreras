<?php $BC = $this->layout->get_breadcrumb(); ?>
<?php if ($BC !== FALSE): ?>
	
	<?php $count = count($BC); ?>
	<ul class="page-breadcrumb breadcrumb">
		<?php foreach ($BC as $_kbc => $_bc): ?>
		    <li>
		        <a href="<?php echo $_bc['url'] ?>"><?php echo $_bc['title'] ?></a>
		        <?php if ($_kbc < $count - 1): ?>
		        <i class="fa fa-circle"></i>
		        <?php endif ?>
		    </li>
		<?php endforeach ?>
	</ul>

<?php endif ?>