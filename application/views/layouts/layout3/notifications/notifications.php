<?php $NF = $this->layout->get_notifications(); ?>
<?php if ($NF !== FALSE): ?>
    <!-- DOC: Apply "dropdown-hoverable" class after below "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
    <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
    <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <i class="icon-bell"></i>
            <span class="badge badge-default"> 
                <?php echo $NF['new_notifications'] ?> 
            </span>
        </a>
        <ul class="dropdown-menu">
            <li class="external">
                <h3>
                    <span class="bold"><?php echo $NF['new_notifications'] ?></span> notificaciones
                </h3>
                <a href="<?php echo $NF['url_notifications'] ?>">ver todas</a>
            </li>
            <li>
                <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                    <?php foreach ($NF['notifications'] as $_knotification => $_notification): ?>
                    <li>
                        <a href="<?php echo $_notification['target'] ?>">
                            <span class="time"><?php echo get_time_ago($_notification['notif_date'], TRUE) ?></span>
                            <span class="details">
                                <?php if (!empty($_notification['icon'])): ?>
                                <span class="label label-sm label-icon label-<?php echo $_notification['label'] ?>">
                                    <i class="<?php echo $_notification['icon'] ?>"></i>
                                </span> 
                                <?php endif ?>
                                <span class="<?php if ($_notification['notif_readed'] == 0) echo 'strong text-black' ?>">
                                    <?php echo $_notification['notif_title'] ?>
                                </span>
                            </span>
                        </a>
                    </li>
                    <?php endforeach ?>
                    <?php if (count($NF['notifications']) == 0): ?>
                        <li>
                            <p class="text-center">
                                No hay notificaciones.
                            </p>
                        </li>
                    <?php endif ?>
                </ul>
            </li>
        </ul>
    </li>
<?php endif ?>