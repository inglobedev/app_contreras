<?php $MS = $this->layout->get_messages(); ?>
<?php if ($MS !== FALSE): ?>

    <li class="droddown dropdown-separator">
        <span class="separator"></span>
    </li>
    <!-- BEGIN INBOX DROPDOWN -->
    <li class="dropdown dropdown-extended dropdown-inbox dropdown-dark" id="header_inbox_bar">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <span class="circle"><?php echo $MS['new_messages'] ?></span>
            <span class="corner"></span>
        </a>
        <ul class="dropdown-menu">
            <li class="external">
                <h3>
                    Tienes 
                    <span class="bold"><?php echo $MS['new_messages'] ?></span> 
                    mensajes nuevos.</h3>
                <a href="<?php echo $MS['url_messages'] ?>">ver todos</a>
            </li>
            <li>
                <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                    <?php foreach ($MS['messages'] as $_kmessage => $_message): ?>
                    <li>
                        <a href="#">
                            <span class="photo">
                                <img src="<?php echo $_message['image'] ?>" class="img-circle" alt="<?php echo $_message['name'] ?>"> 
                            </span>
                            <span class="subject">
                                <span class="from"> <?php echo $_message['name'] ?> </span>
                                <span class="time"> <?php echo $_message['datetime'] ?> </span>
                            </span>
                            <span class="message"> <?php echo $_message['text'] ?> </span>
                        </a>
                    </li>
                    <?php endforeach ?>
                </ul>
            </li>
        </ul>
    </li>

<?php endif ?>