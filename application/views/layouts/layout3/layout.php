<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title><?php echo APP_TITLE ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="<?php echo HTTP_PROTOCOL ?>fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="<?php echo base_url() ?>assets/global/plugins/fileuploader/jquery.fileuploader.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->

        <!-- BEGIN PLUGINS -->
        <?php $this->load->view('components/plugins/header'); ?>
        <!-- END PLUGINS -->

        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?php echo base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/global/css/utils.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?php echo base_url() ?>assets/layouts/layout3/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/layouts/layout3/css/themes/<?php echo APP_THEME; ?>.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?php echo base_url() ?>assets/layouts/layout3/css/custom.css" rel="stylesheet" type="text/css" />
        
        <?php if ($RESOURCES !== FALSE): ?>
        <!-- BEGIN: Page level plugins -->
            <?php foreach ($RESOURCES['header_css'] as $file => $resource): ?>
                <?php if (is_array($resource)): ?> 
                    <?php if (in_array(SUBPAGE, $resource['subpages'])): ?>
                        <?php if (strpos($resource['link'], HTTP_PROTOCOL) === FALSE): ?>
                            <link href="<?php echo base_url() ?>assets/<?php echo $resource['link'] ?>" rel="stylesheet" type="text/css"/>
                        <?php else: ?>
                            <link href="<?php echo $resource['link'] ?>" rel="stylesheet" type="text/css"/>
                        <?php endif ?>
                    <?php endif ?>
                <?php else: ?>
                    <?php if (strpos($resource, HTTP_PROTOCOL) === FALSE): ?>
                        <link href="<?php echo base_url() ?>assets/<?php echo $resource ?>" rel="stylesheet" type="text/css"/>
                    <?php else: ?>
                        <link href="<?php echo $resource ?>" rel="stylesheet" type="text/css"/>
                    <?php endif ?>
                <?php endif ?>
            <?php endforeach ?>
        <!-- END: Page level plugins -->
        <?php endif ?>

        <link href="<?php echo base_url() ?>assets/app/css/library.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/app/css/custom.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="<?php echo base_url() ?>assets/app/img/favicon.jpg" /> 
        <script type="text/javascript">window.base_url = '<?php echo base_url() ?>';</script>
    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid">
        <div class="page-wrapper">
            <div class="page-wrapper-row">
                <div class="page-wrapper-top">
                    <!-- BEGIN HEADER -->
                    <div class="page-header">
                        <!-- BEGIN HEADER TOP -->
                        <div class="page-header-top">
                            <div class="container">
                                <!-- BEGIN LOGO -->
                                <div class="page-logo">
                                    <a href="<?php echo base_url() ?>">
                                        <img src="<?php echo base_url() ?>assets/app/img/logo.png" alt="Logo <?php echo APP_TITLE ?>" class="logo-default" /> 
                                    </a>
                                </div>
                                <!-- END LOGO -->
                                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                                <a href="javascript:;" class="menu-toggler"></a>
                                <!-- END RESPONSIVE MENU TOGGLER -->
                                <!-- BEGIN TOP NAVIGATION MENU -->
                                <div class="top-menu">
                                    <ul class="nav navbar-nav pull-right">
                                        <!-- BEGIN NOTIFICATION DROPDOWN -->
                                        <?php // $this->load->view('layouts/layout3/notifications/notifications'); ?>
                                        <!-- END NOTIFICATION DROPDOWN -->
                                        <!-- BEGIN TODO DROPDOWN -->
                                        <?php $this->load->view('layouts/layout3/tasks/tasks'); ?>
                                        <!-- END TODO DROPDOWN -->
                                        <!-- BEGIN INBOX DROPDOWN -->
                                        <?php $this->load->view('layouts/layout3/messages/messages'); ?>
                                        <!-- END INBOX DROPDOWN -->
                                        <!-- BEGIN USER LOGIN DROPDOWN -->
                                        <?php $this->load->view('layouts/layout3/user_menu/user_menu'); ?>
                                        <!-- END USER LOGIN DROPDOWN -->
                                    </ul>
                                </div>
                                <!-- END TOP NAVIGATION MENU -->
                            </div>
                        </div>
                        <!-- END HEADER TOP -->
                        <!-- BEGIN HEADER MENU -->
                        <div class="page-header-menu">
                            <div class="container">
                                <!-- BEGIN HEADER SEARCH BOX -->
                                <?php $this->load->view('layouts/layout3/search_box/search_box'); ?>
                                <!-- END HEADER SEARCH BOX -->
                                <!-- BEGIN MEGA MENU -->
                                <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                                <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                                <div class="hor-menu  ">
                                    <ul class="nav navbar-nav">
                                        <?php $this->load->view('layouts/layout3/menu_options/menu_options'); ?>
                                    </ul>
                                </div>
                                <!-- END MEGA MENU -->
                            </div>
                        </div>
                        <!-- END HEADER MENU -->
                    </div>
                    <!-- END HEADER -->
                </div>
            </div>
            <div class="page-wrapper-row full-height">
                <div class="page-wrapper-middle">
                    <!-- BEGIN CONTAINER -->
                    <div class="page-container">
                        <!-- BEGIN CONTENT -->
                        <div class="page-content-wrapper">
                            <!-- BEGIN CONTENT BODY -->
                            <!-- BEGIN PAGE HEAD-->
                            <div class="page-head">
                                <div class="container">
                                    <!-- BEGIN PAGE TITLE -->
                                        <?php $this->load->view('layouts/layout3/page_title/page_title'); ?>
                                    <!-- END PAGE TITLE -->
                                </div>
                            </div>
                            <!-- END PAGE HEAD-->
                            <!-- BEGIN PAGE CONTENT BODY -->
                            <div class="page-content">
                                <div class="container">
                                    <!-- BEGIN PAGE BREADCRUMBS -->
                                        <?php $this->load->view('layouts/layout3/breadcrumb/breadcrumb'); ?>
                                    <!-- END PAGE BREADCRUMBS -->
                                    <!-- BEGIN PAGE CONTENT INNER -->
                                    <div class="page-content-inner">

                                        <?php if ($this->session->userdata('tmp_password') == TRUE): ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <strong>¡Atención!</strong> Accediste con una clave temporal que expira hoy a las 23:59 hs.
                                            <a href="<?php echo APP_ACCOUNT_URL; ?>" class="alert-link"> Haz click aquí para establecer una clave. </a>
                                        </div>
                                        <?php endif ?>

                                        <?php echo @$CONTENT; ?>
                                        
                                    </div>
                                    <!-- END PAGE CONTENT INNER -->
                                </div>
                            </div>
                            <!-- END PAGE CONTENT BODY -->
                            <!-- END CONTENT BODY -->
                        </div>
                        <!-- END CONTENT -->
                    </div>
                    <!-- END CONTAINER -->
                </div>
            </div>
            <div class="page-wrapper-row">
                <div class="page-wrapper-bottom">
                    <!-- BEGIN FOOTER -->
                    <!-- BEGIN PRE-FOOTER -->
                    <?php $this->load->view('layouts/layout3/footer/footer'); ?>
                    <!-- END PRE-FOOTER -->
                    <!-- BEGIN INNER FOOTER -->
                    <div class="page-footer">
                        <div class="container">
                            <?php echo APP_FOOTER ?>
                        </div>                        
                    </div>
                    <div class="scroll-to-top">
                        <i class="icon-arrow-up"></i>
                    </div>
                    <!-- END INNER FOOTER -->
                    <!-- END FOOTER -->
                </div>
            </div>
        </div>
        <!-- BEGIN QUICK NAV -->
            <?php $this->load->view('layouts/layout3/quick_nav/quick_nav'); ?>
        <!-- END QUICK NAV -->
        <!--[if lt IE 9]><script src="<?php echo base_url() ?>assets/global/plugins/respond.min.js"></script><script src="<?php echo base_url() ?>assets/global/plugins/excanvas.min.js"></script><script src="<?php echo base_url() ?>assets/global/plugins/ie8.fix.min.js"></script><![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootbox/bootbox.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/fileuploader/jquery.fileuploader.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/app/scripts/library.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/moment.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-datepicker/locales/bootstrap-datepicker.es.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/fileuploader/jquery.fileuploader.min.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/numeric.js" type="text/javascript"></script>
		<script src="<?php echo base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->

        <!-- BEGIN PLUGINS -->
        <?php $this->load->view('components/plugins/footer'); ?>
        <!-- END PLUGINS -->

        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/global/scripts/app.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="<?php echo base_url() ?>assets/layouts/layout3/scripts/layout.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/scripts/utils.js" type="text/javascript"></script>
        <script src="<?php echo base_url() ?>assets/global/plugins/bootstrap-growl/jquery.bootstrap-growl.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        
        <?php if ($RESOURCES !== FALSE): ?>
        <!-- BEGIN: Page level plugins -->
            <?php foreach ($RESOURCES['footer_js'] as $file => $resource): ?>
                <?php if (is_array($resource)): ?> 
                    <?php if (in_array(SUBPAGE, $resource['subpages'])): ?>
                        <?php if (strpos($resource['link'], HTTP_PROTOCOL) === FALSE): ?>
                            <script src="<?php echo base_url() ?>assets/<?php echo $resource['link'] ?>" type="text/javascript"></script>
                        <?php else: ?>
                            <script src="<?php echo $resource['link'] ?>" type="text/javascript"></script>
                        <?php endif ?>
                    <?php endif ?>
                <?php else: ?>
                    <?php if (strpos($resource, HTTP_PROTOCOL) === FALSE): ?>
                        <script src="<?php echo base_url() ?>assets/<?php echo $resource ?>" type="text/javascript"></script>
                    <?php else: ?>
                        <script src="<?php echo $resource ?>" type="text/javascript"></script>
                    <?php endif ?>
                <?php endif ?>
            <?php endforeach ?>
        <!-- END: Page level plugins -->
        <?php endif ?>

        <script src="<?php echo base_url() ?>assets/app/scripts/custom.js" type="text/javascript"></script>
    </body>

</html>