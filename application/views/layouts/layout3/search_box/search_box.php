<?php $SB = $this->layout->get_search_box(); ?>
<?php if ($SB !== FALSE): ?>

	<!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
	<form id="<?php echo $SB['id'] ?>" class="search-form search-form-expanded <?php echo $SB['class'] ?>" action="<?php echo $SB['action'] ?>" method="<?php echo $SB['method'] ?>">
	    <div class="input-group">
	        <input type="text" class="form-control" placeholder="<?php echo $SB['placeholder'] ?>" name="<?php echo $SB['name'] ?>">
	        <span class="input-group-btn">
	            <a href="javascript:;" class="btn submit">
	                <i class="icon-magnifier"></i>
	            </a>
	        </span>
	    </div>
	</form>

<?php endif ?>