<?php $UM = $this->layout->get_user_menu(); ?>
<?php if ($UM !== FALSE): ?>

    <li class="dropdown dropdown-user dropdown-dark">
        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
            <img alt="<?php echo $UM['user_name'] ?>" class="img-circle" src="<?php echo $UM['user_picture'] ?>">
            <span class="username username-hide-mobile"><?php echo $UM['user_name'] ?></span>
        </a>
        <ul class="dropdown-menu dropdown-menu-default">
            <?php foreach ($UM['menu'] as $_kopt => $_opt): ?>
            <li>
                <a href="<?php echo $_opt['url'] ?>">
                    <i class="<?php echo $_opt['icon'] ?>"></i> 
                    <?php echo $_opt['name'] ?>
                </a>
            </li>
            <?php endforeach ?>
        </ul>
    </li>

<?php endif ?>