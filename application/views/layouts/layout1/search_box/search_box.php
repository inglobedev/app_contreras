<?php $SB = $this->layout->get_search_box(); ?>
<?php if ($SB !== FALSE): ?>
	
    <!-- DOC: Apply "sidebar-search-bordered" class the below search form to have bordered search box -->
    <!-- DOC: Apply "sidebar-search-bordered sidebar-search-solid" class the below search form to have bordered & solid search box -->
    <form id="<?php echo $SB['id'] ?>" class="sidebar-search sidebar-search-bordered <?php echo $SB['class'] ?>" action="<?php echo $SB['action'] ?>" method="<?php echo $SB['method'] ?>">
        <a href="javascript:;" class="remove">
            <i class="icon-close"></i>
        </a>
        <div class="input-group">
            <input type="text" class="form-control" placeholder="<?php echo $SB['placeholder'] ?>" name="<?php echo $SB['name'] ?>">
            <span class="input-group-btn">
                <a href="javascript:;" class="btn submit">
                    <i class="icon-magnifier"></i>
                </a>
            </span>
        </div>
    </form>

<?php endif ?>