<?php $MNU = $this->layout->get_menu_options(); ?>
<?php if ($MNU !== FALSE): ?>
    <!-- DOC: To remove the sidebar toggler from the sidebar you just need to completely remove the below "sidebar-toggler-wrapper" LI element -->
    <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
    <li class="sidebar-toggler-wrapper hide">
        <div class="sidebar-toggler">
            <span></span>
        </div>
    </li>
    <!-- END SIDEBAR TOGGLER BUTTON -->
    <!-- DOC: To remove the search box from the sidebar you just need to completely remove the below "sidebar-search-wrapper" LI element -->
    <li class="sidebar-search-wrapper">
        <!-- BEGIN RESPONSIVE QUICK SEARCH FORM -->
        <?php $this->load->view('layouts/layout1/search_box/search_box'); ?>
        <!-- END RESPONSIVE QUICK SEARCH FORM -->
    </li>

    <?php foreach ($MNU as $_kmnu => $_mnu): ?>
        
        <li class="nav-item <?php if (@$_mnu['active']) echo 'active' ?> <?php echo ($_kmnu == 0) ? 'start' : NULL; ?>">
            <a href="<?php echo $_mnu['url'] ?>" class="nav-link nav-toggle">
                <i class="<?php echo $_mnu['icon'] ?>"></i>
                <span class="title"><?php echo $_mnu['title'] ?></span>
                <?php if (count($_mnu['submenu']) > 0): ?>
                <span class="arrow"></span>
                <?php endif ?>
            </a>
            <?php if (count($_mnu['submenu']) > 0): ?>
            <ul class="sub-menu">

                <?php foreach ($_mnu['submenu'] as $_ksmenu => $_smenu): ?>
                    <li class="nav-item <?php if (@$_smenu['active']) echo 'active' ?> <?php echo ($_kmnu == 0) ? 'start' : NULL; ?>">
                        <a href="<?php echo $_smenu['url'] ?>" class="nav-link <?php if (isset($_smenu['submenu'])) echo 'nav-toggle' ?> ">
                            <i class="<?php echo $_smenu['icon'] ?>"></i>
                            <span class="title"><?php echo $_smenu['title'] ?></span>
                            <?php if (isset($_smenu['submenu'])): ?>
                            <span class="arrow"></span>
                            <?php endif ?>
                        </a>

                        <?php if (isset($_smenu['submenu'])): ?>
                        <ul class="sub-menu">

                            <?php foreach ($_smenu['submenu'] as $_kssmenu => $_ssmenu): ?>
                                <li class="nav-item <?php if (@$_ssmenu['active']) echo 'active' ?> <?php echo ($_kmnu == 0) ? 'start' : NULL; ?>">
                                    <a href="<?php echo $_ssmenu['url'] ?>" class="nav-link ">
                                        <i class="<?php echo $_ssmenu['icon'] ?>"></i>
                                        <span class="title"><?php echo $_ssmenu['title'] ?></span>
                                    </a>
                                </li>
                            <?php endforeach ?>

                        </ul>
                        <?php endif ?>

                    </li>
                <?php endforeach ?>

            </ul>
            <?php endif ?>
        </li>

    <?php endforeach ?>

<?php endif ?>