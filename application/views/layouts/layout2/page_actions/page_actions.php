<?php $PA = $this->layout->get_page_actions(); ?>
<?php if ($PA !== FALSE): ?>

    <!-- DOC: Remove "hide" class to enable the page header actions -->
    <div class="page-actions">
        <div class="btn-group">
            <button type="button" class="btn btn-circle btn-outline red dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-plus"></i>&nbsp;
                <span class="hidden-sm hidden-xs"><?php echo $PA['current'] ?>&nbsp;</span>&nbsp;
                <i class="fa fa-angle-down"></i>
            </button>
            <ul class="dropdown-menu" role="menu">
                <?php foreach ($PA['actions'] as $_kaction => $_action): ?>
                <li>
                    <a href="<?php echo $_action['url'] ?>">
                        <i class="<?php echo $_action['icon'] ?>"></i> <?php echo $_action['title'] ?>
                    </a>
                </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>

<?php endif ?>
