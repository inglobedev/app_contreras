<?php $PT = $this->layout->get_page_title(); ?>
<?php if ($PT !== FALSE): ?>
    
    <?php if (is_array($PT)): ?>
        <h1 class="page-title"> <?php echo $PT[0] ?>
            <small><?php echo $PT[1] ?></small>
        </h1>
    <?php else: ?>
        <h1 class="page-title"> <?php echo $PT ?>
        </h1>
    <?php endif ?>

<?php endif ?>
