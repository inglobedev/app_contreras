<?php 
	/*
	Title:      Thumb.php
	URL:        http://github.com/jamiebicknell/Thumb
	Author:     Jamie Bicknell
	Twitter:    @jamiebicknell
	*/
	function resize_image($src = FALSE, $target, $size = '100', $crop = 1, $trim = 0, $zoom = 0, $align = FALSE)
	{
		$ADJUST_ORIENTATION = true;
		$JPEG_QUALITY 		= 80;
		$size = !empty($size) ? str_replace(array('<', 'x'), '', $size) != '' ? $size : 100 : 100;

		if (!in_array(strtolower(substr(strrchr($src, '.'), 1)), array('gif', 'jpg', 'jpeg', 'png'))) {
		    return FALSE;
		}

		if (!file_exists($target)) 
		{
		    list($w0, $h0, $type) = getimagesize($src);
		    $data 	= file_get_contents($src);
		    $oi 	= imagecreatefromstring($data);
		    if ( $ADJUST_ORIENTATION ) {
		        // I know supressing errors is bad, but calling exif_read_data on invalid
		        // or corrupted data returns a fatal error and there's no way to validate
		        // the EXIF data before calling the function.
		        $exif = @exif_read_data($src, EXIF);
		        if (isset($exif['Orientation'])) {
		            $degree = 0;
		            $mirror = false;
		            switch ($exif['Orientation']) {
		                case 2:
		                    $mirror = true;
		                    break;
		                case 3:
		                    $degree = 180;
		                    break;
		                case 4:
		                    $degree = 180;
		                    $mirror = true;
		                    break;
		                case 5:
		                    $degree = 270;
		                    $mirror = true;
		                    $w0 ^= $h0 ^= $w0 ^= $h0;
		                    break;
		                case 6:
		                    $degree = 270;
		                    $w0 ^= $h0 ^= $w0 ^= $h0;
		                    break;
		                case 7:
		                    $degree = 90;
		                    $mirror = true;
		                    $w0 ^= $h0 ^= $w0 ^= $h0;
		                    break;
		                case 8:
		                    $degree = 90;
		                    $w0 ^= $h0 ^= $w0 ^= $h0;
		                    break;
		            }
		            if ($degree > 0) {
		                $oi = imagerotate($oi, $degree, 0);
		            }
		            if ($mirror) {
		                $nm = $oi;
		                $oi = imagecreatetruecolor($w0, $h0);
		                imagecopyresampled($oi, $nm, 0, 0, $w0 - 1, 0, $w0, $h0, -$w0, $h0);
		                imagedestroy($nm);
		            }
		        }
		    }
		    list($w,$h) = explode('x', str_replace('<', '', $size) . 'x');
		    $w = ($w != '') ? floor(max(8, min(1500, $w))) : '';
		    $h = ($h != '') ? floor(max(8, min(1500, $h))) : '';
		    if (strstr($size, '<')) {
		        $h = $w;
		        $crop = 0;
		        $trim = 1;
		    } elseif (!strstr($size, 'x')) {
		        $h = $w;
		    } elseif ($w == '' || $h == '') {
		        $w = ($w == '') ? ($w0 * $h) / $h0 : $w;
		        $h = ($h == '') ? ($h0 * $w) / $w0 : $h;
		        $crop = 0;
		        $trim = 1;
		    }
		    $trim_w = ($trim) ? 1 : ($w == '') ? 1 : 0;
		    $trim_h = ($trim) ? 1 : ($h == '') ? 1 : 0;
		    if ($crop) {
		        $w1 = (($w0 / $h0) > ($w / $h)) ? floor($w0 * $h / $h0) : $w;
		        $h1 = (($w0 / $h0) < ($w / $h)) ? floor($h0 * $w / $w0) : $h;
		        if (!$zoom) {
		            if ($h0 < $h || $w0 < $w) {
		                $w1 = $w0;
		                $h1 = $h0;
		            }
		        }
		    } else {
		        $w1 = (($w0 / $h0) < ($w / $h)) ? floor($w0 * $h / $h0) : floor($w);
		        $h1 = (($w0 / $h0) > ($w / $h)) ? floor($h0 * $w / $w0) : floor($h);
		        $w = floor($w);
		        $h = floor($h);
		        if (!$zoom) {
		            if ($h0 < $h && $w0 < $w) {
		                $w1 = $w0;
		                $h1 = $h0;
		            }
		        }
		    }
		    $w = ($trim_w) ? (($w0 / $h0) > ($w / $h)) ? min($w, $w1) : $w1 : $w;
		    $h = ($trim_h) ? (($w0 / $h0) < ($w / $h)) ? min($h, $h1) : $h1 : $h;
		    $x = strpos($align, 'l') !== false ? 0 : (strpos($align, 'r') !== false ? $w - $w1 : ($w - $w1) / 2);
		    $y = strpos($align, 't') !== false ? 0 : (strpos($align, 'b') !== false ? $h - $h1 : ($h - $h1) / 2);
		    $im = imagecreatetruecolor($w, $h);
		    $bg = imagecolorallocate($im, 255, 255, 255);
		    imagefill($im, 0, 0, $bg);
		    switch ($type) {
		        case 1:
		            imagecopyresampled($im, $oi, $x, $y, 0, 0, $w1, $h1, $w0, $h0);
		            imagegif($im, $target);
		            break;
		        case 2:
		            imagecopyresampled($im, $oi, $x, $y, 0, 0, $w1, $h1, $w0, $h0);
		            imagejpeg($im, $target, $JPEG_QUALITY);
		            break;
		        case 3:
		            imagefill($im, 0, 0, imagecolorallocatealpha($im, 0, 0, 0, 127));
		            imagesavealpha($im, true);
		            imagealphablending($im, false);
		            imagecopyresampled($im, $oi, $x, $y, 0, 0, $w1, $h1, $w0, $h0);
		            imagepng($im, $target);
		            break;
		    }
		    @chmod($target, 0666);
		    imagedestroy($im);
		    imagedestroy($oi);
		}
	}