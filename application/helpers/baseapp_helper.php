<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Helpers varios para cuestiones generales, de configuracion, permisos y otros.
 */

	// No se para que habia echo esto.. queda.
	if ( ! function_exists('clean_explode'))
	{
		function clean_explode($delimiter, $string) 
		{
			$r = array();
			$array = explode($delimiter, $string);

			foreach ($array as $element) {
				if (!empty($element) OR $element === 0 OR $element == '0') {
					$r[] = $element;
				}
			}

			return $r;
		}
	}

	function download_attachments($attachments = NULL, $show_name = FALSE)
	{
		$html = NULL;

		$attachments = clean_explode(',', $attachments);
		foreach ($attachments as $id_file) 
		{
			$_name 	= file_name($id_file);
			$_dname = ($show_name) ? $_name : NULL;
			$html .= "<a class='btn btn-xs btn-info' title='".$_name."' href='".file_uri($id_file)."' download='".$_name."'><i class='fa fa-cloud-download'></i> ".mb_strtolower($_dname)."</a>";
		}

		return $html;
	}

	function not_empty($string)
	{
		if (strlen($string) > 0)
			return TRUE;

		return FALSE;
	}

	if ( ! function_exists('print_time_diference'))
	{
		// Calcula la diferencia desde el momento actual hasta la fecha indicada, a menos que se le pase un datetime_Start.
		// Depth es que tanto detalla a partir de la unidad mas grande de distancia..
		// Short es si se abrevia.
		function print_time_diference($datetime, $depth = 2, $short = TRUE, $datetime_start = FALSE) 
		{
			if (empty($datetime))
				return '-';

			if (strlen($datetime) == 10)
				$datetime = $datetime . ' 00:00:00';

			$timezone  		= new DateTimeZone('America/Argentina/Buenos_Aires');
			$target_date 	= DateTime::createFromFormat('Y-m-d H:i:s', $datetime, $timezone);
			$start_date 	= ($datetime_start == FALSE) ? new DateTime('now', $timezone) : DateTime::createFromFormat('Y-m-d H:i:s', $datetime_start, $timezone);
			$diff 			= $start_date->diff($target_date);

			$edad 	= NULL;
			$años 	= $diff->y;
			$meses 	= $diff->m;
			$dias 	= $diff->d;
			$horas 	= $diff->h;
			$minut  = $diff->i;
			$segun  = $diff->s;
			$_depth  = 0;

			if ($años > 0) {
				$edad .= $años;
				$edad .= ($short) ? 'A' : ' año';
				$_depth++;
			}
			if ($años > 1 AND !$short)
				$edad .= 's';

			if ($_depth >= $depth) return $edad;

			$edad .= ' ';
			if ($meses > 0) {
				$edad .= $meses;
				$edad .= ($short) ? 'M' : ' mes';
				$_depth++;
			}
			if ($meses > 1 AND !$short)
				$edad .= 'es';

			if ($_depth >= $depth) return $edad;

			$edad .= ' ';
			if ($dias > 0) {
				$edad .= $dias;
				$edad .= ($short) ? 'D' : ' dia';
				$_depth++;
			}
			if ($dias > 1 AND !$short)
				$edad .= 's';

			if ($_depth >= $depth) return $edad;

			$edad .= ' ';
			if ($horas > 0) {
				$edad .= $horas;
				$edad .= ($short) ? 'h' : ' hora';
				$_depth++;
			}
			if ($horas > 1 AND !$short)
				$edad .= 's';

			if ($_depth >= $depth) return $edad;

			$edad .= ' ';
			if ($minut > 0) {
				$edad .= $minut;
				$edad .= ($short) ? 'm' : ' minuto';
				$_depth++;
			}
			if ($minut > 1 AND !$short)
				$edad .= 's';

			if ($_depth >= $depth) return $edad;

			$edad .= ' ';
			if ($segun > 0) {
				$edad .= $segun;
				$edad .= ($short) ? 's' : ' segundo';
				$_depth++;
			}
			if ($segun > 1 AND !$short)
				$edad .= 's';

			if (empty(trim($edad)))
				return '0s';

			return $edad;
		}
	}

	// Para debugear, imprime una variable preada.
	if ( ! function_exists('config_form'))
	{
		function config_form($scope, $reference = NULL, $custom_data = array(), $return = FALSE) 
		{
			$CI 			=& get_instance();
		  	$d['configs'] 	= $CI->app_settings->get_configurations($scope, $reference);
		  	$d['scope'] 	= $scope;
		  	$d['reference'] = $reference;
		  	
		  	if (is_array($custom_data) AND !empty($custom_data) AND $d['configs'] !== FALSE)
		  		$d['configs'] 	= array_replace_recursive($d['configs'], $custom_data);

		  	return $CI->load->view('components/configuration/form', $d, $return);
		}
	}

	// Para debugear, imprime una variable preada.
	if ( ! function_exists('debugger'))
	{
		function debugger($array, $exit = FALSE) 
		{
		  	$taran = "<pre>".print_r($array, TRUE)."</pre>";
		  	
		  	if ($exit)
		  		exit($taran);

		  	echo $taran;
		}
	}

	// Retorna si o no segun valor.
	if ( ! function_exists('si_no'))
	{
		function si_no($value, $si = 'Si', $no = 'No') 
		{
			if ($value === TRUE) 
				return $si;

			if (mb_strtolower($value) === 'true') 
				return $si;

			if ($value === '1') 
				return $si;

			if ($value === 1) 
				return $si;

			return $no;
		}
	}

	// Retorna el nombre de un rol en el lenguaje de presentacion.
	if ( ! function_exists('get_role_name'))
	{
		function get_role_name($id_role) 
		{
			// Hay que obtener el area del modelo, y retornar su traduccion:

			$CI =& get_instance();
			$role = $CI->app_users->get_role($id_role);
			
			if ($role !== FALSE) 
				return $role['role_name'];

			return '{:ERROR:}';
		}
	}

	// Retorna el valor del permiso solicitado para el rol del usuario logeado
	if ( ! function_exists('check_permissions'))
	{
		function check_permissions($permission_group, $permission_item, $id_role = FALSE, $interrupt = FALSE) // Interrupt es para los llamados ajax.
		{
			$CI =& get_instance();
			if ($id_role == FALSE) $id_role = (int)$CI->session->userdata('id_role');
			if ($id_role == 0) return FALSE;

			$permission = $CI->app_permissions->get($id_role, $permission_group, $permission_item);

			if ($interrupt == TRUE AND $permission == FALSE)
				ajax_response('success', 'Tu cuenta no posee permisos para realizar esta acción.', '1', '1');
			else
				return $permission;
		}
	}

	// Handler para erorres de foreign key. Solo funciona en entornos de PROD.
	if ( ! function_exists('handle_custom_db_errors'))
	{
		function handle_custom_db_errors($error) 
		{
			if (!is_array($error) OR !isset($error['code']))
				return TRUE;

			$CI =& get_instance();

			switch ($error['code']) 
			{
				case '1451': // Foreign Key Check
					ajax_response('danger', $CI->lang->line('general_error_foreign_key'), '0', '1');
					break;
				
				default:
					# code...
					break;
			}
		}
	}
	
	// Toma los files de POST, que estan entrecruzados. Y los retorna ordenados para iterar.
	if ( ! function_exists('handle_files'))
	{
		function handle_files($input_name) 
		{
			$files = array();
            if (count($_FILES[$input_name]) > 0) 
            {
                foreach ($_FILES[$input_name]['size'] as $item => $size) 
                {
                    if ($size > 0) 
                    {
                        $add_file['name']       = $_FILES[$input_name]["name"][$item];
                        $add_file['type']       = $_FILES[$input_name]["type"][$item];
                        $add_file['tmp_name']   = $_FILES[$input_name]["tmp_name"][$item];
                        $add_file['error']      = $_FILES[$input_name]["error"][$item];
                        $add_file['size']       = $_FILES[$input_name]["size"][$item];
                        $files[$input_name.'_'.$item]   = $add_file;
                    }
                }
            }
            return $files;
		}
	}

	// Transforma en condiciones para el query_builder un string.
	// Resulta util cuando hay que encadenar select2s o filtrar resultads ajax.
	// ej: id_country:2;user_name:notnull;age>10
	if ( ! function_exists('handle_filters'))
	{
		function handle_filters($filter) 
		{
			if (empty($filter) OR $filter == FALSE)
				return FALSE;
			else
			{	
				$filters = explode(';', $filter);
				$cond = array();

				foreach ($filters as $key => $filter) 
				{
					$filter_segments = explode(':', $filter);

					if (count($filter_segments) > 1) 
					{
						$filter_segment_value = explode(',', $filter_segments[1]);

						if (count($filter_segment_value)>1) {

							foreach ($filter_segment_value as $key => $filter_segment_value_item) 
							{
								$cond[$filter_segments[0]][] = strtolower($filter_segment_value_item) == 'null' ? NULL : $filter_segment_value_item;
							}
							
						}else{

							if ($filter_segments[1] == 'notnull')
								$cond[$filter_segments[0].'!='] = NULL;
							else
								$cond[$filter_segments[0]] = (strtolower($filter_segments[1]) == 'null') ? NULL : $filter_segments[1];
						}
					}
				}
				return $cond;
			}
		}
	}

	// Retorna los lenguajes disponibles en la instalacion de CI.
	if ( ! function_exists('get_languages'))
	{
		function get_languages() 
		{
			$lang_path     = APPPATH.'language';

	        // Tenemos un layout por carpeta, asique buscamos las carpetas.
	        $languages = glob($lang_path . '/*' , GLOB_ONLYDIR);

	        // Limpiamos las rutas, porque glob nos da la ruta completa del directorio.
	        foreach ($languages as $key => $lang) 
	        {
	            $lang         = explode('/', $lang);
	            if (isset($lang[1]))
	                $languages[$key]  = $lang[count($lang) - 1];   // tomamos solo el nombre del directorio.
	        }

	        return $languages;
		}
	}

	// Retorna la foto del usuario.
	if ( ! function_exists('avatar'))
	{
		function avatar($id_user = FALSE, $file_name = FALSE) 
		{
			if ($id_user !== FALSE) 
			{
				$CI 		=& get_instance();
				$id_user 	= $CI->session->userdata('id_user');
				$user 		= $CI->app_users->get_user($id_user);
				
				if (empty($user['user_picture'])) 
					$user['user_picture'] = 'default.png';

				return base_url(APP_UPLOADS_URL.'avatars/'.$user['user_picture']);
			}
			elseif ($file_name !== FALSE)
			{
				if (empty($file_name))
					$file_name = 'default.png';
				return base_url(APP_UPLOADS_URL.'avatars/'.$file_name);
			}
			return NULL;
		}
	}


	// Retorna el nombre del usuario.
	if ( ! function_exists('get_property_name'))
	{
		function get_property_name($id_prop = FALSE) 
		{
			$CI =& get_instance();

			//if ($id_user == FALSE OR $id_user == $CI->session->userdata('id_user')) 
			//	return $CI->session->userdata('user_name') . ' ' . $CI->session->userdata('user_lastname');

			$prop = $CI->m_inmuebles->get_inmueble($id_prop);
			return $prop['property_name'];
		}
	}

	// Retorna el nombre del usuario.
	if ( ! function_exists('get_user_name'))
	{
		function get_user_name($id_user = FALSE) 
		{
			$CI =& get_instance();

			if ($id_user == FALSE OR $id_user == $CI->session->userdata('id_user')) 
				return $CI->session->userdata('user_name') . ' ' . $CI->session->userdata('user_lastname');

			$user = $CI->app_users->get_user($id_user);
			return trim($user['user_name']) . ' ' . trim($user['user_lastname']);
		}
	}

	// Retorna el nombre del usuario.
	if ( ! function_exists('get_user_email'))
	{
		function get_user_email($id_user = FALSE) 
		{
			$CI =& get_instance();

			if ($id_user == FALSE OR $id_user == $CI->session->userdata('id_user')) 
				return $CI->session->userdata('user_email');

			$user = $CI->app_users->get_user($id_user);
			return trim($user['user_email']);
		}
	}
	// Renderiza una pagina
	if ( ! function_exists('show_page'))
	{
		function show_page($page, $data = array(), $layout = APP_LAYOUT) 
		{
			$CI =& get_instance();

			// Identificamos si se esta mostrando la base o una subpagina
			$sections = explode('/', $page);
				$page = $sections[0];

			if (count($sections) > 1)
			{
				$default_view = (isset($sections[2])) ? $sections[2] : 'main';
				$section = 'subpages/'.$sections[1].'/'.$default_view;
			}
			else
				$section = 'main';

			// Vemos si hay un archivo de recursos para cargar
			$resouce_file = APPPATH.'views/'.DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.$page.DIRECTORY_SEPARATOR.'custom_page_resources.php';
			if (file_exists($resouce_file))
			{
				include($resouce_file);
				$data['RESOURCES'] = @$RESOURCES;
				$data['COMPONENTS'] = @$COMPONENTS;
			}
			else
			{
				$data['RESOURCES'] = FALSE;
				$data['COMPONENTS'] = FALSE;
			}

			// Cargamos los datos de la pagina y subpagina que se esta mostrando
			define('PAGE', 		strtolower($page));
			define('SUBPAGE', 	strtolower(@$sections[1]));

			// Cargamos la pagina a mostrar
			$data['CONTENT'] = $CI->load->view("pages/$page/$section", $data, TRUE);
			$CI->load->view("layouts/$layout/layout", $data);
		}
	}

	// Esta funcion solo puede ser llamada desde vistas mostradas con show_page
	if ( ! function_exists('show_component'))
	{
		function show_component($component, $data = array(), $return = FALSE, $set_page = FALSE) 
		{
			$CI =& get_instance();

			if (!defined('PAGE') AND $set_page !== FALSE)
				define('PAGE', $set_page);

			if (!defined('PAGE'))
				echo '{Error al cargar el componente. No esta definida la página a la que pertenece el bloque.}';

			$resouce_file 	= APPPATH.'views/'.DIRECTORY_SEPARATOR.'pages'.DIRECTORY_SEPARATOR.PAGE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.$component.'.php';
			$view_file 		= 'pages'.DIRECTORY_SEPARATOR.PAGE.DIRECTORY_SEPARATOR.'components'.DIRECTORY_SEPARATOR.$component;
			if (file_exists($resouce_file))
				if (count($data) > 0)
					return $CI->load->view($view_file, $data, $return);
				else
					return $CI->load->view($view_file, FALSE, $return);
			else
				echo '{Error al cargar el componente. No existe}';
		}
	}

	// Recorta un texto
	if ( ! function_exists('text_preview'))
	{
		function text_preview($text, $max_lenght = 100, $final_string = "...") 
		{
			$return = strip_tags(trim($text));
	        if (strlen($return) >= $max_lenght) 
	            $return = substr($return, 0, $max_lenght - strlen($final_string)).$final_string;
	        return preg_replace("/&#?[a-z0-9]+;áéíóúñ/i","",$return);
		}
	}

	// Retorna la diferencia en dias entre dos fechas.
	if ( ! function_exists('get_days_between'))
	{
		function get_days_between($date1, $date2) 
		{
			$start 	= new DateTime($date1);
			$end 	= new DateTime($date2);
			$diff 	= round(($end->format('U') - $start->format('U')) / (60*60*24));
			return $diff;
		}
	}

if ( ! function_exists('get_time_ago'))
	{
		function get_time_ago($datetime, $short = FALSE, $datetime_start = FALSE, $detailed = FALSE) 
		{
			if (empty($datetime))
				return '-';

			$timezone  		= new DateTimeZone('America/Argentina/Buenos_Aires');
			$target_date 	= DateTime::createFromFormat('Y-m-d H:i:s', $datetime, $timezone);
			$start_date 	= ($datetime_start == FALSE) ? new DateTime('now', $timezone) : DateTime::createFromFormat('Y-m-d H:i:s', $datetime_start, $timezone);
			$diff 			= $start_date->diff($target_date);

			$edad 	= NULL;
			$años 	= $diff->y;
			$meses 	= $diff->m;
			$dias 	= $diff->d;
			$horas 	= $diff->h;
			$minut  = $diff->i;
			$segun  = $diff->s;

			if ($short AND $detailed) 
				if ($años > 0) return "{$años}A{$meses}M{$dias}D{$horas}h";
				elseif ($meses > 0) return "{$meses}M{$dias}D{$horas}h{$minut}m";
				elseif ($dias > 0) return "{$dias}D{$horas}h{$minut}m";
				elseif ($horas > 0) return "{$horas}h{$minut}m";
				elseif ($minut > 0) return "{$minut}m{$segun}s";
				elseif ($segun > 0) return "{$segun}s";

			if ($short == TRUE) 
				if ($años > 0) return "{$años}A";
				elseif ($meses > 0) return "{$meses}M";
				elseif ($dias > 0) return "{$dias}D";
				elseif ($horas > 0) return "{$horas}h";
				elseif ($minut > 0) return "{$minut}m";
				elseif ($segun > 0) return "{$segun}s";

			if ($años > 0)
				$edad .= $años.' año';
			if ($años > 1)
				$edad .= 's';

			$edad .= ' ';
			if ($meses == 1)
				$edad .= '1 mes';
			if ($meses > 1)
				$edad .= $meses.' meses';

			$edad .= ' ';
			if ($dias > 0)
				$edad .= $dias.' dia';
			if ($dias > 1)
				$edad .= 's';

			$edad .= ' ';
			if ($horas > 0)
				$edad .= $horas.' hora';
			if ($horas > 1)
				$edad .= 's';

			$edad .= ' ';
			if ($minut > 0)
				$edad .= $minut.' minuto';
			if ($minut > 1)
				$edad .= 's';

			$edad .= ' ';
			if ($segun > 0)
				$edad .= $segun.' segundo';
			if ($segun > 1)
				$edad .= 's';

			if (empty(trim($edad)))
				return '0s';

			return $edad;
		}
	}

	// Recorta una fecha de DB
	if ( ! function_exists('date_to_view'))
	{
		function date_to_view($date) 
		{
			return substr($date, 0, 10);
		}
	}

	// Determina si una fecha es valida.
	if ( ! function_exists('check_date'))
	{
		function check_date($date){

			if (empty($date)) return FALSE;

			$separator 	= (strpos('-', $date) !== FALSE) ? '-' : '/';
			$date_parts = explode($separator, $date);
			
			if (count($date_parts) !== 3) 
				return FALSE;

			// Vemos si la fecha esta en formato ddmmyyyy o al revez.
			if (strlen($date_parts[0]) == 4)
			{
				// Formato DB
			    $year 	= (int) $date_parts[0];
			    $month 	= (int) $date_parts[1];
				$day 	= (int) $date_parts[2];
			}
			else
			{
				// Formato Presentación
			    $day 	= (int) $date_parts[0];
			    $month 	= (int) $date_parts[1];
			    $year 	= (int) $date_parts[2];
			}
		    return checkdate($month, $day, $year);
		}
	}

	// Formatea una fecha para DB o para front.
	if ( ! function_exists('transform_date')) 
	{
		function transform_date($date, $new_separator = '/', $intial_separator = '-') {
			return implode($new_separator, array_reverse(explode($intial_separator, substr($date, 0, 10))));
		}
	}

	// Recorta el tiempo de una fecha de DB
	if ( ! function_exists('time_to_view'))
	{
		function time_to_view($date) 
		{
			return substr($date, 11, 5);
		}
	}

	// Retorna el nombre de un mes
	if ( ! function_exists('get_month_name'))
	{
		function get_month_name($month_number) 
		{
			$months = array('enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre');
			return $months[(int)$month_number-1];
		}
	}

	// Retorna el nombre de un usuario company en el lenguaje de presentacion.
	if ( ! function_exists('get_day_name'))
	{
		function get_day_name($week_day_number) 
		{
			switch ($week_day_number) {
				case '1':
					return 'Lunes';
					break;
				case '2':
					return 'Martes';
					break;
				case '3':
					return 'Miercoles';
					break;
				case '4':
					return 'Jueves';
					break;
				case '5':
					return 'Viernes';
					break;
				case '6':
					return 'Sábado';
					break;
				case '7':
					return 'Domingo';
					break;
				case '8':
					return 'Osvaldo';
					break;
				default:
					return 'N/D';
					break;
			}
		}
	}

	// Respuesta AJAX
	if ( ! function_exists('ajax_response'))
	{
		function ajax_response($tipo, $descripcion, $code = NULL, $display = 1, $extra = NULL, $update = NULL) 
		{
			$response['tipo'] 		= $tipo;		// Tipo de mensaje
			$response['mensaje'] 	= $descripcion; // Mensaje orientativo
			$response['cod'] 		= $code;		// Codigo de respuesta
			$response['display'] 	= $display;		// ¿Se sugiere mostrar mensaje al usuario?
			$response['ext'] 		= $extra;		// Datos complementarios
			$response['update'] 	= $update; 		// Contenido HTML a refrescar.
			header('Content-Type: application/json');
			echo json_encode($response);
			exit();
		}
	}
	
	// Transforma un string en algo URL friendly.
	if ( ! function_exists('slug'))
	{
		function slug($text) 
		{
			if (is_array($text)) return 'error';
	        return url_title(strtolower(quitar_acentos($text)));
		}
	}

	// Quita acentos...
	if ( ! function_exists('quitar_acentos'))
	{
		function quitar_acentos($string) 
		{
			$a = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
			$b = array('a', 'e', 'i', 'o', 'u', 'n');		
		  	return str_replace($a, $b, $string);
		}
	}

	// Paginacion. Retorna HTML
	if ( ! function_exists('pag_get_html'))
	{
		function pag_get_html($pagination_current, $pagination_total_items, $pagination_per_page) 
		{
			$data['pagination_current'] 	= $pagination_current;
			$data['pagination_total_items'] = $pagination_total_items;
			$data['pagination_per_page'] 	= $pagination_per_page;

			if ($pagination_total_items == 0 OR $pagination_per_page == 0)
				return NULL;

			$CI =& get_instance();
			return $CI->load->view('components/pagination/pagination', $data, TRUE);
		}
	}
	// Paginacion. Retorna INT numero de pagina actual
	if ( ! function_exists('pag_current_page'))
	{
		function pag_current_page() 
		{
			return ((int)@$_GET['pagina'] > 0) ? (int)@$_GET['pagina'] - 1 : 0;
		}
	}
	// Paginacion. Retorna INT numero de items totales en datos
	if ( ! function_exists('pag_total_items'))
	{
		function pag_total_items($datasource) 
		{

			$elem = reset($datasource);
			return $elem['total_results'];
		}
	}

	// Genera una cadena aleatoria.
	if ( ! function_exists('generate_string'))
	{
		function generate_string($length) {
		    $char = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		    $char = str_shuffle($char);
		    for($i = 0, $rand = '', $l = strlen($char) - 1; $i < $length; $i ++) {
		        $rand .= $char{mt_rand(0, $l)};
		    }
		    return $rand;
		}
	}

	// Retorna el IP del usuario
	if ( ! function_exists('get_user_ip'))
	{
		function get_user_ip() 
		{

		    $ipaddress = '';
		    if (getenv('HTTP_CLIENT_IP'))
		        $ipaddress = getenv('HTTP_CLIENT_IP');
		    else if(getenv('HTTP_X_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
		    else if(getenv('HTTP_X_FORWARDED'))
		        $ipaddress = getenv('HTTP_X_FORWARDED');
		    else if(getenv('HTTP_FORWARDED_FOR'))
		        $ipaddress = getenv('HTTP_FORWARDED_FOR');
		    else if(getenv('HTTP_FORWARDED'))
		       $ipaddress = getenv('HTTP_FORWARDED');
		    else if(getenv('REMOTE_ADDR'))
		        $ipaddress = getenv('REMOTE_ADDR');
		    else
		        $ipaddress = 'UNKNOWN';

			return $ipaddress;
		}
	}

	// Genera una cadena para GET
	if ( ! function_exists('generate_get_string'))
	{
		function generate_get_string($add_key = FALSE, $add_value =FALSE, $except = array()) {
		    $get_vars = $_GET;
		    $key_pairs = array();
		    foreach ($get_vars as $key => $value) 
		    	if (!in_array($key, $except) AND $key !== $add_key)
		    		array_push($key_pairs, $key.'='.$value)	;

		    if ($add_key !== FALSE)
		    	array_push($key_pairs, $add_key.'='.$add_value)	;

		    if (count($key_pairs) > 0)
		    	return '?'.implode('&', $key_pairs);

		    return NULL;
		}
	}
	// Genera una cadena para GET
	if ( ! function_exists('generate_get_string_2'))
	{
		function generate_get_string_2($add_keys = array(), $except = array(), $custom_identifier = NULL) {
		    $get_vars = $_GET;
		    $except = (is_array($except)) ? $except : array();
		    $key_pairs = array();
		    foreach ($get_vars as $key => $value) 
		    	if (!in_array($key, $except) AND !in_array($key, array_keys($add_keys)))
		    		if (is_array($key) OR is_array($value))
		    			continue;
		    		else
		    			array_push($key_pairs, $key.'='.$value);

		    foreach ($add_keys as $add_key => $add_value)
		    	array_push($key_pairs, $add_key.'='.$add_value);

		    if (count($key_pairs) > 0)
		    	return $custom_identifier . '?' . implode('&', $key_pairs);

		    return NULL;
		}
	}
	
	// Si no esta disponible esta funcion, la creamos.
	if ( ! function_exists('array_column'))
	{
	    function array_column($source, $column)
	    { 
		     $namearray = array();
		     foreach ($source as $name) {
		        $namearray[] = $name[$column];
		     }
		     return $namearray;
	    }
	}

	// No se para que habia echo esto.. queda.
	if ( ! function_exists('generate_session_token'))
	{
		function generate_session_token($id_user, $email) 
		{
			#echo "$id_user - $email - ".md5($id_user.':367efa31cbbf5483974b0a809a294679:'.$email).' <- FIN HELPER. <br>';
			return md5($id_user.':367efa31cbbf5483974b0a809a294679:'.$email);
		}
	}

	// Minifica HTML
	if ( ! function_exists('minify_html'))
	{
		function minify_html($text) // 
		{
		    ini_set("pcre.recursion_limit", "16777");  // 8MB stack. *nix
		    $re = '%# Collapse whitespace everywhere but in blacklisted elements.
		        (?>             # Match all whitespaces other than single space.
		          [^\S ]\s*     # Either one [\t\r\n\f\v] and zero or more ws,
		        | \s{2,}        # or two or more consecutive-any-whitespace.
		        ) # Note: The remaining regex consumes no text at all...
		        (?=             # Ensure we are not in a blacklist tag.
		          [^<]*+        # Either zero or more non-"<" {normal*}
		          (?:           # Begin {(special normal*)*} construct
		            <           # or a < starting a non-blacklist tag.
		            (?!/?(?:textarea|pre|script)\b)
		            [^<]*+      # more non-"<" {normal*}
		          )*+           # Finish "unrolling-the-loop"
		          (?:           # Begin alternation group.
		            <           # Either a blacklist start tag.
		            (?>textarea|pre|script)\b
		          | \z          # or end of file.
		          )             # End alternation group.
		        )  # If we made it here, we are not in a blacklist tag.
		        %Six';
		    $minified_html = preg_replace($re, " ", $text);
		    if ($minified_html === null) return $text;
		    return $minified_html;
		}
	}


	// Genera cadena aleatoria.
	if ( ! function_exists('unique_identifier'))
	{
		function unique_identifier() {
			return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',

			  // 32 bits for "time_low"
			  mt_rand(0, 0xffff), mt_rand(0, 0xffff),

			  // 16 bits for "time_mid"
			  mt_rand(0, 0xffff),

			  // 16 bits for "time_hi_and_version",
			  // four most significant bits holds version number 4
			  mt_rand(0, 0x0fff) | 0x4000,

			  // 16 bits, 8 bits for "clk_seq_hi_res",
			  // 8 bits for "clk_seq_low",
			  // two most significant bits holds zero and one for variant DCE1.1
			  mt_rand(0, 0x3fff) | 0x8000,

			  // 48 bits for "node"
			  mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
			);
		}
	}

	if ( ! function_exists('string_to_color'))
	{
		function string_to_color($string, $get = 'background') {
			
			// 59 Colores de tipos
			$font_colors = array('bg-font-white','bg-font-default','bg-font-dark','bg-font-blue','bg-font-blue-madison','bg-font-blue-chambray','bg-font-blue-ebonyclay','bg-font-blue-hoki','bg-font-blue-steel','bg-font-blue-soft','bg-font-blue-dark','bg-font-blue-sharp','bg-font-blue-oleo','bg-font-green','bg-font-green-meadow','bg-font-green-seagreen','bg-font-green-turquoise','bg-font-green-haze','bg-font-green-jungle','bg-font-green-soft','bg-font-green-dark','bg-font-green-sharp','bg-font-green-steel','bg-font-grey','bg-font-grey-steel','bg-font-grey-cararra','bg-font-grey-gallery','bg-font-grey-cascade','bg-font-grey-silver','bg-font-grey-salsa','bg-font-grey-salt','bg-font-grey-mint','bg-font-red','bg-font-red-pink','bg-font-red-sunglo','bg-font-red-intense','bg-font-red-thunderbird','bg-font-red-flamingo','bg-font-red-soft','bg-font-red-haze','bg-font-red-mint','bg-font-yellow','bg-font-yellow-gold','bg-font-yellow-casablanca','bg-font-yellow-crusta','bg-font-yellow-lemon','bg-font-yellow-saffron','bg-font-yellow-soft','bg-font-yellow-haze','bg-font-yellow-mint','bg-font-purple','bg-font-purple-plum','bg-font-purple-medium','bg-font-purple-studio','bg-font-purple-wisteria','bg-font-purple-seance','bg-font-purple-intense','bg-font-purple-sharp','bg-font-purple-soft');
			// 59 Colores de fondo
			$bg_colors = array('bg-white','bg-default','bg-dark','bg-blue','bg-blue-madison','bg-blue-chambray','bg-blue-ebonyclay','bg-blue-hoki','bg-blue-steel','bg-blue-soft','bg-blue-dark','bg-blue-sharp','bg-blue-oleo','bg-green','bg-green-meadow','bg-green-seagreen','bg-green-turquoise','bg-green-haze','bg-green-jungle','bg-green-soft','bg-green-dark','bg-green-sharp','bg-green-steel','bg-grey','bg-grey-steel','bg-grey-cararra','bg-grey-gallery','bg-grey-cascade','bg-grey-silver','bg-grey-salsa','bg-grey-salt','bg-grey-mint','bg-red','bg-red-pink','bg-red-sunglo','bg-red-intense','bg-red-thunderbird','bg-red-flamingo','bg-red-soft','bg-red-haze','bg-red-mint','bg-yellow','bg-yellow-gold','bg-yellow-casablanca','bg-yellow-crusta','bg-yellow-lemon','bg-yellow-saffron','bg-yellow-soft','bg-yellow-haze','bg-yellow-mint','bg-purple','bg-purple-plum','bg-purple-medium','bg-purple-studio','bg-purple-wisteria','bg-purple-seance','bg-purple-intense','bg-purple-sharp','bg-purple-soft');

			$crc 	= abs(crc32($string));
			$index 	= $crc % 59;

			return ($get == 'background') ? $bg_colors[$index] : $font_colors[$index];
		}
	}	

	if ( ! function_exists('print_size'))
	{
		function print_size($bytes, $dec = 2) 
		{
		    $size   = array('B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
		    $factor = floor((strlen($bytes) - 1) / 3);

		    return sprintf("%.{$dec}f", $bytes / pow(1024, $factor)) . @$size[$factor];
		}
	}

	// Esta funcion convierte una fecha al formato para mysql.
	if ( ! function_exists('store_date'))
	{
		function store_date($date, $modify = NULL) 
		{
			if (empty($date))
				return NULL;

			// Determinamos el formato de la fecha. ESTO
			$_semgents = explode('-', $date);
			if (strlen($_semgents[0]) == 4)
            	$d = DateTime::createFromFormat('Y-m-d', $date);
            else
            	$d = DateTime::createFromFormat('d/m/Y', $date);

            $ts = $d->getTimestamp();

            // Retornamos la fecha formateada para mysql.
		  	return date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', $ts) . $modify));
		}
	}

	// Esta funcion representa una fecha en el formato configurado.
	if ( ! function_exists('print_time'))
	{
		function print_time($date, $has_date = TRUE, $modify = NULL) 
		{
			if (empty($date))
				return NULL;
			
            // Convertimos la fecha proporcionada a timestamp.
            if ($has_date)
            	$d 	= DateTime::createFromFormat('Y-m-d H:i:s', $date);
            else
            	$d 	= DateTime::createFromFormat('H:i:s', $date);
            
            if ($d == FALSE)
            	return '';

            $ts = $d->getTimestamp();

            // Determinamos el formato a representar.
           	$format = ' H:i:s';

            // Retornamos la fecha formateada.
		  	return date($format, strtotime(date('Y-m-d H:i:s', $ts) . $modify));
		}
	}

	// Esta funcion representa una fecha en el formato configurado.
	if ( ! function_exists('print_datetime'))
	{
		function print_datetime($date, $show_time = TRUE, $modify = NULL, $custom_format = FALSE) 
		{
			
			setlocale(LC_ALL, "es_ES", 'Spanish_Spain', 'Spanish');
			if (empty($date))
				return NULL;
			
            // Convertimos la fecha proporcionada a timestamp.
            $d 	= DateTime::createFromFormat('Y-m-d H:i:s', $date);
            
            if ($d == FALSE)
            	return '';

            $ts = $d->getTimestamp();
            // Determinamos el formato a representar.
            $format = 'd/m/Y';

            // Agregamos el tiempo.
            if ($show_time == TRUE)
            	$format .= ' H:i:s';

            if ($custom_format)
            	$format = $custom_format;

            return strftime("%d de %B de %Y",$ts);
            // Retornamos la fecha formateada.
		}
	}

	// Esta funcion representa una fecha en el formato configurado.
	if ( ! function_exists('print_date'))
	{
		function print_date($date, $modify = NULL) 
		{
			if (empty($date))
				return NULL;
			
            // Convertimos la fecha proporcionada a timestamp.
            $d 	= DateTime::createFromFormat('Y-m-d', $date);
            
            if ($d == FALSE)
            	return '';

            $ts = $d->getTimestamp();
            
            // Determinamos el formato a representar.
            $format = 'd/m/Y';

            // Retornamos la fecha formateada.
		  	return date($format, strtotime(date('Y-m-d', $ts) . $modify));
		}
	}
	// Esta funcion escribe los nombres a partir de un array de ids de usuarios
	if ( ! function_exists('file_uri'))
	{
		function file_uri($id_file, $thumb = FALSE) 
		{
			$CI 	=& get_instance();
			$file 	= $CI->libreria->get_file($id_file);

			if ($file !== FALSE) 
			{
				$thumb = ($thumb AND $file['is_image']) ? 'thumb/' : NULL;

				return base_url() . '/uploads/' . $thumb . $file['file_key'];
			}

			if (empty($file))
					$file = 'default.png';
				return base_url(APP_UPLOADS_URL.'avatars/'.$file);

			return NULL;
		}
	}

	// Esta funcion escribe los nombres a partir de un array de ids de usuarios
	if ( ! function_exists('file_name'))
	{
		function file_name($id_file) 
		{
			$CI 	=& get_instance();
			$file 	= $CI->libreria->get_file($id_file);

			if ($file !== FALSE) 
			{
				return $file['file_name'] . '.' . $file['file_ext'];
			}

			return NULL;
		}
	}

	if ( ! function_exists('str_extract'))
	{
		function str_extract($string, $delimiter_open, $delimiter_close)
		{
			$pattern = "/$delimiter_open(.*?)$delimiter_close/";
			preg_match_all($pattern, $string, $matches);
			return $matches;
		}
	}

	// Agrupa un array a aprtir de una columna.
	if ( ! function_exists('array_group'))
	{
	    function array_group($source, $column, $column2 = FALSE, $column3 = FALSE, $column4 = FALSE)
	    { 
			$arr = array();

			if ($column4 !== FALSE) {
				foreach($source as $key => $item)
				   $arr[$item[$column]][$item[$column2]][$item[$column3]][$item[$column4]][] = $item;
			}
			elseif ($column3 !== FALSE) {
				foreach($source as $key => $item)
				   $arr[$item[$column]][$item[$column2]][$item[$column3]][] = $item;
			}
			elseif ($column2 !== FALSE) {
				foreach($source as $key => $item)
				   $arr[$item[$column]][$item[$column2]][] = $item;
			}
			else
				foreach($source as $key => $item)
				   $arr[$item[$column]][] = $item;

			return $arr;
	    }
	}

/* End of file peybol_helper.php */
/* Location: ./application/controllers/peybol_helper.php */