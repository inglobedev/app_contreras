<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Herramientas de Pachi
 */
	
	if ( ! function_exists('pachi_fullmanaged_table'))
	{
		function pachi_fullmanaged_table($table, $columns = array(), $options = FALSE, $class = 'table-bordered table-hover bg-white table-vertical-align' ) 
		{
			$CI =& get_instance();

			$table_key 	= substr(pachi_mask($table), 0, 5);
			$fixed_filter 	= (isset($options['fixed_filters'])) ? $options['fixed_filters'] : array();
   			$fixed_order 	= (isset($options['fixed_orders'])) ? $options['fixed_orders'] : array();
   			$dataset 	= $CI->pachi->fetch_table($table_key, $table, $fixed_filter, FALSE, FALSE, $fixed_order);

			$data['table_key'] 	= $table_key;
			$data['table'] 		= $table;
			$data['dataset'] 	= $dataset;
			$data['columns'] 	= $columns;
			$data['options'] 	= $options;
			$data['class'] 		= $class;

			return $CI->load->view("pachi/table/fullmanaged", $data, TRUE);
		}
	}

	if ( ! function_exists('pachi_table_toolbar'))
	{
		function pachi_table_toolbar($table = FALSE, $dataset, $columns = array(), $options = array()) 
		{
			$CI =& get_instance();
			
			$table_key 	= substr(pachi_mask($table), 0, 5);

			$data['table_key'] 	= $table_key;
			$data['table'] 		= $table;
			$data['dataset'] 	= $dataset;
			$data['columns'] 	= $columns;
			$data['options'] 	= $options;

			return $CI->load->view("pachi/table/toolbar", $data, TRUE);
		}
	}

	// Genera una tabla a partir de un set de datos cualquiera.
	// Si es un set de datos obtenido a traves de un fetch, se puede utilizar paginacion con algunos tweaks.
	if ( ! function_exists('pachi_table'))
	{
		function pachi_table($table_key = FALSE, $dataset, $columns = array(), $class = 'table-bordered table-hover bg-white table-vertical-align') 
		{
			$CI =& get_instance();

			$data['table'] 		= $CI->pachi->_analize_dataset($dataset);
			$data['table_key'] 	= $table_key;
			$data['class'] 		= $class;

			// Mergeamos la tabla autogenerada, con la config custom de columnas.
			$tmp_columns = array_merge_recursive($data['table']['header'], $columns);

			foreach ($columns as $_column => $_options) {
				$columns[$_column] = $tmp_columns[$_column];
				unset($tmp_columns[$_column]);
			}

			$columns = array_merge($columns, $tmp_columns);
			$data['table']['header'] = $columns;

			return $CI->load->view("pachi/table/default", $data, TRUE);
		}
	}

	// Establece en GET la config de una tabla dada.
	if ( ! function_exists('pachi_table_set'))
	{
		function pachi_table_set($table, $page = FALSE, $items_per_page = FALSE, $filters = FALSE, $order = FALSE, $search = NULL) 
		{
			$CI 		=& get_instance();
			$table 		= pachi_mask($table);
			$setted 	= $CI->input->get($table);
			$setted 	= json_decode(pachi_unmask($setted), TRUE);

			if ($page !== FALSE)
				$setted['p'] 			= $page;
			if ($items_per_page !== FALSE)
				$setted['ipp'] 	= $items_per_page;
			if (is_array($filters))
				$setted['f'] 			= $filters;
			if (is_array($order))
				$setted['o'] 			= $order;
			if (is_array($order))
				$setted['s'] 			= $search;

			return array($table => pachi_mask(json_encode($setted)));
		}
	}

	// Obtiene de GET la config de una tabla dada.
	if ( ! function_exists('pachi_table_get'))
	{
		function pachi_table_get($table, $default_page = 0, $default_items_per_page = 15) 
		{
			$CI 		=& get_instance();
			$table 		= pachi_mask($table);
			$setted 	= $CI->input->get($table);
			$setted 	= json_decode(pachi_unmask($setted), TRUE);

			$setted['p'] 			= isset($setted['p']) ? $setted['p'] : (int)$default_page;
			$setted['ipp'] 			= isset($setted['ipp']) ? $setted['ipp'] : (int)$default_items_per_page;
			$setted['f'] 			= isset($setted['f']) ? $setted['f'] : array();
			$setted['o'] 			= isset($setted['o']) ? $setted['o'] : array();
			$setted['s'] 			= isset($setted['s']) ? $setted['s'] : NULL;

			return $setted;
		}
	}


	if ( ! function_exists('pachi_form'))
	{
		function pachi_form($table, $columns = FALSE, $entity_id = FALSE, $_data = array(), $design = 'default') 
		{
			$CI =& get_instance();

			$data = $CI->pachi->get_form_data($table, $entity_id, $columns);
			
			$data['ext'] = $_data;

			// Datas sgugeridos
			// $data['id']			= string;
			// $data['action']		= url;
			// $data['class']		= clases csv;
			// $data['redirect']	= url;
			// $data['cancel']		= url;

			return $CI->load->view("pachi/form/{$design}", $data, TRUE);
		}
	}

	if ( ! function_exists('pachi_retrieve_form'))
	{
		function pachi_retrieve_form() 
		{
			$CI =& get_instance();
			$received = $CI->input->post();
			
			// Informacion del formulario.
			$return 		= json_decode(pachi_unmask($received['form']), TRUE);
			$return['meta'] = json_decode(pachi_unmask($received['meta']), TRUE);

			// Datos del formulario			
			$return['data'] = array();
			if (ENVIRONMENT == 'production' OR PACHI_MASK == TRUE) {
				foreach ($received['data'] as $_field => $_value) {
					$return['data'][pachi_unmask($_field)] = $_value;
				}
			}
			else {
				foreach ($received['data'] as $_field => $_value) {
					$return['data'][$_field] = $_value;
				}
			}

			return $return;
		}
	}
	
	if ( ! function_exists('pachi_search'))
	{
		function pachi_search($table, $column_selectable, $column_showable, $filter = FALSE, $custom_display = FALSE) 
		{
			$key['table'] 	= $table;
			$key['column'] 	= $column_selectable;
			$key['display'] 		= $column_showable;
			$key['custom_display'] 	= $custom_display;
			$key['filter'] 	= $filter;

			return pachi_mask(json_encode($key));
		}
	}

	if ( ! function_exists('pachi_retrieve_search'))
	{
		function pachi_retrieve_search() 
		{
			$CI =& get_instance();
			$received = $CI->input->post();
			
			// Informacion del formulario.
			$return = json_decode(pachi_unmask($received['search']), TRUE);
			$return['search'] = $CI->input->post('term');
			return $return;
		}
	}

	if ( ! function_exists('pachi_modal'))
	{
		function pachi_modal($table, $columns = FALSE, $entity_id = FALSE, $_data = array(), $title = FALSE, $design = 'default') 
		{
			$frm['table'] 		= $table;
			$frm['columns'] 	= $columns;
			$frm['entity_id'] 	= $entity_id;
			$frm['_data'] 		= $_data;
			$frm['modal_title'] = $title;
			$frm['design'] 		= $design;

			return pachi_mask(json_encode($frm));
		}
	}

	if ( ! function_exists('pachi_retrieve_modal'))
	{
		function pachi_retrieve_modal() 
		{
			$CI =& get_instance();
			$received = $CI->input->post();
			
			// Informacion del formulario.
			$return = json_decode(pachi_unmask($received['modal']), TRUE);
			return $return;
		}
	}

/**
 * Helpers para Pachi
 */

	if ( ! function_exists('pachi_mask'))
	{
		function pachi_mask($string) 
		{
			$CI =& get_instance();
			$str = base64_encode($CI->session->userdata('user_email'));
	    	return bin2hex(openssl_encrypt ($string, 'aes128', $str, 0, date('YmdYmd')));
		}
	}

	if ( ! function_exists('pachi_unmask'))
	{
		function pachi_unmask($string) 
		{
			$CI =& get_instance();
			$str = base64_encode($CI->session->userdata('user_email'));
	    	return openssl_decrypt (hex2bin($string), 'aes128', $str, 0, date('YmdYmd'));
		}
	}

/**
 * Otras dependencias.
 */

	if ( ! function_exists('str_extract'))
	{
		function str_extract($string, $delimiter_open, $delimiter_close)
		{
			$pattern = "/$delimiter_open(.*?)$delimiter_close/";
			preg_match_all($pattern, $string, $matches);
			return $matches;
		}
	}