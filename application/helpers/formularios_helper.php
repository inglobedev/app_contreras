<?php 
	// Input selector de archivos
	if ( ! function_exists('input_files'))
	{
		function input_files($input_name, $input_values, $input_label = 'Seleccione archivos', $input_max = 1, $input_extensions = FALSE, $isolated_selection = FALSE) 
		{
			$CI =& get_instance();

			$data['input_label'] 	= $input_label;
			$data['input_name'] 	= $input_name;
			$data['input_values'] 	= $input_values;
			$data['input_max'] 		= $input_max;
			$data['input_extensions'] 			= $input_extensions;
			$data['input_isolated_selection'] 	= (int)$isolated_selection;

            return $CI->load->view('components/forms/file_selector', $data, TRUE);
		}
	}
	// retorna previews de archivos
	if ( ! function_exists('input_files_preview'))
	{
		function input_files_preview($selection) 
		{
			$CI 	=& get_instance();
            // Cargamos elementos de la libreria.
            $cond['id_file IN ('.$selection.')'] = NULL;
            $data['selection'] = (empty($selection)) ? array() : $CI->libreria->get_files($cond, FALSE, 0, 50, FALSE);

            return $CI->load->view('components/forms/files_preview', $data, TRUE);
		}
	}

	// Input selector de archivos
	if ( ! function_exists('input_images'))
	{
		function input_images($input_name, $input_values, $input_label = 'Seleccione imagenes', $input_max = 1, $input_extensions = FALSE, $isolated_selection = FALSE) 
		{
			$CI =& get_instance();

			$data['input_label'] 	= $input_label;
			$data['input_name'] 	= $input_name;
			$data['input_values'] 	= $input_values;
			$data['input_max'] 		= $input_max;
			$data['input_extensions'] 			= $input_extensions;
			$data['input_isolated_selection'] 	= (int)$isolated_selection;

            return $CI->load->view('components/forms/image_selector', $data, TRUE);
		}
	}

	// retorna previews de archivos
	if ( ! function_exists('input_images_preview'))
	{
		function input_images_preview($selection) 
		{
			$CI 	=& get_instance();
            // Cargamos elementos de la libreria.
            $cond['id_file IN ('.$selection.')'] = NULL;
            $data['selection'] = (empty($selection)) ? array() : $CI->libreria->get_files($cond, FALSE, 0, 50, FALSE);

            return $CI->load->view('components/forms/images_preview', $data, TRUE);
		}
	}
?>