<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  Remapper
 *
 *  Este archivo es para mapear hacia los archvos de cfg exteriores a CI.
 */

	// Archivo de Notificaciones
	if (file_exists(APPPATH . 'app/notifications.php')) {
		include_once(APPPATH . 'app/notifications.php');
	}

	// Archivo de Permisos
	if (file_exists(APPPATH . 'app/permissions.php')) {
		include_once(APPPATH . 'app/permissions.php');
	}
