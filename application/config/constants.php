<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// Info de version
@include('version.php');

// Cargamos la configuración que podria variar segun entornos. Indispensable para utilizar el gestor.
@include(FCPATH.'instance-cfg.php');

if (!defined('CFG_LOADED')) {
	@include(FCPATH.'instance-cfg-default.php');
}
/*
|--------------------------------------------------------------------------
| Base App
|--------------------------------------------------------------------------
|
|
*/

// URLs
	define('APP_LOGIN', 'login/');
	define('APP_ALQUILERES', 'rents/');
	define('APP_BACKEND', 'backend/');
	define('APP_MAINTENANCES', 'maintenances/');
	define('APP_IMPUESTOS', 'expenses/');
	define('APP_INMUEBLES', 'inmuebles/');
	define('APP_CONTRACTS', 'contracts/');
	define('APP_PROPIETARIOS', 'owners/');
	define('APP_PROPIEDADES', 'properties/');
	define('APP_TEMPORARIOS', 'temporary/');
	define('APP_ADMINISTRATION', 'administration/');
	define('APP_HOME', APP_ALQUILERES);

// Roles
	define('ROLE_SISTEM_ADMINTRATOR', 1);
	define('ROLE_ADMINISTRATOR', 2);
	define('ROLE_OWNER', 3);
	define('ROLE_TENANT', 4);
	define('ROLE_GUARANTOR', 5);
	define('ROLE_PROFESSIONAL', 6);
	define('ROLE_CONSORTIUM', 7);
	define('ROLE_SYSTEM', 8);

// Cuentas
	define('ACCOUNT_ADMINISTRATION', 	1);
	define('ACCOUNT_RENTS', 			2);
	define('ACCOUNT_MAINTENANCES', 		3);
	define('ACCOUNT_CONSORTIUMS', 		4);
	define('ACCOUNT_EXPENSES', 			5);
	define('ACCOUNT_PAYED', 			6);

	// Para el usuario receptor de pagos y servicios, se usa un id fijo.
	define('USER_PAYED', 				4);

// Tipos de gastos
	define('ET_ADMINISTRATIVO', 1);
	define('ET_IMPUESTO', 2);
	define('ET_EXPENSA', 3);
	define('ET_INTERES', 4);
	define('ET_ALQUILER', 5);
	define('ET_VARIOS', 6);
	define('ET_MANTENIMIENTO', 7);


// Sys.
	define('APP_UPLOADS_FOLDER', 	FCPATH.'uploads/app/');
	define('APP_UPLOADS_URL', 		'uploads/app/');
	define('APP_ACCOUNT_URL', 		APP_HOME.'account/');
	define('APP_NOTIFICATIONS_URL', APP_HOME.'notifications/');

	// Extensiones para carga de archivos
	define('ALL_EXTENSIONS', '');
	define('ALL_IMAGE_EXTENSIONS', 'jpg,jpeg,png,bmp,svg,gif');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code
