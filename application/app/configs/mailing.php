<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/**
	 *  type 		label 	default 	options 	id 		class 	placeholder 	reveal
	 * 	checkbox 	X 		01		  	- 			X 		X 		-				X
	 *	text 		X 		NULL 		-			X 		X 		X				-
	 *	textarea 	X 		NULL  	 	-			X 		X 		X				-
	 *	select 		X 		X (opt)  	X 			X 		X 		-				-
	 */

	$_CFG['enable'] = array(
			'type'			=> 'checkbox',
			'label'			=> 'Activar envío de emails',
			'default'		=> 0,
		);

	$_CFG['address'] = array(
			'type'			=> 'text',
			'label'			=> 'Dirección de envío',
			'default'		=> NULL,
			'placeholder'	=> 'Escriba aquí',
		);

	$_CFG['name'] = array(
			'type'			=> 'text',
			'label'			=> 'Nombre de remitente',
			'default'		=> NULL,
			'placeholder'	=> 'Escriba aquí',
		);

	$_CFG['signature'] = array(
			'type'			=> 'textarea',
			'label'			=> 'Firma',
			'default'		=> NULL,
			'placeholder'	=> 'Escriba aquí',
		);

	$_CFG['enable_smtp'] = array(
			'type'			=> 'checkbox',
			'label'			=> 'Activar SMTP',
			'default'		=> 0,
		);

	$_CFG['smtp_host'] = array(
			'type'			=> 'text',
			'label'			=> 'SMTP Host',
			'default'		=> NULL,
			'placeholder'	=> 'Escriba aquí',
		);

	$_CFG['smtp_user'] = array(
			'type'			=> 'text',
			'label'			=> 'SMTP User',
			'default'		=> NULL,
			'placeholder'	=> 'Escriba aquí',
		);

	$_CFG['smtp_pass'] = array(
			'type'			=> 'text',
			'label'			=> 'SMTP Password',
			'default'		=> NULL,
			'placeholder'	=> 'Escriba aquí',
		);

	$_CFG['smtp_port'] = array(
			'type'			=> 'text',
			'label'			=> 'SMTP Port',
			'default'		=> NULL,
			'placeholder'	=> 'Escriba aquí',
		);



