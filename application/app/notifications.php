<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  Configuracion de Notificaciones
 *
 *  $config[notifications][NOTIF_GROUP][NOTIF_DESCR] = DEFAULT_STATUS
 *
 *
 *  DEFAULT_STATUS puede ser TRUE o FALSE, indica si por defecto se notifica por mail o no.
 */

// Estructura de permisos de la app base

		// Permisos para gestion de usuarios
			$config['notifications']['groups']['app']['Notificaciones generales']	= TRUE;
