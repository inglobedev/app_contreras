<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *  Configuracion de Permisos
 *
 *  $config[permissions][PERMISION_GROUP][PERMISSION_ITEM] = PERMISSION_DEFAULT_VALUE
 *
 *  Asegurarse que el PERMISSION_ITEM tenga su linea de traduccion en general_lang, de lo
 *  contrario en las pantallas de configuracion se vera el rotulo en blanco.
 *
 *  PERMISSION_DEFAULT_VALUE puede ser TRUE o FALSE
 */

// Estructura de permisos de la app base

	$config['permissions']['app']['manage_users']			= TRUE;
	$config['permissions']['app']['manage_roles']			= TRUE;
	$config['permissions']['app']['manage_contracts']		= TRUE;
	
	$config['permissions']['app']['handle_payments']		= TRUE;

	
	$config['permissions']['app']['mnu_rents']				= TRUE;
	$config['permissions']['app']['mnu_maintenances']		= TRUE;
	$config['permissions']['app']['mnu_taxes']				= TRUE;
	$config['permissions']['app']['mnu_properties']			= TRUE;
	$config['permissions']['app']['mnu_owners']				= TRUE;
	$config['permissions']['app']['mnu_administration']		= TRUE;
	$config['permissions']['app']['mnu_configuration']		= TRUE;







