<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Baseapp {

    private $CI = FALSE;

    function __construct() 
    {
        if ($this->CI == FALSE)
            $this->CI =&get_instance();
    }

    /**
     * Valida una sesion activa
     */
    public function session_check()
    {
        $login_status = ($this->CI->session->userdata('logged_in')=='TRUE' AND !empty($this->CI->session->userdata('user_email')));

        if ($login_status == TRUE) 
        {
            $token      = $this->CI->session->userdata('token');
            $user_id    = $this->CI->session->userdata('id_user');
            $user_email = $this->CI->session->userdata('user_email');

            // wft? no recuerdo bien que hice o intente hacer aca, pero creo que era para prevenir una sesion falsificada.
            $valid_token = generate_session_token($user_id, $user_email);
            $integrity = ($token == $valid_token) ? TRUE : FALSE;

            if ($integrity === TRUE)
                return $user_id;
            else
                return FALSE;
        }

        // Calculamos a donde va despues de iniciar sesion.
        $this->CI->session->set_flashdata('login_redirect', uri_string());
        
        // Hay alguna notificacion en la url?
        if (!empty($this->CI->input->get('nf')))
            $this->CI->session->set_flashdata('login_from_notification', $this->CI->input->get('nf'));

        if($this->CI->input->is_ajax_request())
        {
            // El 410 esta manejado por utils.js, rompe sesion y manda al login.
            header('HTTP', TRUE, 410);
            exit();            
        }
        else
            redirect(base_url().APP_LOGIN);
    }

    /**
     * Refresca los datos de la session del user. Se usa despeus de actualzar datos que impactan directamente en el front. nombre. foto.
     */
    public function session_refresh()
    {
        $id_user    = $this->CI->session->userdata('id_user');
        $result     = $this->CI->app_users->get_user($id_user);
        $result['logged_in'] = 'TRUE';
        $result['token'] = generate_session_token($result['id_user'], $result['user_email']);
        unset($result['user_password']);
        $this->CI->session->set_userdata($result);
    }
}