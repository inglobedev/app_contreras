<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class App_uploader {

    private $CI = FALSE;
    private $FU = FALSE;
    private $_THUMB_PATH = FCPATH . 'uploads' . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR;
    private $_GENER_PATH = FCPATH . 'uploads' . DIRECTORY_SEPARATOR;


    function __construct() 
    {
        if ($this->CI == FALSE)
            $this->CI =&get_instance();

        // Tenemos una funcion necesaria para redimensionar imagenes.
        $this->CI->load->helper('images');
        
        include(APPPATH . 'libraries/fileuploader/class.fileuploader.php');

        if (!file_exists($this->_THUMB_PATH)) 
            mkdir($this->_THUMB_PATH, 0777, TRUE);
        

        if ($this->FU == FALSE)
            $this->FU = new FileUploader('upload', array(
                'uploadDir' => $this->_GENER_PATH,
                'thumbDir'  => $this->_THUMB_PATH,
                'title'     => unique_identifier()
            ));
    }

    public function upload()
    {
        $id_user = $this->CI->session->userdata('id_user');
        $_result = $this->FU->upload();

        $success    = array();
        $failed     = array();
        foreach ($_result['files'] as $_kfile => $_file) 
            $success[] = $_file;

        return array('success' => $success, 'failed' => $failed);
    }

    // Elimina todos los archivos con una fecha de expiracion definida y pasada.
    public function cleanup()
    {
        $cond['file_cleanup IS NOT NULL'] 	= NULL;
        $cond['file_cleanup<NOW()'] 		= NULL;
        $files_to_delete = $this->CI->libreria->get_files($cond, FALSE, 0, 1000);

        foreach ($files_to_delete as $kfile => $_file) 
        {
        	$this->CI->libreria->del_file($_file['id_file']);
        	@unlink($this->_GENER_PATH . $_file['file_name'] . '.' . $_file['file_ext']);
        	@unlink($this->_THUMB_PATH . $_file['file_name'] . '.' . $_file['file_ext']);
        }
    }

    // Elimina los archivos con determinada antiguedad. No aplica en bureau
    public function cleanup_older($minutes = 5)
    {
        $files_general  = glob($this->_GENER_PATH."*.*");
        $files_temporal = glob($this->_THUMB_PATH."*.*");
        $now   = time();

        $files = array_merge($files_temporal, $files_general);

        foreach ($files as $file) {
            if (is_file($file)) {
                if ($now - filemtime($file) >= 60 * $minutes * 1 * 1) { // dos minutos
                    chmod($file, 0666);
                    unlink($file);
                }
            }
        }
    }
}