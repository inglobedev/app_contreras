<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Layout {
    private $CI              = FALSE;
    public $menu_options     = TRUE;
    public $user_menu        = TRUE;
    public $notifications    = TRUE;
    public $breadcrumb       = FALSE;
    public $footer           = FALSE;
    public $messages         = FALSE;
    public $page_title       = FALSE;
    public $quick_nav        = FALSE;
    public $search_box       = FALSE;
    public $tasks            = FALSE;
    public $page_actions     = FALSE;

    function __construct() 
    {
        if ($this->CI == FALSE)
            $this->CI =&get_instance();
    }

    public function get_breadcrumb()
    {
        if ($this->breadcrumb === FALSE)
            return FALSE;

        $BC = array();

            $it['url']      = 'javascript:;';
            $it['title']    = 'Segmento 1';
            $BC[] = $it;

            $it['url']      = 'javascript:;';
            $it['title']    = 'Segmento 2';
            $BC[] = $it;

            $it['url']      = 'javascript:;';
            $it['title']    = 'Segmento 3';
            $BC[] = $it;            

        return $BC;
    }

    public function get_footer()
    {
        if ($this->footer === FALSE)
            return FALSE;

        return $this->footer;
    }

    public function get_menu_options()
    {
        if ($this->menu_options === FALSE)
            return FALSE;

        $MN = array();


        // OPCION RECOMENDADA!
            $mnu['url']         = base_url(APP_ALQUILERES);
            $mnu['icon']        = 'icon-list';
            $mnu['title']       = 'Alquileres';
            $mnu['submenu']     = array();
            if (check_permissions('app', 'mnu_rents')) 
            	$MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!


        // OPCION RECOMENDADA!
            $mnu['url']         = base_url(APP_MAINTENANCES);
            $mnu['icon']        = 'icon-briefcase';
            $mnu['title']       = ucfirst($this->CI->lang->line('entity_maintenances'));
            $mnu['submenu']     = array();
            if (check_permissions('app', 'mnu_maintenances')) 
            	$MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!


       // OPCION RECOMENDADA!
            $mnu['url']         = base_url(APP_IMPUESTOS.'taxes');
            $mnu['icon']        = 'icon-equalizer';
            $mnu['title']       = 'Impuestos';
            $mnu['submenu']     = array();
            if (check_permissions('app', 'mnu_taxes')) 
            	$MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!


        // OPCION RECOMENDADA!
            // $mnu['url']         = base_url(APP_IMPUESTOS.'results');
            // $mnu['icon']        = 'icon-reload';
            // $mnu['title']       = 'Resultados';
            // $mnu['submenu']     = array();
            // $MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!

        // OPCION RECOMENDADA!
            $mnu['url']         = base_url(APP_PROPIEDADES);
            $mnu['icon']        = 'icon-home';
            $mnu['title']       = 'Inmuebles';
            $mnu['submenu']     = array();
            if (check_permissions('app', 'mnu_properties')) 
            	$MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!

        // OPCION RECOMENDADA!
            $mnu['url']         = base_url(APP_CONTRACTS);
            $mnu['icon']        = 'icon-home';
            $mnu['title']       = 'Contratos';
            $mnu['submenu']     = array();
            if (check_permissions('app', 'mnu_contracts')) 
                $MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!


        // OPCION RECOMENDADA!
            $mnu['url']         = base_url(APP_BACKEND.'owners');
            $mnu['icon']        = 'icon-reload';
            $mnu['title']       = 'Propietarios';
            $mnu['submenu']     = array();
            if (check_permissions('app', 'mnu_owners')) 
            	$MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!


        // OPCION RECOMENDADA!
            $mnu['url']         = base_url(APP_ADMINISTRATION);
            $mnu['icon']        = 'icon-reload';
            $mnu['title']       = 'Administración';
            $mnu['submenu']     = array();
            if (check_permissions('app', 'mnu_administration')) 
            	$MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!

        // OPCION RECOMENDADA!
            // $mnu['url']         = base_url(APP_PROPIEDADES.'sales');
            // $mnu['icon']        = 'icon-reload';
            // $mnu['title']       = 'Ventas';
            // $mnu['submenu']     = array();
            // $MN[] = $mnu; unset($mnu);
        // OPCION RECOMENDADA!

        // OPCION RECOMENDADA!
            $mnu['url']         = 'javascript:;';
            $mnu['icon']        = 'icon-notebook';
            $mnu['title']       = 'Configuracion';
            $mnu['submenu']     = array();

                unset($smnu);
                $smnu['url']         = base_url(APP_BACKEND.'users');
                $smnu['icon']        = 'icon-user';
                $smnu['title']       = ucfirst($this->CI->lang->line('entity_users'));
                if (check_permissions('app', 'manage_users')) 
                    $mnu['submenu'][] = $smnu;


                unset($smnu);
                $smnu['url']         = base_url(APP_BACKEND.'tenants');
                $smnu['icon']        = 'icon-user';
                $smnu['title']       = ucfirst($this->CI->lang->line('entity_tenants'));
                if (check_permissions('app', 'manage_users')) 
                    $mnu['submenu'][] = $smnu;


                unset($smnu);
                $smnu['url']         = base_url(APP_BACKEND.'guarantors');
                $smnu['icon']        = 'icon-user';
                $smnu['title']       = ucfirst($this->CI->lang->line('entity_guarantors'));
                if (check_permissions('app', 'manage_users')) 
                    $mnu['submenu'][] = $smnu;



                unset($smnu);
                $smnu['url']         = base_url(APP_BACKEND.'professionals');
                $smnu['icon']        = 'icon-user';
                $smnu['title']       = ucfirst($this->CI->lang->line('entity_professionals'));
                if (check_permissions('app', 'manage_users')) 
                    $mnu['submenu'][] = $smnu;

                unset($smnu);
                $smnu['url']         = base_url(APP_BACKEND.'consortiums');
                $smnu['icon']        = 'icon-user';
                $smnu['title']       = ucfirst($this->CI->lang->line('entity_consortiums'));
                if (check_permissions('app', 'manage_users')) 
                    $mnu['submenu'][] = $smnu;

                unset($smnu);
                $smnu['url']         = base_url(APP_IMPUESTOS);
                $smnu['icon']        = 'icon-equalizer';
                $smnu['title']       = 'Gastos';
                if (check_permissions('app', 'manage_users')) 
                    $mnu['submenu'][] = $smnu;


                unset($smnu);
                $smnu['url']         = base_url(APP_IMPUESTOS.'type_expense');
                $smnu['icon']        = 'icon-graph';
                $smnu['title']       = 'Tipos de gastos';
                if (check_permissions('app', 'manage_users')) 
                    $mnu['submenu'][] = $smnu;

                unset($smnu);
                $smnu['url']         = base_url(APP_IMPUESTOS.'administration_expenses');
                $smnu['icon']        = 'icon-graph';
                $smnu['title']       = 'Gastos adminitrativos';
                if (check_permissions('app', 'manage_users')) 
                    $mnu['submenu'][] = $smnu;
                
                unset($smnu);
                $smnu['url']         = base_url(APP_BACKEND.'roles');
                $smnu['icon']        = 'icon-lock';
                $smnu['title']       = 'Roles';
                if (check_permissions('app', 'manage_roles')) 
                    $mnu['submenu'][] = $smnu;

                unset($smnu);
                $smnu['url']         = base_url(APP_BACKEND.'system');
                $smnu['icon']        = 'icon-equalizer';
                $smnu['title']       = 'Sistema';
                if (check_permissions('app', 'manage_system')) 
                    $mnu['submenu'][] = $smnu;

                // unset($smnu);
                // $smnu['url']         = base_url(APP_BACKEND.'tools');
                // $smnu['icon']        = 'icon-social-dropbox';
                // $smnu['title']       = 'Herramientas';
                // if (check_permissions('app', 'use_tools')) 
                //     $mnu['submenu'][] = $smnu;

            if (check_permissions('app', 'mnu_configuration')) 
            	$MN[] = $mnu; unset($mnu);$smnu; unset($smnu);
        // OPCION RECOMENDADA!
        
                
        return $MN;
    }

    public function get_messages()
    {
        if ($this->messages === FALSE)
            return FALSE;

        $MS['new_messages']    = 1;
        $MS['url_messages']    = 'javascript:;';
        $MS['messages'] = array();

            $msg['name']        = 'Juan Perez';
            $msg['image']       = '/uploads/app/avatars/avatar2.jpg';
            $msg['url']         = base_url();
            $msg['datetime']    = date('Y-m-d H:i:s');
            $msg['text']        = ' Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... ';
            $MS['messages'][]  = $msg;

        return $MS;
    }

    public function get_notifications()
    {
        if ($this->notifications === FALSE)
            return FALSE;

        $id_user        = $this->CI->session->userdata('id_user');
        $order['notif_readed']  = 'ASC';
        $order['notif_date']    = 'DESC';
        $notifications  = $this->CI->app_notifications->get_notifications('id_user', $id_user, 0, 10, $order);
        
        // Calculamos las sin leer.
        $unreaded['id_user']      = $id_user;
        $unreaded['notif_readed'] = 0;
        $unreaded = $this->CI->app_notifications->get_notifications($unreaded, FALSE, 0, 1);
        $unreaded = (isset($unreaded[0]['total_results'])) ? $unreaded[0]['total_results'] : 0;

        $NF['new_notifications']    = $unreaded;
        $NF['url_notifications']    = base_url(APP_NOTIFICATIONS_URL);
        $NF['notifications'] = $notifications;

        return $NF;
    }

    public function get_page_title()
    {
        if ($this->page_title === FALSE)
            return FALSE;

        return $this->page_title;
    }

    public function get_page_actions()
    {
        if ($this->page_actions === FALSE)
            return FALSE;

        $pa['current'] = 'Accion Seleccionada';
        $pa['actions'] = array();

            $act['title']    = 'Accion 1';
            $act['url']      = base_url();
            $act['icon']     = 'icon-user';
            $pa['actions'][] = $act;

            $act['title']    = 'Accion 2';
            $act['url']      = base_url();
            $act['icon']     = 'icon-user';
            $pa['actions'][] = $act;

        return $pa;
    }

    public function get_quick_nav()
    {
        if ($this->quick_nav === FALSE)
            return FALSE;

        $qn = array();

            $op['title']    = 'Opcion 1';
            $op['url']      = base_url();
            $op['icon']     = 'icon-user';
            $qn[] = $op;

            $op['title']    = 'Opcion 2';
            $op['url']      = base_url();
            $op['icon']     = 'icon-user';
            $qn[] = $op;

        return $qn;
    }

    public function get_search_box()
    {
        if ($this->search_box === FALSE)
            return FALSE;
        
        $sb['id']           = NULL; // form id
        $sb['class']        = NULL; // form class
        $sb['method']       = NULL; // form method
        $sb['action']       = NULL; // form action
        $sb['name']         = NULL; // field name
        $sb['placeholder']  = 'Buscar'; // placeholder

        return $sb;
    }

    // task_color: danger, success, info, warning, etc.
    // task_percent: 1-100.
    public function get_tasks()
    {
        if ($this->tasks === FALSE)
            return FALSE;

        $tk['unreaded_tasks']   = 1;
        $tk['url_tasks']        = 'javascript:;';
        $tk['tasks']            = array();

            $task['task_name']      = 'Tarea de ejemplo.';
            $task['task_percent']   = '60';
            $task['task_color']     = 'danger';
            $task['task_url']       = 'javascript:;';

        $tk['tasks'][] = $task;
        
        return $tk;
    }

    public function get_user_menu()
    {
        if ($this->user_menu === FALSE)
            return FALSE;

        $um['user_name']    = $this->CI->session->userdata('user_name');
        $um['user_picture'] = avatar(FALSE, $this->CI->session->userdata('user_picture'));
        $um['menu']         = array();

        // OPCION RECOMENDADA!
            $opt['name']      = 'Mi Cuenta';
            $opt['icon']      = 'icon-user';
            $opt['url']       = base_url(APP_BACKEND . 'account');
            $um['menu'][] = $opt;
            unset($opt);
        // OPCION RECOMENDADA!
        
        // OPCIONES CUSTOM
            #$opt['name']      = 'Nombre de item';
            #$opt['icon']      = 'icon-user';
            #$opt['url']       = 'javascript:;';
            #$um['menu'][] = $opt;
        // OPCIONES CUSTOM

        // OPCION RECOMENDADA!
            $opt['name']      = 'Salir';
            $opt['icon']      = 'icon-logout';
            $opt['url']       = 'javascript:app_logout();';
            $um['menu'][] = $opt;
            unset($opt);
        // OPCION RECOMENDADA!

        return $um;
    }

}

/* End of file layout_library.php */