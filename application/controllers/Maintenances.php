<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maintenances extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de inicio/dashboard.
     * 
     */
    public function index()
    {
        $this->layout->page_title[0] = ucfirst($this->lang->line('entity_maintenances'));
        $this->layout->page_title[1] = '
            <span class="pull-right" style="right: 15px; top: 15px; position: absolute;">
                <a class="btn btn-default btn-md btn_new_coupon" href="javascript:;" data-roles="'.ROLE_PROFESSIONAL.'" data-counterpart_name="Proveedor/Profesional">'.ucfirst($this->lang->line('lbl_payment')) . ' ' . $this->lang->line('lbl_provider').'</a>
            </span>
            ';

        if ($this->session->userdata('id_role') == ROLE_OWNER)
        	$this->layout->page_title[1] = NULL;

        show_page('maintenances');
    }

    public function view($id_maintenance)
    {   
        $data['maintenance'] = $this->pachi->get('vw_maintenances', array('id_maintenance' => $id_maintenance), FALSE);
        
        if(empty($data['maintenance'])) {
            show_404();
        }

        $this->layout->page_title[0] = ucfirst($this->lang->line('entity_maintenance'));
        $this->layout->page_title[1] = $data['maintenance']['maintenance_name'];

        show_page('maintenances/maintenance_editor', $data);
    }

}