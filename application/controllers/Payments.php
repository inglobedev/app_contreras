<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payments extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de inicio/dashboard.
     * 
     */
    public function receipt($id_receipt)
    {
        $this->layout->page_title = 'Recibo';
        
        $data['receipt'] = $this->pachi->get('receipts', $id_receipt);

        if (empty($data['receipt']))
        	redirect(APP_HOME);

    	// Obtenemos detalles
    	$_details_filter['id_receipt'] = $id_receipt;
    	$details = $this->pachi->fetch('vw_receipt_details', $_details_filter, 0, 1000, array('type_order' => 'ASC'));
    	
    	$data['details'] 	 = $details;
    	$data['receiver'] 	 = $this->pachi->get('users', $data['receipt']['id_user_receiver']);;

        show_page('payments/receipt', $data);
    }
}