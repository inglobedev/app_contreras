<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notifications extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();
    }

    /**
     * Tarea programada de despacho de notificaciones.
     * Trabaja en batchs de a 50.
     */
    public function index()
    {
        // Buscamos la sque estan en ESTADO 1. Es decir, disponibles para enviar pero no se han enviado.
        // Y que no esten leidas para no romperle los huevos al usuario.
        $cond['notif_mail']     = 1;
        $cond['notif_readed']   = 0;

        // Procesamos desde las mas atrasadas a las mas nuevas.
        $order['notif_date'] = 'ASC';

        $notifications = $this->app_notifications->get_notifications($cond, FALSE, 0, 50, $order);

        foreach ($notifications as $_knotification => $_notification) 
        {
            // Despachamos la notificacion
            $result = $this->app_mailing->notification($_notification);

            // Si salio OK, la marcamos como envida. ESTADO 2.
            
            $this->app_notifications->set_notification($_notification['id_notification'], 'notif_mail', 2);
        }
    }
	
}
