<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Core extends CI_Controller {

    public $PERIOD = FALSE;
    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->PERIOD = $this->_get_period();

        if (empty($this->PERIOD))
            exit('Hay un problema en la determinación del periodo.');
    }

    /*
     *  Chequeamos si hay periodo asignado para la fecha actual, de no haber lo creamos.
     *  RETORNO: id_periodo actual.
     */
    private function _get_period()
    {
        $_current_period = date('m-Y');

        $_period['period_code'] = $_current_period;
        $period = $this->pachi->fetch('_periods', $_period, -1, 1);

        if (empty($period)) 
        {
            $_period['period_year']     = date('Y');
            $_period['period_month']    = date('m');
            $id_period = $this->pachi->insert('_periods', $_period);
            return $this->PERIOD = $this->pachi->get('_periods', $id_period);
        }

        return $this->PERIOD = $period;
    }

    public function index()
    {
    	$this->generate_rent_charge_orders();
    	$this->generate_rent_liquidation_orders();
    	$this->generate_maintenance_charge_orders();
    	$this->generate_maintenance_liquidation_orders();
    	$this->generate_expenses_charge_orders();
    	$this->generate_administrative_interests();
        echo "Cron OK.";
    }

    /**
	 *	- Generador de conceptos:
	 *		- Alquileres (Sale de contracts + bonifications)
	 *			-- un concepto para el inquilino, paga inquilino, recibe administracion.
	 *			-- un concepto para el dueño, paga administracion, recibe dueño.
	 *
	 *		- Impuestos y servicios cobrables. (Sale de property_expenses)
	 *			-- un concepto para el inquilino, paga inquilino, recibe administracion.
	 *			-- un concepto para el servicio, paga administracion, recibe el servicio.
	 *
	 *		- Mantenimientos (sale de property_maintenances)
	 *			-- un concepto para el inquilino, paga inquilino, recibe administracion.
	 *			-- un concepto para el dueño, paga dueño, recibe administracion.
	 *			-- un concepto para el profesional, paga administracion recibe el profesional.
	 *			-- un concepto para 
     */
    
    public function generate_rent_charge_orders()
    {
        // Obtenemos todos los contratos vigentes, con su bonificacion actual.
        $_contracts_filter['_is_active'] = 1;
        $contracts = $this->pachi->fetch('vw_contracts', $_contracts_filter);

        foreach ($contracts as $_kcontract => $_contract) 
        {
            // Generamos un tag para el pago de este alquiler. Nos sirve para chequear si ya fue generadio previamente para el periodo indicado.
            $_unique_concept = mb_strtoupper("GET-RENT-{$_contract['id_contract']}");

            // Verificamos que el concepto no exista.
            $_receipt_detail_filter['id_period']        = $this->PERIOD['id_period'];
            $_receipt_detail_filter['detail_concept']   = $_unique_concept;
            $_prexistent_order = $this->pachi->fetch('receipt_details', $_receipt_detail_filter);

            if (empty($_prexistent_order)) 
            {
                // Calculamos la fecha de vencimiento para este pago.
                $_expire_date = date('Y-m-'.$_contract['contract_payday_limit']);
                $_interest_since = date('Y-m-'.$_contract['contract_payday_start']);

                if ($_contract['bonification_rate'] > 0)
                {
                    $_description   = 'Alquiler ' . $this->PERIOD['period_code'];
                    $_ammount       = $_contract['contract_rent_price'] - $_contract['bonification_rate'];
                }
                else
                {
                    $_description   = 'Alquiler ' . $this->PERIOD['period_code'];
                    $_ammount       = $_contract['contract_rent_price'];
                }

                $detail['id_period']            = $this->PERIOD['id_period'];
                $detail['id_property']  		= $_contract['id_property'];
                $detail['id_expense_type']      = ET_ALQUILER;
                $detail['detail_description']   = $_description;
                $detail['detail_info']  		= $_contract['property_name'];
                $detail['detail_ammount']       = $_ammount;
                $detail['detail_original_ammount'] = $detail['detail_ammount'];
                $detail['detail_custom']		= 0;
                $detail['id_issuing_account']   = $this->m_accounts->get_user_account($_contract['id_user_tenant']);
                $detail['id_receiving_account'] = ACCOUNT_RENTS;
                $detail['detail_concept']       = $_unique_concept;
                $detail['detail_expire_date']   = $_expire_date;
                $detail['detail_interest_since'] = $_interest_since;
                $detail['detail_interest_rate']  = $_contract['contract_interest'];

                $id_detail = $this->pachi->insert('receipt_details', $detail);
            }
        }
    }

    public function generate_rent_liquidation_orders()
    {
        // Obtenemos todos los contratos vigentes, con su bonificacion actual.
        $_contracts_filter['_is_active'] = 1;
        $contracts = $this->pachi->fetch('vw_contracts', $_contracts_filter);

        foreach ($contracts as $_kcontract => $_contract) 
        {
            // Generamos un tag para el pago de este alquiler. Nos sirve para chequear si ya fue generadio previamente para el periodo indicado.
            $_unique_concept = mb_strtoupper("LIQ-RENT-{$_contract['id_contract']}");
            $_unique_concept_adm = mb_strtoupper("LIQ-ADM-RENT-{$_contract['id_contract']}");

            // Si el contrato no tiene un propietario configurado, no se puede liquidar.
            if (empty($_contract['id_user_owner'])) {
            	// TODO: Disparar un alerta.
            	continue;
            }

            // Verificamos que el concepto no exista.
            $_receipt_detail_filter['id_period']        = $this->PERIOD['id_period'];
            $_receipt_detail_filter['detail_concept']   = $_unique_concept;
            $_prexistent_order = $this->pachi->fetch('receipt_details', $_receipt_detail_filter);

            if (empty($_prexistent_order)) 
            {
                // No hay fecha limite para pagos a propietarios.
                $_expire_date = NULL;

                if ($_contract['bonification_rate'] > 0)
                {
                    $_description   = 'Liquidación periodo ' . $this->PERIOD['period_code'] ;
                    $_ammount       = $_contract['contract_rent_price'] - $_contract['bonification_rate'];
                    $_comission 	= $_ammount * $_contract['ownership_administrative_rate'];
                }
                else
                {
                    $_description   = 'Liquidación periodo ' . $this->PERIOD['period_code'];
                    $_ammount       = $_contract['contract_rent_price'];
                    $_comission 	= $_ammount * $_contract['ownership_administrative_rate'];
                }

                $detail['id_period']            = $this->PERIOD['id_period'];
                $detail['id_property']  		= $_contract['id_property'];
                $detail['id_expense_type']      = ET_ALQUILER;
                $detail['detail_description']   = $_description;
                $detail['detail_info']			= $_contract['property_name'];
                $detail['detail_ammount']       = $_ammount;
                $detail['detail_original_ammount'] = $detail['detail_ammount'];
                $detail['detail_custom']		= 0;
                $detail['id_issuing_account']   = ACCOUNT_RENTS;
                $detail['id_receiving_account'] = $this->m_accounts->get_user_account($_contract['id_user_owner']);
                $detail['detail_concept']       = $_unique_concept;
                $detail['detail_expire_date']   = $_expire_date;
                $detail['detail_interest_rate'] = 0;

                $id_detail = $this->pachi->insert('receipt_details', $detail);

                // Insertamos la comision.
                if ($_comission > 0)
                {
	                $detail_comission['id_period']            = $this->PERIOD['id_period'];
	                $detail_comission['id_property']  		= $_contract['id_property'];
	                $detail_comission['id_expense_type']      = ET_ADMINISTRATIVO;
	                $detail_comission['detail_description']   = 'Gasto administrativo periodo ' . $this->PERIOD['period_code'];;
	                $detail_comission['detail_info']		  = $_contract['property_name'];
	                $detail_comission['detail_ammount']       = $_comission;
	                $detail_comission['detail_original_ammount'] 	= $detail_comission['detail_ammount'];
	                $detail_comission['detail_custom']				= 0;
	                $detail_comission['id_issuing_account']   = $this->m_accounts->get_user_account($_contract['id_user_owner']);
	                $detail_comission['id_receiving_account'] = ACCOUNT_ADMINISTRATION;
	                $detail_comission['detail_concept']       = $_unique_concept_adm;
	                $detail_comission['detail_expire_date']   = $_expire_date;
	                $detail_comission['detail_interest_rate'] = 0;

	                $id_detail = $this->pachi->insert('receipt_details', $detail_comission);
                }
            }
        }
    }
	
    public function generate_expenses_charge_orders()
    {
        // Obtenemos todos los gastos de propiedades que son cobrables..
        $_expenses_filter['(expense_chargeable OR expense_checkable)'] = NULL;
        $property_expenses = $this->pachi->fetch('vw_property_expenses', $_expenses_filter);

        // Generamos las ordenes de cobro para los locatarios o los dueños, dependiendo el estado del inmueble.
        foreach ($property_expenses as $_kexpense => $_expense) 
        {
        	unset($detail);

        	// Si no esta definido el dueño del inmueble, error.
            $_id_user_payer = (!empty($_expense['id_user_tenant'])) ? $_expense['id_user_tenant'] : $_expense['id_user_owner'];
            if (empty($_id_user_payer)) 
            {
            	// TODO: Algun error, no hay ni dueño ni locatario.	
            	continue;
            }

            $_unique_concept = mb_strtoupper("GET-EXPENSE-{$_expense['id_property_expense']}");
            $_unique_concept_pay = mb_strtoupper("PAY-EXPENSE-{$_expense['id_property_expense']}");

            // Verificamos que el concepto no exista.
            $_receipt_detail_filter['detail_concept']   = $_unique_concept;
            $_receipt_detail_filter['id_period']   		= $this->PERIOD['id_period'];
            $_prexistent_expense = $this->pachi->fetch('receipt_details', $_receipt_detail_filter);

            if (empty($_prexistent_expense)) 
            {
            	// Registramos el cobro del servicio/gasto
				$detail['id_period']            = $this->PERIOD['id_period'];
				$detail['id_property']  		= $_expense['id_property'];
				$detail['id_expense_type']      = $_expense['id_expense_type'];
                $detail['detail_description']   = $_expense['expense_name'];
                $detail['detail_info']   		= $_expense['property_name'] . ' - ' . $_expense['expense_identification'];
                $detail['detail_ammount']       = 0;
                $detail['detail_original_ammount'] = $detail['detail_ammount'];
                $detail['detail_custom']		= 0;
                $detail['id_issuing_account']   = $this->m_accounts->get_user_account($_id_user_payer);
                $detail['id_receiving_account'] = ACCOUNT_EXPENSES;
                $detail['detail_concept']       = $_unique_concept;
                $detail['detail_expire_date']   = NULL;
                $detail['detail_interest_rate'] = 0;

                $id_detail_payer = $this->pachi->insert('receipt_details', $detail);

                // Registramos la orden de pago para este servicio/gasto.
                unset($detail);
				$detail['id_period']            = $this->PERIOD['id_period'];
				$detail['id_property']  		= $_expense['id_property'];
				$detail['id_expense_type']      = $_expense['id_expense_type'];
				$detail['detail_id_detail_sibling'] = $id_detail_payer;
                $detail['detail_description']   = $_expense['expense_name'];
                $detail['detail_info']   		= $_expense['property_name'] . ' - ' . $_expense['expense_identification'];
                $detail['detail_ammount']       = 0;
                $detail['detail_original_ammount'] = $detail['detail_ammount'];
                $detail['detail_custom']		= 0;
                $detail['id_issuing_account']   = ACCOUNT_EXPENSES;
                $detail['id_receiving_account'] = ACCOUNT_PAYED;
                $detail['detail_concept']       = $_unique_concept_pay;
                $detail['detail_expire_date']   = NULL;
                $detail['detail_interest_rate'] = 0;

                $id_detail_payed = $this->pachi->insert('receipt_details', $detail);

                $this->pachi->set('receipt_details', $id_detail_payer, array('detail_id_detail_sibling' => $id_detail_payed));
            }
        }
    }

	public function generate_maintenance_charge_orders()
	{
        // Obtenemos todos los contratos vigentes, con su bonificacion actual.
        $_maintenance_filter['maintenance_status'] 		= 1; // 0=pending|1=approveed|2=rejected
        $_maintenance_filter['maintenance_completed'] 	= 1; // Manteniemientos completos.
        $maintenances = $this->pachi->fetch('vw_maintenances', $_maintenance_filter);

        // Para dueños
        foreach ($maintenances as $_kmaintenance => $_maintenance) 
        {
        	unset($detail);
            // Generamos un tag para el cobro de este mantenimiento al dueño. Nos sirve para chequear si ya fue generadio previamente para el periodo indicado.
            if ($_maintenance['maintenance_owner_cost'] != 0)
            {
            	// Si no esta definido el dueño del inmueble, error.
	            if (empty($_maintenance['id_user_owner'])) {
	            	// TODO: Disparar un alerta.
	            	continue;
	            }

	            $_unique_concept = mb_strtoupper("GET-OWNER-MAINTENANCE-{$_maintenance['id_maintenance']}");

	            // Verificamos que el concepto no exista.
	            $_receipt_detail_filter['detail_concept']   = $_unique_concept;
	            $_prexistent_maintenance = $this->pachi->fetch('receipt_details', $_receipt_detail_filter);

	            if (empty($_prexistent_maintenance)) 
	            {
					$detail['id_period']            = $this->PERIOD['id_period'];
					$detail['id_property']  		= $_maintenance['id_property'];
					$detail['id_expense_type']      = ET_MANTENIMIENTO;
	                $detail['detail_description']   = 'Mantenimiento: ' . $_maintenance['maintenance_name'];
	                $detail['detail_ammount']       = $_maintenance['maintenance_owner_cost'];
	                $detail['detail_original_ammount'] = $detail['detail_ammount'];
	                $detail['detail_custom']		= 0;
	                $detail['id_issuing_account']   = $this->m_accounts->get_user_account($_maintenance['id_user_owner']);
	                $detail['id_receiving_account'] = ACCOUNT_MAINTENANCES;
	                $detail['detail_concept']       = $_unique_concept;
	                $detail['detail_expire_date']   = NULL;
	                $detail['detail_interest_rate'] = 0;

	                $id_detail = $this->pachi->insert('receipt_details', $detail);
	            }
            }
        }
        // Para locatarios
        foreach ($maintenances as $_kmaintenance => $_maintenance) 
        {
        	unset($detail);
            // Generamos un tag para el cobro de este mantenimiento al dueño. Nos sirve para chequear si ya fue generadio previamente para el periodo indicado.
            if ($_maintenance['maintenance_tenant_cost'] != 0)
            {
            	// Si no esta definido el dueño del inmueble, error.
	            if (empty($_maintenance['id_user_tenant'])) {
	            	// TODO: Disparar un alerta.
	            	continue;
	            }

	            $_unique_concept = mb_strtoupper("GET-TENANT-MAINTENANCE-{$_maintenance['id_maintenance']}");

	            // Verificamos que el concepto no exista.
	            $_receipt_detail_filter['detail_concept']   = $_unique_concept;
	            $_prexistent_maintenance = $this->pachi->fetch('receipt_details', $_receipt_detail_filter);

	            if (empty($_prexistent_maintenance)) 
	            {
					$detail['id_period']            = $this->PERIOD['id_period'];
					$detail['id_property']  		= $_maintenance['id_property'];
					$detail['id_expense_type']      = ET_MANTENIMIENTO;
	                $detail['detail_description']   = 'Mantenimiento: ' . $_maintenance['maintenance_name'];
	                $detail['detail_ammount']          = $_maintenance['maintenance_tenant_cost'];
	                $detail['detail_original_ammount'] = $detail['detail_ammount'];
	                $detail['detail_custom']		= 0;
	                $detail['id_issuing_account']   = $this->m_accounts->get_user_account($_maintenance['id_user_tenant']);
	                $detail['id_receiving_account'] = ACCOUNT_MAINTENANCES;
	                $detail['detail_concept']       = $_unique_concept;
	                $detail['detail_expire_date']   = NULL;
	                $detail['detail_interest_rate'] = 0;

	                $id_detail = $this->pachi->insert('receipt_details', $detail);
	            }
            }
        }
	}

	public function generate_maintenance_liquidation_orders()
	{
        // Obtenemos todos los contratos vigentes, con su bonificacion actual.
        $_maintenance_filter['maintenance_status'] 		= 1;
        $_maintenance_filter['maintenance_completed'] 	= 1;
        $maintenances = $this->pachi->fetch('vw_maintenances', $_maintenance_filter);

        // Para la administracion
        foreach ($maintenances as $_kmaintenance => $_maintenance) 
        {
        	unset($detail);
            // Generamos un tag para el cobro de este mantenimiento al dueño. Nos sirve para chequear si ya fue generadio previamente para el periodo indicado.
            if ($_maintenance['maintenance_administrative_fee'] != 0)
            {
	            $_unique_concept = mb_strtoupper("LIQ-ADM-MAINTENANCE-{$_maintenance['id_maintenance']}");

	            // Verificamos que el concepto no exista.
	            $_receipt_detail_filter['detail_concept']   = $_unique_concept;
	            $_prexistent_maintenance = $this->pachi->fetch('receipt_details', $_receipt_detail_filter);

	            if (empty($_prexistent_maintenance)) 
	            {
					$detail['id_period']            = $this->PERIOD['id_period'];
					$detail['id_property']  		= $_maintenance['id_property'];
					$detail['id_expense_type']      = ET_MANTENIMIENTO;
	                $detail['detail_description']   = 'Hon. Adm. Mantenimiento: ' . $_maintenance['maintenance_name'];
	                $detail['detail_ammount']       = $_maintenance['maintenance_administrative_fee'];
	                $detail['detail_original_ammount'] = $detail['detail_ammount'];
	                $detail['detail_custom']		= 0;
	                $detail['id_issuing_account']   = ACCOUNT_MAINTENANCES;
	                $detail['id_receiving_account'] = ACCOUNT_ADMINISTRATION;
	                $detail['detail_concept']       = $_unique_concept;
	                $detail['detail_expire_date']   = NULL;
	                $detail['detail_interest_rate'] = 0;

	                $id_detail = $this->pachi->insert('receipt_details', $detail);
	            }
            }
        }

        // Para el profesional
        foreach ($maintenances as $_kmaintenance => $_maintenance) 
        {
        	unset($detail);
            // Generamos un tag para el cobro de este mantenimiento al dueño. Nos sirve para chequear si ya fue generadio previamente para el periodo indicado.
            $_unique_concept = mb_strtoupper("LIQ-PROFESSIONAL-MAINTENANCE-{$_maintenance['id_maintenance']}");

            // Verificamos que el concepto no exista.
            $_receipt_detail_filter['detail_concept']   = $_unique_concept;
            $_prexistent_maintenance = $this->pachi->fetch('receipt_details', $_receipt_detail_filter);

            if (empty($_prexistent_maintenance)) 
            {
				$detail['id_period']            = $this->PERIOD['id_period'];
				$detail['id_expense_type']      = ET_MANTENIMIENTO;
                $detail['detail_description']   = 'Hon. Mantenimiento: ' . $_maintenance['maintenance_name'];
                $detail['detail_ammount']       = $_maintenance['quote_price'];
                $detail['detail_original_ammount'] = $detail['detail_ammount'];
                $detail['detail_custom']		= 0;
                $detail['id_issuing_account']   = ACCOUNT_MAINTENANCES;
                $detail['id_receiving_account'] = $this->m_accounts->get_user_account($_maintenance['id_user_professional']);
                $detail['detail_concept']       = $_unique_concept;
                $detail['detail_expire_date']   = NULL;
                $detail['detail_interest_rate'] = 0;

                $id_detail = $this->pachi->insert('receipt_details', $detail);
            }
        }
	}

	public function generate_administrative_interests()
	{
		// Buscar conceptos a pagar vencidos, no pagados.
        $_details_filter['_payed'] 					= 0;
        $_details_filter['detail_interest_rate>'] 	= 0;
        $_details_filter['detail_expire_date<'] 	= date('Y-m-d');
        //$_details_filter[''] 	= 0;
        
        $details = $this->pachi->fetch('vw_receipt_details', $_details_filter);

        // Para la administracion
        foreach ($details as $_kdetail => $_detail) 
        {
            $_unique_concept = mb_strtoupper("INT-{$_detail['id_receipt_detail']}");

            // Verificamos que el concepto no exista.
            $_receipt_detail_filter['detail_concept']   	= $_unique_concept;
            $_prexistent_interest = $this->pachi->fetch('receipt_details', $_receipt_detail_filter, -1, 1);
        	
            $_days_delayed 		= get_days_between($_detail['detail_interest_since'], date('Y-m-d'));
            $_interest_ammount 	= $_days_delayed * ($_detail['detail_ammount'] * $_detail['detail_interest_rate']);
            $_description 		= "Intereses por {$_days_delayed} días de atraso.";
			$_info	   			= $_detail['detail_description'];

			$detail['id_period']            = $_detail['id_period'];
			$detail['id_property']          = $_detail['id_property'];
			$detail['id_expense_type']      = ET_INTERES;
            $detail['detail_description']   = $_description;
            $detail['detail_info']   		= $_info;
            $detail['detail_ammount']       = $_interest_ammount;
            $detail['detail_original_ammount'] = $detail['detail_ammount'];
            $detail['detail_custom']		= 0;
            $detail['id_issuing_account']   = $_detail['id_issuing_account'];
            $detail['id_receiving_account'] = $_detail['id_receiving_account'];
            $detail['detail_concept']       = $_unique_concept;
            $detail['detail_expire_date']   = NULL;
            $detail['detail_interest_rate'] = 0;

            if (empty($_prexistent_interest)) 
                $id_detail = $this->pachi->insert('receipt_details', $detail);
            else
            	$this->pachi->set('receipt_details', $_prexistent_interest['id_receipt_detail'], $detail);
        }
	}
}
