<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Rents extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de inicio/dashboard.
     * 
     */
    public function index()
    {
        if (!check_permissions('app', 'mnu_rents')) 
            redirect(APP_PROPIEDADES);

        // Levantamos variables GET
        $action     = $this->input->get('action');
        $id_role    = $this->input->get('id');
        $page       = pag_current_page();

        switch ($action) {
            case 'edit':
                $data['permissions']    = $this->app_permissions->get_role_permissions($id_role);
                $data['role']           = $this->app_users->get_role($id_role);
                show_page('rents/editar_contrato', $data);
                break;
            case 'new':
                show_page('rents/nuevo_contrato', FALSE);
                break;
            default:


                $this->layout->page_title[0] = ucfirst($this->lang->line('lbl_rents'));
                $this->layout->page_title[1] = '
                <span class="pull-right" style="right: 15px; top: 15px; position: absolute;">
	                <a class="btn btn-default btn-md btn_new_coupon" data-roles="'.ROLE_TENANT.'" data-counterpart_name="'.ucfirst($this->lang->line('entity_tenant')).'" href="javascript:;">'.ucfirst($this->lang->line('lbl_collect_rent')) . '</a>
	                <a class="btn btn-default btn-md" href="/'.APP_ALQUILERES.'contract">'.ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_contract').'</a>
	                <a class="btn btn-default btn-md" href="/'.APP_ALQUILERES.'contract">'.ucfirst($this->lang->line('lbl_see_collection_history')) . '</a>
                </span>';
                
                show_page('rents');
                break;
        }
    }

        /**
     * Pagina de gestion de contrato.
     * 
     */
    public function contract()
    {

        $this->layout->page_title =ucfirst($this->lang->line('entity_contract'));
        show_page('rents/contract');

    }

}