<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * handlers para llamados de cuestiones generales a al app base. Por ejemplo gestion de cuentas, roles, etc.
 * 
 */
class Ajax_payments extends CI_Controller {

    /**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();

        $this->baseapp->session_check();
    }


    public function get_coupon_generator()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

    	$coupon_title 		= $this->input->post('coupon_title');
    	$counterpart_name 	= $this->input->post('counterpart_name');
    	$id_user_receiver 	= $this->input->post('id_user_receiver');
        $roles              = $this->input->post('roles');
    	

    	
    	// Se podria especificar un csv id de detail, para generar un cupon exclsivo.
    	$filter_details 		= $this->input->post('filter_details');

    	$data['coupon_title'] 		= (!empty($coupon_title)) ? $coupon_title : 'Nuevo cupón';
    	$data['counterpart_name'] 	= (!empty($counterpart_name)) ? $counterpart_name : 'Contraparte';
    	$data['id_user_receiver'] 	= $id_user_receiver;
    	$data['roles'] 				= $roles;
    	$data['filter_details'] 	= $filter_details;
    	$data['filtered']    		= !empty($filter_details);

        $ext['trigger_modal'] = '#modal_coupon';
        $ext['modal'] = $this->load->view('pages/payments/components/modal_coupon', $data ,TRUE);
        ajax_response('success', 'ok', '1', '0', $ext);

    }

    public function get_detail_generator()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

    	// Contraparte del concepto.
        $id_user = $this->input->post('id_user');
    	$counterpart = $this->input->post('counterpart');

        $data['id_user'] = $id_user;
    	$data['calltype'] = $counterpart;

        $ext['trigger_modal'] = '#modal_detail';
        $ext['modal'] = $this->load->view('pages/payments/components/modal_detail', $data ,TRUE);
        ajax_response('success', 'ok', '1', '0', $ext);
    }

    public function load_details()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

    	$id_user 		= $this->input->post('id_user'); // <- Contraparte
    	$filter_details = $this->input->post('filter_details'); // <- Contraparte
        $calltype = $this->input->post('calltype'); // <- Contraparte


    	if (empty($id_user))
    		ajax_response('danger', 'Ocurrió un problema cargando los conceptos de la contraparte.', '0', '1');

    	$id_account = $this->m_accounts->get_user_account($id_user);

    	if (empty($id_account))
    		ajax_response('danger', 'Ocurrió un problema cargando los conceptos de la contraparte.', '0', '1');

    	// Obtenemos detalles
    	$_details_filter["(id_issuing_account = $id_account OR id_receiving_account = $id_account)"] = NULL;
    	$_details_filter['_payed'] = 0;

    	if (!empty($filter_details))
    		$_details_filter["id_receipt_detail IN ($filter_details)"] = NULL;


    	$details = $this->pachi->fetch('vw_receipt_details', $_details_filter, 0, 1000, array('type_order' => 'ASC'));
    	
    	// Obtenemos datos de contratox existentes para esta persona.
        $_contracts_filter['id_user_tenant'] = $id_user;
        $contracts          = $this->pachi->fetch('vw_contracts', $_contracts_filter, 0, 100);

    	// Datos para el modal
    	$data['details'] 	 = $details;
    	$data['id_user'] 	 = $id_user;
        $data['id_account']  = $id_account;
        $data['contracts']   = $contracts;
    	$data['calltype']   = $calltype;
    	$data['filtered']    = !empty($filter_details);

    	$update['#table-coupon-details'] = $this->load->view('pages/payments/components/coupon_details', $data ,TRUE);
    	$ext['allow_submit'] 			 = 1;

    	ajax_response('success', 'ok', '1', '0', $ext, $update);
    }

    public function load_detail_sibling_suggestion()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

        $id_issuing_account     = $this->input->post('id_issuing_account');
        $id_receiving_account   = $this->input->post('id_receiving_account');
        $issuing_account      	= $this->pachi->get('accounts', $id_issuing_account);

        // Verificamos si la cuenta que recibe este movimiento es de administracion o de un tercero.
        // Si es de administración, puede que al dinero lo tengamos que liquidar, entonces sugerminos liquidarlo.
        // Si es de un tercero, puede que tengmos que cobrarle a otra persona.

        if (empty($id_receiving_account) OR empty($id_issuing_account))
        {
            $ext['allow_submit']            = 0;
            $update['#sibling_detail']      = ' ';
            ajax_response('success', 'ok', '1', '0', $ext, $update);
        }

        $_table                     = 'vw_accounts'; // Tabla o vista en la que trabaja
        $_column_to_save            = 'id_account'; // nombre de la columna a guardar el valor
        $_column_to_display         = '_account_description'; // nombre de la columna a mostrar
        $_fixed_filters             = array("id_user_owner IS NOT NULL OR id_account = " . ACCOUNT_PAYED => NULL); // filtros.
        $_search_key_accounts       = pachi_search($_table, $_column_to_save, $_column_to_display, $_fixed_filters);

        // La cuenta que recibe el dinero pertenece a la administracion, posiblemente se liquidara a otra cuenta.
        if ($issuing_account['id_user_owner'] == NULL)
        {
            $data['_search_key_accounts']   = $_search_key_accounts;
            $data['operation_type_label']   = 'Se cobrara a cuenta';
            $data['operation_type']         = 'chargue';
            $update['#sibling_detail']      = $this->load->view('pages/payments/components/detail_sibling_suggestion', $data ,TRUE);
            $ext['allow_submit']            = 1;

            ajax_response('success', 'ok', '1', '0', $ext, $update);
        }
        // La cuenta que recibe no pertenece a la administracion, entonces se podria tener que cobrar a alguien luego..
        else
        {
            $data['_search_key_accounts']   = $_search_key_accounts;
            $data['operation_type_label']   = 'Se liquida a cuenta';
            $data['operation_type']         = 'liquidation';
            $update['#sibling_detail']      = $this->load->view('pages/payments/components/detail_sibling_suggestion', $data ,TRUE);
            $ext['allow_submit']            = 1;

            ajax_response('success', 'ok', '1', '0', $ext, $update);
        }

        debugger($_POST);
    }

    public function update_receipt_detail_check()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

    	$id_receipt_detail	= $this->input->post('id_receipt_detail');
		$checked 	= ($this->input->post('checked') == 'true') ? 1 : 0;

	    $r = $this->pachi->set('receipt_details', $id_receipt_detail, array('_checked' => $checked));
    }

    public function update_receipt_detail_ammount()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

        $name   = $this->input->post('name');
        $value  = str_replace(',', '', $this->input->post('value'));
        $pk     = $this->input->post('pk');

        $id_receipt_detail  = $this->input->post('id_receipt_detail');
        $checked    = ($this->input->post('checked') == 'true') ? 1 : 0;

        $data['detail_ammount']             = $value;
        $data['detail_id_user_modifier']    = $this->session->userdata('id_user');

        $r = $this->pachi->set('receipt_details', $pk, $data);

        // Actualizamos los detalles siblings si es que tiene.
        $filter['detail_id_detail_sibling'] = $pk;
        $siblings = $this->pachi->fetch('vw_receipt_details', $filter, 0, 100);

        foreach ($siblings as $_kdetail => $_detail) {
        	if ($_detail['_payed']) {
        		// Si ya esta pagado, a quien le imputamos la diferencia?
        		// TODO: Imputar diferencia.
        	}
        	else
        		$r = $this->pachi->set('receipt_details', $_detail['id_receipt_detail'], array('detail_ammount' => $value));
        }
    }

    public function create_receipt_detail()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

		$detail['id_expense_type'] 		    = $this->input->post('id_expense_type');
		$detail['id_period'] 			    = $this->input->post('id_period');
		$detail['detail_description'] 	    = $this->input->post('detail_description');
		$detail['detail_info'] 			    = 'Generado por '.get_user_name($this->session->userdata('id_user'));
		$detail['detail_ammount'] 		    = str_replace(',', '', $this->input->post('detail_ammount'));
		$detail['id_issuing_account'] 	    = $this->input->post('id_issuing_account');
		$detail['id_receiving_account']     = $this->input->post('id_receiving_account');
		$detail['detail_concept'] 		    = 'CUSTOM-'.date('YmdHis');
		$detail['detail_observations'] 		= $this->input->post('detail_observations');
		$detail['detail_expire_date'] 		= (empty($this->input->post('detail_expire_date'))) ? NULL : $this->input->post('detail_expire_date');
		$detail['detail_interest_since'] 	= (!empty($this->input->post('detail_interest_rate'))) ? $detail['detail_expire_date'] : NULL;
		$detail['detail_interest_rate']  	= $this->input->post('detail_interest_rate');
		$detail['detail_original_ammount']  = str_replace(',', '', $this->input->post('detail_ammount'));
		$detail['detail_custom']  			= 1;
		$detail['detail_id_user_modifier']  = $this->session->userdata('id_user');
		$detail['_checked']  				= 1;
		
        // En caso que haya siblings
        $has_sibling                        = $this->input->post('has_sibling');
        $id_sibling_account                 = $this->input->post('id_sibling_account');
        $operation                          = $this->input->post('operation');

        if ($has_sibling AND empty($id_sibling_account)) 
        {
        	ajax_response('danger', 'No se especifico el "se cobra a cuenta" / "se liquida a cuenta".', '1', '1');
        }

	    $id_detail = $this->pachi->insert('receipt_details', $detail);
		
		if ($id_detail != FALSE) 
        {
            if ($has_sibling)
            {
                $siblings['id_expense_type']          = $detail['id_expense_type'];
                $siblings['id_period']                = $detail['id_period'];
                $siblings['detail_description']       = $detail['detail_description'];
                $siblings['detail_info']              = $detail['detail_info'];
                $siblings['detail_ammount']           = $detail['detail_ammount'];
                $siblings['detail_concept']           = 'CUSTOM-' . mb_strtoupper($operation) . '-' . date('YmdHis');
                $siblings['detail_observations']      = $detail['detail_observations'];
                $siblings['detail_original_ammount']  = $detail['detail_ammount'];
                $siblings['detail_custom']            = 1;
                $siblings['detail_id_user_modifier']  = $this->session->userdata('id_user');
                $siblings['_checked']                 = 1;
                $siblings['detail_id_detail_sibling'] = $id_detail;

                if ($operation == 'liquidation') {
                    $siblings['id_issuing_account']       = $detail['id_receiving_account'];
                    $siblings['id_receiving_account']     = $id_sibling_account;
                }
                else {
                    $siblings['id_issuing_account']       = $id_sibling_account;
                    $siblings['id_receiving_account']     = $detail['id_issuing_account'];
                }

                $id_sibling_detail = $this->pachi->insert('receipt_details', $siblings);

                $this->pachi->set('receipt_details', $id_detail, array('detail_id_detail_sibling' => $id_sibling_detail));
            }

        	$ext['action'] = 'dismiss_modal';
        	$ext['target'] = '#modal_detail';
        	$ext['delay']  = '500';
        	ajax_response('success', 'ok', '1', '0', $ext);
		}
        
        ajax_response('danger', 'Error', '1', '0');
    }

    public function delete_receipt_detail()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

    	$id_receipt_detail = $this->input->post('id_receipt_detail');
	    
	    $r = $this->pachi->del('receipt_details', array('id_receipt_detail' => $id_receipt_detail));

        ajax_response('success', 'ok', '1', '0');
    }

    public function confirm_coupon()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

    	// Crear un receipt
		$receipt['receipt_date']			= date('Y-m-d');
		$receipt['id_user_issuer']			= $this->input->post('id_user_issuer');;
		$receipt['id_user_receiver']		= $this->input->post('id_user_receiver');;
		$receipt['receipt_payed']			= 1;
		$receipt['receipt_pay_date']		= date('Y-m-d H:i:s');
		$receipt['receipt_voided']			= 0;
		$receipt['receipt_attachments']		= $this->input->post('receipt_attachments');;
		$receipt['receipt_observations']	= $this->input->post('receipt_observations');
		$receipt['receipt_description']		= $this->input->post('receipt_description');
    	
    	$id_receipt = $this->pachi->insert('receipts', $receipt);

    	if (count($this->pachi->errors))
    		ajax_response('danger', $this->pachi->errors[0], '0', '1');

    	// Asociar los items provistos al receipt.
    	$receipt_details = $this->input->post('receipt_detail');
    	if (is_array($receipt_details) AND count($receipt_details)) {
    		foreach ($receipt_details as $_id_receipt_detail => $_selected) {
    			if ($_selected) {
    				// Asociamos el cupon.
    				$this->pachi->set('receipt_details', $_id_receipt_detail, array('id_receipt' => $id_receipt));
					$_detail = $this->pachi->get('receipt_details', $_id_receipt_detail);

    				// Generamos transacciones.
    				// Originante
    				unset($transaction_issuer);
    				$transaction_issuer['id_account']			= $_detail['id_issuing_account'];
    				$transaction_issuer['transaction_ammount']	= $_detail['detail_ammount'] * -1;
    				$transaction_issuer['id_receipt_detail']	= $_id_receipt_detail;
    				$transaction_issuer['transaction_id']		= md5(json_encode($transaction_issuer));
    				$this->pachi->insert('account_transactions', $transaction_issuer);
    				
    				// Destinatario
    				unset($transaction_receiver);
    				$transaction_receiver['id_account']				= $_detail['id_receiving_account'];
    				$transaction_receiver['transaction_ammount']	= $_detail['detail_ammount'];
    				$transaction_receiver['id_receipt_detail']		= $_id_receipt_detail;
    				$transaction_receiver['transaction_id']			= md5(json_encode($transaction_receiver));
    				$this->pachi->insert('account_transactions', $transaction_receiver);
    			}
    		}

        	$ext['id_receipt'] = $id_receipt;
        	$ext['action'] = 'dismiss_modal';
        	$ext['target'] = '#modal_coupon';
        	$ext['delay']  = '10';
    		ajax_response('success', 'Se registro el pago del cupon', '1', '0', $ext);
    	}
    	else
    		ajax_response('danger', 'No se puede generar el cupón porque no hay detalles a incluir.', '0', '1');
    }

    public function dummy() 
    {
    }

    public function get_cashout_modal()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

        $ext['trigger_modal'] = '#modal_cashout';
        $ext['modal'] = $this->load->view('pages/payments/components/modal_cashout', FALSE ,TRUE);

        ajax_response('success', 'ok', '1', '0', $ext);
    }

    public function get_payment_modal()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

        $ext['trigger_modal'] = '#modal_payment';
        $ext['modal'] = $this->load->view('pages/payments/components/modal_payment', FALSE ,TRUE);

        ajax_response('success', 'ok', '1', '0', $ext);
    }

    public function cashout()
    {
    	check_permissions('app', 'handle_payments', FALSE, TRUE);

		$id_account	= $this->input->post('id_account');
		$ammount	= str_replace(',', '', $this->input->post('ammount'));

		$transaction['id_account'] 					= $id_account;
		$transaction['transaction_ammount'] 		= $ammount * -1;
		$transaction['id_user'] 					= $this->session->userdata('id_user');
		$transaction['transaction_observations'] 	= 'Extracción';
		$transaction['transaction_id'] 				= md5(json_encode($transaction));
		$transaction['transaction_type'] 			= 1;

		if ($ammount == 0 OR empty($ammount)) 
			ajax_response('danger', 'Debe especificar un importe.', '0', '1');
		
		if (empty($id_account))
			ajax_response('danger', 'Debe especificar una caja.', '0', '1');

		// Generamos una nueva transaccion
		$r = $this->pachi->insert('account_transactions', $transaction);

		if ($r) {
	    	$ext['action'] = 'dismiss_modal';
	    	$ext['target'] = '#modal_cashout';
	    	$ext['delay']  = '500';
	    	ajax_response('success', 'ok', '1', '0', $ext);
		}

		ajax_response('danger', 'Error', '1', '0');
    }

    public function payment()
	{
		check_permissions('app', 'handle_payments', FALSE, TRUE);

		$id_account		= $this->input->post('id_account');
		$observations	= $this->input->post('observations');
		$ammount	= str_replace(',', '', $this->input->post('ammount'));

		$transaction['id_account'] 					= $id_account;
		$transaction['transaction_ammount'] 		= $ammount * -1;
		$transaction['id_user'] 					= $this->session->userdata('id_user');
		$transaction['transaction_observations'] 	= $observations;
		$transaction['transaction_id'] 				= md5(json_encode($transaction));
		$transaction['transaction_type'] 			= 2;

		if ($ammount == 0 OR empty($ammount)) 
			ajax_response('danger', 'Debe especificar un importe.', '0', '1');
		
		if (empty($id_account))
			ajax_response('danger', 'Debe especificar una caja.', '0', '1');

		if (empty($observations))
			ajax_response('danger', 'Debe especificar un concepto.', '0', '1');

		// Generamos una nueva transaccion
		$r = $this->pachi->insert('account_transactions', $transaction);

		if ($r) {
			$ext['action'] = 'dismiss_modal';
			$ext['target'] = '#modal_payment';
			$ext['delay']  = '500';
			ajax_response('success', 'ok', '1', '0', $ext);
		}

		ajax_response('danger', 'Error', '1', '0');
	}

    public function convert_to_letters()
    {
        $number = str_replace('.', ',', $this->input->post('number'));

        $ext['string'] = mb_strtoupper($this->cifrasenletras->convertirNumeroEnLetras($number, 2));
        ajax_response('danger', 'string', '1', '0', $ext);
    }
}
