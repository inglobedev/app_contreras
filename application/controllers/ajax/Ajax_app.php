<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * handlers para llamados de cuestiones generales a al app base. Por ejemplo gestion de cuentas, roles, etc.
 * 
 */
class Ajax_app extends CI_Controller {

    /**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Usuarios
     * 
     */
        /**
         * Crear cuenta
         * 
         */
        public function create_account()
        {
            $id_role        = $this->input->post('id_role');
            $user_name      = $this->input->post('user_name');
            $user_address   = $this->input->post('user_address');
            $user_email     = $this->input->post('user_email');
            $user_cuit      = $this->input->post('user_cuit');
            $user_phone     = $this->input->post('user_phone');
            $user_pin       = $this->input->post('user_pin');
            $generar_clave  = $this->input->post('generar_clave');
            
            $user_password      = $this->input->post('user_password');
            $user_r_password    = $this->input->post('user_r_password');

            // Controlamos que el usuario tenga permiso de gestionar usuarios
            check_permissions('app', 'manage_users', FALSE, TRUE);

            // Que el email no este usado.
            if ($this->app_users->chk_email($user_email) != FALSE)
                ajax_response('danger', 'La dirección de email ya esta utilizada.', '0', '1');

            if (!$generar_clave AND $user_password != $user_r_password) {
                ajax_response('danger', 'Verifique la clave establecida.', '0', '1');
            }
            
            if (empty($user_password)) $user_password = generate_string(10);

            $id_user = $this->app_users->new_user($user_name,$user_address, $user_email, $user_cuit, md5($user_password), $id_role);

            // Reponse
            if ($id_user !== FALSE) {
                
                if ($generar_clave) {
                    $tmp_clave = $this->app_users->gen_temporal_password($id_user);
                    $this->load->model('app_mailing');
                    $this->app_mailing->welcome($id_user, $tmp_clave);
                }

                $this->app_users->set_user($id_user, 'user_pin', $user_pin);
                $this->app_users->set_user($id_user, 'user_phone', $user_phone);

                $ext['action'] = 'redirect';
                $ext['delay']  = '500';            
                $ext['target'] = base_url(APP_BACKEND.'users?action=edit&id='.$id_user);            
                ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

        /**
         * Actualizar los datos del usuario
         * 
         */
        public function update_account()
        {
            $id_user        = $this->input->post('id_user');
            $id_role        = $this->input->post('id_role');
            $user_name      = $this->input->post('user_name');
            $user_address   = $this->input->post('user_address');
            $user_email     = $this->input->post('user_email');
            $user_cuit      = $this->input->post('user_cuit');
            $user_phone     = $this->input->post('user_phone');
            $user_pin       = $this->input->post('user_pin');

            // Controlamos que el usuario tenga permiso de gestionar usuarios
            if ($this->session->userdata('id_user') !== $id_user)
                check_permissions('app', 'manage_users', FALSE, TRUE);

            // Que el email no este usado.
            if ($this->app_users->chk_email($user_email, $id_user) != FALSE)
                ajax_response('danger', 'La dirección de email ya esta utilizada.', '0', '1');

            // Realizamos el update.
            $update['user_name']    = $user_name;
            $update['user_address'] = $user_address;
            $update['user_email']   = $user_email;
            $update['user_cuit']    = $user_cuit;
            $update['user_phone']   = $user_phone;
            $update['user_pin']     = $user_pin;
            
            if ($id_user != $this->session->userdata('id_user')) 
                $update['id_role']      = $id_role;
            
            $result = $this->app_users->set_user($id_user, $update);

            // Reponse
            if ($result !== FALSE) {
                $this->baseapp->session_refresh($id_user);
                $ext['action']   = 'reload';
                $ext['delay']    = '1000';            
                ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }
        
        /**
         * Actualizar clave de usuario
         * 
         */
        public function update_password()
        {
            $id_user            = $this->input->post('id_user');
            $user_old_password  = $this->input->post('old_password');
            $user_password      = $this->input->post('new_password');
            $user_r_password    = $this->input->post('new_r_password');

            if ($user_password != $user_r_password)
                ajax_response('danger', 'Verifique la clave, la confirmación no coincide.', '0', '1');

            // Controlamos que el usuario tenga permiso de gestionar usuarios
            if ($this->session->userdata('id_user') != $id_user)
                check_permissions('app', 'manage_users', FALSE, TRUE);
            else {
                $user = $this->app_users->get_user($id_user);
                if (md5($user_old_password) != $user['user_password'] AND md5(md5($user_old_password).date('Y-m-d')) != $user['user_password_restore'])
                    ajax_response('danger', 'Su clave actual es incorrecta.', '0', '1');
            }

            $result = $this->app_users->set_user($id_user, 'user_password', md5($user_password));

            if ($result == TRUE) {
                // Reseteamos claves temporales.
                $this->session->set_userdata('tmp_password', FALSE);
                $this->app_users->set_user($id_user, 'user_password_restore', NULL);
                // Cerramos
                $ext['action']  = 'reload';
                $ext['delay']   = '1000';
                ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

        /**
         * Actualizar foto de usuario
         * 
         */
        public function update_avatar()
        {
            $id_user        = $this->input->post('id_user');
            $new_picture    = 'avatar';

            if ($this->session->userdata('id_user') != $id_user)
                check_permissions('app', 'manage_users', FALSE, TRUE);

            // Si recibimos un archivo, seguramente es porque hay que actualizar la foto de perfil. Y no cropear la existente.
            if (isset($_FILES[$new_picture]))
            {
                $this->load->model('app_storage');
                $upload_result = $this->app_storage->upload_picture($new_picture, APP_UPLOADS_FOLDER.'avatars/', 4096, TRUE);
                if ($upload_result !== FALSE)
                {
                    // Ajustamos el tamaño de la imagen subida, y la conservamos como original.
                    $this->app_storage->resize_picture(APP_UPLOADS_FOLDER.'avatars/'.$upload_result['file_name'], FALSE, 500, 500, FALSE);

                    // Eliminamos la fotografia de perfil anterior.
                    $this->app_users->del_profile_picture($id_user);
                    
                    // Fijamos el nombre de la nueva imagen.
                    $result = $this->app_users->set_user_profile_picture($id_user, $upload_result['file_name']);
                    
                    if ($result == TRUE)
                    {
                        if ($this->session->userdata('id_user') == $id_user)
                            $this->baseapp->session_refresh($id_user);
                        
                        $ext['action']   = 'reload';
                        $ext['delay']    = '1000';
                        ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
                    }
                    else
                        ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
                }
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

        /**
         * Actualizar configuracion de notificaciones
         * 
         */
        public function update_notification_settings()
        {
            $notifications = $this->input->post();

            foreach ($notifications as $group_name => $value) 
                $this->app_settings->save($group_name, $value);

            ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1');
        }


        /**
         * Desactivar usuario
         * 
         */
        public function disable_user()
        {
            $id_user = $this->input->post('id_user');

            if ($id_user == $this->session->userdata('id_user')) 
                ajax_response('danger', 'No puedes suicidate.', '0', '1');

            // Realizamos el update.
            $result = $this->app_users->set_user($id_user, 'user_deletion_date', date('Y-m-d H:i:s'));

            if ($result == TRUE)
            {
                $ext['action']          = 'reload';
                $ext['delay']           = '1000';
                ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

        /**
         * Reactivar usuario
         * 
         */
        public function enable_user()
        {
            $id_user = $this->input->post('id_user');

            if ($id_user == $this->session->userdata('id_user')) 
                ajax_response('danger', 'No puedes revivirte.', '0', '1');

            // Realizamos el update.
            $result = $this->app_users->set_user($id_user, 'user_deletion_date', NULL);
                    
            if ($result == TRUE)
            {
                $ext['action']          = 'reload';
                $ext['delay']           = '1000';
                ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

    /**
     * Roles
     * 
     */
        /**
         * Alta de rol.
         * 
         */
        public function create_role()
        {
            check_permissions('app', 'manage_roles', FALSE, TRUE);
            
            $role_name = $this->input->post('role_name');
            
            $id_role = $this->app_users->new_role($role_name);

            if ($id_role !== FALSE) {
                $ext['action'] = 'redirect';
                $ext['delay']  = '500';            
                $ext['target'] = base_url(APP_BACKEND.'roles?action=edit&id='.$id_role);            
                ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

        /**
         * Modif de rol.
         * 
         */
        public function update_role()
        {
            check_permissions('app', 'manage_roles', FALSE, TRUE);

            $id_role    = $this->input->post('id_role');
            $role_name  = $this->input->post('role_name');

            $id_role = $this->app_users->set_role($id_role, 'role_name', $role_name);

            if ($id_role !== FALSE) {
                $ext['action'] = 'reload';
                $ext['delay']  = '1000';            
                ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

        /**
         * Actualiza batch de permisos.
         * 
         */
        public function update_role_permissions()
        {
            check_permissions('app', 'manage_roles', FALSE, TRUE);

            $id_role            = $this->input->post('id_role');
            $permission_group   = $this->input->post('permission_group');

            unset($_POST['id_role']);
            unset($_POST['permission_group']);

            $permissions = $this->input->post();
            
            foreach ($permissions as $permission_item => $value) {
                $this->app_permissions->set($id_role, $permission_group, $permission_item, (bool)$value);
            }

            ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1');
        }

    /**
     * Notificaciones
     * 
     */
        /**
         * Marca como leidas todas las notificaciones del usuario.
         * 
         */
        public function notifications_read_all()
        {
            $id_user = $this->session->userdata('id_user');
            
            $result = (!empty($id_user)) ? $this->app_notifications->view($id_user) : FALSE;;

            if ($result !== FALSE) {
                $ext['action'] = 'reload';
                $ext['delay']  = '500';            
                ajax_response('success', 'Se leyeron todas las notificaciones.', '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

        public function notifications_clean()
        {
            $id_user = $this->session->userdata('id_user');
            
            $result = (!empty($id_user)) ? $this->app_notifications->clean($id_user) : FALSE;;

            if ($result !== FALSE) {
                $ext['action'] = 'reload';
                $ext['delay']  = '500';            
                ajax_response('success', 'Se eliminaron todas las notificaciones leidas.', '1', '1', $ext);
            }
            else
                ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
        }

    /**
     * Configurations
     *
     */
        public function update_configurations()
        {
            $items = $this->input->post('items');
            $scope       = $this->input->post('config_scope');
            $reference   = $this->input->post('config_reference');

            foreach ($items as $item => $value) {
                $this->app_settings->set_config($scope, $item, $value, $reference);
            }

            ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1');
        }

        public function test_email()
        {
            ob_start();
            $id_user = $this->session->userdata('id_user');

            // Generamos una notificación.
            $id_notification = $this->app_notifications->new_notification($id_user, 'app', 'Testing de emails', 'Se intentado enviar un email a tu correo. Revisa si lo recibiste correctamente.', APP_BACKEND.'system', TRUE, 'info|fa fa-cog');
            
            $this->app_mailing->debugger = TRUE;
            $this->app_mailing->notification($id_notification);
            
            $output = ob_get_contents();
            ob_end_clean();
            $ext['update']['#mailing-results'] = $output;
            ajax_response('success', 'Se completó el testing. Revise los resultados.', '1', '1', $ext);
        }
        
        public function tool_test_email_design()
        {
        	$target 	= $this->input->post('target');
        	$subject 	= $this->input->post('subject');
        	$content 	= $this->input->post('content');

            $result = $this->app_mailing->send($target, $subject, $content, $on_debug = 'DFLT');
            
            ajax_response('success', 'Se completó el envío, suponiendo que el mailing esta configurado.', '1', '1', $result);
        }
}

