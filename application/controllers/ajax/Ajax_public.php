<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_public extends CI_Controller {

    /**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();
        $this->session->keep_flashdata('login_redirect');
        $this->session->keep_flashdata('login_from_notification');

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();
    }

    /**
     * Llamado para login
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    public function login()
    {
        // Si el usuario esta logeado lo mandamos al dashboard
        if ($this->session->userdata('logged_in') == 'TRUE') {
            // Selectivamente borramos algunos datos de sesion, en cao de que haya algo roto. No borramos todo porque los redirect del login los tenemos en flashdata.
            $this->session->set_userdata('logged_in', NULL);
            $this->session->set_userdata('id_user', NULL);
        }

        $email          = $this->input->post('email', TRUE);
        $password       = $this->input->post('password', TRUE);

        // Comprobamos las credenciales del usuario.
        $result = $this->app_users->login($email, md5($password));

        // Si las credenciales son invalidas
        if ($result == FALSE)
            ajax_response('danger', 'Los datos de acceso son incorrectos.', '0', '1', NULL);
        else
        {
            // Sino inicializamos la sesion.
            $result['logged_in'] = 'TRUE';
            $result['token'] = generate_session_token($result['id_user'], $result['user_email']);
            $this->session->set_userdata($result);

            // Dejamos un marcador en la sesion para recordar cambiar clave. Esta manejado en cada layout.
            if (md5(md5($password).date('Y-m-d')) == $result['user_password_restore'])
                $this->session->set_userdata('tmp_password', TRUE);

            // Hay alguna notificacion que marcar como vista?
            $notif_token   = $this->session->flashdata('login_from_notification');
            if (!empty($notif_token)) $this->app_notifications->view($result['id_user'], $notif_token);

            // Y redirigimos el usuario a donde estaba.
            $redirect       = $this->session->flashdata('login_redirect');
            $ext['action'] = 'redirect';
            $ext['delay']  = '1000';
            $ext['target'] = (!empty($redirect)) ? base_url($redirect) : base_url(APP_HOME);
            ajax_response('success', '<i class="fa fa-spinner fa-spin"></i> Bienvenido '.$result['user_name'].', accediendo ...', '1', '1', $ext);
        }
    }
    
    /**
     * Llamado para recuperar clave
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    public function forget()
    {
        // Si el usuario esta logeado lo mandamos al dashboard
        if ($this->session->userdata('logged_in') == 'TRUE')
            redirect(APP_HOME);

        $usr_email = $this->input->post('email');
        
        // Verificamos que el usuario exista en el sistema.
            $data['user'] = $this->app_users->get_user_by('user_email', $usr_email);
            if ($data['user'] == FALSE) {
                echo ajax_response('danger', 'Aparentemente no estas registrado.', 0, 1);
                exit();
            }

        // Generamos la clave temporal
            $password = $this->app_users->gen_temporal_password($data['user']['id_user']);
            if ($password == FALSE) {
                echo ajax_response('danger', 'No se pudo generar tu clave temporal. Reintenta luego.', 0, 1);
                exit();
            }

        // Le despachamos el email con la clave
            $this->load->model('app_mailing');
            $this->app_mailing->new_temporal_password($data['user']['id_user'], $password);

        $ext['action'] = 'redirect';
        $ext['delay']  = '1000';
        $ext['target'] = base_url(APP_LOGIN);
        echo ajax_response('success', 'Enviamos una clave de acceso temporal a tu email.', '1', '1', $ext);
    }
    
    /**
     * Cerrar sesion
     */
    public function logout()
    {
        $this->session->sess_destroy();

        $ext['action'] = 'redirect';
        $ext['delay']  = '1000';
        $ext['target'] = base_url(APP_LOGIN);
        ajax_response('success', '<i class="fa fa-spinner fa-spin"></i> Su sesión finalizó, redirigiendo ...', '1', '1', $ext);
    }
}

