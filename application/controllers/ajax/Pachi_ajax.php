<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pachi_ajax extends CI_Controller {

    /**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();
    }

    /**
     * Handler para procesar formularios.
     */
	    public function form()
	    {
	    	$form = pachi_retrieve_form();

	    	switch ($form['method']) {
	    		case 'update':
	    			$result 	= $this->pachi->set($form['table'], $form['id'], $form['data']);
	    			$_result 	= $this->pachi->get($form['table'], $form['id']);
	    			break;
	    		case 'create':
	    			$result 	= $this->pachi->insert($form['table'], $form['data']);
	    			$_result 	= $this->pachi->get($form['table'], $result);
	    			break;
	    		default:
	    			ajax_response('danger', $this->lang->line('pachi_error_form'), '0', '1');
	    			break;
	    	}

	    	$ext = FALSE;

	    	if (isset($form['meta']['save_action']))
	    	{
				$ext = $form['meta']['save_action'];

	            if (isset($ext['target']))
	            	$ext['target'] = $this->parser->parse_string($ext['target'], $_result, TRUE);
	    	}

	    	if ($result)
	    		ajax_response('success', $this->lang->line('pachi_data_saved'), '1', '1', $ext);
	    	else
	   			ajax_response('danger', $this->pachi->errors[0], '0', '1');
	    }

	    public function search()
	    {
	    	$s = pachi_retrieve_search();

	    	$result = $this->pachi->fetch($s['table'], $s['filter'], 0, 20, FALSE, $s['search']);

	    	$display_column 	= (is_array($s['display'])) ? end($s['display']) : $s['display'];
	    	$custom_display 	= $s['custom_display'];
	    	$selectable_column 	= $s['column'];

	        $results = array();
	        foreach ($result as $_item) 
	        	if (!empty($custom_display))
	            	$results[] = array('id' => $_item[$selectable_column], 'text' => $this->parser->parse_string($custom_display, $_item, TRUE));
	        	else
	            	$results[] = array('id' => $_item[$selectable_column], 'text' => $_item[$display_column]);

	        $ext['feed'] = $results;
	        ajax_response('success', $this->lang->line('pachi_search_ok'), '1', '0', $ext);
	    }

	    public function modal()
	    {
	    	$modal 			= pachi_retrieve_modal();

	    	$__id_modal 	= uniqid(pachi_mask('modal_'.$modal['table']));

	    	$modal['_data']['__id_modal'] 	= $__id_modal;
	    	$modal['_data']['modal_title'] 	= $modal['modal_title'];
	    	
	        if (!isset($modal['_data']['save_action'])) {
	        	$modal['_data']['save_action']['action'] = 'dismiss_modal';
	        	$modal['_data']['save_action']['target'] = '#' . $__id_modal;
	        	$modal['_data']['save_action']['delay']  = '500';
	        }

	        $ext['trigger_modal'] 	= '#' . $__id_modal;
	        $ext['modal'] 			= pachi_form($modal['table'], $modal['columns'], $modal['entity_id'], $modal['_data'], $design = 'modal');
	        ajax_response('success', 'ok', '1', '0', $ext);
	    }

    /**
     * Adicionales.
     */
    	public function search_table()
    	{
    		$search = 	$this->input->post('search');
    		$url 	= 	$this->input->post('url');
    		$key 	= 	$this->input->post('key');

    		$_table_dat = pachi_table_get($key);
    		$_table_dat['s'] = $search;

    		$ext['action'] = 'redirect';
            $ext['delay']  = '1';
            $ext['target'] = $url . generate_get_string_2(pachi_table_set($key, $_table_dat['p'] , $_table_dat['ipp'], $_table_dat['f'], $_table_dat['o'], $_table_dat['s']));

    		ajax_response('success', '', '1', '0', $ext);
    	}
}

