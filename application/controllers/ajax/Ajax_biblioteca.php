<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_biblioteca extends CI_Controller {

    /**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();
    }

    /**
     * Biblioteca
     * 
     */
        /**
         * Retorna modal de de biblioteca mutlimedia.
         * Solo el contenedor.
         * 
         */
        public function get_modal_library()
        {
            $id_target      = $this->input->post('id_target');
            $max_items      = $this->input->post('max_items');
            $selection      = $this->input->post('selection');
            $extension      = $this->input->post('extension');
            $isolated       = $this->input->post('isolated');

            $data['id_target']      = $id_target;
            $data['max_items']      = $max_items;
            $data['selection']      = $selection;
            $data['extension']      = $extension;
            $data['isolated']       = $isolated;

            $ext['trigger_modal'] = '#mod_library';
            $ext['modal'] = $this->load->view('components/library/modal_library', $data, TRUE);
            ajax_response('success', 'ok', '1', '0', $ext);
        }

        /**
         * Carga archivos.
         * 
         */
        public function upload_files($isolated = 0)
        {
            $this->load->library('app_uploader');

            $id_user    = $this->session->userdata('id_user');
            $result     = $this->app_uploader->upload();
            $uploaded   = array();

            $this->app_uploader->cleanup();

            foreach ($result['success'] as $_kfile => $_file) 
            {
                // Registramos el archivo.
                $id_file = $this->libreria->new_file($_file['old_title'], mb_strtolower($_file['extension']));
                // Actualizmaos los parametros del archivo.
                $opt['file_key']    = $_file['name']; // <-- Nombre encriptado
                $opt['id_user']     = $id_user;
                $opt['file_type']   = $_file['type'];
                $opt['file_size']   = $_file['size'];
                $opt['is_image']    = (strpos($_file['type'], 'image') !== FALSE);

                // Si trabajamos con un grupo aislado, hacemos que los archivos expiren en 2 horas. Al guardar el formulario debemos indicar que los archivos se conserven.
                if ($isolated == 1)
                    $opt['file_cleanup'] = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s') . '+2 hour'));

                $this->libreria->set_file($id_file, $opt);
                
                // Agregamos el archivo subido a la libreria del usuario
                if ($isolated == 0)
                    $this->libreria->new_element($id_user, $id_file);

                // Mandamos al front los IDs, por si se usan para algo.
                $uploaded[] = $id_file;
            }
            
            // Corremos proceso de limpieza de archivos temporales.

            $ext['result']      = $result;
            $ext['completed']   = $uploaded;
            ajax_response('success', 'ok', '1', '0', $ext);
        }

        /**
         * Retorna una coleccion de items para libreria multimedia.
         * 
         */
        public function get_library_elements()
        {
            $selection  = $this->input->post('selection');
            $search     = $this->input->post('search');
            $extension  = $this->input->post('extension');
            $offset     = $this->input->post('offset');
            $isolated   = $this->input->post('isolated');
            $page       = ($offset / $pp = 64);
            $filter     = FALSE;

            if (!empty($extension)) {
                $extension = "'" . implode("','", explode(',', $extension)) . "'";
                $filter['files.file_ext IN ('.$extension.')'] = NULL;
            }

            // En caso de que estemos trabajando con una seleccion aislada, los elementos no provienen de una biblioteca.
            if (!empty($isolated)) {
                
                if (empty($selection)) 
                    $selection = -1;

                $filter['id_file IN ('.$selection.')'] = NULL;
                
                // Cargamos elementos del indice general.
                $data['elements']       = $this->libreria->get_files($filter, FALSE, $page, $pp, array('files.id_file' => 'DESC'), $search, $selection);
                $data['dataset']        = $offset + $pp;
            }
            else
            {
                // Cargamos elementos de la libreria.
                $filter['id_user']      = $this->session->userdata('id_user');
                $data['elements']       = $this->libreria->get_elements($filter, FALSE, $page, $pp, array('files.id_file' => 'DESC'), $search, $selection);
                $data['dataset']        = $offset + $pp;
            }

            $html = $this->load->view('components/library/elements', $data, TRUE);

            $ext['elements'] = count($data['elements']);

            if ($page > 0) {
                $update['#library_btn_load_more'] = array('remove' => TRUE);
                $update['#library_elements'] = ($page > 0) ? array('append' => $html) : $html;
            }
            else
                $update['#library_elements'] = $html;

            ajax_response('success', 'ok', '1', '0', $ext, $update);
        }

        /**
         * Retorna una coleccion de previews de elementos de libreria.
         * 
         */
        public function get_library_files_preview()
        {
            $selection  = $this->input->post('selection');
            $id_target  = $this->input->post('id_target');

            // Cargamos elementos de la libreria.
            $cond['id_file IN ('.$selection.')'] = NULL;
            $data['selection'] = (empty($selection)) ? array() : $this->libreria->get_files($cond, FALSE, 0, 50, FALSE);

            $update['.hnd_selected_files_previews.' . $id_target] = $this->load->view('components/forms/files_preview', $data, TRUE);
            ajax_response('success', 'ok', '1', '0', FALSE, $update);
        }

        /**
         * Retorna una coleccion de previews de elementos de libreria.
         * 
         */
        public function get_library_images_preview()
        {
            $selection  = $this->input->post('selection');
            $id_target  = $this->input->post('id_target');

            // Cargamos elementos de la libreria.
            $cond['id_file IN ('.$selection.')'] = NULL;
            $data['selection'] = (empty($selection)) ? array() : $this->libreria->get_files($cond, FALSE, 0, 50, FALSE);

            $update['.hnd_selected_images_previews.' . $id_target] = $this->load->view('components/forms/images_preview', $data, TRUE);
            ajax_response('success', 'ok', '1', '0', FALSE, $update);
        }

}

