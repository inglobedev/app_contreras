<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * handlers para llamados de cuestiones generales a al app base. Por ejemplo gestion de cuentas, roles, etc.
 * 
 */
class Ajax_properties extends CI_Controller {

    /**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();

        $this->baseapp->session_check();
    }

    public function delete_property_expense()
    {
    	$id_property_expense 	= $this->input->post('id_property_expense');
    	$return_url 			= $this->input->post('return_url');

	    $r = $this->pachi->del('property_expenses', array('id_property_expense' => $id_property_expense));

		$ext['action'] = 'redirect';
        $ext['delay']  = '500';            
        $ext['target'] = $return_url;   

        ajax_response('success', 'ok', '1', '0', $ext);
    }

    public function update_property_expense()
    {
	    $attribute 	= $this->input->post('attribute');
	    $id_property_expense 	= $this->input->post('id_property_expense');
	    $checked 	= ($this->input->post('checked') == 'true') ? 1 : 0;

	    $r = $this->pachi->set('property_expenses', $id_property_expense, array($attribute => $checked));
    }

    public function collect_rent()
    {
        $ext['trigger_modal'] = '#mod_rent_payment';
        $ext['modal'] = $this->load->view('pages/payments/components/modal_rent', FALSE ,TRUE);
        ajax_response('success', 'ok', '1', '0', $ext);
    }

    public function update_owner()
    {	
    	$datos = pachi_retrieve_form();
    	
    	exit();

    	if (empty($datos['data']['id_user_professional']))
    		ajax_response('danger', 'No se especificó profesional.', '0', '1');	

    	$mantenice['id_property'] 					= $datos['data']['id_property'];
    	$mantenice['maintenance_name'] 				= $datos['data']['maintenance_name'];
        $mantenice['maintenance_description']       = $datos['data']['maintenance_description'];
    	$mantenice['maintenance_date'] 		        = $datos['data']['maintenance_date'];
    	$mantenice['maintenance_owner_cost'] 		= $datos['data']['maintenance_owner_cost'];
    	$mantenice['maintenance_tenant_cost'] 		= $datos['data']['maintenance_tenant_cost'];
    	$mantenice['maintenance_administrative_fee']= $datos['data']['maintenance_tenant_cost'] + $datos['data']['maintenance_owner_cost'] - $datos['data']['quote_price'];
    	$mantenice['maintenance_completed'] 		= 0;
        $mantenice['maintenance_status']            = 0;
    	$mantenice['maintenance_archived'] 			= 0;

    	$id_maintenance = $this->pachi->insert('property_maintenances', $mantenice);
    	
    	$mantenice_quotes['id_maintenance'] 		= $id_maintenance;
    	$mantenice_quotes['id_user_professional'] 	= $datos['data']['id_user_professional'];
        $mantenice_quotes['quote_price']            = $datos['data']['quote_price'];
    	$mantenice_quotes['quote_attachments'] 		= $datos['data']['quote_attachments'];
    	$mantenice_quotes['quote_ready'] 			= 1;

    	$id_maintenance_quote 						= $this->pachi->insert('property_maintenance_quotes', $mantenice_quotes);
		$id_quote['id_maintenance_quote'] 			= $id_maintenance_quote;
	    	
		if ($id_quote != FALSE) 
		{
    		$this->pachi->set('property_maintenances', $id_maintenance, $id_quote);

    		$ext['action'] = 'redirect';
            $ext['delay']  = '500';            
            $ext['target'] = base_url(APP_MAINTENANCES);            
            ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
		}
		else
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');	
	}

	public function create_ownership()
	{
    	$datos = pachi_retrieve_form();
    	
    	if (empty($datos['data']['id_user_owner']))
    		ajax_response('danger', 'No se especificó el propietario.', '0', '1');	

    	if (empty($datos['data']['ownership_administrative_rate']))
    		ajax_response('danger', 'No se especificó el gasto administrativo.', '0', '1');	

    	$ownership['id_property'] 					= $datos['data']['id_property'];
    	$ownership['id_user_owner'] 				= $datos['data']['id_user_owner'];
        $ownership['ownership_administrative_rate'] = $datos['data']['ownership_administrative_rate'];
    	$ownership['ownership_date_start'] 			= date('Y-m-d');

        $old_ownership = $this->pachi->fetch('ownerships', array('id_property' => $ownership['id_property']), -1, 1, array('id_ownership' => 'DESC'));

    	$id_ownership = $this->pachi->insert('ownerships', $ownership);
	    	
		if ($id_ownership != FALSE) 
		{
    		$this->pachi->set('ownerships', $old_ownership['id_ownership'], array('ownership_date_end' => date("Y-m-d",strtotime("-1 day"))));

    		$ext['action'] = 'redirect';
            $ext['delay']  = '500';            
            $ext['target'] = base_url(APP_PROPIEDADES . 'view/' . $ownership['id_property'] . '?r=' . rand(555,9897) . '#tab_owner');            
            ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
		}
		else
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');	
	}

    public function restore_property()
    {
        $id_property = $this->input->post('id_property');

        $result = $this->pachi->set('properties', $id_property, array('property_archived' => 0));

        if ($result == TRUE)
        {
            $ext['action']          = 'reload';
            $ext['delay']           = '1000';
            ajax_response('success', 'Se restauro el inmueble.', '1', '1', $ext);
        }
        else
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
    }

    public function archive_property()
    {
        $id_property = $this->input->post('id_property');

        $result = $this->pachi->set('properties', $id_property, array('property_archived' => 1));

        if ($result == TRUE)
        {
            $ext['action']          = 'reload';
            $ext['delay']           = '1000';
            ajax_response('success', 'Se archivo el inmueble.', '1', '1', $ext);
        }
        else
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
    }

}
