<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * handlers para llamados de cuestiones generales a al app base. Por ejemplo gestion de cuentas, roles, etc.
 * 
 */
class Ajax_administration extends CI_Controller {

    /**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();

        $this->baseapp->session_check();
    }

    public function load_account_transactions()
    {
    	$filter    = $this->input->post('filter');
        $id_period = $this->input->post('id_period');
        $period    = FALSE;

        switch ($filter) 
        {
            case 'adm-ext':
                $_transactions_filter['transaction_type'] = 1;
                $transaction_type = 'Extracciones';
                break;
            case 'adm-payments':
                $_transactions_filter['transaction_type'] = 2;
                $transaction_type = 'Pagos de Imp & Serv Administracion';
                break;
            default:
                ajax_response('danger', 'Ocurrió un error en la ultima acción.', '1', '1'); 
                break;
        }

        $data['periods'] = $this->pachi->fetch('_periods', FALSE, 0, 1000, array('id_period' => 'DESC'));

        // Vemos si se especifico un periodo, de lo contrario tomamos las transacciones del actual.
        if (!empty($id_period)) 
        {
            $period = $this->pachi->get('_periods', $id_period);

            if (empty($period))
                ajax_response('danger', 'Ocurrió un error en la ultima acción. E07', '1', '1');
        }

        // Si a esta linea no tenemos un periodo establecido, tomamos el actual.
        if (empty($period))
        {
            $_period_filter['period_year'] = date('Y');
            $_period_filter['period_month'] = date('m');
            $period = $this->pachi->fetch('_periods', $_period_filter, -1, 1);
            if (empty($period))
                ajax_response('danger', 'Ocurrió un error en la ultima acción. E08', '1', '1');
        }

        $_transactions_filter['YEAR(transaction_date)'] = $period['period_year'];
        $_transactions_filter['MONTH(transaction_date)'] = $period['period_month'];

        $data['dataset'] = $this->pachi->fetch('account_transactions', $_transactions_filter, 0, 1000, array('id_transaction' => 'DESC'));
        $data['filter']  = $filter;
        $data['period']  = $period;

        $update['#account-transactions'] 	= $this->load->view('pages/administration/components/transactions_plain', $data, TRUE);
        $update['#account-title'] 			= 'Movimientos de ' . $transaction_type;

        ajax_response('success', 'ok', '1', '0', FALSE, $update);
    }

}
