<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * handlers para llamados de cuestiones generales a al app base. Por ejemplo gestion de cuentas, roles, etc.
 * 
 */
class Ajax_contracts extends CI_Controller {

    /**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();

        $this->baseapp->session_check();
    }

    public function create_contract()
    {

        $datos = pachi_retrieve_form();

          print_r($datos);

        if (empty($datos['data']['id_property']))
            ajax_response('danger', 'No se especificó propiedad.', '0', '1'); 

        if(empty($datos['data']['contract_observations'])) {
                 $contrato['contract_observations'] = NULL;
             }else{
                 $contrato['contract_observations'] = $datos['data']['contract_observations'];
             }
                                                           
            

      
            $contrato['id_property']                  =   $datos['data']['id_property'];
            $contrato['id_user_tenant']               =   $datos['data']['id_user_tenant'];             
            $contrato['contract_date_start']          =   $datos['data']['contract_date_start'];          
            $contrato['contract_date_end']            =   $datos['data']['contract_date_end'];            
            $contrato['contract_rent_price']          =   $datos['data']['contract_rent_price'];         
            $contrato['contract_interest']            =   $datos['data']['contract_interest'];            
            $contrato['contract_gas_check']           =   $datos['data']['contract_gas_check'];           
            $contrato['contract_electricity_check']   =   $datos['data']['contract_electricity_check'];   
            $contrato['contract_expenses_check']      =   $datos['data']['contract_expenses_check'];      
            $contrato['contract_payday_start']        =   $datos['data']['contract_payday_start'];        
            $contrato['contract_payday_limit']        =   $datos['data']['contract_payday_limit'];            
            $contrato['contract_commission']          =   $datos['data']['contract_commission'];



           


            try {
               $_result =   $this->pachi->insert('contracts', $contrato);
                
            } catch (Exception $e) {
                 print_r($e);
            }
           
            //print_r($_result);
    }

   

}