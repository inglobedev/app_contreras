<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * handlers para llamados de cuestiones generales a al app base. Por ejemplo gestion de cuentas, roles, etc.
 * 
 */
class Ajax_maintenance extends CI_Controller {

	/**
     * Constructor
     * Si el llamado no es ajax, y no estamos en desarrollo, esta pagina no existe.
     */
    function __construct() 
    {
        parent::__construct();

        if(!$this->input->is_ajax_request() AND ENVIRONMENT == 'production') 
            show_404();

        $this->baseapp->session_check();
    }

    function create_maintenance()
    {	
    	$datos = pachi_retrieve_form();

    	if (empty($datos['data']['id_user_professional']))
    		ajax_response('danger', 'No se especificó profesional.', '0', '1');	

    	$mantenice['id_property'] 					= $datos['data']['id_property'];
    	$mantenice['maintenance_name'] 				= $datos['data']['maintenance_name'];
        $mantenice['maintenance_description']       = $datos['data']['maintenance_description'];
    	$mantenice['maintenance_date'] 		        = $datos['data']['maintenance_date'];
    	$mantenice['maintenance_owner_cost'] 		= $datos['data']['maintenance_owner_cost'];
    	$mantenice['maintenance_tenant_cost'] 		= $datos['data']['maintenance_tenant_cost'];
    	$mantenice['maintenance_administrative_fee']= $datos['data']['maintenance_tenant_cost'] + $datos['data']['maintenance_owner_cost'] - $datos['data']['quote_price'];
    	$mantenice['maintenance_completed'] 		= 0;
        $mantenice['maintenance_status']            = 0;
    	$mantenice['maintenance_archived'] 			= 0;

    	$id_maintenance = $this->pachi->insert('property_maintenances', $mantenice);
    	
    	$mantenice_quotes['id_maintenance'] 		= $id_maintenance;
    	$mantenice_quotes['id_user_professional'] 	= $datos['data']['id_user_professional'];
        $mantenice_quotes['quote_price']            = $datos['data']['quote_price'];
    	$mantenice_quotes['quote_attachments'] 		= $datos['data']['quote_attachments'];
    	$mantenice_quotes['quote_ready'] 			= 1;

    	$id_maintenance_quote 						= $this->pachi->insert('property_maintenance_quotes', $mantenice_quotes);
		$id_quote['id_maintenance_quote'] 			= $id_maintenance_quote;
	    	
		if ($id_quote != FALSE) 
		{
    		$_result = $this->pachi->set('property_maintenances', $id_maintenance, $id_quote);

	    	$ext = FALSE;

	    	if (isset($datos['meta']['save_action']))
	    	{
				$ext = $datos['meta']['save_action'];

	            if (isset($ext['target']))
	            	$ext['target'] = $this->parser->parse_string($ext['target'], $_result, TRUE);
	    	}
	    	else
	    	{
	    		$ext['action'] = 'redirect';
	            $ext['delay']  = '500';            
	            $ext['target'] = base_url(APP_MAINTENANCES);  
	    	}

	    	if ($id_quote)
	    		ajax_response('success', $this->lang->line('pachi_data_saved'), '1', '1', $ext);
	    	else
	   			ajax_response('danger', $this->pachi->errors[0], '0', '1');
		}
		else
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');	
	}

	function update_maintenante()
	{
    	$form 				= pachi_retrieve_form();
    	$_extra_message 	= '';
		
        // Si cambian los importes tenemos que ver que los cupnes no esten pagados.
        $_previous_data = $this->pachi->get('property_maintenances', $form['id']);
        $_tenant_coupon = $this->pachi->fetch('receipt_detail', array('detail_concept' => 'GET-TENANT-MAINTENANCE-'), -1, 1);
        $_owner_coupon  = $this->pachi->fetch('receipt_detail', array('detail_concept' => 'GET-OWNER-MAINTENANCE-'), -1, 1);
        // Agregar controles y actualizar. ¿Que pasaria con la comisino administrativa?

        // Este bloque de codigo es similar al de pachi_ajax, solo que le agregamos en el medio unas modificaciones adicionales para cuando se cumple que:
        switch ($form['method']) {
            case 'update':
                $result     = $this->pachi->set($form['table'], $form['id'], $form['data']);
                $_result    = $this->pachi->get($form['table'], $form['id']);
                break;
            case 'create':
                $result     = $this->pachi->insert($form['table'], $form['data']);
                $_result    = $this->pachi->get($form['table'], $result);
                break;
            default:
                ajax_response('danger', $this->lang->line('pachi_error_form'), '0', '1');
                break;
        }

		// Cuando un mantenimiento se marca como completo, el estado se establece en Aprobado y se archiva.
    	if ($form['data']['maintenance_completed'] == '1') 
    	{
    		$ndata['maintenance_completed'] = 1;
    		$ndata['maintenance_status'] 	= 1;
    		$ndata['maintenance_archived'] 	= 1;

    		$_extra_message = '. Al estar marcado como completo, se verificó que quede en estado aprobado, y fue archivado.';

    		$result 	= $this->pachi->set($form['table'], $form['id'], $ndata);
    	}

    	$ext = FALSE;

    	if (isset($form['meta']['save_action']))
    	{
			$ext = $form['meta']['save_action'];

            if (isset($ext['target']))
            	$ext['target'] = $this->parser->parse_string($ext['target'], $_result, TRUE);
    	}

    	if ($result)
    		ajax_response('success', $this->lang->line('pachi_data_saved') . $_extra_message, '1', '1', $ext);
    	else
   			ajax_response('danger', $this->pachi->errors[0], '0', '1');
	}

	function restore_maintenance()
	{
		$id_maintenance = $this->input->post('id_maintenance');

		$result = $this->pachi->set('property_maintenances', $id_maintenance, array('maintenance_archived' => 0));

        if ($result == TRUE)
        {
            $ext['action']          = 'reload';
            $ext['delay']           = '1000';
            ajax_response('success', 'Se restauro el mantenimiento.', '1', '1', $ext);
        }
        else
            ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
	}

	function archive_maintenance()
	{
		$id_maintenance = $this->input->post('id_maintenance');

		$result = $this->pachi->set('property_maintenances', $id_maintenance, array('maintenance_archived' => 1));

	    if ($result == TRUE)
	    {
	        $ext['action']          = 'reload';
	        $ext['delay']           = '1000';
	        ajax_response('success', 'Se archivo el mantenimiento.', '1', '1', $ext);
	    }
	    else
	        ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
	}

	function delete_maintenance()
	{
		$id_maintenance = $this->input->post('id_maintenance');

		$result = $this->pachi->del('property_maintenances', $id_maintenance);

	    if ($result == TRUE)
	    {
	        $ext['action']          = 'reload';
	        $ext['delay']           = '1000';
	        ajax_response('success', $this->lang->line('general_msg_saved_ok'), '1', '1', $ext);
	    }
	    else
	        ajax_response('danger', $this->lang->line('general_error_ocurred'), '0', '1');
	}

}