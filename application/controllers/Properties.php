<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Properties extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de inicio/dashboard.
     * 
     */
    public function index()
    {
        // Levantamos variables GET
        $action     = $this->input->get('action');
        $id_propiedad    = $this->input->get('id');
        $page       = pag_current_page();

        switch ($action) {
            case 'new':
                $this->layout->page_title = ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_property');
                show_page('properties/properties_creator', FALSE);
                break;
            default:
                $this->layout->page_title = ucfirst($this->lang->line('entity_properties'));
                #$this->layout->page_title[1] = '<a href="'.generate_get_string('action', 'new').'" class="btn green btn-md pull-right">'. ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_property').'</a>';
                show_page('properties');
                break;
        }
    }

    public function sales()
    {
        $this->layout->page_title = ucfirst($this->lang->line('lbl_sales'));
        show_page('properties/sales');
    }

    public function view($id_property)
    {   
        $data['property'] = $this->pachi->get('vw_properties', array('id_property' => $id_property), FALSE);
        
        if(empty($data['property'])) {
            show_404();
        }
            
        $cond['id_contract']    = $data['property']['id_contract'];
        $data['guarantor']      = $this->pachi->fetch('vw_guarantors', $cond);
        $data['periods']        = $this->pachi->fetch('_periods', FALSE, 0, 24, array('id_period' => 'DESC'));

        show_page('properties/properties_editor', $data);
    }

}