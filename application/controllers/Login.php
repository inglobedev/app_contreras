<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        $this->session->keep_flashdata('login_redirect');
        $this->session->keep_flashdata('login_from_notification');

        if($this->input->is_ajax_request()) 
            show_404();

        if ($this->session->userdata('logged_in') == 'TRUE')
            redirect(base_url().APP_HOME);
    }

    /**
     * Pagina de Acceso y Recuperación de clave.
     * Mostramos la página.
     */
    public function index()
    {
        show_page('login', array(), 'login');
    }
	
}
