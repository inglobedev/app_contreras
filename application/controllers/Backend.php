<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de inicio/dashboard.
     * 
     */
    public function index()
    {
        show_page('backend', array());
    }

    /**
     * Pagina de autogestion de cuenta.
     * Mostramos la página.
     */
    public function account($id_user = FALSE)
    {
        $id_user = ($id_user === FALSE) ? $this->session->userdata('id_user') : $id_user;;

        $data['roles']      	= $this->app_users->get_roles(FALSE, FALSE, 0, 100);
        $data['user']       	= $this->app_users->get_user($id_user);
        $data['notifications']	= $this->app_notifications->get_settings();

        show_page('backend/user/user_editor', $data);
    }
    
    /**
     * Pantallas de gestion de usuarios.
     */

	    public function users()
	    {
	        if (!check_permissions('app', 'manage_users')) 
	            redirect(APP_HOME);

	        switch ($this->input->get('action')) {
	            case 'edit':
	                $this->account($this->input->get('id'));
	                break;
	            case 'new':
	                show_page('backend/user/user_creator');
	                break;
	            default:
	            	$this->layout->page_title[0] = ucfirst($this->lang->line('entity_users'));
	                $this->layout->page_title[1] = '';
	                show_page('backend/user/users_list');
	                break;
	        }
	    }


	    public function view_user($id_user)
	    {   
	    	if ($this->session->userdata('id_user') == $id_user)
	    		$data['notifications']	= $this->app_notifications->get_settings();

	    	$data['user'] = $this->pachi->get('users', array('id_user' => $id_user), FALSE);
	        
	        show_page('backend/user/user_editor', $data);
	    }

	    public function owners()
	    {
	        if (!check_permissions('app', 'manage_users')) 
	            redirect(APP_HOME);

	        switch ($this->input->get('action')) {
	            case 'edit':
	                $this->account($this->input->get('id'));
	                break;
	            default:
	            	$this->layout->page_title[0] = ucfirst($this->lang->line('entity_owners'));
	                $this->layout->page_title[1] = '
	                	<span class="pull-right" style="right: 15px; top: 15px; position: absolute;">
	                		<a class="btn btn-default btn-md" href="javascript:;">'.ucfirst($this->lang->line('lbl_liquidation_history')).'</a>
	                		<a class="btn btn-default btn-md" href="javascript:;">'.ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('lbl_advancement').'</a>
	                		<a class="btn btn-default btn-md btn_new_coupon" data-roles="'.ROLE_OWNER.'" data-counterpart_name="'.ucfirst($this->lang->line('entity_owner')).'" href="javascript:;">'.ucfirst($this->lang->line('lbl_liquidate')) . '</a>
	                	</span>
	                	';
	                show_page('backend/owner/owners_list');
	                break;
	        }
	    }

	    public function tenants()
	    {
	        if (!check_permissions('app', 'manage_users')) 
	            redirect(APP_HOME);

	        switch ($this->input->get('action')) {
	            case 'edit':
	                $this->account($this->input->get('id'));
	                break;
	            default:
	            	$this->layout->page_title[0] = ucfirst($this->lang->line('entity_tenants'));
	                $this->layout->page_title[1] = '';
	                show_page('backend/tenant/tenants_list');
	                break;
	        }
	    }

	    public function guarantors()
	    {
	        if (!check_permissions('app', 'manage_users')) 
	            redirect(APP_HOME);

	        switch ($this->input->get('action')) {
	            case 'edit':
	                $this->account($this->input->get('id'));
	                break;
	            default:
	            	$this->layout->page_title[0] = ucfirst($this->lang->line('entity_guarantors'));
	                $this->layout->page_title[1] = '';
	                show_page('backend/guarantor/guarantors_list');
	                break;
	        }
	    }

	    public function consortiums()
	    {
	        if (!check_permissions('app', 'manage_users')) 
	            redirect(APP_HOME);

	        switch ($this->input->get('action')) {
	            case 'edit':
	                $this->account($this->input->get('id'));
	                break;
	            default:
	            	$this->layout->page_title[0] = ucfirst($this->lang->line('entity_consortiums'));
	                $this->layout->page_title[1] = '';
	                show_page('backend/consortium/consortiums_list');
	                break;
	        }
	    }

	    public function professionals()
	    {
	        if (!check_permissions('app', 'manage_users')) 
	            redirect(APP_HOME);

	        switch ($this->input->get('action')) {
	            case 'edit':
	                $this->account($this->input->get('id'));
	                break;
	            default:
	            	$this->layout->page_title[0] = ucfirst($this->lang->line('entity_professionals'));
	                $this->layout->page_title[1] = '';
	                show_page('backend/professional/professionals_list');
	                break;
	        }
	    }

    /**
     * Pagina de gestion de roles.
     * 
     */

	    public function roles()
	    {
	        if (!check_permissions('app', 'manage_roles')) 
	            redirect(APP_HOME);

	        // Levantamos variables GET
	        $action     = $this->input->get('action');
	        $id_role    = $this->input->get('id');
	        $page       = pag_current_page();

	        switch ($action) {
	            case 'edit':
	                $data['permissions']    = $this->app_permissions->get_role_permissions($id_role);
	                $data['role']           = $this->app_users->get_role($id_role);
	                show_page('backend/roles/role_editor', $data);
	                break;
	            case 'new':
	                show_page('backend/roles/role_creator', FALSE);
	                break;
	            default:
	                $data['roles'] = $this->app_users->get_roles(FALSE, FALSE, $page, 15, array('id_role' => 'DESC'), $this->input->get('busqueda'));
	                
	                $data['pagination_current']     = $page;
	                $data['pagination_total_items'] = pag_total_items($data['roles']);
	                $data['pagination_per_page']    = 15;

	                show_page('backend/roles/role_list', $data);
	                break;
	        }
	    }

    /**
     * Pagina de configuración de sistema.
     * 
     */

	    public function system()
	    {
	        show_page('backend/system');
	    }

    /**
     * Pagina de configuración de sistema.
     * 
     */

	    public function tools()
	    {
	        show_page('backend/tools');
	    }

    /**
     * Pagina de notificaciones.
     * 
     */
    
	    public function notifications()
	    {
	        $id_user = $this->session->userdata('id_user');
	        $order['notif_readed']  = 'ASC';
	        $order['notif_date']    = 'DESC';
	        $data['notifications']  = $this->app_notifications->get_notifications('id_user', $id_user, 0, 1000, $order);

	        show_page('backend/notifications', $data);
	    }

}
