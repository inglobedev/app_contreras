<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expenses extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de inicio/dashboard.
     * 
     */
    public function index()
    {   
        $this->layout->page_title = ucfirst($this->lang->line('entity_expenses'));
        show_page('expenses');
    }

    public function expense_view($id_expense)
    {   
        $data['expense'] = $this->pachi->get('expenses', array('id_expense' => $id_expense), FALSE);
        $this->layout->page_title = ucfirst($this->lang->line('entity_expense'));
        show_page('expenses/expense_edit', $data);
    }

    public function results()
    {
        $this->layout->page_title = ucfirst($this->lang->line('lbl_results'));
        show_page('expenses/results');
    }


    public function taxes()
    {
        // Levantamos variables GET
        $action     = $this->input->get('action');
        $id_gasto    = $this->input->get('id');
        $page       = pag_current_page();

        switch ($action) {
            case 'edit':
                $data['gasto'] = $this->m_impuestos->get_gasto($id_gasto);
                show_page('impuestos/gastos/gasto_editor', $data);
                break;
            case 'new':
                show_page('impuestos/gastos/gasto_creador', FALSE);
                break;
            case 'new_p_provider':
                show_page('impuestos/gastos/gasto_creador', FALSE);
                break;
            default:
            	$this->layout->page_title[0] = ucfirst($this->lang->line('entity_taxes'));
                $this->layout->page_title[1] = '
                	<span class="pull-right" style="right: 15px; top: 15px; position: absolute;">
                		<a class="btn btn-default btn-md btn_new_coupon" href="javascript:;" data-roles="'.ROLE_SYSTEM.'" data-id_user_receiver="'.USER_PAYED.'" data-counterpart_name="Destino">' . ucfirst($this->lang->line('lbl_payment')) . ' ' . $this->lang->line('lbl_taxes') . ' & ' . $this->lang->line('lbl_services') . '</a>
                		<a class="btn btn-default btn-md btn_new_coupon" href="javascript:;" data-roles="'.ROLE_PROFESSIONAL.'" data-counterpart_name="Proveedor/Profesional">'.ucfirst($this->lang->line('lbl_payment')) . ' ' . $this->lang->line('lbl_provider').'</a>
                	</span>
                	';
                #$this->layout->page_title[0] = '';
                #$this->layout->page_title[1] = '<a style="margin-left:15px;" href="'.generate_get_string('action', 'new_p_provider').'" class="btn green btn-md pull-right">'. ucfirst($this->lang->line('lbl_payment')) . ' ' . $this->lang->line('lbl_provider').'</a><a href="'.generate_get_string('action', 'new_p_taxe').'" class="btn green btn-md pull-right">'. ucfirst($this->lang->line('lbl_payment')) . ' ' . $this->lang->line('lbl_taxe').'</a>';
                show_page('expenses/taxes');
                break;
        }
    }


    public function type_expense()
    {
        
       $this->layout->page_title = ucfirst($this->lang->line('entity_expense_types'));             
       show_page('expenses/type_expense');
    }

    public function view_type_expense($id_expense_type)
    {
        $data['type'] = $this->pachi->get('expense_types', array('id_expense_type' => $id_expense_type), FALSE);
        $this->layout->page_title = ucfirst($this->lang->line('entity_expense_type'));
        show_page('expenses/type_expense/type_edit', $data);
    }

    public function administration_expenses()
    {
        $this->layout->page_title = ucfirst($this->lang->line('lbl_administration_expenses'));
        show_page('expenses/administration');
    }

    public function maintenance() 
    {
        show_page('expenses/maintenance');
    } 

}