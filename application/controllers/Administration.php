<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de dashboard.
     * 
     */
    public function index()
    {
        $this->layout->page_title[0] = ucfirst($this->lang->line('entity_administration'));
	    $this->layout->page_title[1] = '
	    	<span class="pull-right" style="right: 15px; top: 15px; position: absolute;">
	    		<a class="btn btn-default btn-md btn_payment" href="javascript:;">' . ucfirst($this->lang->line('lbl_payment')) . ' ' . $this->lang->line('lbl_taxes') . ' & ' . $this->lang->line('lbl_services') . ' de Administración</a>
	    		<a class="btn btn-default btn-md btn_cashout" href="javascript:;"> Extraccion de Administración </a>
	    	</span>
	    	';
	    	
        // Saldos de cajas.
        $accounts_filter['id_user_owner'] = NULL;
        $accounts = $this->pachi->fetch('accounts', $accounts_filter, 0, 25);

        $data['accounts'] = $accounts;
        $data['periods']  = $this->pachi->fetch('_periods', FALSE, 0, 24, array('id_period' => 'DESC'));

        show_page('administration', $data);
    }

    /**
     * Pagina de dashboard.
     * 
     */
    public function account($id_account = FALSE)
    {
        $this->layout->page_title[0] = ucfirst($this->lang->line('entity_administration'));
	    $this->layout->page_title[1] = '
	    	<span class="pull-right" style="right: 15px; top: 15px; position: absolute;">
	    		<a class="btn btn-default btn-md btn_payment" href="javascript:;">' . ucfirst($this->lang->line('lbl_payment')) . ' ' . $this->lang->line('lbl_taxes') . ' & ' . $this->lang->line('lbl_services') . ' de Administración</a>
	    		<a class="btn btn-default btn-md btn_cashout" href="javascript:;"> Extraccion de Administración </a>
	    	</span>
	    	';

        if (empty($id_account) OR empty($account = $this->pachi->fetch('accounts', array('id_account' => $id_account), -1, 1)))
        	redirect(APP_ADMINISTRATION);

        // Saldos de cajas.
        $accounts_filter['id_user_owner'] = NULL;
        $accounts = $this->pachi->fetch('accounts', $accounts_filter, 0, 25);
        $data['accounts'] 	= $accounts;
        $data['id_account'] = $id_account;
        $data['account'] 	= $account;
        $data['periods']  = $this->pachi->fetch('_periods', FALSE, 0, 24, array('id_period' => 'DESC'));
        
        show_page('administration', $data);
    }

}