<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contracts extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de inicio/dashboard.
     * 
     */
    public function index()
    {
        // Levantamos variables GET
        $action     = $this->input->get('action');
        $id_contract    = $this->input->get('id');
        $page       = pag_current_page();

        switch ($action) {
            case 'new':
                $this->layout->page_title = ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_contract');
                show_page('properties/properties_creator', FALSE);
                break;
            default:
                $this->layout->page_title = ucfirst($this->lang->line('entity_contracts'));
                #$this->layout->page_title[1] = '<a href="'.generate_get_string('action', 'new').'" class="btn green btn-md pull-right">'. ucfirst($this->lang->line('lbl_new')) . ' ' . $this->lang->line('entity_property').'</a>';
                show_page('contracts');
                break;
        }
    }

    public function view($id_contract)
    {   
      //  $contracts          = $this->pachi->fetch('vw_contracts', $_contracts_filter, 0, 100);
        $data['contract'] = $this->pachi->get('vw_contracts', array('id_contract' => $id_contract), FALSE);
        $data['property'] = $this->pachi->get('properties', array('id_property' => $data['contract']['id_property']), FALSE);
        $data['bonifications'] = $this->pachi->fetch('contract_bonifications', array('id_contract' => $data['contract']['id_contract']));

        if(empty($data['contract'])) {
            show_404();
        }

        show_page('contracts/contracts_editor', $data);
    }

}