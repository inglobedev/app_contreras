<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Temporary extends CI_Controller {

    /**
     * OUTPUT
     * Tomamos el output por si necesitamos hacer algo...
     */
    function _output($output)
    {
        echo $output;
    }

    /**
     * Constructor
     * Si el llamado es ajax esta pagina no existe.
     * Si el usuario esta logeado, lo mandamos a la home de la app.
     */
    function __construct() 
    {
        parent::__construct();
        
        if($this->input->is_ajax_request()) 
            show_404();

        $this->baseapp->session_check();
    }

    /**
     * Pagina de inicio/dashboard.
     * 
     */
    public function index()
    {
        $this->layout->page_title = ucfirst($this->lang->line('lbl_temporary'));
        show_page('temporary');
    }
}