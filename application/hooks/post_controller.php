<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
    function post_controller()
    {
    	$CI = &get_instance();

    	// Hay notificaciones?
    	$notif_token = $CI->input->get('nf');
    	if (!empty($notif_token)) 
    		$CI->app_notifications->view($CI->session->userdata('id_user'), $notif_token);
    	
    }