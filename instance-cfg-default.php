<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	// Database
	define('DB_HOST', 'localhost');
	define('DB_USER', 'root');
	define('DB_PASW', '50106250');
	define('DB_NAME', 'baseapp_' . str_replace('.', '_', $_SERVER['HTTP_HOST']));

	// Language
	define('DEFAULT_LANGUAGE', 'spanish');

	// App Settings
	define('APP_TITLE', 'Contreras Administración');
	define('APP_FOOTER', '2018 &copy; '.APP_TITLE.' powered by <a target="_blank" href="http://www.inglobe.com.ar">Inglobe</a> &nbsp;|&nbsp; <a href="http://www.inglobe.com.ar" target="_blank">www.inglobe.com.ar</a>');
	define('APP_LAYOUT', 'layout3');
	define('APP_THEME', 'default');

	// Permissions
	define('DEFAULT_PERMISSION_VALUE', TRUE);
	
	// Otros
	define('CFG_LOADED', TRUE);
	define('PACHI_MASK', TRUE);
 ?>