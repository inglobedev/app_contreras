/*
	Dependencias:

	DatePicker.
	TimePicker.
	DateTime Picker.
	Numeric.
	Colors.
	Maxlength.

*/

$(document).ready(function() {    
    pachi_init();
});

function pachi_init()
{
	PachiDateTimePickers.init();
	PachiInputMask.init();
	ComponentsSearchers.init();
	ComponentsBootstrapMultiselect.init(); 
	
	$.fn.modal.Constructor.prototype.enforceFocus = function() {};
}

// Date Pickers

	var PachiDateTimePickers = function () {

	    var handleDatePickers = function () {

	        if (jQuery().datepicker) {
	            $('.pachi-date-picker').datepicker({
	                orientation: "left",
	                autoclose: true,
	                language: 'es',
	                todayHighlight: true,
	                todayBtn: true,
	                keyboardNavigation:true
	            });
	        }
	        else
	        {
	        	console.log('No se pueden inicializar datepickers.');
	        }
	    }

	    var handleTimePickers = function () {

	        if (jQuery().timepicker) {

	            $('.pachi-time-picker').timepicker({
	                autoclose: true,
	                minuteStep: 5,
	                showSeconds: false,
	                showMeridian: false
	            });

	            // handle input group button click
	            $('.pachi-time-picker').parent('.input-group').on('click', '.input-group-btn', function(e){
	                e.preventDefault();
	                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
	            });

	            // Workaround to fix timepicker position on window scroll
	            $( document ).scroll(function(){
	                $('.modal .pachi-time-picker').timepicker('place'); //#modal is the id of the modal
	            });
	        }
	        else
	        {
	        	console.log('No se pueden inicializar timepicker.');
	        }
	    }

	    var handleDatetimePicker = function () {

	        if (!jQuery().datetimepicker) {
	        	console.log('No se pueden inicializar datetimepicker.');
	            return;
	        }

	        $(".pachi-datetime-picker").datetimepicker({
	            autoclose: true,
	            format: "yyyy-mm-dd hh:ii:ss",
	            fontAwesome: true,
	            pickerPosition: "bottom-rigth"
	        });

	        $(".pachi-datetime-picker-advanced").datetimepicker({
	            format: "yyyy-mm-dd hh:ii:ss",
	            autoclose: true,
	            todayBtn: true,
	            fontAwesome: true,
	            startDate: "2013-02-14 10:00",
	            pickerPosition: "bottom-rigth",
	            minuteStep: 10
	        });

	        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal

	        // Workaround to fix datetimepicker position on window scroll
	        $( document ).scroll(function(){
	            $('#form_modal1 .form_datetime, #form_modal1 .form_advance_datetime, #form_modal1 .form_meridian_datetime').datetimepicker('place'); //#modal is the id of the modal
	        });
	    }

	    return {
	        //main function to initiate the module
	        init: function () {
	            handleDatePickers();
	            handleTimePickers();
	            handleDatetimePicker();
	        }
	    };

	}();

// MASKS

	var PachiInputMask = function () {
	    
	    var handleInputMasks = function () {
	        
	    	if (jQuery().inputmask) {

		        $(".pachi-mask-date").inputmask("d/m/y", {
		            autoUnmask: true,
		            "placeholder": "dd/mm/yyyy"
		        });  
		       
		        $(".pachi-mask-phone").inputmask("mask", {
		            "mask": "(999) 999-9999"
		        });

		 		$('.pachi-mask-number').each(function(){
		 			var min = $(this).data('min');
		 			var max = $(this).data('max');

		 			if ($(this).hasClass('decimal'))
						$(this).inputmask('decimal',{min:min, max:max, rightAlign: false});
					else
						$(this).inputmask('integer',{min:min, max:max, rightAlign: false});
		        });

				$('.pachi-mask-money').mask('#,##0.00', {reverse: true});

		 	}
		 	else
		 	{
		 		console.log('No se pueden inicializar inputmask.');
		 	}
	    }

	    var handleBootstrapMaxlength = function() {
	        $('[maxlength]').maxlength({
	            limitReachedClass: "label label-danger",
	        })
	    }

	    return {
	        //main function to initiate the module
	        init: function () {
	            handleInputMasks();
	            handleBootstrapMaxlength();
	        }
	    };

	}();

// Select 2

	var ComponentsSelect2 = function() {

	    var initSelects = function() {

	        // Set the "bootstrap" theme as the default theme for all Select2
	        // widgets.
	        //
	        // @see https://github.com/select2/select2/issues/2927
	        $.fn.select2.defaults.set("theme", "bootstrap");

	        var placeholder = "asdasdate";

	        $('.select2').not('.ajax-search.select2-hidden-accessible').each(function(){
	            var element_id = $(this).attr('id');
	            var placeholder = $(this).attr('placeholder');
	            $('#'+element_id).select2({
	                placeholder: placeholder,
	                width: null,
	                language: {
	                    noResults: function () {
	                        return 'No se encontraron resultados.';
	                    }
	                },
	            });
	        });



	        $(".select2-allow-clear:not(.select2-hidden-accessible)").select2({
	            allowClear: true,
	            placeholder: placeholder,
	            width: null
	        });

	        $("button[data-select2-open]").click(function() {
	            $("#" + $(this).data("select2-open")).select2("open");
	        });

	        // copy Bootstrap validation states to Select2 dropdown
	        //
	        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
	        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
	        // body > .select2-container) if _any_ of the opened Select2's parents
	        // has one of these forementioned classes (YUCK! ;-))
	        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
	            if ($(this).parents("[class*='has-']").length) {
	                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

	                for (var i = 0; i < classNames.length; ++i) {
	                    if (classNames[i].match("has-")) {
	                        $("body > .select2-container").addClass(classNames[i]);
	                    }
	                }
	            }
	        });

	        $(".js-btn-set-scaling-classes").on("click", function() {
	            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
	            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
	            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
	        });
	    }

	    return {
	        init: function() {
	            initSelects();
	        }
	    };

	}();

// Searchers

	var ComponentsSearchers = function() {

	    var initSearchers = function() {

	        $.fn.select2.defaults.set("theme", "bootstrap");
	        
	        $('[data-pachi-search]').not('.select2-hidden-accessible').each(function(){
	            var element_id = $(this).attr('id');
	            $('#' + element_id).select2({
	                width: "off",
	                placeholder: $('#' + element_id).attr('placeholder'),
	                minimumInputLength: 0,
	                quietMillis: 300,
	                language: {
	                    noResults: function () {
	                        return 'No se encontraron resultados.';
	                    }
	                },
	                ajax:{  
	                        url: window.base_url + 'ajax/pachi_ajax/search/',
	                        type: 'post',
	                        dataType: 'json',
	                        data: function(params) {
	                            return {
	                                term: params.term, // search term
	                                search: $('#' + element_id).data('pachi-search')
	                            };
	                        },
	                        processResults: function(data, page) {
	                            return {
	                                results: data.ext.feed
	                            };
	                        },
	                        cache: true
	                     },
	                escapeMarkup: function(markup) {
	                    return markup;
	                },
	                templateResult: function f(item) {return item.text;},
	                templateSelection:  function f(item) {return item.text;}, 
	            });
	        });

	    }
	    return {
	        init: function() {
	            initSearchers();
	        }
	    };

	}();

// Multi Selects

	var ComponentsBootstrapMultiselect = function () {

	    return {
	        //main function to initiate the module
	        init: function () {
	        	$('.mt-multiselect').each(function(){
	        		var btn_class = $(this).attr('class');
	        		var clickable_groups = ($(this).data('clickable-groups')) ? $(this).data('clickable-groups') : false ;
	        		var collapse_groups = ($(this).data('collapse-groups')) ? $(this).data('collapse-groups') : false ;
	        		var drop_right = ($(this).data('drop-right')) ? $(this).data('drop-right') : false ;
	        		var drop_up = ($(this).data('drop-up')) ? $(this).data('drop-up') : false ;
	        		var select_all = ($(this).data('select-all')) ? $(this).data('select-all') : false ;
	        		var width = ($(this).data('width')) ? $(this).data('width') : '' ;
	        		var height = ($(this).data('height')) ? $(this).data('height') : '' ;
	        		var filter = ($(this).data('filter')) ? $(this).data('filter') : false ;

	        		// advanced functions
	        		var onchange_function = function(option, checked, select) {
		                alert('Changed option ' + $(option).val() + '.');
		            }
		            var dropdownshow_function = function(event) {
		                alert('Dropdown shown.');
		            }
		            var dropdownhide_function = function(event) {
		                alert('Dropdown Hidden.');
		            }

		            // init advanced functions
		            var onchange = ($(this).data('action-onchange') == true) ? onchange_function : '';
		            var dropdownshow = ($(this).data('action-dropdownshow') == true) ? dropdownshow_function : '';
		            var dropdownhide = ($(this).data('action-dropdownhide') == true) ? dropdownhide_function : '';

		            // template functions
		            // init variables
		            var li_template;
		            if ($(this).attr('multiple')){
		            	li_template = '<li class="mt-checkbox-list"><a href="javascript:void(0);"><label class="mt-checkbox"> <span></span></label></a></li>';
	        		} else {
	        			li_template = '<li><a href="javascript:void(0);"><label></label></a></li>';
	         		}

		            // init multiselect
	        		$(this).multiselect({
	        			enableClickableOptGroups: clickable_groups,
	        			enableCollapsibleOptGroups: collapse_groups,
	        			disableIfEmpty: true,
	        			enableFiltering: filter,
	        			includeSelectAllOption: select_all,
	        			dropRight: drop_right,
	        			buttonWidth: width,
	        			maxHeight: height,
	        			onChange: onchange,
	        			onDropdownShow: dropdownshow,
	        			onDropdownHide: dropdownhide,
	        			buttonClass: btn_class,
	        			//optionClass: function(element) { return "mt-checkbox"; },
	        			//optionLabel: function(element) { console.log(element); return $(element).html() + '<span></span>'; },
	        			/*templates: {
			                li: li_template,
			            }*/
	        		});   
	        	});
	         	
	        }
	    };

	}();

// Handlers

    $(document).on('submit', '.pachi_frm', function(e) {
    	var target_url = $(this).data('target');

    	if (target_url.length == 0)
    		target_url = window.base_url + 'ajax/pachi_ajax/form/';

        send_form(target_url, this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });

    $(document).on('submit', '.pachi_table_search', function(e) {
    	var args = $(this).children('[name="args"]').val();
        send_form(window.base_url + 'ajax/pachi_ajax/search_table?' + args, this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });

    $(document).on('click', '.pachi_table_clean_search', function(e) {
    	var frm = $(this).parent('span').parent('div').parent('form');
    	$(frm).find('input[name="search"]').val('');
    	$(frm).submit();
        e.preventDefault();
    });

    $(document).on('click', '[data-pachi-modal]', function(e) {
    	var params = {};
    		params.modal = $(this).data('pachi-modal');

        send_post(window.base_url + 'ajax/pachi_ajax/modal/', params, function(data) {
            if (data.cod == 1) {
                pachi_init();
            };
        });
        e.preventDefault();
    });

    $(document).on('click', '[data-pachi-remove]', function(e) {
    	var element = $(this).data('pachi-remove');
        setTimeout(function(){ $(element).remove(); }, 500);
    });