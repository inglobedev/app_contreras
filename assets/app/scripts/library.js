$(document).ready(function() {    

});

var ComponentsLibrary = function() {

    var initLibrary = function() {
        $('input[type="file"].library_uploader:not(.initialized)').each(function( index ) {
            $(this).addClass('initialized');
            var isolated = $(this).data('isolated');
            $(this).fileuploader({
                changeInput: '<div class="fileuploader-input">' +
                                  '<div class="fileuploader-input-inner">' +
                                      '<h3 class="fileuploader-input-caption"><span>Arrastrá archivos aquí para subirlos</span></h3>' +
                                      '<p>ó</p>' +
                                      '<div class="float-right btn btn-primary"><span>Seleccionar archivos</span></div>' +
                                  '</div>' +
                              '</div>',
                theme: 'dragdrop margin-top-none',
                upload: {
                    url: window.base_url + 'ajax/ajax_biblioteca/upload_files/' + isolated,
                    data: null,
                    type: 'POST',
                    enctype: 'multipart/form-data',
                    start: true,
                    synchron: true,
                    beforeSend: null,
                    onSuccess: function(result, item) {
                        if (result.cod == 1) 
                        {
                            setTimeout(function() {
                                item.html.fadeOut(400);
                            }, 400);

                            var selection = $('#frm_library').find('input[name="selection"]').val().split(',');
                            $.each(result.ext.completed, function(i, id_file) {
                                selection.push(id_file);
                            });
                            $('#frm_library').find('input[name="selection"]').val(selection.filter(function(v){return v!==''}).join(','));
                        };
                    },
                    onError: function(item) {
                        var progressBar = item.html.find('.progress-bar2');
                        
                        if(progressBar.length > 0) {
                            progressBar.find('span').html(0 + "%");
                            progressBar.find('.fileuploader-progressbar .bar').width(0 + "%");
                            item.html.find('.progress-bar2').fadeOut(400);
                        }
                        
                        item.upload.status != 'cancelled' && item.html.find('.fileuploader-action-retry').length == 0 ? item.html.find('.column-actions').prepend(
                            '<a class="fileuploader-action fileuploader-action-retry" title="Retry"><i></i></a>'
                        ) : null;
                    },
                    onProgress: function(data, item) {
                        var progressBar = item.html.find('.progress-bar2');
                        
                        if(progressBar.length > 0) {
                            progressBar.show();
                            progressBar.find('span').html(data.percentage + "%");
                            progressBar.find('.fileuploader-progressbar .bar').width(data.percentage + "%");
                        }
                        $('.fileuploader-input').css('height', '15vh');
                        block_library();
                    },
                    onComplete: function() {
                        $('.uploader-container').collapse('hide');
                        $('.fileuploader-input').css('height', '53vh');
                        unblock_library();
                        refresh_library();
                    },
                },
                dragDrop: {
                    container: $('#frm_library'),
                    onDragEnter: function(event, listEl, parentEl, newInputEl, inputEl) {
                        $('.uploader-container').collapse('show');
                    },
                    onDragLeave: function(event, listEl, parentEl, newInputEl, inputEl) {
                        $('.uploader-container').collapse('hide');
                    },
                    onDrop: function(event, listEl, parentEl, newInputEl, inputEl) {
                    },
                },
                onRemove: function(item) {
                    $.post('./php/ajax_remove_file.php', {
                        file: item.name
                    });
                },
                captions: {
                    feedback: 'Arrastrá archivos aquí para subirlos',
                    feedback2: 'Arrastrá archivos aquí para subirlos',
                    drop: 'Arrastrá archivos aquí para subirlos',
                    errors: {
                        filesLimit: 'Solo se pueden cargar ${limit} archivos.',
                        filesType: 'Solo se pueden subir archivos con las siguientes extensiones: ${extensions}.',
                        fileSize: '${name} es muy pesado! Por favor seleccione un archivo de hasta ${fileMaxSize}MB.',
                        fileName: 'El archivo ${name} ya esta seleccionado.',
                        folderUpload: 'No se permite cargar carpetas.'
                    }
                }
            });
        });
        refresh_library();
    }

    return {
        init: function() {
            initLibrary();
        }
    };

}();

// Libreria

    $(document).on('click', '.btn_open_library', function(e) {
        var params = $(this).data();
            params.selection = $('#' + $(this).data('id_target')).val();
        
        send_post(window.base_url + 'ajax/ajax_biblioteca/get_modal_library/', params, function(data) {
            if (data.cod == 1) {
            };
        });
        e.preventDefault();
    });

    $(document).on('submit', '#frm_library', function(e) {
        var id_target = $(this).find('input[name="id_target"]').val();
        var selection = $(this).find('input[name="selection"]').val();
        var max_items = $(this).find('input[name="max_items"]').val();

        // Proceso de limpieza. Por las dudas que haya IDs muertos, quitamos los que no se encuentren en la seleccion.
            selection = selection.split(',').filter(function(v){return v!==''});
            selection = selection.filter(function(v){return $('#library_elements .tile.selectable.selected[data-id_file="'+v+'"]').length}).join(',');
            $('#frm_library').find('input[name="selection"]').val(selection);
        // Fin limpieza

        if (max_items > 0 && selection.split(',').length > max_items) 
        {
            if (max_items == 1)
                display_growl('Solo puedes seleccionar ' + max_items + ' elemento', 'danger');
            else
                display_growl('Solo puedes seleccionar ' + max_items + ' elementos', 'danger');
        }
        else
        {
            $('#' + id_target).val(selection).trigger('change');

            $('#mod_library').modal('hide');
            setTimeout(function() {
                $('#mod_library').remove();
            }, 400);
        };
        e.preventDefault();
    });

    $(document).on('click', '#frm_library .tile.selectable', function(e) {
        $(this).toggleClass('selected');
        var id_file = $(this).data('id_file');

        $(this).hasClass('selected') ? select_item(id_file) : unselect_item(id_file);

        $('#frm_library .tile.selectable.selected').length ? $('#library_selection_options').removeClass('hidden') : $('#library_selection_options').addClass('hidden');
        
        e.preventDefault();
    });

    $(document).on('click', '#library_btn_load_more', function(e) {
        var offset = $('#frm_library .tile.selectable').length;
        refresh_library(offset);
        e.preventDefault();
    });

    // Previene el submiteo de la seleccion de libreria en el caso de enter.
    $(document).on("keypress", "#frm_library", function(event) { 
        var search_term = $('#frm_library').find('input[name="search_terms"]').val();

        if (event.keyCode == 13)
            refresh_library();

        return event.keyCode != 13;
    });
// Helpers

    function refresh_library(offset = 0)
    {
        var params = {};
            params.selection = $('#frm_library').find('input[name="selection"]').val();
            params.search = $('#frm_library').find('input[name="search_terms"]').val();
            params.extension = $('#frm_library').find('input[name="extension"]').val();
            params.isolated = $('#frm_library').find('input[name="isolated"]').val();
            params.offset = offset;

        if ($('#library_elements').length) 
        {
            block_library();
            send_post(window.base_url + 'ajax/ajax_biblioteca/get_library_elements/', params, function(data) {
                if (data.cod == 1) {
                    if (data.ext.elements == 0 && params.search.length == 0) 
                    {
                        $('.uploader-container').collapse('show');
                    };
                };
                unblock_library();
            });
        };
    }

    function block_library()
    {
        App.blockUI({
            target: '#library_elements',
            animate: true,
            overlayColor: 'none'
        });
    }

    function unblock_library()
    {
        App.unblockUI('#library_elements');
    }

    function select_item(id_file)
    {
        id_file = '' + id_file;
        var selection = $('#frm_library').find('input[name="selection"]').val().split(',');
        // agregamos el item
        if (selection.indexOf(id_file) === -1)
            selection.push(id_file);

        $('#frm_library').find('input[name="selection"]').val(selection.filter(function(v){return v!==''}).join(','));
    }

    function unselect_item(id_file)
    {
        id_file = '' + id_file;
        var selection = $('#frm_library').find('input[name="selection"]').val().split(',');
        // quitamos el item
        if (selection.indexOf(id_file) !== -1)
            selection.splice(selection.indexOf(id_file), 1);

        $('#frm_library').find('input[name="selection"]').val(selection.filter(function(v){return v!==''}).join(','));
    }

    // Handler, cuando cambia la seleccion de elementos de la libreria.
    $(document).on('change', '.hnd_selected_files', function(e) {
        var params = {};
            params.id_target = $(this).attr('id');
            params.selection = $(this).val();

        send_post(window.base_url + 'ajax/ajax_biblioteca/get_library_files_preview/', params, function(data) {
            if (data.cod == 1) {
            };
        });
    });

    // Handler, cuando cambia la seleccion de elementos de la libreria.
    $(document).on('change', '.hnd_selected_images', function(e) {
        var params = {};
            params.id_target = $(this).attr('id');
            params.selection = $(this).val();

        send_post(window.base_url + 'ajax/ajax_biblioteca/get_library_images_preview/', params, function(data) {
            if (data.cod == 1) {
            };
        });
    });
