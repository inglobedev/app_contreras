$(document).on('click', '.btn-restore-maintenance', function(e) {
    var params = $(this).data();
    send_post(window.base_url + 'ajax/Ajax_maintenance/restore_maintenance/', params, function(data) {
        if (data.cod == 1) {
            
        };
    });    
    e.preventDefault();
});

$(document).on('click', '.btn-archive-maintenance', function(e) {
    var params = $(this).data();
    send_post(window.base_url + 'ajax/Ajax_maintenance/archive_maintenance/', params, function(data) {
        if (data.cod == 1) {
            
        };
    });    
    e.preventDefault();
});

$(document).on('click', '.btn-delete-maintenance', function(e) {
    var params = $(this).data();
    bootbox.confirm(params.confirmation, function(result) {
        if (result == true) {
            send_post(window.base_url + 'ajax/Ajax_maintenance/delete_maintenance/', params, function(data) {
                if (data.cod == 1) {
                    
                };
            });
        }
    });    
    e.preventDefault();
});