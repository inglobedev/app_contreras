$(document).on('click', '.btn_new_coupon', function(e) {
    var params = $(this).data();
    console.log(params);
    send_post(window.base_url + 'ajax/ajax_payments/get_coupon_generator', params, function(data) {
    	pachi_init();
    	refresh_coupon_serverside();
    });
    e.preventDefault();
});

$(document).on('click', '.btn_cashout', function(e) {
    var params = $(this).data();
    send_post(window.base_url + 'ajax/ajax_payments/get_cashout_modal', params, function(data) {
    	pachi_init();
    });
    e.preventDefault();
});

$(document).on('click', '.btn_payment', function(e) {
    var params = $(this).data();
    send_post(window.base_url + 'ajax/ajax_payments/get_payment_modal', params, function(data) {
    	pachi_init();
    });
    e.preventDefault();
});

$(document).on('submit', '.frm_cashout', function(e) {
    send_form(window.base_url + 'ajax/ajax_payments/cashout/', this, function(data) {
        if (data.cod == 1) {
        	App.blockUI({target: 'body', animate: true});
            setTimeout(function() {
                location.reload();
            }, 800);
        };
    });
    e.preventDefault();
});

$(document).on('submit', '.frm_payment', function(e) {
    send_form(window.base_url + 'ajax/ajax_payments/payment/', this, function(data) {
        if (data.cod == 1) {
        	App.blockUI({target: 'body', animate: true});
            setTimeout(function() {
                location.reload();
            }, 800);
        };
    });
    e.preventDefault();
});

$(document).on('click', '.btn_new_detail', function(e) {
    var params = $(this).data();
    console.log(params);
    send_post(window.base_url + 'ajax/ajax_payments/get_detail_generator', params, function(data) {
    	pachi_init();
    });
    e.preventDefault();
});

$(document).on('click', '.btn_delete_receipt_detail', function(e) {
    var params = $(this).data();
    bootbox.confirm(params.confirmation, function(result) {
        if (result == true) {
            send_post('/ajax/ajax_payments/delete_receipt_detail', params, function(data) {
                if (data.cod == 1) {
                    refresh_coupon_serverside();
                };
            });
        }
    });    
    e.preventDefault();
});

$(document).on('change', '.detail-check', function(e) {
	refresh_coupon_frontside();

    var params = $(this).data();
    	params.checked = $(this).is(':checked');

    send_post('/ajax/ajax_payments/update_receipt_detail_check', params, function(data) {
        if (data.cod == 1) {
            
        };
    });
    e.preventDefault();
});

$(document).on('select2:select', '#id_user_receiver', function(e) {
    console.log('change');
    var params = $(this).data();
    console.log(params.type);
 	refresh_coupon_serverside(params.type);
});

function refresh_coupon_frontside()
{
	var coupon_total = 0;

    $('.tr-detail').each(function( index ) {

    	if ($(this).find('.detail-check').is(':checked'))
    	{
    		coupon_total = coupon_total + parseFloat($(this).find('.detail-ammount-full').text().replace(',', '').replace(' ', '').replace(',', '').replace(' ', '').replace(',', '').replace(' ', ''));
    	}
        
        init_payment_editables();
    });

    $('.coupon-total').html(currencyFormat(coupon_total));
}

function refresh_receipt_frontside()
{
    var coupon_total = 0;

    $('.tr-detail').each(function( index ) {
        coupon_total = coupon_total + parseFloat($(this).find('.detail-dummy-money').text().replace(',', '').replace(' ', '').replace(',', '').replace(' ', '').replace(',', '').replace(' ', ''));
    });

    $('.recepit-total').each(function( index ) {
        $(this).html(currencyFormat(coupon_total));
    });

    var params = {};
        params.number = parseFloat(coupon_total).toFixed(2);

    send_post('/ajax/ajax_payments/convert_to_letters', params, function(data) {
        if (data.cod == 1) {
            $('.recepit-total-letters').html(data.ext.string);
        };
    });
}

function refresh_coupon_serverside(calltype)
{

    var params = {};
    	params.id_user = $('#id_user_receiver').val();
    	params.filter_details = $('#filter_details').val();
        params.calltype = calltype;
        console.log('---o' + params.calltype);
    	
    if ($('#id_user_receiver').val())
    {
	    send_post(window.base_url + 'ajax/ajax_payments/load_details', params, function(data) {
	    	if (typeof data.ext.allow_submit != 'undefined') {
		    	if (data.ext.allow_submit) 
		    		$('#btn_submit_coupon').removeClass('hidden');
		    	else
		    		$('#btn_submit_coupon').addClass('hidden');
	    	}      
	        init_payment_editables();
	    });
    }
}

function currencyFormat (num) {
    return num.toFixed(2).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}

function init_payment_editables()
{
    console.log('init_p');
    $('.detail-ammount').each(function( index ) {
        var id_receipt_detail = $(this).data('id_receipt_detail');
        $(this).editable({
            url: window.base_url + 'ajax/ajax_payments/update_receipt_detail_ammount',
            type: 'text',
            pk: id_receipt_detail,
            name: 'detail_ammount',
            title: 'Importe',
            inputclass: 'form-control pachi-mask-money',
            validate: function(value) {
                if ($.trim(value) == '') return 'Este campo es requerido';
            }
        });
        $(this).on('shown', function(e, editable) {
            pachi_init();
        });
        $(this).on('save', function(e, params) {
            setTimeout(function(){ refresh_coupon_frontside(); }, 500);
        });
    });

    $('.detail-dummy-text').each(function( index ) {
        var id_receipt_detail = $(this).data('id_receipt_detail');
        $(this).editable({
            url: window.base_url + 'ajax/ajax_payments/dummy',
            type: 'text',
            pk: 0,
            mode: 'inline',
            name: 'detail_ammount',
            title: 'Importe',
            inputclass: 'form-control',
            emptytext: '<span class="hidden-print"><i class="fa fa-pencil"></i></span> Sin comentarios',
            validate: function(value) {
                if ($.trim(value) == '') return 'Este campo es requerido';
            }
        });
        $(this).on('shown', function(e, editable) {
            pachi_init();
        });
        $(this).on('save', function(e, params) {
            setTimeout(function(){ refresh_receipt_frontside(); }, 500);
        });
    });

    $('.detail-dummy-money').each(function( index ) {
        var id_receipt_detail = $(this).data('id_receipt_detail');
        $(this).editable({
            url: window.base_url + 'ajax/ajax_payments/dummy',
            type: 'text',
            pk: 0,
            mode: 'inline',
            name: 'detail_ammount',
            title: 'Importe',
            inputclass: 'form-control pachi-mask-money',
            validate: function(value) {
                if ($.trim(value) == '') return 'Este campo es requerido';
            }
        });
        $(this).on('shown', function(e, editable) {
            pachi_init();
        });
        $(this).on('save', function(e, params) {
            setTimeout(function(){ refresh_receipt_frontside(); }, 500);
        });
    });
}

$(document).on('submit', '.frm_new_detail', function(e) {
    send_form(window.base_url + 'ajax/ajax_payments/create_receipt_detail/', this, function(data) {
        if (data.cod == 1) {
            refresh_coupon_serverside();
        };
    });
    e.preventDefault();
});

$(document).on('submit', '.frm_coupon', function(e) {
    send_form(window.base_url + 'ajax/ajax_payments/confirm_coupon/', this, function(data) {
        if (data.cod == 1) 
        {
        	setTimeout(
				swal({
				  title: 'Operación confirmada',
				  text: 'La operación fue registrada exitosamente',
				  type: 'success',
				  allowOutsideClick: false,
				  showCancelButton: true,
				  confirmButtonText: 'Imprimir Cupón',
				  cancelButtonClass: 'btn-default',
				  cancelButtonText: 'Cerrar',
				  // showConfirmButton: sa_showConfirmButton,
				  // confirmButtonClass: sa_confirmButtonClass,
				  // closeOnConfirm: sa_closeOnConfirm,
				  // closeOnCancel: sa_closeOnCancel,
				},
				function(isConfirm)
				{
					// La confirmación dispararia el modal de impresion de cupon.
			        if (isConfirm) {
			        	print_coupon(data.ext.id_receipt);
			        }
			        else {
			        	location.reload();
			        }
				})
        	,600)
        };
    });
    e.preventDefault();
});

function print_coupon(id_receipt)
{
	window.location.href = window.base_url + 'payments/receipt/' + id_receipt
}

$(document).ready(function() {    
	init_payment_editables();
});

// En la generacion de detalle.
    
    $(document).on('select2:select', '#id_issuing_account', function(e) {
        refresh_sibling_suggestion();
    });

    $(document).on('select2:select', '#id_receiving_account', function(e) {
        refresh_sibling_suggestion();
    });

    function refresh_sibling_suggestion()
    {
        var params = {}
        params.id_issuing_account = $('#id_issuing_account').val()
        params.id_receiving_account = $('#id_receiving_account').val()

        send_post(window.base_url + 'ajax/ajax_payments/load_detail_sibling_suggestion', params, function(data) {
            pachi_init();
            if (typeof data.ext.allow_submit != 'undefined') {
                if (data.ext.allow_submit) 
                    $('#btn_submit_detail').removeClass('hidden');
                else
                    $('#btn_submit_detail').addClass('hidden');
            }  
        });
    }

// Para los adjuntos de detalles
// $(document).on('click', '.btn_detail_attachments', function(e) {
//     var params = $(this).data();
//     send_post(window.base_url + 'ajax/ajax_payments/get_detail_attachments', params, function(data) {
//     	pachi_init();
//     });
//     e.preventDefault();
// });

$(document).on('click', '.btn_remove_row', function(e) {
    $(this).parent('td').parent('.tr-detail').remove();
    refresh_receipt_frontside();
    e.preventDefault();
});