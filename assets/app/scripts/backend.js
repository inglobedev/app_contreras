// Usuarios 
    // forms
    $(document).on('submit', '.frm-update-password', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/update_password/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });

    $(document).on('submit', '.frm-update-account', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/update_account/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });

    $(document).on('submit', '.frm-update-avatar', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/update_avatar/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });

    $(document).on('submit', '.frm-create-account', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/create_account/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });

    $(document).on('submit', '.frm-update-notifications-settings', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/update_notification_settings/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });
    

    // buttons
    $(document).on('click', '.btn-disable-user', function(e) {
        var params = $(this).data();
        bootbox.confirm(params.confirmation, function(result) {
            if (result == true) {
                send_post(window.base_url + 'ajax/ajax_app/disable_user/', params, function(data) {
                    if (data.cod == 1) {
                        
                    };
                });
            }
        });    
        e.preventDefault();
    });
    $(document).on('click', '.btn-enable-user', function(e) {
        var params = $(this).data();
        bootbox.confirm(params.confirmation, function(result) {
            if (result == true) {
                send_post(window.base_url + 'ajax/ajax_app/enable_user/', params, function(data) {
                    if (data.cod == 1) {
                        
                    };
                });
            }
        });    
        e.preventDefault();
    });


    $(document).on('click', '.modal_rent', function(e) {
        var params = $(this).data();
        send_post(window.base_url + 'ajax/ajax_payments/collect_rent', params, function(data) {
            if (data.cod == 1) {
            };
        });
        e.preventDefault();
    });


    // ui
    $(document).on('click', '#chk_notificar', function(e) {
        var notificar = $(this).is(":checked");
        if (!notificar)
            $('.inputs-clave').removeClass('hidden');
        else
            $('.inputs-clave').addClass('hidden');
    });

// Roles
    // forms
    $(document).on('submit', '.frm-create-role', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/create_role/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });
    $(document).on('submit', '.frm-update-role', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/update_role/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });
    $(document).on('submit', '.frm-update-role-permissions', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/update_role_permissions/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });
    
// Configuraciones

    $(document).on('submit', '.frm-save-configurations', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/update_configurations/', this, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
    });
    
    $(document).on('click', '.btn-test-email', function(e) {
        $('#mailing-results').html('<i class="fa fa-spin fa-spinner"></i>')
        send_post(window.base_url + 'ajax/ajax_app/test_email/', null, function(data) {
            if (data.cod == 1) {
            };
        });
        e.preventDefault();
    });

    $(document).on('submit', '.frm_tool_test_email_design', function(e) {
        send_form(window.base_url + 'ajax/ajax_app/tool_test_email_design/', this, function(data) {
            if (data.cod == 1) {
            };
        });
        e.preventDefault();
    });
    