$('#forget-password').click(function() {
    $('.login-form').hide();
    $('.forget-form').show();
});

$('#back-btn').click(function() {
    $('.login-form').show();
    $('.forget-form').hide();
});

$(document).on('submit', '.login-form', function(e) {
    send_form(window.base_url + 'ajax/ajax_public/login/', this, function(data) {
        if (data.cod == 1) {
            
        };
    });
    e.preventDefault();
});

$(document).on('submit', '.forget-form', function(e) {
    send_form(window.base_url + 'ajax/ajax_public/forget/', this, function(data) {
        if (data.cod == 1) {
            
        };
    });
    e.preventDefault();
});