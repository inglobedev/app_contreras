$(document).on('click', '.btn_delete_expense', function(e) {
        var params = $(this).data();
        bootbox.confirm(params.confirmation, function(result) {
            if (result == true) {
                send_post('/ajax/ajax_properties/delete_property_expense', params, function(data) {
                    if (data.cod == 1) {
                        
                    };
                });
            }
        });    
        e.preventDefault();
});

$(document).on('change', '.chk_expense_update', function(e) {
        var params = $(this).data();
        	params.checked = $(this).is(':checked');
        send_post('/ajax/ajax_properties/update_property_expense', params, function(data) {
            if (data.cod == 1) {
                
            };
        });
        e.preventDefault();
});


$(document).on('click', '.btn_archive_property', function(e) {
    var params = $(this).data();
    bootbox.confirm('¿Estas seguro?', function(result) {
        if (result == true) {
            send_post('/ajax/ajax_properties/archive_property', params, function(data) {
                if (data.cod == 1) {
                    
                };
            });
        }
    });    
    e.preventDefault();
});
$(document).on('click', '.btn_restore_property', function(e) {
    var params = $(this).data();
    bootbox.confirm('¿Estas seguro?', function(result) {
        if (result == true) {
            send_post('/ajax/ajax_properties/restore_property', params, function(data) {
                if (data.cod == 1) {
                    
                };
            });
        }
    });    
    e.preventDefault();
});