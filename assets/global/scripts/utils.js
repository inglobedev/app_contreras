window.open_requests = [];

function send_form(url, form, callback) {
    var formData = new FormData(form);
    var fomulario = $(form);
    App.blockUI({target: fomulario, animate: true});
    $.ajax({
        type: "POST",
        url: url,
        data: formData,
        mimeType:"multipart/form-data",
        contentType: false,
        cache: false,
        processData:false,
        success: function(data, textStatus, jqXHR) {
            App.unblockUI(fomulario);
            procesar_respuesta(data);

            if (typeof data !== "undefined") {
                if (typeof callback !== "undefined") {
                    callback(data);
                };
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            App.unblockUI(fomulario);
            procesar_errores_ajax(jqXHR);
        },
        dataType: "json"
    });
}

function send_post(url, params, callback) {
    var open = window.open_requests.indexOf(url);
    if (open != -1) {
        return false;
    }
    else {
        window.open_requests.push(url);
        $.ajax({
            type: "POST",
            url: url,
            data: params,
            success: function(data, textStatus, jqXHR) {
                window.open_requests.splice(window.open_requests.indexOf(url), 1);
                procesar_respuesta(data);

                if (typeof data !== "undefined") {
                    if (typeof callback == "function") {
                        callback(data);
                    };
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                window.open_requests.splice(window.open_requests.indexOf(url), 1);
                procesar_errores_ajax(jqXHR);
            },
            dataType: "json"
        });
    }
}

function procesar_errores_ajax(jqXHR) {
    switch (jqXHR.status) {
        case 500:
            alert('Ocurrio un error con la ultima accion. Intente nuevamente.');
            break;
        case 410:
            window.location.replace("/");
            break;
    }
}

function procesar_respuesta(data) {
    if (data !== null) {

        // ¿Hay mensajes que mostrar?
        if (data.display == 1) {
            
            if (data.tipo == 'success') {
                var duracion = 1000;
            } else {
                var duracion = 4000;
            }
            
            $.bootstrapGrowl(data.mensaje, {
                ele: 'body',        // which element to append to
                type: data.tipo,    // (null, 'info', 'danger', 'success', 'warning')
                offset: {
                    from: 'top',
                    amount: parseInt(100)
                },
                align: 'center',    // ('left', 'right', or 'center')
                width: 'auto',      // (integer, or 'auto')
                delay: duracion,    // Time while the message will be displayed. It's not equivalent to the *demo* timeOut!
                allow_dismiss: true,// If true then will display a cross to close the popup.
                stackup_spacing: 10 // spacing between consecutively stacked growls.
            });
        }

        // ¿Algo que refrescar?
        if ( data.update ) {
            $.each( data.update, function( elem, html ) {
                if (typeof html == 'string') 
                    $(elem).html(html);

                if (typeof html == 'object') 
                {
                    if (typeof html.remove == 'boolean')
                        $(elem).remove();

                    if (typeof html.append == 'string')
                        $(elem).append(html.append);
                };
            });
        };

        // ¿Acciones?
        if (data.ext) {
            if (data.ext.action == 'redirect') {
                setTimeout(function() {
                    window.location = data.ext.target;
                }, data.ext.delay);
            };
            if (data.ext.action == 'reload') {
                setTimeout(function() {
                    location.reload();
                }, data.ext.delay);
            };
            if (data.ext.trigger_modal) {
                $(data.ext.trigger_modal).remove();
                $('body').append(data.ext.modal);
                $(data.ext.trigger_modal).modal('show');
                init_all();
            };
            if (data.ext.action = 'dismiss_modal') {
                $(data.ext.target).modal('hide');
                setTimeout(function() {
                	$(data.ext.target).remove();
                }, 500);
            };
        }
    };
}

function app_logout() {
    send_post(window.base_url + '/ajax/ajax_public/logout', false, false);
}
function app_notifications_read_all() {
    send_post(window.base_url + '/ajax/ajax_app/notifications_read_all', false, false);
}
function app_notifications_clean() {
    send_post(window.base_url + '/ajax/ajax_app/notifications_clean', false, false);
}

$(document).on('change', '.redirect-to-option-value', function(e)
{ 
    var goto_url = $(this).find("option:selected").val();
    window.location = goto_url;
    e.preventDefault();
});

function display_growl(mensaje, tipo) {
    $.bootstrapGrowl(mensaje, {
        ele: 'body',
        type: tipo,
        offset: {
            from: 'top',
            amount: parseInt(100)
        },
        align: 'center',
        width: 'auto',
        delay: 1500,
        allow_dismiss: true,
        stackup_spacing: 10
    });
}

$(document).ready(function() {    
    init_all();
});
$( window ).resize(function() {
    
});

function init_all()
{
   	ComponentsDateTimePickers.init(); 
   	ComponentsSelect2.init(); 
   	init_ajax_searchers();
   	init_numeric();
   	App.initAjax();

   	if ($('input[type="file"].library_uploader:not(.initialized)').length)
   		ComponentsLibrary.init(); 
}

// Date Pickers

	var ComponentsDateTimePickers = function () {

	    var handleDatePickers = function () {

	        if (jQuery().datepicker) {
	            $('.date-picker').datepicker({
	                rtl: App.isRTL(),
	                orientation: "left",
	                autoclose: true,
	                language: 'es',
	                todayHighlight: true,
	                todayBtn: true,
	                keyboardNavigation:true
	            });
	        }

	    }

	    var handleTimePickers = function () {

	        if (jQuery().timepicker) {

	            $('.time-picker').timepicker({
	                autoclose: true,
	                minuteStep: 5,
	                showSeconds: false,
	                showMeridian: false
	            });

	            // handle input group button click
	            $('.time-picker').parent('.input-group').on('click', '.input-group-btn', function(e){
	                e.preventDefault();
	                $(this).parent('.input-group').find('.timepicker').timepicker('showWidget');
	            });

	            // Workaround to fix timepicker position on window scroll
	            $( document ).scroll(function(){
	                $('.modal .time-picker').timepicker('place'); //#modal is the id of the modal
	            });
	        }
	    }

	    var handleDateRangePickers = function () {
	        if (!jQuery().daterangepicker) {
	            return;
	        }

	        $( ".date-range-picker" ).each(function( index ) {
	            // Formato de fechas definido por la app.
	            var drp     = $(this);
	            var format  = $(this).data('format').toUpperCase().replace("YYYY", "Y");
	            // Inputs donde se guardan las fechas a submittear
	            var start_input = $(this).data('input-start');
	            var end_input   = $(this).data('input-end');
	            var suggest     = $(this).data('suggest');

	            // Fecha de inicio seteada incialmente
	            var startDate   = $(start_input).val();
	            if(startDate.length > 0)
	                startDate = moment(startDate, format);
	            else
	                startDate = moment().startOf('month');

	            // Fecha de fin seteada incialmente
	            var endDate     = $(end_input).val();
	            if(endDate.length > 0)
	                endDate = moment(endDate, format);
	            else
	                endDate = moment().endOf('month');

	            if (suggest == 'future')
	                suggest =   { 
	                                'Este mes':                 [moment().startOf('month'), moment().endOf('month')],
	                                'El mes que viene':         [moment().add(1, 'month').startOf('month'), moment().add(1, 'month').endOf('month')],
	                                'Primer Semestre':          [moment().startOf('year'), moment().startOf('year').add(6, 'month').subtract(1, 'days')],
	                                'Segundo Semestre':         [moment().startOf('year').add(6, 'month'), moment().endOf('year')],
	                                'Primer Cuatrimestre':      [moment().startOf('year'), moment().startOf('year').add(4, 'month').subtract(1, 'days')],
	                                'Segundo Cuatrimestre':     [moment().startOf('year').add(4, 'month'),moment().startOf('year').add(8, 'month').subtract(1, 'days')],
	                                'Tercer Cuatrimestre':      [moment().startOf('year').add(8, 'month'), moment().endOf('year')] 
	                            }
	            if (suggest == 'past')
	                suggest =   { 
	                                'Hoy':              [moment(), moment()],
	                                'Ayer':             [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	                                'Últimos 7 dias':   [moment().subtract(6, 'days'), moment()],
	                                'Últimos 30 dias':  [moment().subtract(29, 'days'), moment()],
	                                'El mes pasado':    [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')] 
	                            }
	            // Inicializacion.
	            $(this).daterangepicker({
	                    opens: (App.isRTL() ? 'left' : 'right'),
	                    format: format,
	                    separator: ' a ',
	                    startDate: startDate,
	                    endDate: endDate,
	                    ranges: suggest,
	                    locale: {
	                        applyLabel: 'Aplicar',
	                        cancelLabel: 'Cancelar',
	                        fromLabel: 'Desde',
	                        toLabel: 'Hasta',
	                        customRangeLabel: 'Rango Personalizado',
	                        firstDay: 1,
	                        format: format
	                    }
	                },
	                function (start, end) {
	                    $(drp).find('input.preview').val(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
	                    $(start_input).val(start.format(format));
	                    $(end_input).val(end.format(format));
	                }
	            );
	            // Valor inicial.
	            $(drp).find('input').val(startDate.format('MMMM D, YYYY') + ' - ' + endDate.format('MMMM D, YYYY'));  
	            $(start_input).val(startDate.format(format));
	            $(end_input).val(endDate.format(format));
	        });
	    }

	    var handleDatetimePicker = function () {

	        if (!jQuery().datetimepicker) {
	            return;
	        }

	        $(".form_datetime").datetimepicker({
	            autoclose: true,
	            isRTL: App.isRTL(),
	            format: "dd MM yyyy - hh:ii",
	            fontAwesome: true,
	            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left")
	        });

	        $(".form_advance_datetime").datetimepicker({
	            isRTL: App.isRTL(),
	            format: "dd MM yyyy - hh:ii",
	            autoclose: true,
	            todayBtn: true,
	            fontAwesome: true,
	            startDate: "2013-02-14 10:00",
	            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
	            minuteStep: 10
	        });

	        $(".form_meridian_datetime").datetimepicker({
	            isRTL: App.isRTL(),
	            format: "dd MM yyyy - HH:ii P",
	            showMeridian: true,
	            autoclose: true,
	            fontAwesome: true,
	            pickerPosition: (App.isRTL() ? "bottom-right" : "bottom-left"),
	            todayBtn: true
	        });

	        $('body').removeClass("modal-open"); // fix bug when inline picker is used in modal

	        // Workaround to fix datetimepicker position on window scroll
	        $( document ).scroll(function(){
	            $('#form_modal1 .form_datetime, #form_modal1 .form_advance_datetime, #form_modal1 .form_meridian_datetime').datetimepicker('place'); //#modal is the id of the modal
	        });
	    }

	    return {
	        //main function to initiate the module
	        init: function () {
	            handleDatePickers();
	            handleDateRangePickers();
	            handleTimePickers();
	        }
	    };

	}();

// Select 2

	var ComponentsSelect2 = function() {

	    var initSelects = function() {

	        // Set the "bootstrap" theme as the default theme for all Select2
	        // widgets.
	        //
	        // @see https://github.com/select2/select2/issues/2927
	        $.fn.select2.defaults.set("theme", "bootstrap");

	        var placeholder = "asdasdate";

	        $('.select2').not('.ajax-search.select2-hidden-accessible').each(function(){
	            var element_id = $(this).attr('id');
	            var placeholder = $(this).attr('placeholder');
	            $('#'+element_id).select2({
	                placeholder: placeholder,
	                width: null,
	                language: {
	                    noResults: function () {
	                        return 'No se encontraron resultados.';
	                    }
	                },
	            });
	        });



	        $(".select2-allow-clear:not(.select2-hidden-accessible)").select2({
	            allowClear: true,
	            placeholder: placeholder,
	            width: null
	        });

	        $("button[data-select2-open]").click(function() {
	            $("#" + $(this).data("select2-open")).select2("open");
	        });

	        // copy Bootstrap validation states to Select2 dropdown
	        //
	        // add .has-waring, .has-error, .has-succes to the Select2 dropdown
	        // (was #select2-drop in Select2 v3.x, in Select2 v4 can be selected via
	        // body > .select2-container) if _any_ of the opened Select2's parents
	        // has one of these forementioned classes (YUCK! ;-))
	        $(".select2, .select2-multiple, .select2-allow-clear, .js-data-example-ajax").on("select2:open", function() {
	            if ($(this).parents("[class*='has-']").length) {
	                var classNames = $(this).parents("[class*='has-']")[0].className.split(/\s+/);

	                for (var i = 0; i < classNames.length; ++i) {
	                    if (classNames[i].match("has-")) {
	                        $("body > .select2-container").addClass(classNames[i]);
	                    }
	                }
	            }
	        });

	        $(".js-btn-set-scaling-classes").on("click", function() {
	            $("#select2-multiple-input-sm, #select2-single-input-sm").next(".select2-container--bootstrap").addClass("input-sm");
	            $("#select2-multiple-input-lg, #select2-single-input-lg").next(".select2-container--bootstrap").addClass("input-lg");
	            $(this).removeClass("btn-primary btn-outline").prop("disabled", true);
	        });
	    }

	    return {
	        init: function() {
	            initSelects();
	        }
	    };

	}();

// Numeric fields

	function init_numeric() 
	{
	    $('input.numeric').numeric();
	}

// Busquedas Ajax

    function init_ajax_searchers()
    {
        $.fn.select2.defaults.set("theme", "bootstrap");
        
        $('.ajax-search').not('.ajax-search.select2-hidden-accessible').each(function(){
            var element_id = $(this).attr('id');
            $('#'+element_id).select2({
                width: "off",
                placeholder: $('#' + element_id).data('placeholder'),
                minimumInputLength: 0,
                quietMillis: 300,
                language: {
                    noResults: function () {
                        return 'No se encontraron resultados.';
                    }
                },
                ajax:{  
                        url: $('#' + element_id).data('url'),
                        type: 'post',
                        dataType: 'json',
                        data: function(params) {
                            return {
                                q: params.term, // search term
                                filter: $('#' + element_id).data('filter')
                            };
                        },
                        processResults: function(data, page) {
                            return {
                                results: data.ext.feed
                            };
                        },
                        cache: true
                     },
                escapeMarkup: function(markup) {
                    return markup;
                },
                templateResult: function f(item) {return item.text;},
                templateSelection:  function f(item) {return item.text;}, 
            });
        });
    }

// Revelar / Ocultar cosas basado en booleano de un chk
	$(document).on('change', 'input[data-reveal][type="checkbox"],input[data-hide][type="checkbox"]', function(e) {
	    handle_checkbox_rules();
	});
	function handle_checkbox_rules()
	{
	    $( 'input[data-reveal][type="checkbox"],input[data-hide][type="checkbox"]' ).each(function( index ) {
	        var reveal = $(this).data('reveal');
	        if (typeof reveal != 'undefined') 
	        {
	            if ($(this).is(':checked'))
	                $(reveal).removeClass('hidden');
	            else
	                $(reveal).addClass('hidden');
	        }

	        var hide = $(this).data('hide');
	        if (typeof hide != 'undefined') 
	        {
	            if ($(this).is(':checked'))
	                $(hide).addClass('hidden');
	            else
	                $(hide).removeClass('hidden');
	        };
	    });
	}

$(function(){
  var hash = window.location.hash;
  hash && $('ul.nav a[href="' + hash + '"]').tab('show');

  $('.nav li a').click(function (e) {
    $(this).tab('show');
    var scrollmem = $('body').scrollTop() || $('html').scrollTop();
    window.location.hash = this.hash;
    $('html,body').scrollTop(scrollmem);
  });
});