DELIMITER $$
CREATE TRIGGER `account_transactions_AFTER_INSERT` 
AFTER INSERT ON `account_transactions` FOR EACH ROW
BEGIN
	UPDATE accounts SET account_balance = (SELECT SUM(transaction_ammount) account_balance FROM account_transactions WHERE id_account = NEW.id_account) WHERE id_account = NEW.id_account;
END$$
DELIMITER ;